.class abstract Lcom/nimbusds/jose/proc/AbstractJWKSelectorWithSource;
.super Ljava/lang/Object;
.source "AbstractJWKSelectorWithSource.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C::",
        "Lcom/nimbusds/jose/proc/SecurityContext;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lnet/jcip/annotations/ThreadSafe;
.end annotation


# instance fields
.field private final jwkSource:Lcom/nimbusds/jose/jwk/source/JWKSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/nimbusds/jose/jwk/source/JWKSource",
            "<TC;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/nimbusds/jose/jwk/source/JWKSource;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jose/jwk/source/JWKSource",
            "<TC;>;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p0, "this":Lcom/nimbusds/jose/proc/AbstractJWKSelectorWithSource;, "Lcom/nimbusds/jose/proc/AbstractJWKSelectorWithSource<TC;>;"
    .local p1, "jwkSource":Lcom/nimbusds/jose/jwk/source/JWKSource;, "Lcom/nimbusds/jose/jwk/source/JWKSource<TC;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    if-nez p1, :cond_0

    .line 31
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The JWK source must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 33
    :cond_0
    iput-object p1, p0, Lcom/nimbusds/jose/proc/AbstractJWKSelectorWithSource;->jwkSource:Lcom/nimbusds/jose/jwk/source/JWKSource;

    .line 34
    return-void
.end method


# virtual methods
.method public getJWKSource()Lcom/nimbusds/jose/jwk/source/JWKSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/nimbusds/jose/jwk/source/JWKSource",
            "<TC;>;"
        }
    .end annotation

    .prologue
    .line 43
    .local p0, "this":Lcom/nimbusds/jose/proc/AbstractJWKSelectorWithSource;, "Lcom/nimbusds/jose/proc/AbstractJWKSelectorWithSource<TC;>;"
    iget-object v0, p0, Lcom/nimbusds/jose/proc/AbstractJWKSelectorWithSource;->jwkSource:Lcom/nimbusds/jose/jwk/source/JWKSource;

    return-object v0
.end method
