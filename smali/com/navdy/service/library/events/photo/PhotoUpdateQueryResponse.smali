.class public final Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;
.super Lcom/squareup/wire/Message;
.source "PhotoUpdateQueryResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_IDENTIFIER:Ljava/lang/String; = ""

.field public static final DEFAULT_UPDATEREQUIRED:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final identifier:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final updateRequired:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;->DEFAULT_UPDATEREQUIRED:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$Builder;

    .prologue
    .line 30
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$Builder;->identifier:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$Builder;->updateRequired:Ljava/lang/Boolean;

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;-><init>(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 31
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 32
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$Builder;Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;-><init>(Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "updateRequired"    # Ljava/lang/Boolean;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;->identifier:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;->updateRequired:Ljava/lang/Boolean;

    .line 27
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 36
    if-ne p1, p0, :cond_1

    .line 40
    :cond_0
    :goto_0
    return v1

    .line 37
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 38
    check-cast v0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;

    .line 39
    .local v0, "o":Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;
    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;->identifier:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;->identifier:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;->updateRequired:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;->updateRequired:Ljava/lang/Boolean;

    .line 40
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 45
    iget v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;->hashCode:I

    .line 46
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 47
    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;->identifier:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;->identifier:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 48
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;->updateRequired:Ljava/lang/Boolean;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;->updateRequired:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 49
    iput v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;->hashCode:I

    .line 51
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 47
    goto :goto_0
.end method
