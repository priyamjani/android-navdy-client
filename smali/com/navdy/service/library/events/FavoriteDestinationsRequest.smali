.class public final Lcom/navdy/service/library/events/FavoriteDestinationsRequest;
.super Lcom/squareup/wire/Message;
.source "FavoriteDestinationsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/FavoriteDestinationsRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final serial_number:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/FavoriteDestinationsRequest;->DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/FavoriteDestinationsRequest$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/navdy/service/library/events/FavoriteDestinationsRequest$Builder;

    .prologue
    .line 32
    iget-object v0, p1, Lcom/navdy/service/library/events/FavoriteDestinationsRequest$Builder;->serial_number:Ljava/lang/Long;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/events/FavoriteDestinationsRequest;-><init>(Ljava/lang/Long;)V

    .line 33
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/FavoriteDestinationsRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/FavoriteDestinationsRequest$Builder;Lcom/navdy/service/library/events/FavoriteDestinationsRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/FavoriteDestinationsRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/FavoriteDestinationsRequest$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/FavoriteDestinationsRequest;-><init>(Lcom/navdy/service/library/events/FavoriteDestinationsRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;)V
    .locals 0
    .param p1, "serial_number"    # Ljava/lang/Long;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/navdy/service/library/events/FavoriteDestinationsRequest;->serial_number:Ljava/lang/Long;

    .line 29
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 38
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    .line 40
    .end local p1    # "other":Ljava/lang/Object;
    :goto_0
    return v0

    .line 39
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lcom/navdy/service/library/events/FavoriteDestinationsRequest;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 40
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/events/FavoriteDestinationsRequest;->serial_number:Ljava/lang/Long;

    check-cast p1, Lcom/navdy/service/library/events/FavoriteDestinationsRequest;

    .end local p1    # "other":Ljava/lang/Object;
    iget-object v1, p1, Lcom/navdy/service/library/events/FavoriteDestinationsRequest;->serial_number:Ljava/lang/Long;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/service/library/events/FavoriteDestinationsRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 45
    iget v0, p0, Lcom/navdy/service/library/events/FavoriteDestinationsRequest;->hashCode:I

    .line 46
    .local v0, "result":I
    if-eqz v0, :cond_0

    .end local v0    # "result":I
    :goto_0
    return v0

    .restart local v0    # "result":I
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/events/FavoriteDestinationsRequest;->serial_number:Ljava/lang/Long;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/FavoriteDestinationsRequest;->serial_number:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    :goto_1
    iput v1, p0, Lcom/navdy/service/library/events/FavoriteDestinationsRequest;->hashCode:I

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
