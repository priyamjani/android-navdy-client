.class public final Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;
.super Lcom/squareup/wire/Message;
.source "NotificationsStatusUpdate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ERRORDETAILS:Lcom/navdy/service/library/events/notification/NotificationsError;

.field public static final DEFAULT_SERVICE:Lcom/navdy/service/library/events/notification/ServiceType;

.field public static final DEFAULT_STATE:Lcom/navdy/service/library/events/notification/NotificationsState;

.field private static final serialVersionUID:J


# instance fields
.field public final errorDetails:Lcom/navdy/service/library/events/notification/NotificationsError;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final service:Lcom/navdy/service/library/events/notification/ServiceType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final state:Lcom/navdy/service/library/events/notification/NotificationsState;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/navdy/service/library/events/notification/NotificationsState;->NOTIFICATIONS_ENABLED:Lcom/navdy/service/library/events/notification/NotificationsState;

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->DEFAULT_STATE:Lcom/navdy/service/library/events/notification/NotificationsState;

    .line 19
    sget-object v0, Lcom/navdy/service/library/events/notification/ServiceType;->SERVICE_ANCS:Lcom/navdy/service/library/events/notification/ServiceType;

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->DEFAULT_SERVICE:Lcom/navdy/service/library/events/notification/ServiceType;

    .line 20
    sget-object v0, Lcom/navdy/service/library/events/notification/NotificationsError;->NOTIFICATIONS_ERROR_UNKNOWN:Lcom/navdy/service/library/events/notification/NotificationsError;

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->DEFAULT_ERRORDETAILS:Lcom/navdy/service/library/events/notification/NotificationsError;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/notification/NotificationsState;Lcom/navdy/service/library/events/notification/ServiceType;Lcom/navdy/service/library/events/notification/NotificationsError;)V
    .locals 0
    .param p1, "state"    # Lcom/navdy/service/library/events/notification/NotificationsState;
    .param p2, "service"    # Lcom/navdy/service/library/events/notification/ServiceType;
    .param p3, "errorDetails"    # Lcom/navdy/service/library/events/notification/NotificationsError;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->state:Lcom/navdy/service/library/events/notification/NotificationsState;

    .line 43
    iput-object p2, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    .line 44
    iput-object p3, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->errorDetails:Lcom/navdy/service/library/events/notification/NotificationsError;

    .line 45
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;)V
    .locals 3
    .param p1, "builder"    # Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;

    .prologue
    .line 48
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;->state:Lcom/navdy/service/library/events/notification/NotificationsState;

    iget-object v1, p1, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    iget-object v2, p1, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;->errorDetails:Lcom/navdy/service/library/events/notification/NotificationsError;

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;-><init>(Lcom/navdy/service/library/events/notification/NotificationsState;Lcom/navdy/service/library/events/notification/ServiceType;Lcom/navdy/service/library/events/notification/NotificationsError;)V

    .line 49
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 50
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;-><init>(Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 54
    if-ne p1, p0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v1

    .line 55
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 56
    check-cast v0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;

    .line 57
    .local v0, "o":Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;
    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->state:Lcom/navdy/service/library/events/notification/NotificationsState;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->state:Lcom/navdy/service/library/events/notification/NotificationsState;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    .line 58
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->errorDetails:Lcom/navdy/service/library/events/notification/NotificationsError;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->errorDetails:Lcom/navdy/service/library/events/notification/NotificationsError;

    .line 59
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 64
    iget v0, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->hashCode:I

    .line 65
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 66
    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->state:Lcom/navdy/service/library/events/notification/NotificationsState;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->state:Lcom/navdy/service/library/events/notification/NotificationsState;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/notification/NotificationsState;->hashCode()I

    move-result v0

    .line 67
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/notification/ServiceType;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 68
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->errorDetails:Lcom/navdy/service/library/events/notification/NotificationsError;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->errorDetails:Lcom/navdy/service/library/events/notification/NotificationsError;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/notification/NotificationsError;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 69
    iput v0, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->hashCode:I

    .line 71
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 66
    goto :goto_0

    :cond_3
    move v2, v1

    .line 67
    goto :goto_1
.end method
