.class public final Lcom/navdy/service/library/events/file/PreviewFileRequest;
.super Lcom/squareup/wire/Message;
.source "PreviewFileRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/file/PreviewFileRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_FILENAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final filename:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/navdy/service/library/events/file/PreviewFileRequest$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/navdy/service/library/events/file/PreviewFileRequest$Builder;

    .prologue
    .line 27
    iget-object v0, p1, Lcom/navdy/service/library/events/file/PreviewFileRequest$Builder;->filename:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/events/file/PreviewFileRequest;-><init>(Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/file/PreviewFileRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 29
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/file/PreviewFileRequest$Builder;Lcom/navdy/service/library/events/file/PreviewFileRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/file/PreviewFileRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/file/PreviewFileRequest$1;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/file/PreviewFileRequest;-><init>(Lcom/navdy/service/library/events/file/PreviewFileRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/navdy/service/library/events/file/PreviewFileRequest;->filename:Ljava/lang/String;

    .line 24
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 33
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    .line 35
    .end local p1    # "other":Ljava/lang/Object;
    :goto_0
    return v0

    .line 34
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lcom/navdy/service/library/events/file/PreviewFileRequest;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 35
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/events/file/PreviewFileRequest;->filename:Ljava/lang/String;

    check-cast p1, Lcom/navdy/service/library/events/file/PreviewFileRequest;

    .end local p1    # "other":Ljava/lang/Object;
    iget-object v1, p1, Lcom/navdy/service/library/events/file/PreviewFileRequest;->filename:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/service/library/events/file/PreviewFileRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 40
    iget v0, p0, Lcom/navdy/service/library/events/file/PreviewFileRequest;->hashCode:I

    .line 41
    .local v0, "result":I
    if-eqz v0, :cond_0

    .end local v0    # "result":I
    :goto_0
    return v0

    .restart local v0    # "result":I
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/events/file/PreviewFileRequest;->filename:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/file/PreviewFileRequest;->filename:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_1
    iput v1, p0, Lcom/navdy/service/library/events/file/PreviewFileRequest;->hashCode:I

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
