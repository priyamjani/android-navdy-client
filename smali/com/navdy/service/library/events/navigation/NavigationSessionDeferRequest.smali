.class public final Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;
.super Lcom/squareup/wire/Message;
.source "NavigationSessionDeferRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_DELAY:Ljava/lang/Integer;

.field public static final DEFAULT_EXPIRETIMESTAMP:Ljava/lang/Long;

.field public static final DEFAULT_ORIGINDISPLAY:Ljava/lang/Boolean;

.field public static final DEFAULT_REQUESTID:Ljava/lang/String; = ""

.field public static final DEFAULT_ROUTEID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final delay:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final expireTimestamp:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final originDisplay:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final requestId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final routeId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->DEFAULT_DELAY:Ljava/lang/Integer;

    .line 25
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->DEFAULT_ORIGINDISPLAY:Ljava/lang/Boolean;

    .line 26
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->DEFAULT_EXPIRETIMESTAMP:Ljava/lang/Long;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;)V
    .locals 6
    .param p1, "builder"    # Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;

    .prologue
    .line 69
    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;->requestId:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;->routeId:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;->delay:Ljava/lang/Integer;

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;->originDisplay:Ljava/lang/Boolean;

    iget-object v5, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;->expireTimestamp:Ljava/lang/Long;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Long;)V

    .line 70
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 71
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$1;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Long;)V
    .locals 0
    .param p1, "requestId"    # Ljava/lang/String;
    .param p2, "routeId"    # Ljava/lang/String;
    .param p3, "delay"    # Ljava/lang/Integer;
    .param p4, "originDisplay"    # Ljava/lang/Boolean;
    .param p5, "expireTimestamp"    # Ljava/lang/Long;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->requestId:Ljava/lang/String;

    .line 62
    iput-object p2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->routeId:Ljava/lang/String;

    .line 63
    iput-object p3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->delay:Ljava/lang/Integer;

    .line 64
    iput-object p4, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->originDisplay:Ljava/lang/Boolean;

    .line 65
    iput-object p5, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->expireTimestamp:Ljava/lang/Long;

    .line 66
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    if-ne p1, p0, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v1

    .line 76
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 77
    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;

    .line 78
    .local v0, "o":Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;
    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->requestId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->requestId:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->routeId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->routeId:Ljava/lang/String;

    .line 79
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->delay:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->delay:Ljava/lang/Integer;

    .line 80
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->originDisplay:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->originDisplay:Ljava/lang/Boolean;

    .line 81
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->expireTimestamp:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->expireTimestamp:Ljava/lang/Long;

    .line 82
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 87
    iget v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->hashCode:I

    .line 88
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 89
    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->requestId:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->requestId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 90
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->routeId:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->routeId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 91
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->delay:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->delay:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 92
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->originDisplay:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->originDisplay:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 93
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->expireTimestamp:Ljava/lang/Long;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->expireTimestamp:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 94
    iput v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->hashCode:I

    .line 96
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 89
    goto :goto_0

    :cond_3
    move v2, v1

    .line 90
    goto :goto_1

    :cond_4
    move v2, v1

    .line 91
    goto :goto_2

    :cond_5
    move v2, v1

    .line 92
    goto :goto_3
.end method
