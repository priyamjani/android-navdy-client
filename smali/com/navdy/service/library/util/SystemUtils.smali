.class public Lcom/navdy/service/library/util/SystemUtils;
.super Ljava/lang/Object;
.source "SystemUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/util/SystemUtils$CpuInfo;,
        Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
    }
.end annotation


# static fields
.field private static final COMMA:Ljava/lang/String; = ","

.field private static final PERCENTAGE:Ljava/lang/String; = "%"

.field private static final SPACE:Ljava/lang/String; = " "

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/util/SystemUtils;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/util/SystemUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static extractNumberFromPercent(Ljava/lang/String;)I
    .locals 4
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 271
    if-eqz p0, :cond_1

    .line 272
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 273
    const-string v3, "%"

    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 274
    .local v0, "index":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 275
    const/4 v3, 0x0

    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 277
    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 283
    .end local v0    # "index":I
    :cond_1
    :goto_0
    return v2

    .line 279
    :catch_0
    move-exception v1

    .line 280
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/service/library/util/SystemUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static getCpuUsage()Lcom/navdy/service/library/util/SystemUtils$CpuInfo;
    .locals 27

    .prologue
    .line 178
    const/4 v13, 0x0

    .line 181
    .local v13, "inputStream":Ljava/io/InputStream;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v24

    const-string v25, "top -m 5 -t -n 1"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v18

    .line 182
    .local v18, "process":Ljava/lang/Process;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Process;->waitFor()I

    .line 183
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v13

    .line 184
    const-string v24, "UTF-8"

    move-object/from16 v0, v24

    invoke-static {v13, v0}, Lcom/navdy/service/library/util/IOUtils;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 185
    .local v20, "str":Ljava/lang/String;
    new-instance v19, Ljava/io/BufferedReader;

    new-instance v24, Ljava/io/StringReader;

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 187
    .local v19, "reader":Ljava/io/BufferedReader;
    const/16 v16, 0x1

    .line 188
    .local v16, "lineCount":I
    const/16 v23, 0x0

    .line 189
    .local v23, "usr":Ljava/lang/String;
    const/16 v21, 0x0

    .line 190
    .local v21, "system":Ljava/lang/String;
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 192
    .local v17, "plist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;>;"
    :cond_0
    :goto_0
    invoke-virtual/range {v19 .. v19}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v15

    .local v15, "line":Ljava/lang/String;
    if-eqz v15, :cond_6

    .line 193
    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v24

    if-eqz v24, :cond_0

    .line 196
    const/16 v24, 0x1

    move/from16 v0, v16

    move/from16 v1, v24

    if-ne v0, v1, :cond_4

    .line 197
    new-instance v22, Ljava/util/StringTokenizer;

    const-string v24, ","

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-direct {v0, v15, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    .local v22, "tokenizer":Ljava/util/StringTokenizer;
    :cond_1
    :goto_1
    invoke-virtual/range {v22 .. v22}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v24

    if-eqz v24, :cond_3

    .line 199
    invoke-virtual/range {v22 .. v22}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 200
    .local v11, "element":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    .line 201
    const-string v24, " "

    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v12

    .line 202
    .local v12, "index":I
    if-ltz v12, :cond_1

    .line 203
    if-nez v23, :cond_2

    .line 204
    add-int/lit8 v24, v12, 0x1

    move/from16 v0, v24

    invoke-virtual {v11, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v23

    goto :goto_1

    .line 206
    :cond_2
    add-int/lit8 v24, v12, 0x1

    move/from16 v0, v24

    invoke-virtual {v11, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v21

    .line 258
    .end local v11    # "element":Ljava/lang/String;
    .end local v12    # "index":I
    .end local v22    # "tokenizer":Ljava/util/StringTokenizer;
    :cond_3
    :goto_2
    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    .line 211
    :cond_4
    const/16 v24, 0x4

    move/from16 v0, v16

    move/from16 v1, v24

    if-lt v0, v1, :cond_3

    .line 212
    const/4 v10, 0x0

    .line 213
    .local v10, "cpu":Ljava/lang/String;
    const/4 v8, 0x0

    .line 214
    .local v8, "thread":Ljava/lang/String;
    const/4 v7, 0x0

    .line 215
    .local v7, "pname":Ljava/lang/String;
    new-instance v22, Ljava/util/StringTokenizer;

    const-string v24, " "

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-direct {v0, v15, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    .restart local v22    # "tokenizer":Ljava/util/StringTokenizer;
    const/4 v14, 0x1

    .line 217
    .local v14, "item":I
    const/4 v5, 0x0

    .line 218
    .local v5, "pid":I
    const/4 v6, 0x0

    .line 219
    .local v6, "tid":I
    :goto_3
    invoke-virtual/range {v22 .. v22}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v24

    if-eqz v24, :cond_5

    .line 220
    invoke-virtual/range {v22 .. v22}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221
    .restart local v11    # "element":Ljava/lang/String;
    packed-switch v14, :pswitch_data_0

    .line 251
    :goto_4
    :pswitch_0
    add-int/lit8 v14, v14, 0x1

    .line 252
    goto :goto_3

    .line 224
    :pswitch_1
    :try_start_1
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    goto :goto_4

    .line 231
    :pswitch_2
    :try_start_2
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v6

    goto :goto_4

    .line 237
    :pswitch_3
    move-object v10, v11

    .line 238
    goto :goto_4

    .line 241
    :pswitch_4
    move-object v8, v11

    .line 242
    goto :goto_4

    .line 245
    :pswitch_5
    move-object v7, v11

    .line 246
    goto :goto_4

    .line 254
    .end local v11    # "element":Ljava/lang/String;
    :cond_5
    :try_start_3
    invoke-static {v10}, Lcom/navdy/service/library/util/SystemUtils;->extractNumberFromPercent(Ljava/lang/String;)I

    move-result v9

    .line 255
    .local v9, "nCpu":I
    new-instance v4, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;

    invoke-direct/range {v4 .. v9}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;-><init>(IILjava/lang/String;Ljava/lang/String;I)V

    .line 256
    .local v4, "pInfo":Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 261
    .end local v4    # "pInfo":Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
    .end local v5    # "pid":I
    .end local v6    # "tid":I
    .end local v7    # "pname":Ljava/lang/String;
    .end local v8    # "thread":Ljava/lang/String;
    .end local v9    # "nCpu":I
    .end local v10    # "cpu":Ljava/lang/String;
    .end local v14    # "item":I
    .end local v15    # "line":Ljava/lang/String;
    .end local v16    # "lineCount":I
    .end local v17    # "plist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;>;"
    .end local v18    # "process":Ljava/lang/Process;
    .end local v19    # "reader":Ljava/io/BufferedReader;
    .end local v20    # "str":Ljava/lang/String;
    .end local v21    # "system":Ljava/lang/String;
    .end local v22    # "tokenizer":Ljava/util/StringTokenizer;
    .end local v23    # "usr":Ljava/lang/String;
    :catch_0
    move-exception v24

    .line 263
    invoke-static {v13}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 266
    const/16 v24, 0x0

    :goto_5
    return-object v24

    .line 260
    .restart local v15    # "line":Ljava/lang/String;
    .restart local v16    # "lineCount":I
    .restart local v17    # "plist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;>;"
    .restart local v18    # "process":Ljava/lang/Process;
    .restart local v19    # "reader":Ljava/io/BufferedReader;
    .restart local v20    # "str":Ljava/lang/String;
    .restart local v21    # "system":Ljava/lang/String;
    .restart local v23    # "usr":Ljava/lang/String;
    :cond_6
    :try_start_4
    new-instance v24, Lcom/navdy/service/library/util/SystemUtils$CpuInfo;

    invoke-static/range {v23 .. v23}, Lcom/navdy/service/library/util/SystemUtils;->extractNumberFromPercent(Ljava/lang/String;)I

    move-result v25

    invoke-static/range {v21 .. v21}, Lcom/navdy/service/library/util/SystemUtils;->extractNumberFromPercent(Ljava/lang/String;)I

    move-result v26

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    move-object/from16 v3, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/util/SystemUtils$CpuInfo;-><init>(IILjava/util/ArrayList;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 263
    invoke-static {v13}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_5

    .end local v15    # "line":Ljava/lang/String;
    .end local v16    # "lineCount":I
    .end local v17    # "plist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;>;"
    .end local v18    # "process":Ljava/lang/Process;
    .end local v19    # "reader":Ljava/io/BufferedReader;
    .end local v20    # "str":Ljava/lang/String;
    .end local v21    # "system":Ljava/lang/String;
    .end local v23    # "usr":Ljava/lang/String;
    :catchall_0
    move-exception v24

    invoke-static {v13}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v24

    .line 225
    .restart local v5    # "pid":I
    .restart local v6    # "tid":I
    .restart local v7    # "pname":Ljava/lang/String;
    .restart local v8    # "thread":Ljava/lang/String;
    .restart local v10    # "cpu":Ljava/lang/String;
    .restart local v11    # "element":Ljava/lang/String;
    .restart local v14    # "item":I
    .restart local v15    # "line":Ljava/lang/String;
    .restart local v16    # "lineCount":I
    .restart local v17    # "plist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;>;"
    .restart local v18    # "process":Ljava/lang/Process;
    .restart local v19    # "reader":Ljava/io/BufferedReader;
    .restart local v20    # "str":Ljava/lang/String;
    .restart local v21    # "system":Ljava/lang/String;
    .restart local v22    # "tokenizer":Ljava/util/StringTokenizer;
    .restart local v23    # "usr":Ljava/lang/String;
    :catch_1
    move-exception v24

    goto :goto_4

    .line 232
    :catch_2
    move-exception v24

    goto :goto_4

    .line 221
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static getNativeProcessId(Ljava/lang/String;)I
    .locals 9
    .param p0, "processName"    # Ljava/lang/String;

    .prologue
    .line 116
    const/4 v0, 0x0

    .line 118
    .local v0, "inputStream":Ljava/io/InputStream;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v7

    const-string v8, "ps"

    invoke-virtual {v7, v8}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v3

    .line 119
    .local v3, "process":Ljava/lang/Process;
    invoke-virtual {v3}, Ljava/lang/Process;->waitFor()I

    .line 120
    invoke-virtual {v3}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 121
    const-string v7, "UTF-8"

    invoke-static {v0, v7}, Lcom/navdy/service/library/util/IOUtils;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 122
    .local v5, "str":Ljava/lang/String;
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/StringReader;

    invoke-direct {v7, v5}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 124
    .local v4, "reader":Ljava/io/BufferedReader;
    :cond_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .local v1, "line":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 125
    invoke-virtual {v1, p0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 126
    new-instance v6, Ljava/util/StringTokenizer;

    invoke-direct {v6, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    .line 127
    .local v6, "tokenizer":Ljava/util/StringTokenizer;
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    .line 128
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 129
    .local v2, "pid":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 130
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 135
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 137
    .end local v1    # "line":Ljava/lang/String;
    .end local v2    # "pid":Ljava/lang/String;
    .end local v3    # "process":Ljava/lang/Process;
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .end local v5    # "str":Ljava/lang/String;
    .end local v6    # "tokenizer":Ljava/util/StringTokenizer;
    :goto_0
    return v7

    .line 135
    .restart local v1    # "line":Ljava/lang/String;
    .restart local v3    # "process":Ljava/lang/Process;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "str":Ljava/lang/String;
    :cond_1
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 137
    .end local v1    # "line":Ljava/lang/String;
    .end local v3    # "process":Ljava/lang/Process;
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .end local v5    # "str":Ljava/lang/String;
    :goto_1
    const/4 v7, -0x1

    goto :goto_0

    .line 133
    :catch_0
    move-exception v7

    .line 135
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_0
    move-exception v7

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v7
.end method

.method public static getProcessId(Landroid/content/Context;Ljava/lang/String;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "processName"    # Ljava/lang/String;

    .prologue
    .line 106
    const-string v2, "activity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 107
    .local v0, "activityManager":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 108
    .local v1, "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v3, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-static {v3, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 109
    iget v2, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    .line 112
    .end local v1    # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :goto_0
    return v2

    :cond_1
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public static getProcessName(Landroid/content/Context;I)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pid"    # I

    .prologue
    .line 91
    const-string v3, "activity"

    .line 92
    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 94
    .local v0, "activityManager":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    .line 95
    .local v2, "runningAppProcesses":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    if-eqz v2, :cond_1

    .line 96
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 97
    .local v1, "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    if-eqz v1, :cond_0

    iget v4, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v4, p1, :cond_0

    .line 98
    iget-object v3, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    .line 102
    .end local v1    # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :goto_0
    return-object v3

    :cond_1
    const-string v3, ""

    goto :goto_0
.end method

.method public static isConnectedToCellNetwork(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 166
    const-string v3, "connectivity"

    .line 167
    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 168
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 169
    .local v1, "wifiNetworkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method public static isConnectedToNetwork(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 144
    const-string v2, "connectivity"

    .line 145
    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 147
    .local v1, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 148
    .local v0, "activeNetwork":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    .line 149
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isConnectedToWifi(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 156
    const-string v3, "connectivity"

    .line 157
    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 158
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 159
    .local v1, "wifiNetworkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
