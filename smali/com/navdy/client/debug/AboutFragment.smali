.class public Lcom/navdy/client/debug/AboutFragment;
.super Landroid/app/Fragment;
.source "AboutFragment.java"


# instance fields
.field appVersionLabel:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10021d
    .end annotation
.end field

.field displayInfo:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100220
    .end annotation
.end field

.field serialLabel:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100221
    .end annotation
.end field

.field serialNumber:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100222
    .end annotation
.end field

.field updateStatusTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10021e
    .end annotation
.end field

.field vin:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100224
    .end annotation
.end field

.field vinLabel:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100223
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 59
    return-void
.end method


# virtual methods
.method public onCheckForUpdate(Landroid/widget/Button;)V
    .locals 3
    .param p1, "button"    # Landroid/widget/Button;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10021f
        }
    .end annotation

    .prologue
    .line 100
    iget-object v1, p0, Lcom/navdy/client/debug/AboutFragment;->updateStatusTextView:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 101
    invoke-virtual {p0}, Lcom/navdy/client/debug/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/MainDebugActivity;

    .line 102
    .local v0, "mainDebugActivity":Lcom/navdy/client/debug/MainDebugActivity;
    new-instance v1, Lcom/navdy/client/debug/AboutFragment$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/debug/AboutFragment$1;-><init>(Lcom/navdy/client/debug/AboutFragment;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/MainDebugActivity;->checkForUpdates(Lnet/hockeyapp/android/UpdateManagerListener;)V

    .line 108
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v5

    invoke-virtual {v5, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 67
    const v5, 0x7f030084

    const/4 v6, 0x0

    invoke-virtual {p1, v5, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 68
    .local v2, "rootView":Landroid/view/View;
    invoke-static {p0, v2}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 70
    iget-object v5, p0, Lcom/navdy/client/debug/AboutFragment;->appVersionLabel:Landroid/widget/TextView;

    const-string v6, "1.3.1718-e615013"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 74
    .local v0, "device":Lcom/navdy/service/library/device/RemoteDevice;
    const/16 v4, 0x8

    .line 75
    .local v4, "visibility":I
    if-eqz v0, :cond_0

    .line 76
    const/4 v4, 0x0

    .line 77
    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v1

    .line 78
    .local v1, "info":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v1, :cond_1

    iget-object v3, v1, Lcom/navdy/service/library/events/DeviceInfo;->deviceUuid:Ljava/lang/String;

    .line 79
    .local v3, "serial":Ljava/lang/String;
    :goto_0
    iget-object v5, p0, Lcom/navdy/client/debug/AboutFragment;->serialNumber:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    .end local v1    # "info":Lcom/navdy/service/library/events/DeviceInfo;
    .end local v3    # "serial":Ljava/lang/String;
    :cond_0
    iget-object v5, p0, Lcom/navdy/client/debug/AboutFragment;->displayInfo:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 82
    iget-object v5, p0, Lcom/navdy/client/debug/AboutFragment;->serialLabel:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 83
    iget-object v5, p0, Lcom/navdy/client/debug/AboutFragment;->serialNumber:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 85
    return-object v2

    .line 78
    .restart local v1    # "info":Lcom/navdy/service/library/events/DeviceInfo;
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/client/debug/AboutFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0804da

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 112
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 113
    return-void
.end method

.method public onObdStatusResponse(Lcom/navdy/service/library/events/obd/ObdStatusResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/navdy/service/library/events/obd/ObdStatusResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 123
    iget-object v0, p0, Lcom/navdy/client/debug/AboutFragment;->vinLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/navdy/client/debug/AboutFragment;->vin:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 125
    iget-object v1, p0, Lcom/navdy/client/debug/AboutFragment;->vin:Landroid/widget/TextView;

    iget-object v0, p1, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->vin:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->vin:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    return-void

    .line 125
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/debug/AboutFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0804da

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 117
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->unregister(Ljava/lang/Object;)V

    .line 118
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 119
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 90
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 92
    iget-object v0, p0, Lcom/navdy/client/debug/AboutFragment;->vinLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lcom/navdy/client/debug/AboutFragment;->vin:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 95
    new-instance v0, Lcom/navdy/service/library/events/obd/ObdStatusRequest;

    invoke-direct {v0}, Lcom/navdy/service/library/events/obd/ObdStatusRequest;-><init>()V

    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 96
    return-void
.end method
