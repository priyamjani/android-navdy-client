.class public Lcom/navdy/client/app/ui/routing/RoutingActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "RoutingActivity.java"

# interfaces
.implements Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;


# static fields
.field private static final HERE_MAP_PADDING:I

.field private static final HERE_MAP_TOP_PADDING:I

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private activeTripCardFrameLayoutContainer:Landroid/widget/RelativeLayout;

.field private canRetry:Z

.field private destinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

.field private fluctuatorAnimatorView:Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;

.field private gauge:Lcom/navdy/client/app/ui/customviews/Gauge;

.field private hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

.field private final navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

.field networkStatusManager:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private offlineBanner:Landroid/widget/RelativeLayout;

.field private routeName:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 44
    new-instance v2, Lcom/navdy/service/library/log/Logger;

    const-class v3, Lcom/navdy/client/app/ui/routing/RoutingActivity;

    invoke-direct {v2, v3}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v2, Lcom/navdy/client/app/ui/routing/RoutingActivity;->logger:Lcom/navdy/service/library/log/Logger;

    .line 50
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 51
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 53
    .local v1, "res":Landroid/content/res/Resources;
    const v2, 0x7f0b012d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sget v3, Lcom/navdy/client/app/framework/map/MapUtils;->hereMapTopDownPadding:I

    add-int/2addr v2, v3

    sput v2, Lcom/navdy/client/app/ui/routing/RoutingActivity;->HERE_MAP_TOP_PADDING:I

    .line 56
    sget v2, Lcom/navdy/client/app/framework/map/MapUtils;->hereMapSidePadding:I

    sput v2, Lcom/navdy/client/app/ui/routing/RoutingActivity;->HERE_MAP_PADDING:I

    .line 57
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->networkStatusManager:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    .line 87
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getInstance()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .line 88
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/routing/RoutingActivity;)Lcom/here/android/mpa/mapping/MapMarker;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/routing/RoutingActivity;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->destinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    return-object v0
.end method

.method static synthetic access$002(Lcom/navdy/client/app/ui/routing/RoutingActivity;Lcom/here/android/mpa/mapping/MapMarker;)Lcom/here/android/mpa/mapping/MapMarker;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/routing/RoutingActivity;
    .param p1, "x1"    # Lcom/here/android/mpa/mapping/MapMarker;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->destinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    return-object p1
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/routing/RoutingActivity;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/routing/RoutingActivity;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    return-object v0
.end method

.method private initUi()V
    .locals 2

    .prologue
    .line 269
    invoke-direct {p0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->setUpHereMap()V

    .line 272
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v1, 0x7f0800e2

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 275
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->activeTripCardFrameLayoutContainer:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 276
    return-void
.end method

.method private setUpHereMap()V
    .locals 6

    .prologue
    .line 280
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f1000ad

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    iput-object v1, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .line 281
    iget-object v1, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    if-nez v1, :cond_0

    .line 282
    sget-object v1, Lcom/navdy/client/app/ui/routing/RoutingActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Calling setUpHereMap with a null hereMapFragment"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 304
    :goto_0
    return-void

    .line 286
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->hide()V

    .line 287
    iget-object v1, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    sget v2, Lcom/navdy/client/app/ui/routing/RoutingActivity;->HERE_MAP_TOP_PADDING:I

    sget v3, Lcom/navdy/client/app/ui/routing/RoutingActivity;->HERE_MAP_PADDING:I

    sget v4, Lcom/navdy/client/app/ui/routing/RoutingActivity;->HERE_MAP_PADDING:I

    sget v5, Lcom/navdy/client/app/ui/routing/RoutingActivity;->HERE_MAP_PADDING:I

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->setUsableArea(IIII)V

    .line 292
    iget-object v1, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->centerOnUserLocation()V

    .line 294
    new-instance v0, Lcom/navdy/client/app/ui/routing/RoutingActivity$2;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/routing/RoutingActivity$2;-><init>(Lcom/navdy/client/app/ui/routing/RoutingActivity;)V

    .line 302
    .local v0, "listener":Lcom/here/android/mpa/mapping/MapGesture$OnGestureListener;
    iget-object v1, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-virtual {v1, v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->setMapGesture(Lcom/here/android/mpa/mapping/MapGesture$OnGestureListener;)V

    .line 303
    iget-object v1, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->show()V

    goto :goto_0
.end method

.method public static startRoutingActivity(Landroid/app/Activity;)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 60
    if-eqz p0, :cond_0

    .line 61
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/routing/RoutingActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 62
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 66
    .end local v0    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 64
    :cond_0
    sget-object v1, Lcom/navdy/client/app/ui/routing/RoutingActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "could not start routing activity, origin activity is null"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateOfflineBannerVisibility()V
    .locals 3

    .prologue
    .line 320
    iget-object v1, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->offlineBanner:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 321
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v0

    .line 322
    .local v0, "canReachInternet":Z
    iget-object v2, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->offlineBanner:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 324
    .end local v0    # "canReachInternet":Z
    :cond_0
    return-void

    .line 322
    .restart local v0    # "canReachInternet":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public appInstanceDeviceDisconnectedEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;)V
    .locals 2
    .param p1, "deviceDisconnectedEvent"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 111
    sget-object v0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "App Disconnected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 113
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->finish()V

    .line 114
    return-void
.end method

.method public handleReachabilityStateChange(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;)V
    .locals 0
    .param p1, "reachabilityEvent"    # Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 310
    if-eqz p1, :cond_0

    .line 315
    invoke-direct {p0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->updateOfflineBannerVisibility()V

    .line 317
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->stopRouting()V

    .line 185
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onBackPressed()V

    .line 186
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 120
    sget-object v0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onCreate"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 122
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 123
    const v0, 0x7f0300d6

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->setContentView(I)V

    .line 124
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/navdy/client/app/framework/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 127
    const v0, 0x7f10033f

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->activeTripCardFrameLayoutContainer:Landroid/widget/RelativeLayout;

    .line 128
    const v0, 0x7f100341

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/customviews/Gauge;

    iput-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->gauge:Lcom/navdy/client/app/ui/customviews/Gauge;

    .line 129
    const v0, 0x7f100340

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->fluctuatorAnimatorView:Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;

    .line 130
    const v0, 0x7f1000aa

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->offlineBanner:Landroid/widget/RelativeLayout;

    .line 131
    const v0, 0x7f10033d

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->routeName:Landroid/widget/TextView;

    .line 133
    invoke-direct {p0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->initUi()V

    .line 134
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 158
    sget-object v0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onDestroy"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 159
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 160
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->fluctuatorAnimatorView:Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->stop()V

    .line 162
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onDestroy()V

    .line 163
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 167
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 172
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 169
    :pswitch_0
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->onBackPressed()V

    .line 170
    const/4 v0, 0x1

    goto :goto_0

    .line 167
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 150
    sget-object v0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onPause"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->removeListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;)V

    .line 153
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onPause()V

    .line 154
    return-void
.end method

.method public onPendingRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "pendingRoute"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->finish()V

    .line 198
    return-void
.end method

.method public onPendingRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->finish()V

    .line 193
    return-void
.end method

.method public onRefreshConnectivityClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 327
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->checkForNetwork()V

    .line 328
    return-void
.end method

.method public onReroute()V
    .locals 0

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->finish()V

    .line 255
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 138
    sget-object v0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onResume"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 139
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 141
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->onResume()V

    .line 142
    const-string v0, "Routing"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->addListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;)V

    .line 145
    invoke-direct {p0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->updateOfflineBannerVisibility()V

    .line 146
    return-void
.end method

.method public onRouteArrived(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 258
    return-void
.end method

.method public onRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 2
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 232
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;->NONE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    if-ne p1, v0, :cond_0

    .line 233
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->finish()V

    .line 240
    :goto_0
    return-void

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->fluctuatorAnimatorView:Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->stop()V

    .line 236
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->fluctuatorAnimatorView:Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->setVisibility(I)V

    .line 237
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->routeName:Landroid/widget/TextView;

    const v1, 0x7f080163

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 238
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->canRetry:Z

    goto :goto_0
.end method

.method public onRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 2
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 202
    sget-object v0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onRouteCalculating"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->routeName:Landroid/widget/TextView;

    const v1, 0x7f0800e1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 207
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->gauge:Lcom/navdy/client/app/ui/customviews/Gauge;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/customviews/Gauge;->setVisibility(I)V

    .line 209
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->fluctuatorAnimatorView:Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->setVisibility(I)V

    .line 210
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->fluctuatorAnimatorView:Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->start()V

    .line 211
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->fluctuatorAnimatorView:Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->animate()Landroid/view/ViewPropertyAnimator;

    .line 213
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    new-instance v1, Lcom/navdy/client/app/ui/routing/RoutingActivity$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/routing/RoutingActivity$1;-><init>(Lcom/navdy/client/app/ui/routing/RoutingActivity;Lcom/navdy/client/app/framework/models/Destination;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenReady(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;)V

    .line 228
    return-void
.end method

.method public onRouteStarted(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->finish()V

    .line 245
    return-void
.end method

.method public onStopRoute()V
    .locals 0

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->finish()V

    .line 263
    return-void
.end method

.method public onTripProgress(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "progress"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->finish()V

    .line 250
    return-void
.end method

.method public retryRoute(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->canRetry:Z

    if-eqz v0, :cond_0

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->canRetry:Z

    .line 96
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/RoutingActivity;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->retryRoute()V

    .line 98
    :cond_0
    return-void
.end method
