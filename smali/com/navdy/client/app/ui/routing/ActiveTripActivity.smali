.class public Lcom/navdy/client/app/ui/routing/ActiveTripActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "ActiveTripActivity.java"

# interfaces
.implements Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;


# static fields
.field private static final HERE_PADDING:I

.field private static final MAP_ORIENTATION:F


# instance fields
.field private currentProgressMapPolyline:Lcom/here/android/mpa/mapping/MapPolyline;

.field private destinationMapMarker:Lcom/here/android/mpa/mapping/MapMarker;

.field private hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

.field private final navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

.field private routeMapPolyline:Lcom/here/android/mpa/mapping/MapPolyline;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    sget v0, Lcom/navdy/client/app/framework/map/MapUtils;->hereMapSidePadding:I

    sput v0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->HERE_PADDING:I

    .line 45
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    .line 71
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getInstance()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/here/android/mpa/mapping/MapMarker;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->destinationMapMarker:Lcom/here/android/mpa/mapping/MapMarker;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;Lcom/here/android/mpa/mapping/MapMarker;)Lcom/here/android/mpa/mapping/MapMarker;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/routing/ActiveTripActivity;
    .param p1, "x1"    # Lcom/here/android/mpa/mapping/MapMarker;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->destinationMapMarker:Lcom/here/android/mpa/mapping/MapMarker;

    return-object p1
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/here/android/mpa/mapping/MapPolyline;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->currentProgressMapPolyline:Lcom/here/android/mpa/mapping/MapPolyline;

    return-object v0
.end method

.method static synthetic access$402(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;Lcom/here/android/mpa/mapping/MapPolyline;)Lcom/here/android/mpa/mapping/MapPolyline;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/routing/ActiveTripActivity;
    .param p1, "x1"    # Lcom/here/android/mpa/mapping/MapPolyline;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->currentProgressMapPolyline:Lcom/here/android/mpa/mapping/MapPolyline;

    return-object p1
.end method

.method static synthetic access$500(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/here/android/mpa/mapping/MapPolyline;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->routeMapPolyline:Lcom/here/android/mpa/mapping/MapPolyline;

    return-object v0
.end method

.method static synthetic access$502(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;Lcom/here/android/mpa/mapping/MapPolyline;)Lcom/here/android/mpa/mapping/MapPolyline;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/routing/ActiveTripActivity;
    .param p1, "x1"    # Lcom/here/android/mpa/mapping/MapPolyline;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->routeMapPolyline:Lcom/here/android/mpa/mapping/MapPolyline;

    return-object p1
.end method

.method private clearMapObjects()V
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    if-nez v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Calling clearMapObjects with a null hereMapFragment"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 314
    :goto_0
    return-void

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    new-instance v1, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$7;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$7;-><init>(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenInitialized(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;)V

    goto :goto_0
.end method

.method public static startActiveTripActivity(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    if-eqz p0, :cond_0

    instance-of v1, p0, Landroid/app/Activity;

    if-nez v1, :cond_1

    .line 49
    :cond_0
    sget-object v1, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "startActiveTripActivity, context is invalid"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 60
    :goto_0
    return-void

    .line 53
    :cond_1
    instance-of v1, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    if-eqz v1, :cond_2

    .line 54
    sget-object v1, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "starting ActiveTripActivity from itself, no-op"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 58
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 59
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private startMapRoute(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 2
    .param p1, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 317
    invoke-direct {p0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->clearMapObjects()V

    .line 319
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    if-nez v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Calling startMapRoute with a null hereMapFragment"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 347
    :goto_0
    return-void

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    new-instance v1, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$8;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$8;-><init>(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenReady(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;)V

    goto :goto_0
.end method

.method private updateOfflineBannerVisibility()V
    .locals 3

    .prologue
    .line 363
    const v2, 0x7f1000aa

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 364
    .local v1, "offlineBanner":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 365
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v0

    .line 366
    .local v0, "canReachInternet":Z
    if-eqz v0, :cond_1

    const/16 v2, 0x8

    :goto_0
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 368
    .end local v0    # "canReachInternet":Z
    :cond_0
    return-void

    .line 366
    .restart local v0    # "canReachInternet":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public centerOnDriver(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Calling centerOnDriver with a null hereMapFragment"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 162
    :goto_0
    return-void

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    new-instance v1, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$2;-><init>(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenInitialized(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;)V

    goto :goto_0
.end method

.method public handleReachabilityStateChange(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;)V
    .locals 0
    .param p1, "reachabilityEvent"    # Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 353
    if-eqz p1, :cond_0

    .line 358
    invoke-direct {p0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->updateOfflineBannerVisibility()V

    .line 360
    :cond_0
    return-void
.end method

.method public northUpOrientation(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    if-nez v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Calling northUpOrientation with a null hereMapFragment"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 182
    :goto_0
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    new-instance v1, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$3;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$3;-><init>(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenInitialized(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 79
    const v5, 0x7f03001b

    invoke-virtual {p0, v5}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->setContentView(I)V

    .line 82
    new-instance v5, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v6, 0x7f0800ab

    .line 83
    invoke-virtual {v5, v6}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v5

    .line 84
    invoke-virtual {v5}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 87
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const v6, 0x7f1000ad

    invoke-virtual {v5, v6}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v5

    check-cast v5, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    iput-object v5, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .line 88
    const v5, 0x7f1000af

    invoke-virtual {p0, v5}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 90
    .local v2, "northUpImg":Landroid/view/View;
    iget-object v5, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    if-eqz v5, :cond_0

    .line 91
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 92
    .local v3, "res":Landroid/content/res/Resources;
    const v5, 0x7f0b012d

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sget v6, Lcom/navdy/client/app/framework/map/MapUtils;->hereMapTopDownPadding:I

    add-int v1, v5, v6

    .line 95
    .local v1, "hereMapTopPaddingInPixels":I
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v5

    const v6, 0x7f0b0136

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    sget v6, Lcom/navdy/client/app/framework/map/MapUtils;->hereMapSidePadding:I

    add-int v4, v5, v6

    .line 98
    .local v4, "utilButtonsWidth":I
    iget-object v5, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    sget v6, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->HERE_PADDING:I

    sget v7, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->HERE_PADDING:I

    invoke-virtual {v5, v1, v4, v6, v7}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->setUsableArea(IIII)V

    .line 100
    iget-object v5, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    new-instance v6, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$1;

    invoke-direct {v6, p0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$1;-><init>(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)V

    invoke-virtual {v5, v6}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenInitialized(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;)V

    .line 117
    .end local v1    # "hereMapTopPaddingInPixels":I
    .end local v3    # "res":Landroid/content/res/Resources;
    .end local v4    # "utilButtonsWidth":I
    :cond_0
    const v5, 0x7f1000b2

    invoke-virtual {p0, v5}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 118
    .local v0, "end":Landroid/widget/TextView;
    if-eqz v0, :cond_1

    .line 119
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    :cond_1
    return-void
.end method

.method public onEndTripClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 185
    const v1, 0x7f0800c2

    const v2, 0x7f080137

    const v3, 0x7f08015c

    const v4, 0x7f0800e5

    new-instance v5, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$4;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$4;-><init>(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)V

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->showQuestionDialog(IIIILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 199
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->removeListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;)V

    .line 136
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onPause()V

    .line 137
    return-void
.end method

.method public onPendingRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "pendingRoute"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 210
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->finish()V

    .line 211
    return-void
.end method

.method public onPendingRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->finish()V

    .line 206
    return-void
.end method

.method public onRefreshConnectivityClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 371
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->checkForNetwork()V

    .line 372
    return-void
.end method

.method public onReroute()V
    .locals 0

    .prologue
    .line 274
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 125
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 126
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->addListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;)V

    .line 128
    invoke-direct {p0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->updateOfflineBannerVisibility()V

    .line 130
    const-string v0, "Active_Trip"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 131
    return-void
.end method

.method public onRouteArrived(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 277
    return-void
.end method

.method public onRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 239
    return-void
.end method

.method public onRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 2
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 215
    invoke-direct {p0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->clearMapObjects()V

    .line 217
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    if-nez v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Calling hereMapFragment with a null hereMapFragment"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 236
    :goto_0
    return-void

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    new-instance v1, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$5;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$5;-><init>(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;Lcom/navdy/client/app/framework/models/Destination;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenReady(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;)V

    goto :goto_0
.end method

.method public onRouteStarted(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 243
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->startMapRoute(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    .line 244
    return-void
.end method

.method public onStopRoute()V
    .locals 0

    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->finish()V

    .line 282
    return-void
.end method

.method public onTripProgress(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 2
    .param p1, "progress"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 249
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    if-nez v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Calling onTripProgress with a null hereMapFragment"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 271
    :goto_0
    return-void

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    new-instance v1, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$6;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$6;-><init>(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenInitialized(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;)V

    goto :goto_0
.end method
