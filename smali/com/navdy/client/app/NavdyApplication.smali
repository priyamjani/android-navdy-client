.class public Lcom/navdy/client/app/NavdyApplication;
.super Landroid/support/multidex/MultiDexApplication;
.source "NavdyApplication.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/NavdyApplication$AppTaskQueue;
    }
.end annotation


# static fields
.field private static final HAS_KILLED:Ljava/lang/String; = "crashed_mapengine_kill"

.field private static final LOG_FILENAME:Ljava/lang/String; = "PhoneLogs"

.field private static final MAX_LOG_FILES:I = 0xc

.field private static final MAX_LOG_SIZE:I = 0x40000

.field private static final NASTY_HERE_ANALYTICS_CLASS:Ljava/lang/String; = "com.here.sdk.analytics.internal.a$1"

.field private static final TAG:Ljava/lang/String;

.field private static memoryMapFileAppender:Lcom/navdy/service/library/log/MemoryMapFileAppender;

.field private static sAppContext:Landroid/content/Context;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mHttpManager:Lcom/navdy/service/library/network/http/IHttpManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private mObjectGraph:Ldagger/ObjectGraph;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    const-class v0, Lcom/navdy/client/app/NavdyApplication;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/NavdyApplication;->TAG:Ljava/lang/String;

    .line 58
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    sget-object v1, Lcom/navdy/client/app/NavdyApplication;->TAG:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/client/app/NavdyApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 72
    const/4 v0, 0x0

    sput-object v0, Lcom/navdy/client/app/NavdyApplication;->memoryMapFileAppender:Lcom/navdy/service/library/log/MemoryMapFileAppender;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/support/multidex/MultiDexApplication;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/navdy/client/app/NavdyApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/NavdyApplication;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/NavdyApplication;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/navdy/client/app/NavdyApplication;->logIfKilled()V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/client/app/NavdyApplication;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/NavdyApplication;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/navdy/client/app/NavdyApplication;->initSharedPrefs()V

    return-void
.end method

.method static synthetic access$300()Landroid/content/Context;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/navdy/client/app/NavdyApplication;->sAppContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getAppContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lcom/navdy/client/app/NavdyApplication;->sAppContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getLogFiles()Ljava/util/ArrayList;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    sget-object v0, Lcom/navdy/client/app/NavdyApplication;->memoryMapFileAppender:Lcom/navdy/service/library/log/MemoryMapFileAppender;

    if-eqz v0, :cond_0

    .line 184
    sget-object v0, Lcom/navdy/client/app/NavdyApplication;->memoryMapFileAppender:Lcom/navdy/service/library/log/MemoryMapFileAppender;

    invoke-virtual {v0}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->getLogFiles()Ljava/util/ArrayList;

    move-result-object v0

    .line 186
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method private initApp()V
    .locals 8

    .prologue
    .line 280
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/IMMLeaks;->fixFocusedViewLeak(Landroid/app/Application;)V

    .line 282
    new-instance v0, Lcom/navdy/client/app/framework/ProdModule;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/ProdModule;-><init>(Landroid/content/Context;)V

    .line 283
    .local v0, "prodModule":Lcom/navdy/client/app/framework/ProdModule;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v4}, Ldagger/ObjectGraph;->create([Ljava/lang/Object;)Ldagger/ObjectGraph;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/client/app/NavdyApplication;->mObjectGraph:Ldagger/ObjectGraph;

    .line 284
    iget-object v4, p0, Lcom/navdy/client/app/NavdyApplication;->mObjectGraph:Ldagger/ObjectGraph;

    invoke-virtual {v4, p0}, Ldagger/ObjectGraph;->inject(Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    sget-object v4, Lcom/navdy/client/app/NavdyApplication;->TAG:Ljava/lang/String;

    const-string v5, "Starting the HttpManager"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    sget-object v4, Lcom/navdy/client/app/NavdyApplication;->TAG:Ljava/lang/String;

    const-string v5, "install crash handler"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    invoke-direct {p0}, Lcom/navdy/client/app/NavdyApplication;->installCrashHandler()V

    .line 288
    sget-object v4, Lcom/navdy/client/app/NavdyApplication;->TAG:Ljava/lang/String;

    const-string v5, "init logger"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->initLogger()V

    .line 290
    invoke-static {}, Lcom/navdy/client/app/tracking/Tracker;->getUserId()Ljava/lang/String;

    move-result-object v1

    .line 292
    .local v1, "userId":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->isDeveloperBuild()Z

    move-result v4

    if-nez v4, :cond_0

    .line 293
    sget-object v4, Lcom/navdy/client/app/NavdyApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "register crashlytics"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 294
    invoke-static {}, Lcom/navdy/client/app/framework/util/CrashReporter;->getInstance()Lcom/navdy/client/app/framework/util/CrashReporter;

    move-result-object v4

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Lcom/navdy/client/app/framework/util/CrashReporter;->installCrashHandler(Landroid/content/Context;Ljava/lang/String;)V

    .line 297
    :cond_0
    sget-object v4, Lcom/navdy/client/app/NavdyApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "init taskmgr"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 298
    invoke-direct {p0}, Lcom/navdy/client/app/NavdyApplication;->initTaskManager()V

    .line 299
    invoke-direct {p0}, Lcom/navdy/client/app/NavdyApplication;->registerActivityLifecycleCallbacks()V

    .line 300
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 301
    .local v2, "time":J
    sget-object v4, Lcom/navdy/client/app/NavdyApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "init AppInstance"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 302
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/AppInstance;->initializeApp()V

    .line 304
    sget-object v4, Lcom/navdy/client/app/NavdyApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "It took "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ms. to initialize App."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 305
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v4

    new-instance v5, Lcom/navdy/client/app/NavdyApplication$3;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/NavdyApplication$3;-><init>(Lcom/navdy/client/app/NavdyApplication;)V

    const/4 v6, 0x4

    invoke-virtual {v4, v5, v6}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 330
    return-void
.end method

.method private static initLogger()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 158
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v8

    .line 159
    .local v8, "internalStorageFolder":Ljava/io/File;
    const-string v2, ""

    .line 160
    .local v2, "internalStorageFolderPath":Ljava/lang/String;
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 163
    :cond_0
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 164
    new-instance v0, Lcom/navdy/service/library/log/MemoryMapFileAppender;

    sget-object v1, Lcom/navdy/client/app/NavdyApplication;->sAppContext:Landroid/content/Context;

    const-string v3, "PhoneLogs"

    const-wide/32 v4, 0x40000

    const/16 v6, 0xc

    invoke-direct/range {v0 .. v7}, Lcom/navdy/service/library/log/MemoryMapFileAppender;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JIZ)V

    sput-object v0, Lcom/navdy/client/app/NavdyApplication;->memoryMapFileAppender:Lcom/navdy/service/library/log/MemoryMapFileAppender;

    .line 171
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/service/library/log/LogAppender;

    new-instance v1, Lcom/navdy/service/library/log/LogcatAppender;

    invoke-direct {v1}, Lcom/navdy/service/library/log/LogcatAppender;-><init>()V

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/client/app/NavdyApplication;->memoryMapFileAppender:Lcom/navdy/service/library/log/MemoryMapFileAppender;

    aput-object v1, v0, v9

    invoke-static {v0}, Lcom/navdy/service/library/log/Logger;->init([Lcom/navdy/service/library/log/LogAppender;)V

    .line 180
    :goto_0
    return-void

    .line 176
    :cond_1
    new-array v0, v9, [Lcom/navdy/service/library/log/LogAppender;

    new-instance v1, Lcom/navdy/service/library/log/LogcatAppender;

    invoke-direct {v1}, Lcom/navdy/service/library/log/LogcatAppender;-><init>()V

    aput-object v1, v0, v7

    invoke-static {v0}, Lcom/navdy/service/library/log/Logger;->init([Lcom/navdy/service/library/log/LogAppender;)V

    goto :goto_0
.end method

.method private initSharedPrefs()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 343
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 344
    .local v2, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v3, "shared_pref_version"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 346
    .local v1, "prefVersion":I
    const/4 v0, 0x1

    .line 347
    .local v0, "currentSharedPrefVersion":I
    if-le v1, v5, :cond_1

    .line 348
    invoke-static {v1, v5}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->onSharedPrefsDowngrade(II)V

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 349
    :cond_1
    if-ge v1, v5, :cond_0

    .line 350
    invoke-static {v1, v5}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->onSharedPrefsUpgrade(II)V

    goto :goto_0
.end method

.method private initTaskManager()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 206
    sget-object v1, Lcom/navdy/client/app/NavdyApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, ":initializing taskMgr"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 207
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    .line 208
    .local v0, "taskManager":Lcom/navdy/service/library/task/TaskManager;
    invoke-virtual {v0, v3, v4}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 209
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 210
    invoke-virtual {v0, v4, v4}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 211
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 212
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 213
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v4}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 214
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 215
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 216
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 217
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 218
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 219
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 220
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 221
    invoke-virtual {v0}, Lcom/navdy/service/library/task/TaskManager;->init()V

    .line 222
    return-void
.end method

.method private installCrashHandler()V
    .locals 2

    .prologue
    .line 190
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    .line 191
    .local v0, "defaultHandler":Ljava/lang/Thread$UncaughtExceptionHandler;
    new-instance v1, Lcom/navdy/client/app/NavdyApplication$1;

    invoke-direct {v1, p0, v0}, Lcom/navdy/client/app/NavdyApplication$1;-><init>(Lcom/navdy/client/app/NavdyApplication;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-static {v1}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 202
    return-void
.end method

.method public static isDeveloperBuild()Z
    .locals 1

    .prologue
    .line 366
    const/4 v0, 0x0

    return v0
.end method

.method public static kill()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitPrefEdits"
        }
    .end annotation

    .prologue
    .line 150
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 151
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "crashed_mapengine_kill"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 153
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    .line 154
    return-void
.end method

.method private logIfKilled()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 333
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 334
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v2, "crashed_mapengine_kill"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 336
    .local v0, "hasKilled":I
    if-lez v0, :cond_0

    .line 337
    const-string v2, "MapEngine_Forced_Crash"

    invoke-static {v2}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 338
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "crashed_mapengine_kill"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 340
    :cond_0
    return-void
.end method

.method private registerActivityLifecycleCallbacks()V
    .locals 1

    .prologue
    .line 225
    new-instance v0, Lcom/navdy/client/app/NavdyApplication$2;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/NavdyApplication$2;-><init>(Lcom/navdy/client/app/NavdyApplication;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/NavdyApplication;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 277
    return-void
.end method


# virtual methods
.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 8
    .param p1, "base"    # Landroid/content/Context;

    .prologue
    .line 123
    invoke-super {p0, p1}, Landroid/support/multidex/MultiDexApplication;->attachBaseContext(Landroid/content/Context;)V

    .line 124
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 125
    .local v0, "l1":J
    invoke-static {p0}, Landroid/support/multidex/MultiDex;->install(Landroid/content/Context;)V

    .line 126
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 127
    .local v2, "l2":J
    sget-object v4, Lcom/navdy/client/app/NavdyApplication;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Time to install Multidex: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long v6, v2, v0

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    return-void
.end method

.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 355
    invoke-static {p1}, Lcom/navdy/client/app/framework/Injector;->matchesService(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/navdy/client/app/NavdyApplication;->mObjectGraph:Ldagger/ObjectGraph;

    .line 358
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/multidex/MultiDexApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 76
    invoke-super {p0}, Landroid/support/multidex/MultiDexApplication;->onCreate()V

    .line 77
    sput-object p0, Lcom/navdy/client/app/NavdyApplication;->sAppContext:Landroid/content/Context;

    .line 78
    sget-object v2, Lcom/navdy/client/app/NavdyApplication;->sAppContext:Landroid/content/Context;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {v2, v3}, Lcom/navdy/service/library/util/SystemUtils;->getProcessName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    .line 80
    .local v1, "processName":Ljava/lang/String;
    const v2, 0x7f08054d

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/NavdyApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 111
    :goto_0
    return-void

    .line 102
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/NavdyApplication;->initApp()V

    .line 103
    sget-object v2, Lcom/navdy/client/app/NavdyApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Starting app on "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 106
    :try_start_0
    new-instance v2, Lcom/localytics/android/LocalyticsActivityLifecycleCallbacks;

    invoke-direct {v2, p0}, Lcom/localytics/android/LocalyticsActivityLifecycleCallbacks;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/NavdyApplication;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 108
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/navdy/client/app/NavdyApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to register for LocalyticsActivityLifecycleCallbacks."

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onTrimMemory(I)V
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 115
    const/16 v0, 0x50

    if-ne p1, v0, :cond_0

    .line 116
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->getInstance()Lcom/navdy/client/app/service/DataCollectionService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/service/DataCollectionService;->handleLowMemory()V

    .line 118
    :cond_0
    invoke-super {p0, p1}, Landroid/support/multidex/MultiDexApplication;->onTrimMemory(I)V

    .line 119
    return-void
.end method

.method public registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V
    .locals 4
    .param p1, "callback"    # Landroid/app/Application$ActivityLifecycleCallbacks;

    .prologue
    .line 132
    const-string v0, ""

    .line 133
    .local v0, "className":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 134
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 136
    :cond_0
    sget-object v1, Lcom/navdy/client/app/NavdyApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Activity Callback "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 137
    const-string v1, "com.here.sdk.analytics.internal.a$1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 138
    sget-object v1, Lcom/navdy/client/app/NavdyApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Preventing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " as an activity callback"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 142
    :goto_0
    return-void

    .line 141
    :cond_1
    invoke-super {p0, p1}, Landroid/support/multidex/MultiDexApplication;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    goto :goto_0
.end method
