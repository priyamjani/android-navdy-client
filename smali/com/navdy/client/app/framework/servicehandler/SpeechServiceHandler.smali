.class public Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;
.super Ljava/lang/Object;
.source "SpeechServiceHandler.java"


# static fields
.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field audioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field sharedPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/navdy/client/app/framework/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 35
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;->audioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->setSpeechServiceHadler(Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;)V

    .line 37
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 38
    return-void
.end method

.method private shouldRequestBeProcessed(Lcom/navdy/service/library/events/audio/SpeechRequest;)Z
    .locals 3
    .param p1, "request"    # Lcom/navdy/service/library/events/audio/SpeechRequest;

    .prologue
    const/4 v0, 0x0

    .line 63
    iget-object v1, p1, Lcom/navdy/service/library/events/audio/SpeechRequest;->category:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    sget-object v2, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_WELCOME_MESSAGE:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "audio_play_welcome"

    .line 64
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method


# virtual methods
.method public onCancelSpeechRequest(Lcom/navdy/service/library/events/audio/CancelSpeechRequest;)V
    .locals 4
    .param p1, "request"    # Lcom/navdy/service/library/events/audio/CancelSpeechRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 51
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/CancelSpeechRequest;->id:Ljava/lang/String;

    .line 52
    .local v0, "id":Ljava/lang/String;
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[tts-cancel] id["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 53
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 54
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;->audioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-virtual {v1, v0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->cancelTTSRequest(Ljava/lang/String;)V

    .line 56
    :cond_0
    return-void
.end method

.method public onSpeechRequest(Lcom/navdy/service/library/events/audio/SpeechRequest;)V
    .locals 5
    .param p1, "request"    # Lcom/navdy/service/library/events/audio/SpeechRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 42
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/SpeechRequest;->words:Ljava/lang/String;

    .line 43
    .local v0, "words":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;->shouldRequestBeProcessed(Lcom/navdy/service/library/events/audio/SpeechRequest;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[tts-play] category["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/audio/SpeechRequest;->category:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] id ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/audio/SpeechRequest;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] text["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 45
    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;->audioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    iget-object v3, p1, Lcom/navdy/service/library/events/audio/SpeechRequest;->words:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/audio/SpeechRequest;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/audio/SpeechRequest;->sendStatus:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/navdy/service/library/events/audio/SpeechRequest;->sendStatus:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :goto_0
    invoke-virtual {v2, v3, v4, v1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 47
    :cond_0
    return-void

    .line 45
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public sendSpeechNotification(Lcom/navdy/service/library/events/audio/SpeechRequestStatus;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/service/library/events/audio/SpeechRequestStatus;

    .prologue
    .line 70
    :try_start_0
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "send speech notification id["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] type["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->status:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 71
    invoke-static {p1}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :goto_0
    return-void

    .line 72
    :catch_0
    move-exception v0

    .line 73
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
