.class public Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;
.super Ljava/lang/Object;
.source "PhotoServiceHandler.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private context:Landroid/content/Context;

.field private thumbnailSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->context:Landroid/content/Context;

    .line 46
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0018

    .line 47
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->thumbnailSize:I

    .line 49
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;

    .prologue
    .line 37
    iget v0, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->thumbnailSize:I

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->getContactPhoto(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Landroid/graphics/Bitmap;Lcom/navdy/service/library/events/photo/PhotoRequest;)Lcom/navdy/service/library/events/photo/PhotoResponse;
    .locals 1
    .param p0, "x0"    # Landroid/graphics/Bitmap;
    .param p1, "x1"    # Lcom/navdy/service/library/events/photo/PhotoRequest;

    .prologue
    .line 37
    invoke-static {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->buildPhotoResponse(Landroid/graphics/Bitmap;Lcom/navdy/service/library/events/photo/PhotoRequest;)Lcom/navdy/service/library/events/photo/PhotoResponse;

    move-result-object v0

    return-object v0
.end method

.method public static buildPhotoChecksum(Landroid/graphics/Bitmap;)Ljava/lang/String;
    .locals 6
    .param p0, "photo"    # Landroid/graphics/Bitmap;

    .prologue
    .line 88
    const-string v0, ""

    .line 89
    .local v0, "checksum":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 90
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Trying to buildPhotoChecksum from a null photo."

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    move-object v1, v0

    .line 99
    .end local v0    # "checksum":Ljava/lang/String;
    .local v1, "checksum":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 94
    .end local v1    # "checksum":Ljava/lang/String;
    .restart local v0    # "checksum":Ljava/lang/String;
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/navdy/service/library/util/IOUtils;->bitmap2ByteBuffer(Landroid/graphics/Bitmap;)[B

    move-result-object v2

    .line 95
    .local v2, "data":[B
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->hashForBytes([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .end local v2    # "data":[B
    :goto_1
    move-object v1, v0

    .line 99
    .end local v0    # "checksum":Ljava/lang/String;
    .restart local v1    # "checksum":Ljava/lang/String;
    goto :goto_0

    .line 96
    .end local v1    # "checksum":Ljava/lang/String;
    .restart local v0    # "checksum":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 97
    .local v3, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Error computing checksum for the bitmap data"

    invoke-virtual {v4, v5, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private static buildPhotoResponse(Landroid/graphics/Bitmap;Lcom/navdy/service/library/events/photo/PhotoRequest;)Lcom/navdy/service/library/events/photo/PhotoResponse;
    .locals 18
    .param p0, "photo"    # Landroid/graphics/Bitmap;
    .param p1, "request"    # Lcom/navdy/service/library/events/photo/PhotoRequest;

    .prologue
    .line 103
    if-eqz p0, :cond_2

    .line 104
    const/4 v15, 0x1

    .line 106
    .local v15, "changed":Z
    const/4 v6, 0x0

    .line 107
    .local v6, "newHash":Ljava/lang/String;
    invoke-static/range {p0 .. p0}, Lcom/navdy/service/library/util/IOUtils;->bitmap2ByteBuffer(Landroid/graphics/Bitmap;)[B

    move-result-object v16

    .line 109
    .local v16, "data":[B
    :try_start_0
    invoke-static/range {v16 .. v16}, Lcom/navdy/service/library/util/IOUtils;->hashForBytes([B)Ljava/lang/String;

    move-result-object v6

    .line 110
    if-eqz v6, :cond_0

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoChecksum:Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 111
    const/4 v15, 0x0

    .line 116
    :cond_0
    :goto_0
    invoke-static/range {v16 .. v16}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object v14

    .line 117
    .local v14, "byteStringPhoto":Lokio/ByteString;
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PhotoServiceHandler: changed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " size:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 118
    invoke-virtual {v14}, Lokio/ByteString;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 117
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 119
    new-instance v1, Lcom/navdy/service/library/events/photo/PhotoResponse;

    if-eqz v15, :cond_1

    move-object v2, v14

    :goto_1
    sget-object v3, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    const/4 v4, 0x0

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/photo/PhotoRequest;->identifier:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-direct/range {v1 .. v7}, Lcom/navdy/service/library/events/photo/PhotoResponse;-><init>(Lokio/ByteString;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)V

    .line 123
    .end local v6    # "newHash":Ljava/lang/String;
    .end local v14    # "byteStringPhoto":Lokio/ByteString;
    .end local v15    # "changed":Z
    .end local v16    # "data":[B
    :goto_2
    return-object v1

    .line 113
    .restart local v6    # "newHash":Ljava/lang/String;
    .restart local v15    # "changed":Z
    .restart local v16    # "data":[B
    :catch_0
    move-exception v17

    .line 114
    .local v17, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Error computing hash for the bitmap data"

    move-object/from16 v0, v17

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 119
    .end local v17    # "e":Ljava/lang/Exception;
    .restart local v14    # "byteStringPhoto":Lokio/ByteString;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 123
    .end local v6    # "newHash":Ljava/lang/String;
    .end local v14    # "byteStringPhoto":Lokio/ByteString;
    .end local v15    # "changed":Z
    .end local v16    # "data":[B
    :cond_2
    new-instance v7, Lcom/navdy/service/library/events/photo/PhotoResponse;

    const/4 v8, 0x0

    sget-object v9, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_AVAILABLE:Lcom/navdy/service/library/events/RequestStatus;

    const/4 v10, 0x0

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/navdy/service/library/events/photo/PhotoRequest;->identifier:Ljava/lang/String;

    const/4 v12, 0x0

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-direct/range {v7 .. v13}, Lcom/navdy/service/library/events/photo/PhotoResponse;-><init>(Lokio/ByteString;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)V

    move-object v1, v7

    goto :goto_2
.end method

.method private getContactPhoto(Landroid/content/ContentResolver;Landroid/database/Cursor;I)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "contact"    # Landroid/database/Cursor;
    .param p3, "thumbnailSize"    # I

    .prologue
    .line 199
    const-string v1, "_id"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 200
    .local v2, "userId":J
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    .line 201
    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 202
    .local v0, "photoUri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 203
    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$2;

    invoke-direct {v1, p0, p1, v0}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$2;-><init>(Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;Landroid/content/ContentResolver;Landroid/net/Uri;)V

    invoke-static {v1, p3, p3}, Lcom/navdy/client/app/framework/util/ImageUtils;->getScaledBitmap(Lcom/navdy/client/app/framework/util/ImageUtils$StreamFactory;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 210
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getContactPhoto(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "thumbnailSize"    # I

    .prologue
    .line 134
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 135
    :cond_0
    const/4 v0, 0x0

    .line 149
    :cond_1
    :goto_0
    return-object v0

    .line 138
    :cond_2
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->isNumber(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 140
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->getContactPhotoViaName(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .local v0, "bitmap":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 143
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->getContactPhotoViaPhoneNumber(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 144
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 146
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->getContactPhotoViaName(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private getContactPhotoViaName(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "thumbnailSize"    # I

    .prologue
    const/4 v8, 0x0

    .line 175
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 176
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "lower(display_name) LIKE \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 177
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 178
    .local v3, "selection":Ljava/lang/String;
    const/4 v6, 0x0

    .line 180
    .local v6, "contact":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 185
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 186
    invoke-direct {p0, v0, v6, p2}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->getContactPhoto(Landroid/content/ContentResolver;Landroid/database/Cursor;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 191
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 193
    :goto_0
    return-object v1

    .line 191
    :cond_0
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    :goto_1
    move-object v1, v8

    .line 193
    goto :goto_0

    .line 188
    :catch_0
    move-exception v7

    .line 189
    .local v7, "t":Ljava/lang/Throwable;
    :try_start_1
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 191
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_1

    .end local v7    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v1
.end method

.method private getContactPhotoViaPhoneNumber(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "thumbnailSize"    # I

    .prologue
    const/4 v8, 0x0

    .line 153
    sget-object v2, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    .line 154
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 153
    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 155
    .local v1, "phoneUri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 156
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 158
    .local v6, "contactCursor":Landroid/database/Cursor;
    const/4 v2, 0x1

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 163
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 164
    invoke-direct {p0, v0, v6, p2}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->getContactPhoto(Landroid/content/ContentResolver;Landroid/database/Cursor;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 169
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 171
    :goto_0
    return-object v2

    .line 169
    :cond_0
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    :goto_1
    move-object v2, v8

    .line 171
    goto :goto_0

    .line 166
    :catch_0
    move-exception v7

    .line 167
    .local v7, "t":Ljava/lang/Throwable;
    :try_start_1
    sget-object v2, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_1

    .end local v7    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v2
.end method

.method private isNumber(Ljava/lang/String;)Z
    .locals 4
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 216
    :try_start_0
    const-string v2, "[^a-zA-Z0-9\\._]+"

    const-string v3, ""

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 218
    .local v0, "str":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 219
    const/4 v2, 0x1

    .line 221
    .end local v0    # "str":Ljava/lang/String;
    :goto_0
    return v2

    .line 220
    :catch_0
    move-exception v1

    .line 221
    .local v1, "t":Ljava/lang/Throwable;
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onPhotoRequest(Lcom/navdy/service/library/events/photo/PhotoRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/navdy/service/library/events/photo/PhotoRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 55
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;-><init>(Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;Lcom/navdy/service/library/events/photo/PhotoRequest;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 85
    return-void
.end method
