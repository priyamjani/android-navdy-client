.class public final Lcom/navdy/client/app/framework/Injector;
.super Ljava/lang/Object;
.source "Injector.java"


# static fields
.field private static final INJECTOR_SERVICE:Ljava/lang/String; = "com.navdy.app.client.injector"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 16
    invoke-static {p0}, Lcom/navdy/client/app/framework/Injector;->obtain(Landroid/content/Context;)Ldagger/ObjectGraph;

    move-result-object v0

    .line 17
    .local v0, "objectGraph":Ldagger/ObjectGraph;
    if-eqz v0, :cond_0

    .line 18
    invoke-virtual {v0, p1}, Ldagger/ObjectGraph;->inject(Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    return-void

    .line 20
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Please provide application context"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static matchesService(Ljava/lang/String;)Z
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    const-string v0, "com.navdy.app.client.injector"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static obtain(Landroid/content/Context;)Ldagger/ObjectGraph;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 12
    const-string v0, "com.navdy.app.client.injector"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldagger/ObjectGraph;

    return-object v0
.end method
