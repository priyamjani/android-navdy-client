.class final Lcom/navdy/client/app/framework/util/SuggestionManager$6;
.super Ljava/lang/Object;
.source "SuggestionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/SuggestionManager;->buildMachineLearnedRecommendation(Ljava/util/ArrayList;Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$calendarEventsAndRecommendations:Ljava/util/ArrayList;

.field final synthetic val$callback:Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;


# direct methods
.method constructor <init>(Ljava/util/ArrayList;Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V
    .locals 0

    .prologue
    .line 877
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$6;->val$calendarEventsAndRecommendations:Ljava/util/ArrayList;

    iput-object p2, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$6;->val$callback:Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDestinationSuggestion(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 4
    .param p1, "suggestedDestination"    # Lcom/navdy/client/app/framework/models/Destination;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 881
    if-eqz p1, :cond_0

    iget v1, p1, Lcom/navdy/client/app/framework/models/Destination;->id:I

    if-lez v1, :cond_0

    .line 882
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Machine learning returned a destination recommendation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 885
    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->RECOMMENDATION:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-static {p1, v1}, Lcom/navdy/client/app/framework/util/SuggestionManager;->access$800(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)Lcom/navdy/client/app/framework/models/Suggestion;

    move-result-object v0

    .line 887
    .local v0, "recommendation":Lcom/navdy/client/app/framework/models/Suggestion;
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/navdy/client/app/framework/models/Suggestion;->calculateSuggestedRoute:Z

    .line 890
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$6;->val$calendarEventsAndRecommendations:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/SuggestionManager;->access$600(Lcom/navdy/client/app/framework/models/Suggestion;Ljava/util/ArrayList;)Z

    .line 894
    .end local v0    # "recommendation":Lcom/navdy/client/app/framework/models/Suggestion;
    :goto_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$6;->val$callback:Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;

    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$6;->val$calendarEventsAndRecommendations:Ljava/util/ArrayList;

    invoke-interface {v1, v2}, Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;->onSuggestionBuildComplete(Ljava/util/ArrayList;)V

    .line 895
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->unregister(Ljava/lang/Object;)V

    .line 896
    return-void

    .line 892
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Machine learning did not return any destination recommendation."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method
