.class public Lcom/navdy/client/app/framework/util/TTSAudioRouter;
.super Landroid/content/BroadcastReceiver;
.source "TTSAudioRouter.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/util/TTSAudioRouter$NavdyUtteranceProgressListener;,
        Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;,
        Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSVolumeChanged;,
        Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;,
        Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;,
        Lcom/navdy/client/app/framework/util/TTSAudioRouter$VoiceSearchSetupListener;,
        Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;
    }
.end annotation


# static fields
.field private static final CHECK_TTS_FINISHED_DELAY:I = 0x64

.field public static final EXTRA_VOLUME_STREAM_TYPE:Ljava/lang/String; = "android.media.EXTRA_VOLUME_STREAM_TYPE"

.field public static final EXTRA_VOLUME_STREAM_VALUE:Ljava/lang/String; = "android.media.EXTRA_VOLUME_STREAM_VALUE"

.field public static final GOOGLE_TTS_ENGINE:Ljava/lang/String; = "com.google.android.tts"

.field private static final HFP_CONNECTION_MAX_WAIT_TIME:J = 0x1388L

.field private static final HFP_RETRY_AFTER_FAILED_ATTEMPT:J

.field public static final MEASURED_DELAY_DEFAULT:I = 0x1f4

.field private static final MESSAGE_CHECK_SCO:I = 0x3

.field private static final MESSAGE_NEW_MESSAGE:I = 0x1

.field private static final MESSAGE_SCO_CONNECTION_ATTEMPT_TIMEOUT:I = 0x4

.field private static final MESSAGE_SCO_CONNECTION_FAILURE_RESET:I = 0x5

.field private static final MESSAGE_SCO_STATE_CHANGED:I = 0x0

.field private static final MESSAGE_START_VOICE_SEARCH_SESSION:I = 0x6

.field private static final MESSAGE_STOP_VOICE_SEARCH_SESSION:I = 0x7

.field private static final MESSAGE_TTS_FINISHED:I = 0x2

.field public static final MINIMUM_HFP_DELAY:I = 0x1f4

.field public static final SPEECH_DELAY_LEVELS:I = 0x6

.field public static final SPEECH_DELAY_SEQUENCE_ID_PREFIX:Ljava/lang/String; = "NavdyTTS_SPEECH_DELAY"

.field public static final SPEECH_DELAY_SEQUENCE_PREFIX:Ljava/lang/String; = "SPEECH_DELAY"

.field public static final STREAM_BLUETOOTH_SCO:I = 0x6

.field public static final STREAM_MUSIC:I = 0x3

.field public static final STREAM_SPEAKER:I = 0x4

.field public static final TTS_OVER_SPEAKER_DELAY:I = 0x3e8

.field private static final UTTERANCE_ID:Ljava/lang/String; = "NavdyTTS_"

.field public static final VOLUME_CHANGED_ACTION:Ljava/lang/String; = "android.media.VOLUME_CHANGED_ACTION"

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private a2dpProxy:Landroid/bluetooth/BluetoothA2dp;

.field private audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

.field bus:Lcom/squareup/otto/Bus;

.field private hfpProxy:Landroid/bluetooth/BluetoothHeadset;

.field private isVoiceSearchSessionInitializing:Z

.field private isVoiceSearchSessionStarted:Z

.field private mA2dpTurnedOff:Z

.field private mAudioManager:Landroid/media/AudioManager;

.field private volatile mCurrent:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mLastHfpConnectionAttemptedTime:J

.field mPlayThroughBluetooth:Ljava/util/concurrent/atomic/AtomicBoolean;

.field mPlayThroughHFP:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

.field private volatile mScoConnected:Z

.field mSpeechAvailable:Z

.field private mSpeechIds:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSpeechRequests:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected mTextToSpeech:Landroid/speech/tts/TextToSpeech;

.field private mTtsVolume:F

.field private mTurnOffSCOCalledAfterFailure:Z

.field private mTurnedScoOn:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private volatile mWaitingForScoConnection:Z

.field private measuredTTSSequenceTimings:[I

.field private newMeasuredSequenceTimings:[I

.field private playingSpeechDelaySequence:Z

.field preferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private preferredHFPDelay:I

.field private scoInitialized:Z

.field private speechServiceHandler:Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;

.field private utteranceStartTime:J

.field private voiceSearchSetupListener:Lcom/navdy/client/app/framework/util/TTSAudioRouter$VoiceSearchSetupListener;

.field private volumeLevels:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 162
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 190
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->HFP_RETRY_AFTER_FAILED_ATTEMPT:J

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 250
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->volumeLevels:Ljava/util/HashMap;

    .line 88
    iput-boolean v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isVoiceSearchSessionInitializing:Z

    .line 89
    iput-boolean v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isVoiceSearchSessionStarted:Z

    .line 132
    new-instance v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    .line 136
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->utteranceStartTime:J

    .line 140
    iput-boolean v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->scoInitialized:Z

    .line 164
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mPlayThroughHFP:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 165
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mPlayThroughBluetooth:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 166
    iput-boolean v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechAvailable:Z

    .line 171
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "NavdyTTSThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mHandlerThread:Landroid/os/HandlerThread;

    .line 173
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechRequests:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 174
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechIds:Ljava/util/concurrent/ConcurrentHashMap;

    .line 182
    iput-boolean v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mA2dpTurnedOff:Z

    .line 183
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTurnedScoOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 185
    iput-boolean v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTurnOffSCOCalledAfterFailure:Z

    .line 198
    iput-boolean v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mScoConnected:Z

    .line 252
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->bus:Lcom/squareup/otto/Bus;

    .line 253
    return-void
.end method

.method static synthetic access$202(Lcom/navdy/client/app/framework/util/TTSAudioRouter;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter;
    .param p1, "x1"    # J

    .prologue
    .line 66
    iput-wide p1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->utteranceStartTime:J

    return-wide p1
.end method

.method static synthetic access$300(Lcom/navdy/client/app/framework/util/TTSAudioRouter;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechIds:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/framework/util/TTSAudioRouter;)Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->speechServiceHandler:Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/framework/util/TTSAudioRouter;ZLjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->handleTTSFinished(ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$602(Lcom/navdy/client/app/framework/util/TTSAudioRouter;Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;)Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mCurrent:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;

    return-object p1
.end method

.method static synthetic access$700(Lcom/navdy/client/app/framework/util/TTSAudioRouter;)Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    return-object v0
.end method

.method private getService()Landroid/media/IAudioService;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 1026
    :try_start_0
    const-string v5, "android.os.ServiceManager"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 1027
    .local v2, "serviceManagerClass":Ljava/lang/Class;
    const-string v5, "getService"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v2, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 1028
    .local v1, "getServiceMethod":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "audio"

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;

    .line 1029
    .local v0, "binder":Landroid/os/IBinder;
    invoke-static {v0}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1033
    .end local v0    # "binder":Landroid/os/IBinder;
    .end local v1    # "getServiceMethod":Ljava/lang/reflect/Method;
    .end local v2    # "serviceManagerClass":Ljava/lang/Class;
    :goto_0
    return-object v4

    .line 1030
    :catch_0
    move-exception v3

    .line 1031
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Error getting service "

    invoke-virtual {v5, v6, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private handleScoAudioStateChangedIntent(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 318
    const-string v3, "android.media.extra.SCO_AUDIO_STATE"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 319
    const-string v3, "android.media.extra.SCO_AUDIO_STATE"

    invoke-virtual {p1, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 320
    .local v0, "state":I
    sget-object v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Audio SCO state "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 321
    if-nez v0, :cond_0

    .line 322
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTurnedScoOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 324
    :cond_0
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    if-eqz v3, :cond_1

    .line 325
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    invoke-virtual {v3, v2}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;->sendEmptyMessage(I)Z

    .line 327
    :cond_1
    if-ne v0, v1, :cond_3

    :goto_0
    iput-boolean v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mScoConnected:Z

    .line 329
    .end local v0    # "state":I
    :cond_2
    return-void

    .restart local v0    # "state":I
    :cond_3
    move v1, v2

    .line 327
    goto :goto_0
.end method

.method private handleTTSFinished(ZLjava/lang/String;)V
    .locals 10
    .param p1, "completed"    # Z
    .param p2, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 565
    if-eqz p1, :cond_2

    const-string v4, "onDone"

    .line 566
    .local v4, "method":Ljava/lang/String;
    :goto_0
    const-string v6, "NavdyTTS_SPEECH_DELAY"

    invoke-virtual {p2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 567
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 568
    .local v0, "currentTime":J
    iget-wide v6, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->utteranceStartTime:J

    sub-long v6, v0, v6

    long-to-int v5, v6

    .line 569
    .local v5, "timeTaken":I
    sget-object v6, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Time for Speech delay sequence utterance "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->utteranceStartTime:J

    sub-long v8, v0, v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 570
    const-string v6, "NavdyTTS_SPEECH_DELAY"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 571
    .local v2, "index":I
    iget-object v6, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->newMeasuredSequenceTimings:[I

    aput v5, v6, v2

    .line 572
    const/4 v6, 0x5

    if-ne v2, v6, :cond_0

    .line 573
    sget-object v6, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Finished playing the HFP delay sequence"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 574
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->playingSpeechDelaySequence:Z

    .line 575
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->persistNewMeasuredSequenceTimings()V

    .line 578
    .end local v0    # "currentTime":J
    .end local v2    # "index":I
    .end local v5    # "timeTaken":I
    :cond_0
    iget-object v6, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechIds:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;

    .line 579
    .local v3, "info":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;
    if-eqz v3, :cond_1

    .line 580
    iget v6, v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->subTextCount:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_3

    .line 581
    iget-object v6, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechIds:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, p2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 582
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mCurrent:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;

    .line 583
    iget-boolean v6, v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->sendNotification:Z

    if-eqz v6, :cond_1

    .line 584
    iget-object v6, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->speechServiceHandler:Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;

    new-instance v7, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;

    iget-object v8, v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->originalId:Ljava/lang/String;

    sget-object v9, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;->SPEECH_REQUEST_STOPPED:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    invoke-direct {v7, v8, v9}, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;)V

    invoke-virtual {v6, v7}, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;->sendSpeechNotification(Lcom/navdy/service/library/events/audio/SpeechRequestStatus;)V

    .line 592
    :cond_1
    :goto_1
    iget-object v6, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;->sendEmptyMessage(I)Z

    .line 593
    return-void

    .line 565
    .end local v3    # "info":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;
    .end local v4    # "method":Ljava/lang/String;
    :cond_2
    const-string v4, "onStop"

    goto/16 :goto_0

    .line 588
    .restart local v3    # "info":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;
    .restart local v4    # "method":Ljava/lang/String;
    :cond_3
    iget v6, v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->subTextCount:I

    add-int/lit8 v6, v6, -0x1

    iput v6, v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->subTextCount:I

    .line 589
    sget-object v6, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " called for multi part text , remaining pieces "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->subTextCount:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private handleVolumeChangedIntent(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, -0x1

    const/4 v6, 0x3

    .line 288
    if-eqz p1, :cond_2

    .line 289
    const-string v3, "android.media.EXTRA_VOLUME_STREAM_TYPE"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 290
    .local v1, "streamType":I
    const/4 v3, 0x6

    if-eq v1, v3, :cond_0

    if-eq v1, v6, :cond_0

    if-nez v1, :cond_2

    .line 291
    :cond_0
    const-string v3, "android.media.EXTRA_VOLUME_STREAM_VALUE"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 292
    .local v2, "streamVolume":I
    if-ltz v2, :cond_2

    .line 293
    move v0, v1

    .line 294
    .local v0, "localStreamType":I
    if-ne v1, v6, :cond_4

    .line 295
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isBluetoothA2dpOn()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 296
    sget-object v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "A2dp is on , A2dp Speaker connected "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isA2DPSpeakerConnected()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 297
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->volumeLevels:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    const/4 v0, 0x3

    .line 307
    :goto_0
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iget v3, v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->streamType:I

    if-ne v3, v0, :cond_1

    .line 308
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iput v2, v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamVolume:I

    .line 309
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->publishAudioStatus()V

    .line 311
    :cond_1
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->bus:Lcom/squareup/otto/Bus;

    new-instance v4, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSVolumeChanged;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSVolumeChanged;-><init>(Lcom/navdy/client/app/framework/util/TTSAudioRouter$1;)V

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 315
    .end local v0    # "localStreamType":I
    .end local v1    # "streamType":I
    .end local v2    # "streamVolume":I
    :cond_2
    return-void

    .line 300
    .restart local v0    # "localStreamType":I
    .restart local v1    # "streamType":I
    .restart local v2    # "streamVolume":I
    :cond_3
    sget-object v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "A2dp is off , A2dp Speaker connected "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isA2DPSpeakerConnected()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 301
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->volumeLevels:Ljava/util/HashMap;

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    const/4 v0, 0x4

    goto :goto_0

    .line 305
    :cond_4
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->volumeLevels:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static isA2DPSpeakerConnected()Z
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 979
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isHFPSpeakerConnected()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 975
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidDelayLevel(I)Z
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 1121
    if-ltz p1, :cond_0

    const/4 v0, 0x6

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private persistNewMeasuredSequenceTimings()V
    .locals 6

    .prologue
    const/4 v5, 0x6

    .line 358
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v5, :cond_0

    .line 359
    sget-object v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Old timing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->measuredTTSSequenceTimings:[I

    aget v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , New timing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->newMeasuredSequenceTimings:[I

    aget v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 360
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->measuredTTSSequenceTimings:[I

    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->newMeasuredSequenceTimings:[I

    aget v3, v3, v1

    aput v3, v2, v1

    .line 358
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 362
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 363
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v5, :cond_1

    .line 364
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "audio_hfp_delay_measured"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->newMeasuredSequenceTimings:[I

    aget v3, v3, v1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 363
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 366
    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 367
    return-void
.end method

.method private playSilenceToMeasureVolume()V
    .locals 3

    .prologue
    .line 1131
    const-string v0, " "

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1132
    return-void
.end method

.method private playTTS(ILjava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "stream"    # I
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "id"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 868
    if-nez p3, :cond_0

    .line 869
    const-string p3, "NavdyTTS_"

    .line 872
    :cond_0
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 873
    .local v3, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v7, "streamType"

    .line 874
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 873
    invoke-virtual {v3, v7, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 875
    const-string v7, "utteranceId"

    invoke-virtual {v3, v7, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 876
    const-string v7, "volume"

    iget v9, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTtsVolume:F

    invoke-static {v9}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v7, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 878
    invoke-static {}, Landroid/speech/tts/TextToSpeech;->getMaxSpeechInputLength()I

    move-result v2

    .line 879
    .local v2, "maxInputLength":I
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v7, v2, :cond_2

    .line 880
    iget-object v7, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v7, p2, v8, v3}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 897
    :cond_1
    return-void

    .line 882
    :cond_2
    invoke-static {p2, v2}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->splitLongSpeechText(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v5

    .line 883
    .local v5, "subTexts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    .line 884
    .local v0, "count":I
    iget-object v7, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechIds:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7, p3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;

    .line 885
    .local v6, "ttsInfo":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;
    if-eqz v6, :cond_3

    .line 886
    iput v0, v6, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->subTextCount:I

    .line 888
    :cond_3
    sget-object v7, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "The text is broken into "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " pieces"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 889
    const/4 v1, 0x1

    .line 890
    .local v1, "firstOne":Z
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_4
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 891
    .local v4, "subText":Ljava/lang/String;
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    if-eqz v1, :cond_5

    move v7, v8

    :goto_1
    invoke-virtual {v10, v4, v7, v3}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 892
    if-eqz v1, :cond_4

    .line 893
    const/4 v1, 0x0

    goto :goto_0

    .line 891
    :cond_5
    const/4 v7, 0x1

    goto :goto_1
.end method

.method private routeAudioThroughNonScoChannel(Z)V
    .locals 8
    .param p1, "playThroughBluetooth"    # Z

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x3

    .line 783
    sget-object v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bluetooth SCO On :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v4}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", SCO connected :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mScoConnected:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Did we turn on the SCO :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTurnedScoOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 784
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTurnedScoOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isVoiceSearchSessionStarted:Z

    if-nez v2, :cond_0

    .line 785
    sget-object v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Stopping HFP"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 786
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->setBluetoothScoOn(Z)V

    .line 787
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->stopBluetoothSco()V

    .line 788
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v6, v4, v5}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 790
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v6, v6}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 791
    .local v0, "audioFocusStatus":I
    sget-object v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Audio Focus :"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-ne v0, v7, :cond_2

    const-string v2, "Granted"

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 792
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechRequests:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;

    .line 793
    .local v1, "ttsInfo":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;
    if-eqz v1, :cond_1

    .line 794
    iput-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mCurrent:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;

    .line 795
    if-nez p1, :cond_3

    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isA2DPSpeakerConnected()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 797
    iget-object v2, v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->text:Ljava/lang/String;

    iget-object v3, v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->id:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->routeTTSAudioToSpeaker(Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    :cond_1
    :goto_1
    return-void

    .line 791
    .end local v1    # "ttsInfo":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;
    :cond_2
    const-string v2, "Failed"

    goto :goto_0

    .line 800
    .restart local v1    # "ttsInfo":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;
    :cond_3
    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isA2DPSpeakerConnected()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isBluetoothA2dpOn()Z

    move-result v2

    if-nez v2, :cond_4

    .line 801
    invoke-virtual {p0, v7}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->setBluetoothA2dpOn(Z)V

    .line 803
    :cond_4
    iget-object v2, v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->text:Ljava/lang/String;

    iget-object v3, v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->id:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->routeTTSAudioToDefaultOutput(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private routeTTSAudioToDefaultOutput(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    .line 922
    invoke-direct {p0, v5, p1, p2}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->playTTS(ILjava/lang/String;Ljava/lang/String;)V

    .line 923
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    .line 924
    .local v1, "streamVolume":I
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isBluetoothA2dpOn()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 925
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->volumeLevels:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 926
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iput v5, v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->streamType:I

    .line 931
    :goto_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v5}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    .line 932
    .local v0, "streamMaxVolume":I
    sget-object v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Playing text through default Music stream , Volume ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 933
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iput v0, v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamMaxVolume:I

    .line 934
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->waitingForHfp:Z

    .line 935
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iput v1, v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamVolume:I

    .line 936
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->playingTTS:Z

    .line 937
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isA2DPSpeakerConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->getA2DPDevice()Ljava/lang/String;

    move-result-object v2

    :goto_1
    iput-object v2, v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->outputDeviceName:Ljava/lang/String;

    .line 938
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isA2DPSpeakerConnected()Z

    move-result v3

    iput-boolean v3, v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->throughBluetooth:Z

    .line 939
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->publishAudioStatus()V

    .line 940
    return-void

    .line 928
    .end local v0    # "streamMaxVolume":I
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->volumeLevels:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 929
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iput v6, v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->streamType:I

    goto :goto_0

    .line 937
    .restart local v0    # "streamMaxVolume":I
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x7f080339

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method private routeTTSAudioToHFP(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x6

    .line 851
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    .line 852
    .local v1, "scoVolume":I
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->volumeLevels:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 853
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v5}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    .line 854
    .local v0, "maxVolume":I
    sget-object v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Playing text through HFP , SCO volume ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 855
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iput v0, v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamMaxVolume:I

    .line 856
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iput-boolean v7, v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->waitingForHfp:Z

    .line 857
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iput v1, v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamVolume:I

    .line 858
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iput-boolean v6, v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->playingTTS:Z

    .line 859
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->getHfpDeviceName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->outputDeviceName:Ljava/lang/String;

    .line 860
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iput-boolean v6, v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->throughBluetooth:Z

    .line 861
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iput-boolean v6, v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->throughHFp:Z

    .line 862
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iput v5, v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->streamType:I

    .line 863
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->publishAudioStatus()V

    .line 864
    invoke-direct {p0, v7, p1, p2}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->playTTS(ILjava/lang/String;Ljava/lang/String;)V

    .line 865
    return-void
.end method

.method private routeTTSAudioToSpeaker(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x0

    .line 900
    invoke-virtual {p0, v6}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->setBluetoothA2dpOn(Z)V

    .line 902
    const-wide/16 v4, 0x3e8

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 906
    :goto_0
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3, v7}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v2

    .line 907
    .local v2, "streamVolume":I
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->volumeLevels:Ljava/util/HashMap;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 908
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3, v7}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    .line 909
    .local v1, "streamMaxVolume":I
    sget-object v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Playing text through Speaker , Volume ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 910
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iput v1, v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamMaxVolume:I

    .line 911
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iput v2, v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamVolume:I

    .line 912
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iput-boolean v6, v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->waitingForHfp:Z

    .line 913
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->playingTTS:Z

    .line 914
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f080339

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->outputDeviceName:Ljava/lang/String;

    .line 915
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iput-boolean v6, v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->throughBluetooth:Z

    .line 916
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iput v8, v3, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->streamType:I

    .line 917
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->publishAudioStatus()V

    .line 918
    invoke-direct {p0, v7, p1, p2}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->playTTS(ILjava/lang/String;Ljava/lang/String;)V

    .line 919
    return-void

    .line 903
    .end local v1    # "streamMaxVolume":I
    .end local v2    # "streamVolume":I
    :catch_0
    move-exception v0

    .line 904
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private setPlayThroughBluetooth(Z)V
    .locals 1
    .param p1, "playThroughBluetooth"    # Z

    .prologue
    .line 208
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mPlayThroughBluetooth:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 209
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mWaitingForScoConnection:Z

    .line 210
    return-void
.end method

.method private setPlayThroughHFP(Z)V
    .locals 1
    .param p1, "playThroughHFP"    # Z

    .prologue
    .line 213
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mPlayThroughHFP:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 214
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mWaitingForScoConnection:Z

    .line 215
    return-void
.end method

.method public static splitLongSpeechText(Ljava/lang/String;I)Ljava/util/List;
    .locals 7
    .param p0, "speechText"    # Ljava/lang/String;
    .param p1, "maxInputLength"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1041
    if-eqz p0, :cond_0

    const/4 v5, 0x2

    if-ge p1, v5, :cond_1

    .line 1042
    :cond_0
    const/4 v3, 0x0

    .line 1068
    :goto_0
    return-object v3

    .line 1044
    :cond_1
    const/4 v0, 0x0

    .line 1045
    .local v0, "beginningIndex":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .line 1046
    .local v4, "textLength":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1048
    .local v3, "subTexts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    add-int v5, v0, p1

    add-int/lit8 v5, v5, -0x1

    add-int/lit8 v6, v4, -0x1

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1049
    .local v2, "endIndex":I
    add-int/lit8 v5, v4, -0x1

    if-ge v2, v5, :cond_6

    .line 1050
    :goto_1
    add-int/lit8 v5, v0, 0x1

    if-le v2, v5, :cond_4

    .line 1051
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isLowSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1052
    add-int/lit8 v2, v2, -0x1

    .line 1054
    :cond_3
    invoke-static {p0, v2}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v1

    .line 1055
    .local v1, "codePoint":I
    invoke-static {v1}, Ljava/lang/Character;->isSpaceChar(I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1061
    .end local v1    # "codePoint":I
    :cond_4
    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p0, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1062
    add-int/lit8 v0, v2, 0x1

    .line 1067
    :goto_2
    add-int/lit8 v5, v4, -0x1

    if-lt v0, v5, :cond_2

    goto :goto_0

    .line 1058
    .restart local v1    # "codePoint":I
    :cond_5
    add-int/lit8 v2, v2, -0x1

    .line 1060
    goto :goto_1

    .line 1064
    .end local v1    # "codePoint":I
    :cond_6
    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p0, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1065
    add-int/lit8 v0, v2, 0x1

    goto :goto_2
.end method

.method private startHFPIfNeeded()Z
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 810
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTurnedScoOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mWaitingForScoConnection:Z

    if-nez v1, :cond_0

    .line 811
    sget-object v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Starting HFP"

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 812
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    const/4 v4, 0x1

    iput-boolean v4, v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->waitingForHfp:Z

    .line 813
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    const/4 v4, 0x1

    iput-boolean v4, v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->throughBluetooth:Z

    .line 814
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->getHfpDeviceName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->outputDeviceName:Ljava/lang/String;

    .line 815
    iget-object v4, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->volumeLevels:Ljava/util/HashMap;

    const/4 v5, 0x6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v4, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamVolume:I

    .line 816
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    iget-object v4, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v4

    iput v4, v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamMaxVolume:I

    .line 817
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->publishAudioStatus()V

    .line 818
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mWaitingForScoConnection:Z

    .line 819
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mLastHfpConnectionAttemptedTime:J

    .line 820
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTurnedScoOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 821
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTurnOffSCOCalledAfterFailure:Z

    .line 822
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/media/AudioManager;->setBluetoothScoOn(Z)V

    .line 823
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->startBluetoothSco()V

    .line 824
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    const/4 v4, 0x4

    const-wide/16 v6, 0x1388

    invoke-virtual {v1, v4, v6, v7}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    move v1, v2

    .line 834
    :goto_0
    return v1

    .line 827
    :catch_0
    move-exception v0

    .line 828
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Exception while staring HFP "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 830
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTurnedScoOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 831
    iput-boolean v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTurnOffSCOCalledAfterFailure:Z

    .line 832
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mLastHfpConnectionAttemptedTime:J

    .line 833
    iput-boolean v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mWaitingForScoConnection:Z

    move v1, v3

    .line 834
    goto :goto_0
.end method

.method private waitForScoInitialization()V
    .locals 4

    .prologue
    .line 841
    sget-object v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "SCO connected, waiting for initialization"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 842
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->scoInitialized:Z

    .line 844
    :try_start_0
    iget-boolean v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->playingSpeechDelaySequence:Z

    if-eqz v1, :cond_0

    const-wide/16 v2, 0x1f4

    :goto_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 848
    :goto_1
    return-void

    .line 844
    :cond_0
    iget v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->preferredHFPDelay:I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    int-to-long v2, v1

    goto :goto_0

    .line 845
    :catch_0
    move-exception v0

    .line 846
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "TTS through HFP, Sleep interrupted "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method


# virtual methods
.method public cancelTTSRequest(Ljava/lang/String;)V
    .locals 5
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 439
    sget-object v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cancel TTS request "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechAvailable:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 440
    if-nez p1, :cond_1

    .line 465
    :cond_0
    :goto_0
    return-void

    .line 443
    :cond_1
    iget-boolean v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechAvailable:Z

    if-eqz v2, :cond_0

    .line 446
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NavdyTTS_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 448
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechIds:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;

    .line 450
    .local v0, "info":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;
    if-eqz v0, :cond_3

    .line 452
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechRequests:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    move-result v1

    .line 453
    .local v1, "ret":Z
    if-nez v1, :cond_2

    .line 455
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mCurrent:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;

    if-ne v0, v2, :cond_2

    .line 456
    sget-object v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "stopping current text to speech"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 457
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v2}, Landroid/speech/tts/TextToSpeech;->stop()I

    goto :goto_0

    .line 461
    :cond_2
    sget-object v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "id still exists ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], removed="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 463
    .end local v1    # "ret":Z
    :cond_3
    sget-object v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "id does not exists ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public cancelTtsAndClearQueue()V
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 469
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechIds:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 470
    return-void
.end method

.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 346
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 348
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    .line 349
    iput-boolean v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechAvailable:Z

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechIds:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 352
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechRequests:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 353
    iput-boolean v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mA2dpTurnedOff:Z

    .line 354
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 355
    return-void
.end method

.method public getA2DPDevice()Ljava/lang/String;
    .locals 2

    .prologue
    .line 983
    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isA2DPSpeakerConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->a2dpProxy:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v1, :cond_0

    .line 984
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->a2dpProxy:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothA2dp;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    .line 985
    .local v0, "devices":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 986
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    .line 989
    .end local v0    # "devices":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getDelayInMillisec(I)I
    .locals 4
    .param p1, "level"    # I

    .prologue
    .line 1111
    const/4 v0, 0x0

    .line 1112
    .local v0, "delay":I
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isValidDelayLevel(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1113
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_0

    .line 1114
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->measuredTTSSequenceTimings:[I

    aget v2, v2, v1

    const/16 v3, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1113
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1117
    .end local v1    # "i":I
    :cond_0
    return v0
.end method

.method public getHfpDevice()Landroid/bluetooth/BluetoothDevice;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1001
    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isHFPSpeakerConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->hfpProxy:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v2, :cond_0

    .line 1002
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->hfpProxy:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    move-result-object v1

    .line 1003
    .local v1, "devices":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 1013
    .end local v1    # "devices":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_0
    :goto_0
    return-object v0

    .line 1006
    .restart local v1    # "devices":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 1007
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->hfpProxy:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v3, v0}, Landroid/bluetooth/BluetoothHeadset;->isAudioConnected(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .line 1011
    .end local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_3
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    move-object v0, v2

    goto :goto_0
.end method

.method public getHfpDeviceName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 993
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->getHfpDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 994
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v0, :cond_0

    .line 995
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    .line 997
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getStream()I
    .locals 7

    .prologue
    .line 1148
    iget-object v4, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mPlayThroughBluetooth:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    .line 1149
    .local v2, "playThroughBluetooth":Z
    iget-object v4, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mPlayThroughHFP:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    .line 1150
    .local v3, "playThroughPhoneCall":Z
    iget-object v4, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v4}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    .line 1151
    .local v0, "isMusicPlaying":Z
    sget-object v4, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Play through bluetooth : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", playAsPhoneCall : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", isMusicPlaying :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1153
    if-eqz v2, :cond_2

    .line 1154
    sget-object v4, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Is HFP connected : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isHFPSpeakerConnected()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1155
    sget-object v4, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Is A2dp connected : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isA2DPSpeakerConnected()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1156
    if-nez v0, :cond_0

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isHFPSpeakerConnected()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1157
    const/4 v1, 0x6

    .line 1166
    .local v1, "localStreamType":I
    :goto_0
    sget-object v4, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getStream : stream "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1167
    return v1

    .line 1158
    .end local v1    # "localStreamType":I
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isBluetoothA2dpOn()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1159
    const/4 v1, 0x3

    .restart local v1    # "localStreamType":I
    goto :goto_0

    .line 1161
    .end local v1    # "localStreamType":I
    :cond_1
    const/4 v1, 0x4

    .restart local v1    # "localStreamType":I
    goto :goto_0

    .line 1164
    .end local v1    # "localStreamType":I
    :cond_2
    const/4 v1, 0x4

    .restart local v1    # "localStreamType":I
    goto :goto_0
.end method

.method public getStreamVolume()[I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1140
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->getStream()I

    move-result v0

    .line 1141
    .local v0, "localStreamType":I
    const/4 v2, 0x2

    new-array v1, v2, [I

    .line 1142
    .local v1, "result":[I
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->volumeLevels:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->volumeLevels:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_0
    aput v2, v1, v3

    .line 1143
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    const/4 v4, 0x4

    if-ne v0, v4, :cond_0

    const/4 v0, 0x3

    .end local v0    # "localStreamType":I
    :cond_0
    invoke-virtual {v3, v0}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v3

    aput v3, v1, v2

    .line 1144
    return-object v1

    .restart local v0    # "localStreamType":I
    :cond_1
    move v2, v3

    .line 1142
    goto :goto_0
.end method

.method public getTextToSpeech()Landroid/speech/tts/TextToSpeech;
    .locals 1

    .prologue
    .line 1021
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    return-object v0
.end method

.method public handleMessage(I)V
    .locals 14
    .param p1, "what"    # I

    .prologue
    .line 603
    packed-switch p1, :pswitch_data_0

    .line 780
    :cond_0
    :goto_0
    return-void

    .line 605
    :pswitch_0
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "New message"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 642
    :cond_1
    :goto_1
    const/4 v5, 0x0

    .line 643
    .local v5, "mHFPConnectionFailed":Z
    iget-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mWaitingForScoConnection:Z

    if-eqz v10, :cond_3

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isScoConnected()Z

    move-result v10

    if-nez v10, :cond_3

    .line 644
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 645
    .local v2, "currentTime":J
    iget-wide v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mLastHfpConnectionAttemptedTime:J

    sub-long v10, v2, v10

    const-wide/16 v12, 0x1388

    cmp-long v10, v10, v12

    if-lez v10, :cond_b

    const/4 v5, 0x1

    .line 646
    :goto_2
    if-eqz v5, :cond_3

    .line 647
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "HFP connection has failed, HFP setting will be ignored if set"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 648
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    const/4 v11, 0x4

    invoke-virtual {v10, v11}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;->removeMessages(I)V

    .line 649
    iget-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTurnOffSCOCalledAfterFailure:Z

    if-nez v10, :cond_2

    .line 650
    const-string v10, "TTS_HFP_FAILURE"

    invoke-static {v10}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 651
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Stopping HFP after connection failure"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 652
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTurnOffSCOCalledAfterFailure:Z

    .line 653
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v10}, Landroid/media/AudioManager;->stopBluetoothSco()V

    .line 655
    :cond_2
    iget-wide v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mLastHfpConnectionAttemptedTime:J

    sub-long v10, v2, v10

    sget-wide v12, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->HFP_RETRY_AFTER_FAILED_ATTEMPT:J

    cmp-long v10, v10, v12

    if-lez v10, :cond_3

    .line 656
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Resetting the HFP failure flag as its time to attempt to start the HFP again"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 657
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTurnedScoOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 658
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTurnOffSCOCalledAfterFailure:Z

    .line 659
    const-wide/16 v10, 0x0

    iput-wide v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mLastHfpConnectionAttemptedTime:J

    .line 660
    const/4 v5, 0x0

    .line 661
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mWaitingForScoConnection:Z

    .line 665
    .end local v2    # "currentTime":J
    :cond_3
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mPlayThroughHFP:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v7

    .line 666
    .local v7, "playThroughHFP":Z
    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isHFPSpeakerConnected()Z

    move-result v1

    .line 667
    .local v1, "isHFPSpeakerConnected":Z
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mPlayThroughBluetooth:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v6

    .line 668
    .local v6, "playThroughBluetooth":Z
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v10}, Landroid/media/AudioManager;->isBluetoothScoAvailableOffCall()Z

    move-result v4

    .line 670
    .local v4, "isScoAvailableOffCall":Z
    const/4 v10, 0x6

    if-eq p1, v10, :cond_4

    iget-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isVoiceSearchSessionInitializing:Z

    if-eqz v10, :cond_7

    .line 671
    :cond_4
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Start Voice search session : playThroughHFP :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", isHFPSpeakerConnected :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", isSCOAvailableOffCall:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " , Waiting for SCO :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-boolean v12, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mWaitingForScoConnection:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 672
    if-eqz v6, :cond_5

    if-eqz v1, :cond_5

    if-nez v4, :cond_d

    .line 673
    :cond_5
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->voiceSearchSetupListener:Lcom/navdy/client/app/framework/util/TTSAudioRouter$VoiceSearchSetupListener;

    if-eqz v10, :cond_6

    .line 674
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Starting voice session over phone mic as we do not have to play audio through bluetooth"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 675
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    const/4 v11, 0x0

    const/4 v12, 0x3

    const/4 v13, 0x4

    invoke-virtual {v10, v11, v12, v13}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 676
    .local v0, "audioFocusStatus":I
    sget-object v11, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Audio Focus :"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v10, 0x1

    if-ne v0, v10, :cond_c

    const-string v10, "Granted"

    :goto_3
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 677
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->voiceSearchSetupListener:Lcom/navdy/client/app/framework/util/TTSAudioRouter$VoiceSearchSetupListener;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-interface {v10, v11, v12}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$VoiceSearchSetupListener;->onAudioInputReady(ZZ)V

    .line 679
    .end local v0    # "audioFocusStatus":I
    :cond_6
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isVoiceSearchSessionInitializing:Z

    .line 720
    :cond_7
    :goto_4
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v10}, Landroid/speech/tts/TextToSpeech;->isSpeaking()Z

    move-result v10

    if-nez v10, :cond_1f

    .line 721
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechRequests:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v10}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_1b

    .line 722
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Processing speech request : playThroughHFP :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", isHFPSpeakerConnected :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", isSCOAvailableOffCall:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " , Waiting for SCO :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-boolean v12, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mWaitingForScoConnection:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 723
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Is Music active :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v12}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 724
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Is Voice search session running "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-boolean v12, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isVoiceSearchSessionStarted:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 725
    if-eqz v6, :cond_18

    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v10}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v10

    if-eqz v10, :cond_8

    iget-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->playingSpeechDelaySequence:Z

    if-nez v10, :cond_8

    iget-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isVoiceSearchSessionStarted:Z

    if-eqz v10, :cond_18

    :cond_8
    if-nez v7, :cond_9

    iget-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isVoiceSearchSessionStarted:Z

    if-eqz v10, :cond_18

    :cond_9
    if-eqz v1, :cond_18

    if-eqz v4, :cond_18

    if-nez v5, :cond_18

    .line 726
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isScoConnected()Z

    move-result v10

    if-nez v10, :cond_15

    .line 727
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->startHFPIfNeeded()Z

    move-result v10

    if-nez v10, :cond_0

    .line 728
    const/4 v10, 0x1

    invoke-direct {p0, v10}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->routeAudioThroughNonScoChannel(Z)V

    goto/16 :goto_0

    .line 608
    .end local v1    # "isHFPSpeakerConnected":Z
    .end local v4    # "isScoAvailableOffCall":Z
    .end local v5    # "mHFPConnectionFailed":Z
    .end local v6    # "playThroughBluetooth":Z
    .end local v7    # "playThroughHFP":Z
    :pswitch_1
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SCO state changed Bluetooth SCO on :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v12}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", SCO connected :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-boolean v12, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mScoConnected:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 609
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isScoConnected()Z

    move-result v10

    if-eqz v10, :cond_a

    iget-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->scoInitialized:Z

    if-nez v10, :cond_a

    .line 610
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->waitForScoInitialization()V

    goto/16 :goto_1

    .line 612
    :cond_a
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isScoConnected()Z

    move-result v10

    if-nez v10, :cond_1

    .line 613
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->scoInitialized:Z

    goto/16 :goto_1

    .line 618
    :pswitch_2
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Finished speaking"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 621
    :pswitch_3
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Checking if SCO connection is closed, Connected : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v12}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 622
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    goto/16 :goto_1

    .line 625
    :pswitch_4
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "SCO connection attempt time out"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 628
    :pswitch_5
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "SCO connection failure reset"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 631
    :pswitch_6
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Start voice search session"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 632
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isVoiceSearchSessionStarted:Z

    goto/16 :goto_1

    .line 635
    :pswitch_7
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Stop voice search session"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 636
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isVoiceSearchSessionInitializing:Z

    .line 637
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isVoiceSearchSessionStarted:Z

    goto/16 :goto_1

    .line 645
    .restart local v2    # "currentTime":J
    .restart local v5    # "mHFPConnectionFailed":Z
    :cond_b
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 676
    .end local v2    # "currentTime":J
    .restart local v0    # "audioFocusStatus":I
    .restart local v1    # "isHFPSpeakerConnected":Z
    .restart local v4    # "isScoAvailableOffCall":Z
    .restart local v6    # "playThroughBluetooth":Z
    .restart local v7    # "playThroughHFP":Z
    :cond_c
    const-string v10, "Failed"

    goto/16 :goto_3

    .line 681
    .end local v0    # "audioFocusStatus":I
    :cond_d
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "We can use SCO for voice search session"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 682
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isScoConnected()Z

    move-result v10

    if-eqz v10, :cond_f

    .line 683
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "SCO connected, starting voice search session over bluetooth"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 685
    iget-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->scoInitialized:Z

    if-nez v10, :cond_e

    .line 686
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Waiting for SCO initialization"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 687
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->waitForScoInitialization()V

    .line 689
    :cond_e
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    const/4 v11, 0x4

    invoke-virtual {v10, v11}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;->removeMessages(I)V

    .line 691
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mWaitingForScoConnection:Z

    .line 692
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isVoiceSearchSessionInitializing:Z

    .line 693
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->voiceSearchSetupListener:Lcom/navdy/client/app/framework/util/TTSAudioRouter$VoiceSearchSetupListener;

    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-interface {v10, v11, v12}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$VoiceSearchSetupListener;->onAudioInputReady(ZZ)V

    goto/16 :goto_4

    .line 695
    :cond_f
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "SCO is not connected"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 696
    if-eqz v5, :cond_11

    .line 697
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Use phone mic as HFP connection has failed"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 698
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->voiceSearchSetupListener:Lcom/navdy/client/app/framework/util/TTSAudioRouter$VoiceSearchSetupListener;

    if-eqz v10, :cond_7

    .line 699
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    const/4 v11, 0x0

    const/4 v12, 0x3

    const/4 v13, 0x4

    invoke-virtual {v10, v11, v12, v13}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 700
    .restart local v0    # "audioFocusStatus":I
    sget-object v11, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Audio Focus :"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v10, 0x1

    if-ne v0, v10, :cond_10

    const-string v10, "Granted"

    :goto_5
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 701
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->voiceSearchSetupListener:Lcom/navdy/client/app/framework/util/TTSAudioRouter$VoiceSearchSetupListener;

    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-interface {v10, v11, v12}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$VoiceSearchSetupListener;->onAudioInputReady(ZZ)V

    goto/16 :goto_4

    .line 700
    :cond_10
    const-string v10, "Failed"

    goto :goto_5

    .line 704
    .end local v0    # "audioFocusStatus":I
    :cond_11
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->startHFPIfNeeded()Z

    move-result v10

    if-eqz v10, :cond_12

    .line 705
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isVoiceSearchSessionInitializing:Z

    goto/16 :goto_4

    .line 708
    :cond_12
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->voiceSearchSetupListener:Lcom/navdy/client/app/framework/util/TTSAudioRouter$VoiceSearchSetupListener;

    if-eqz v10, :cond_13

    .line 709
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Starting voice session over phone mic as we do not have to play audio through bluetooth"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 710
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    const/4 v11, 0x0

    const/4 v12, 0x3

    const/4 v13, 0x4

    invoke-virtual {v10, v11, v12, v13}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 711
    .restart local v0    # "audioFocusStatus":I
    sget-object v11, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Audio Focus :"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v10, 0x1

    if-ne v0, v10, :cond_14

    const-string v10, "Granted"

    :goto_6
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 712
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->voiceSearchSetupListener:Lcom/navdy/client/app/framework/util/TTSAudioRouter$VoiceSearchSetupListener;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-interface {v10, v11, v12}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$VoiceSearchSetupListener;->onAudioInputReady(ZZ)V

    .line 714
    .end local v0    # "audioFocusStatus":I
    :cond_13
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isVoiceSearchSessionInitializing:Z

    goto/16 :goto_4

    .line 711
    .restart local v0    # "audioFocusStatus":I
    :cond_14
    const-string v10, "Failed"

    goto :goto_6

    .line 731
    .end local v0    # "audioFocusStatus":I
    :cond_15
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "SCO is connected"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 732
    iget-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->scoInitialized:Z

    if-nez v10, :cond_16

    .line 733
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->waitForScoInitialization()V

    .line 735
    :cond_16
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    const/4 v11, 0x4

    invoke-virtual {v10, v11}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;->removeMessages(I)V

    .line 736
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mWaitingForScoConnection:Z

    .line 737
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x3

    invoke-virtual {v10, v11, v12, v13}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 738
    .restart local v0    # "audioFocusStatus":I
    sget-object v11, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Audio Focus :"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v10, 0x1

    if-ne v0, v10, :cond_17

    const-string v10, "Granted"

    :goto_7
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 739
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechRequests:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v10}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;

    .line 740
    .local v9, "ttsInfo":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;
    if-eqz v9, :cond_0

    .line 741
    iput-object v9, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mCurrent:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;

    .line 742
    iget-object v10, v9, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->text:Ljava/lang/String;

    iget-object v11, v9, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->id:Ljava/lang/String;

    invoke-direct {p0, v10, v11}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->routeTTSAudioToHFP(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 738
    .end local v9    # "ttsInfo":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;
    :cond_17
    const-string v10, "Failed"

    goto :goto_7

    .line 746
    .end local v0    # "audioFocusStatus":I
    :cond_18
    if-eqz v6, :cond_19

    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v10}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v10

    if-nez v10, :cond_19

    if-eqz v7, :cond_19

    if-eqz v1, :cond_19

    if-nez v4, :cond_1a

    .line 747
    :cond_19
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mWaitingForScoConnection:Z

    .line 749
    :cond_1a
    invoke-direct {p0, v6}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->routeAudioThroughNonScoChannel(Z)V

    goto/16 :goto_0

    .line 752
    :cond_1b
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Queue is Empty"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 753
    const/4 v10, 0x2

    if-ne p1, v10, :cond_1c

    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    invoke-virtual {v10}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->reset()Z

    move-result v10

    if-eqz v10, :cond_1c

    .line 754
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->publishAudioStatus()V

    .line 756
    :cond_1c
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Bluetooth SCO On :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v12}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", SCO connected :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-boolean v12, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mScoConnected:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", Did we turn on the SCO :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTurnedScoOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v12}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", Did we detect HFP connection failure :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 757
    if-nez v5, :cond_1d

    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTurnedScoOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v10

    if-eqz v10, :cond_1d

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isScoConnected()Z

    move-result v10

    if-eqz v10, :cond_1d

    iget-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isVoiceSearchSessionStarted:Z

    if-nez v10, :cond_1d

    .line 758
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Stopping HFP"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 759
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v10}, Landroid/media/AudioManager;->stopBluetoothSco()V

    .line 760
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    const/4 v11, 0x3

    const-wide/16 v12, 0x1f4

    invoke-virtual {v10, v11, v12, v13}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 762
    :cond_1d
    iget-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mA2dpTurnedOff:Z

    if-eqz v10, :cond_1e

    .line 763
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Enabling A2dp again"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 764
    const/4 v10, 0x1

    invoke-virtual {p0, v10}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->setBluetoothA2dpOn(Z)V

    .line 766
    :cond_1e
    iget-boolean v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isVoiceSearchSessionStarted:Z

    if-nez v10, :cond_0

    .line 767
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    move-result v8

    .line 768
    .local v8, "result":I
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Audio Focus abandoned : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 774
    .end local v8    # "result":I
    :cond_1f
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Still speaking"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 775
    const/4 v10, 0x2

    if-ne p1, v10, :cond_0

    .line 776
    sget-object v10, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "We received TTS finished message but TTS Engine thinks its still speaking"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 777
    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    const/4 v11, 0x2

    const-wide/16 v12, 0x64

    invoke-virtual {v10, v11, v12, v13}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 603
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public init()V
    .locals 3

    .prologue
    .line 332
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/navdy/client/app/framework/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 333
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    .line 334
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, p0, v2}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 335
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 336
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 339
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.google.android.tts"

    invoke-direct {v0, v1, p0, v2}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    .line 340
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    new-instance v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$NavdyUtteranceProgressListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$NavdyUtteranceProgressListener;-><init>(Lcom/navdy/client/app/framework/util/TTSAudioRouter;Lcom/navdy/client/app/framework/util/TTSAudioRouter$1;)V

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceProgressListener(Landroid/speech/tts/UtteranceProgressListener;)I

    .line 341
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 342
    new-instance v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;-><init>(Lcom/navdy/client/app/framework/util/TTSAudioRouter;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    .line 343
    return-void
.end method

.method public isBluetoothA2dpOn()Z
    .locals 4

    .prologue
    .line 957
    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isA2DPSpeakerConnected()Z

    move-result v2

    if-nez v2, :cond_0

    .line 958
    const/4 v2, 0x0

    .line 970
    :goto_0
    return v2

    .line 960
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->getService()Landroid/media/IAudioService;

    move-result-object v1

    .line 961
    .local v1, "service":Landroid/media/IAudioService;
    if-eqz v1, :cond_1

    .line 963
    :try_start_0
    invoke-interface {v1}, Landroid/media/IAudioService;->isBluetoothA2dpOn()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 964
    :catch_0
    move-exception v0

    .line 965
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Exception while getting a2dp"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 970
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isA2DPSpeakerConnected()Z

    move-result v2

    goto :goto_0

    .line 968
    :cond_1
    sget-object v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to get instance of audio service"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public isScoConnected()Z
    .locals 1

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mScoConnected:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSpeaking()Z
    .locals 1

    .prologue
    .line 596
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->isSpeaking()Z

    move-result v0

    return v0
.end method

.method public isTTSAvailable()Z
    .locals 1

    .prologue
    .line 473
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechAvailable:Z

    return v0
.end method

.method public onHUDConnected(Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1187
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->stopVoiceSearchSession()V

    .line 1188
    return-void
.end method

.method public onHUDDisconnected(Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1182
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->stopVoiceSearchSession()V

    .line 1183
    return-void
.end method

.method public onInit(I)V
    .locals 7
    .param p1, "status"    # I

    .prologue
    const/4 v4, 0x6

    const/4 v6, 0x3

    .line 479
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v2

    .line 480
    .local v2, "scoVolume":I
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->volumeLevels:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 481
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3, v6}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    .line 482
    .local v1, "musicVolume":I
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->volumeLevels:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3, v6}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    .line 484
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->volumeLevels:Ljava/util/HashMap;

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->resetToUserPreference()V

    .line 487
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechAvailable:Z

    .line 488
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 489
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v3, "android.media.ACTION_SCO_AUDIO_STATE_UPDATED"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 490
    const-string v3, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 491
    const-string v3, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 492
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 493
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x2

    .line 257
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 258
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 259
    .local v3, "extras":Landroid/os/Bundle;
    const/4 v6, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v6, :pswitch_data_0

    .line 285
    :cond_1
    :goto_1
    return-void

    .line 259
    :sswitch_0
    const-string v8, "android.media.ACTION_SCO_AUDIO_STATE_UPDATED"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    const/4 v6, 0x0

    goto :goto_0

    :sswitch_1
    const-string v8, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    :sswitch_2
    const-string v8, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    move v6, v7

    goto :goto_0

    .line 261
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->handleScoAudioStateChangedIntent(Landroid/content/Intent;)V

    goto :goto_1

    .line 264
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->handleVolumeChangedIntent(Landroid/content/Intent;)V

    goto :goto_1

    .line 267
    :pswitch_2
    sget-object v6, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 268
    if-eqz v3, :cond_1

    .line 269
    const-string v6, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    invoke-virtual {v3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 270
    .local v4, "previousState":I
    const-string v6, "android.bluetooth.profile.extra.STATE"

    invoke-virtual {v3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 271
    .local v1, "currentState":I
    const-string v6, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {v3, v6}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    .line 272
    .local v2, "device":Landroid/bluetooth/BluetoothDevice;
    sget-object v8, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "BluetoothHeadset State changed Previous state :"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " , Current State : "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " , Device : "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    :goto_2
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 273
    if-eq v4, v7, :cond_2

    const/4 v6, 0x3

    if-ne v4, v6, :cond_1

    :cond_2
    if-nez v1, :cond_1

    .line 274
    sget-object v6, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "HFP device disconnected"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 275
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v5

    .line 276
    .local v5, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v5, :cond_1

    .line 277
    new-instance v6, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;

    invoke-direct {v6}, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;-><init>()V

    sget-object v7, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;->ACCELERATE_REASON_HFP_DISCONNECT:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;->reason(Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;)Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;->build()Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    goto/16 :goto_1

    .line 272
    .end local v5    # "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    :cond_3
    const-string v6, "NONE"

    goto :goto_2

    .line 259
    nop

    :sswitch_data_0
    .sparse-switch
        -0x73abbf83 -> :sswitch_1
        -0x64dbd1dc -> :sswitch_0
        0x2083ec2d -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 0
    .param p1, "profile"    # I
    .param p2, "bluetoothProfile"    # Landroid/bluetooth/BluetoothProfile;

    .prologue
    .line 1084
    packed-switch p1, :pswitch_data_0

    .line 1094
    .end local p2    # "bluetoothProfile":Landroid/bluetooth/BluetoothProfile;
    :goto_0
    return-void

    .line 1086
    .restart local p2    # "bluetoothProfile":Landroid/bluetooth/BluetoothProfile;
    :pswitch_0
    check-cast p2, Landroid/bluetooth/BluetoothA2dp;

    .end local p2    # "bluetoothProfile":Landroid/bluetooth/BluetoothProfile;
    iput-object p2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->a2dpProxy:Landroid/bluetooth/BluetoothA2dp;

    goto :goto_0

    .line 1089
    .restart local p2    # "bluetoothProfile":Landroid/bluetooth/BluetoothProfile;
    :pswitch_1
    check-cast p2, Landroid/bluetooth/BluetoothHeadset;

    .end local p2    # "bluetoothProfile":Landroid/bluetooth/BluetoothProfile;
    iput-object p2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->hfpProxy:Landroid/bluetooth/BluetoothHeadset;

    goto :goto_0

    .line 1084
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onServiceDisconnected(I)V
    .locals 1
    .param p1, "profile"    # I

    .prologue
    const/4 v0, 0x0

    .line 1098
    packed-switch p1, :pswitch_data_0

    .line 1108
    :goto_0
    return-void

    .line 1100
    :pswitch_0
    iput-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->a2dpProxy:Landroid/bluetooth/BluetoothA2dp;

    goto :goto_0

    .line 1103
    :pswitch_1
    iput-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->hfpProxy:Landroid/bluetooth/BluetoothHeadset;

    goto :goto_0

    .line 1098
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized playSpeechDelaySequence()V
    .locals 5

    .prologue
    const/4 v4, 0x6

    .line 1072
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->playingSpeechDelaySequence:Z

    if-nez v1, :cond_0

    .line 1073
    const/4 v1, 0x6

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->newMeasuredSequenceTimings:[I

    .line 1074
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->playingSpeechDelaySequence:Z

    .line 1075
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->cancelTtsAndClearQueue()V

    .line 1076
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 1077
    add-int/lit8 v1, v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SPEECH_DELAY"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1076
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1080
    .end local v0    # "i":I
    :cond_0
    monitor-exit p0

    return-void

    .line 1072
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "sendNotification"    # Z

    .prologue
    .line 412
    sget-object v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "New TTS request "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechAvailable:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 413
    iget-boolean v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechAvailable:Z

    if-nez v2, :cond_1

    .line 436
    :cond_0
    :goto_0
    return-void

    .line 417
    :cond_1
    iget-boolean v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->playingSpeechDelaySequence:Z

    if-eqz v2, :cond_2

    invoke-static {p2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SPEECH_DELAY"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 421
    :cond_2
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;->removeMessages(I)V

    .line 422
    move-object v1, p2

    .line 423
    .local v1, "originalId":Ljava/lang/String;
    invoke-static {p2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 424
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NavdyTTS_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 427
    :cond_3
    new-instance v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;

    invoke-direct {v0, p1, p2, v1, p3}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 429
    .local v0, "info":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;
    invoke-static {p2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 430
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechIds:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    sget-object v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "added id ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 434
    :cond_4
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mSpeechRequests:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 435
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public produceAudioStatus()Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;
    .locals 3
    .annotation runtime Lcom/squareup/otto/Produce;
    .end annotation

    .prologue
    .line 1173
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->copy()Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    move-result-object v0

    .line 1174
    .local v0, "audioStatusCopy":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->getStreamVolume()[I

    move-result-object v1

    .line 1175
    .local v1, "maxStreamVolume":[I
    const/4 v2, 0x0

    aget v2, v1, v2

    iput v2, v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamVolume:I

    .line 1176
    const/4 v2, 0x1

    aget v2, v1, v2

    iput v2, v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamMaxVolume:I

    .line 1177
    return-object v0
.end method

.method public publishAudioStatus()V
    .locals 2

    .prologue
    .line 1191
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->audioStatus:Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->copy()Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    move-result-object v0

    .line 1192
    .local v0, "audioStatusCopy":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1193
    return-void
.end method

.method public resetToUserPreference()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    const/4 v13, 0x6

    .line 370
    iget-object v9, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->preferences:Landroid/content/SharedPreferences;

    const-string v10, "audio_output_preference"

    sget v11, Lcom/navdy/client/app/ui/settings/SettingsConstants;->AUDIO_OUTPUT_PREFERENCE_DEFAULT:I

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 371
    .local v0, "audioOutPreference":I
    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->values()[Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    move-result-object v9

    aget-object v1, v9, v0

    .line 372
    .local v1, "audioOutput":Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;
    const/4 v9, 0x0

    invoke-virtual {p0, v1, v9}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->setAudioOutput(Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;Z)V

    .line 375
    new-array v9, v13, [I

    iput-object v9, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->measuredTTSSequenceTimings:[I

    .line 376
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v13, :cond_0

    .line 377
    iget-object v9, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->measuredTTSSequenceTimings:[I

    iget-object v10, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->preferences:Landroid/content/SharedPreferences;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "audio_hfp_delay_measured"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0x1f4

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v10

    aput v10, v9, v3

    .line 376
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 379
    :cond_0
    iget-object v9, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->preferences:Landroid/content/SharedPreferences;

    const-string v10, "audio_hfp_delay_level"

    invoke-interface {v9, v10, v14}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 380
    .local v4, "preferredHFPDelayLevel":I
    if-ltz v4, :cond_1

    if-le v4, v13, :cond_2

    .line 381
    :cond_1
    iget-object v9, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    const-string v10, "audio_hfp_delay_level"

    invoke-interface {v9, v10, v14}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    invoke-interface {v9}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 383
    :cond_2
    invoke-virtual {p0, v4}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->setPreferredHFPDelayLevel(I)V

    .line 384
    iget-object v9, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->preferences:Landroid/content/SharedPreferences;

    const-string v10, "audio_tts_volume"

    const v11, 0x3f19999a    # 0.6f

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v9

    iput v9, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTtsVolume:F

    .line 385
    iget-object v9, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->preferences:Landroid/content/SharedPreferences;

    const-string v10, "audio_voice"

    const/4 v11, 0x0

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 386
    .local v5, "savedVoice":Ljava/lang/String;
    sget-object v9, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Saved Voice : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 387
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x15

    if-lt v9, v10, :cond_5

    .line 390
    :try_start_0
    iget-object v9, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v9}, Landroid/speech/tts/TextToSpeech;->getVoice()Landroid/speech/tts/Voice;

    move-result-object v6

    .line 391
    .local v6, "selectedVoice":Landroid/speech/tts/Voice;
    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 392
    iget-object v9, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v9}, Landroid/speech/tts/TextToSpeech;->getVoices()Ljava/util/Set;

    move-result-object v8

    .line 393
    .local v8, "voices":Ljava/util/Set;, "Ljava/util/Set<Landroid/speech/tts/Voice;>;"
    if-eqz v8, :cond_6

    .line 394
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/speech/tts/Voice;

    .line 395
    .local v7, "temp":Landroid/speech/tts/Voice;
    invoke-virtual {v7}, Landroid/speech/tts/Voice;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 396
    move-object v6, v7

    .line 404
    .end local v7    # "temp":Landroid/speech/tts/Voice;
    .end local v8    # "voices":Ljava/util/Set;, "Ljava/util/Set<Landroid/speech/tts/Voice;>;"
    :cond_4
    :goto_1
    iget-object v9, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v9, v6}, Landroid/speech/tts/TextToSpeech;->setVoice(Landroid/speech/tts/Voice;)I

    .line 409
    .end local v6    # "selectedVoice":Landroid/speech/tts/Voice;
    :cond_5
    :goto_2
    return-void

    .line 401
    .restart local v6    # "selectedVoice":Landroid/speech/tts/Voice;
    .restart local v8    # "voices":Ljava/util/Set;, "Ljava/util/Set<Landroid/speech/tts/Voice;>;"
    :cond_6
    sget-object v9, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "Voices not available"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 405
    .end local v6    # "selectedVoice":Landroid/speech/tts/Voice;
    .end local v8    # "voices":Ljava/util/Set;, "Ljava/util/Set<Landroid/speech/tts/Voice;>;"
    :catch_0
    move-exception v2

    .line 406
    .local v2, "e":Ljava/lang/Exception;
    sget-object v9, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Text To Speech exception found on: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", error: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public setAudioOutput(Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;Z)V
    .locals 6
    .param p1, "audioOutput"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;
    .param p2, "measureVolume"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 218
    sget-object v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Set Audio output "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 219
    if-eqz p1, :cond_1

    .line 220
    sget-object v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$1;->$SwitchMap$com$navdy$client$app$framework$util$TTSAudioRouter$AudioOutput:[I

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 234
    :goto_0
    if-eqz p2, :cond_1

    .line 235
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->getStream()I

    move-result v0

    .line 236
    .local v0, "stream":I
    sget-object v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Stream selected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 237
    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isBluetoothA2dpOn()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 238
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->playSilenceToMeasureVolume()V

    .line 242
    .end local v0    # "stream":I
    :cond_1
    return-void

    .line 222
    :pswitch_0
    invoke-direct {p0, v5}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->setPlayThroughBluetooth(Z)V

    .line 223
    invoke-direct {p0, v5}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->setPlayThroughHFP(Z)V

    goto :goto_0

    .line 226
    :pswitch_1
    invoke-direct {p0, v5}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->setPlayThroughBluetooth(Z)V

    .line 227
    invoke-direct {p0, v4}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->setPlayThroughHFP(Z)V

    goto :goto_0

    .line 230
    :pswitch_2
    invoke-direct {p0, v4}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->setPlayThroughBluetooth(Z)V

    .line 231
    invoke-direct {p0, v4}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->setPlayThroughHFP(Z)V

    goto :goto_0

    .line 220
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setBluetoothA2dpOn(Z)V
    .locals 4
    .param p1, "on"    # Z

    .prologue
    .line 943
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->getService()Landroid/media/IAudioService;

    move-result-object v1

    .line 944
    .local v1, "service":Landroid/media/IAudioService;
    if-eqz v1, :cond_1

    .line 946
    :try_start_0
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->setBluetoothA2dpOn(Z)V

    .line 947
    if-nez p1, :cond_0

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mA2dpTurnedOff:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 954
    :goto_1
    return-void

    .line 947
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 948
    :catch_0
    move-exception v0

    .line 949
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Exception while turning off a2dp"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 952
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    sget-object v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to get instance of audio service"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setPreferredHFPDelayLevel(I)V
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 1125
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isValidDelayLevel(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1126
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->getDelayInMillisec(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->preferredHFPDelay:I

    .line 1128
    :cond_0
    return-void
.end method

.method public setSpeechServiceHadler(Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;)V
    .locals 0
    .param p1, "speechServiceHandler"    # Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;

    .prologue
    .line 1037
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->speechServiceHandler:Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;

    .line 1038
    return-void
.end method

.method public setTtsVolume(F)V
    .locals 0
    .param p1, "ttsVolume"    # F

    .prologue
    .line 204
    iput p1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mTtsVolume:F

    .line 205
    return-void
.end method

.method public setVoiceSearchSetupListener(Lcom/navdy/client/app/framework/util/TTSAudioRouter$VoiceSearchSetupListener;)V
    .locals 0
    .param p1, "voiceSearchSetupListener"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter$VoiceSearchSetupListener;

    .prologue
    .line 1208
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->voiceSearchSetupListener:Lcom/navdy/client/app/framework/util/TTSAudioRouter$VoiceSearchSetupListener;

    .line 1209
    return-void
.end method

.method public startVoiceSearchSession()V
    .locals 2

    .prologue
    .line 1196
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    if-eqz v0, :cond_0

    .line 1197
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;->sendEmptyMessage(I)Z

    .line 1199
    :cond_0
    return-void
.end method

.method public stopVoiceSearchSession()V
    .locals 2

    .prologue
    .line 1202
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    if-eqz v0, :cond_0

    .line 1203
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->mRequestHandler:Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;->sendEmptyMessage(I)Z

    .line 1205
    :cond_0
    return-void
.end method
