.class public Lcom/localytics/android/GeofenceTransitionsService;
.super Landroid/app/IntentService;
.source "GeofenceTransitionsService.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    const-string v0, "GeofenceTransitionsService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method private eventForTransition(I)Lcom/localytics/android/Region$Event;
    .locals 1
    .param p1, "transition"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 68
    packed-switch p1, :pswitch_data_0

    .line 75
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 71
    :pswitch_0
    sget-object v0, Lcom/localytics/android/Region$Event;->ENTER:Lcom/localytics/android/Region$Event;

    goto :goto_0

    .line 73
    :pswitch_1
    sget-object v0, Lcom/localytics/android/Region$Event;->EXIT:Lcom/localytics/android/Region$Event;

    goto :goto_0

    .line 68
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private logError(I)V
    .locals 1
    .param p1, "errorCode"    # I

    .prologue
    .line 81
    const-string v0, "Geofence: Unknown error"

    .line 82
    .local v0, "message":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 95
    :goto_0
    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 96
    return-void

    .line 85
    :pswitch_0
    const-string v0, "Geofence: Geofence not available"

    .line 86
    goto :goto_0

    .line 88
    :pswitch_1
    const-string v0, "Geofence: Too many geofences"

    .line 89
    goto :goto_0

    .line 91
    :pswitch_2
    const-string v0, "Geofence: Too many pending intents"

    goto :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 24
    if-eqz p1, :cond_0

    .line 26
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/location/GeofencingEvent;->fromIntent(Landroid/content/Intent;)Lcom/google/android/gms/location/GeofencingEvent;

    move-result-object v1

    .line 27
    .local v1, "event":Lcom/google/android/gms/location/GeofencingEvent;
    invoke-virtual {v1}, Lcom/google/android/gms/location/GeofencingEvent;->hasError()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 29
    invoke-virtual {v1}, Lcom/google/android/gms/location/GeofencingEvent;->getErrorCode()I

    move-result v8

    invoke-direct {p0, v8}, Lcom/localytics/android/GeofenceTransitionsService;->logError(I)V

    .line 63
    .end local v1    # "event":Lcom/google/android/gms/location/GeofencingEvent;
    :cond_0
    :goto_0
    return-void

    .line 33
    .restart local v1    # "event":Lcom/google/android/gms/location/GeofencingEvent;
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/location/GeofencingEvent;->getGeofenceTransition()I

    move-result v5

    .line 34
    .local v5, "transition":I
    const/4 v8, 0x1

    if-eq v5, v8, :cond_2

    const/4 v8, 0x2

    if-ne v5, v8, :cond_4

    .line 37
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/gms/location/GeofencingEvent;->getTriggeringGeofences()Ljava/util/List;

    move-result-object v7

    .line 38
    .local v7, "triggeringGeofences":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/location/Geofence;>;"
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 39
    .local v4, "regions":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/Region;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/location/Geofence;

    .line 41
    .local v2, "geofence":Lcom/google/android/gms/location/Geofence;
    new-instance v8, Lcom/localytics/android/CircularRegion$Builder;

    invoke-direct {v8}, Lcom/localytics/android/CircularRegion$Builder;-><init>()V

    invoke-interface {v2}, Lcom/google/android/gms/location/Geofence;->getRequestId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/localytics/android/CircularRegion$Builder;->setUniqueId(Ljava/lang/String;)Lcom/localytics/android/CircularRegion$Builder;

    move-result-object v8

    invoke-virtual {v8}, Lcom/localytics/android/CircularRegion$Builder;->build()Lcom/localytics/android/CircularRegion;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 59
    .end local v1    # "event":Lcom/google/android/gms/location/GeofencingEvent;
    .end local v2    # "geofence":Lcom/google/android/gms/location/Geofence;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "regions":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/Region;>;"
    .end local v5    # "transition":I
    .end local v7    # "triggeringGeofences":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/location/Geofence;>;"
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/lang/Exception;
    const-string v8, "Something went wrong with a geofence transition"

    invoke-static {v8, v0}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 47
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "event":Lcom/google/android/gms/location/GeofencingEvent;
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "regions":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/Region;>;"
    .restart local v5    # "transition":I
    .restart local v7    # "triggeringGeofences":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/location/Geofence;>;"
    :cond_3
    :try_start_1
    invoke-direct {p0, v5}, Lcom/localytics/android/GeofenceTransitionsService;->eventForTransition(I)Lcom/localytics/android/Region$Event;

    move-result-object v6

    .line 48
    .local v6, "transitionEvent":Lcom/localytics/android/Region$Event;
    if-eqz v6, :cond_0

    .line 50
    invoke-static {v4, v6}, Lcom/localytics/android/Localytics;->triggerRegions(Ljava/util/List;Lcom/localytics/android/Region$Event;)V

    goto :goto_0

    .line 55
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "regions":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/Region;>;"
    .end local v6    # "transitionEvent":Lcom/localytics/android/Region$Event;
    .end local v7    # "triggeringGeofences":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/location/Geofence;>;"
    :cond_4
    const-string v8, "Geofence transition ignored - not enter or exit"

    invoke-static {v8}, Lcom/localytics/android/Localytics$Log;->d(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
