.class Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;
.super Ljava/lang/Object;
.source "OkHttpPendingResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/maps/internal/OkHttpPendingResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "QueuedResponse"
.end annotation


# instance fields
.field private final e:Ljava/lang/Exception;

.field private final request:Lcom/google/maps/internal/OkHttpPendingResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/maps/internal/OkHttpPendingResult",
            "<TT;TR;>;"
        }
    .end annotation
.end field

.field private final response:Lcom/squareup/okhttp/Response;

.field final synthetic this$0:Lcom/google/maps/internal/OkHttpPendingResult;


# direct methods
.method public constructor <init>(Lcom/google/maps/internal/OkHttpPendingResult;Lcom/google/maps/internal/OkHttpPendingResult;Lcom/squareup/okhttp/Response;)V
    .locals 1
    .param p3, "response"    # Lcom/squareup/okhttp/Response;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/internal/OkHttpPendingResult",
            "<TT;TR;>;",
            "Lcom/squareup/okhttp/Response;",
            ")V"
        }
    .end annotation

    .prologue
    .line 99
    .local p0, "this":Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>.QueuedResponse;"
    .local p2, "request":Lcom/google/maps/internal/OkHttpPendingResult;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>;"
    iput-object p1, p0, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;->this$0:Lcom/google/maps/internal/OkHttpPendingResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    iput-object p2, p0, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;->request:Lcom/google/maps/internal/OkHttpPendingResult;

    .line 101
    iput-object p3, p0, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;->response:Lcom/squareup/okhttp/Response;

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;->e:Ljava/lang/Exception;

    .line 103
    return-void
.end method

.method public constructor <init>(Lcom/google/maps/internal/OkHttpPendingResult;Lcom/google/maps/internal/OkHttpPendingResult;Ljava/lang/Exception;)V
    .locals 1
    .param p3, "e"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/internal/OkHttpPendingResult",
            "<TT;TR;>;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 104
    .local p0, "this":Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>.QueuedResponse;"
    .local p2, "request":Lcom/google/maps/internal/OkHttpPendingResult;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>;"
    iput-object p1, p0, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;->this$0:Lcom/google/maps/internal/OkHttpPendingResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput-object p2, p0, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;->request:Lcom/google/maps/internal/OkHttpPendingResult;

    .line 106
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;->response:Lcom/squareup/okhttp/Response;

    .line 107
    iput-object p3, p0, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;->e:Ljava/lang/Exception;

    .line 108
    return-void
.end method

.method static synthetic access$000(Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;)Lcom/squareup/okhttp/Response;
    .locals 1
    .param p0, "x0"    # Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;->response:Lcom/squareup/okhttp/Response;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;)Lcom/google/maps/internal/OkHttpPendingResult;
    .locals 1
    .param p0, "x0"    # Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;->request:Lcom/google/maps/internal/OkHttpPendingResult;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;)Ljava/lang/Exception;
    .locals 1
    .param p0, "x0"    # Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;->e:Ljava/lang/Exception;

    return-object v0
.end method
