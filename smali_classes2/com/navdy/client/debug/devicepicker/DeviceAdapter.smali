.class public Lcom/navdy/client/debug/devicepicker/DeviceAdapter;
.super Landroid/widget/ArrayAdapter;
.source "DeviceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/navdy/client/debug/devicepicker/Device;",
        ">;"
    }
.end annotation


# static fields
.field static button_ids:[I

.field static types:[Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->values()[Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/debug/devicepicker/DeviceAdapter;->types:[Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/debug/devicepicker/Device;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p2, "objects":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/debug/devicepicker/Device;>;"
    const v0, 0x7f0300b4

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 46
    return-void
.end method

.method static lookupIds()V
    .locals 10

    .prologue
    .line 28
    const-class v0, Lcom/navdy/client/R$id;

    .line 29
    .local v0, "clazz":Ljava/lang/Class;
    sget-object v4, Lcom/navdy/client/debug/devicepicker/DeviceAdapter;->types:[Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    array-length v4, v4

    new-array v4, v4, [I

    sput-object v4, Lcom/navdy/client/debug/devicepicker/DeviceAdapter;->button_ids:[I

    .line 30
    sget-object v5, Lcom/navdy/client/debug/devicepicker/DeviceAdapter;->types:[Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    array-length v6, v5

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v6, :cond_0

    aget-object v3, v5, v4

    .line 31
    .local v3, "type":Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;
    const/4 v2, 0x0

    .line 33
    .local v2, "f":Ljava/lang/reflect/Field;
    :try_start_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "connection_type_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->name()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v8, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 34
    sget-object v7, Lcom/navdy/client/debug/devicepicker/DeviceAdapter;->button_ids:[I

    invoke-virtual {v3}, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->getPriority()I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v9

    aput v9, v7, v8
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 30
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 35
    :catch_0
    move-exception v1

    .line 36
    .local v1, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v1}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_1

    .line 37
    .end local v1    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v1

    .line 38
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 41
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    .end local v2    # "f":Ljava/lang/reflect/Field;
    .end local v3    # "type":Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;
    :cond_0
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "pos"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v3, 0x0

    .line 51
    if-nez p2, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/navdy/client/debug/devicepicker/DeviceAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0300b4

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 54
    new-instance v0, Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;

    invoke-direct {v0, p2, v3}, Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;-><init>(Landroid/view/View;Lcom/navdy/client/debug/devicepicker/DeviceAdapter$1;)V

    .line 55
    .local v0, "holder":Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 59
    :goto_0
    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/devicepicker/DeviceAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/debug/devicepicker/Device;

    invoke-static {v0, v1}, Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;->access$100(Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;Lcom/navdy/client/debug/devicepicker/Device;)V

    .line 60
    return-object p2

    .line 57
    .end local v0    # "holder":Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;
    goto :goto_0
.end method
