.class public Lcom/navdy/client/debug/navigation/HUDNavigationManager;
.super Lcom/navdy/client/debug/navigation/NavigationManager;
.source "HUDNavigationManager.java"


# static fields
.field private static final VERBOSE:Z

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/debug/navigation/HUDNavigationManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/navigation/HUDNavigationManager;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/navdy/client/debug/navigation/NavigationManager;-><init>()V

    return-void
.end method


# virtual methods
.method public startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;)Z
    .locals 8
    .param p1, "coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p2, "label"    # Ljava/lang/String;
    .param p3, "streetAddress"    # Ljava/lang/String;
    .param p4, "destinationId"    # Ljava/lang/String;
    .param p5, "destinationType"    # Lcom/navdy/service/library/events/destination/Destination$FavoriteType;
    .param p6, "display"    # Lcom/navdy/service/library/events/location/Coordinate;

    .prologue
    .line 27
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/navdy/client/debug/navigation/HUDNavigationManager;->startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;)Z

    move-result v0

    return v0
.end method

.method public startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;)Z
    .locals 9
    .param p1, "coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p2, "label"    # Ljava/lang/String;
    .param p4, "streetAddress"    # Ljava/lang/String;
    .param p5, "destinationId"    # Ljava/lang/String;
    .param p6, "destinationType"    # Lcom/navdy/service/library/events/destination/Destination$FavoriteType;
    .param p7, "display"    # Lcom/navdy/service/library/events/location/Coordinate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/destination/Destination$FavoriteType;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 36
    .local p3, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/location/Coordinate;>;"
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-virtual/range {v0 .. v8}, Lcom/navdy/client/debug/navigation/HUDNavigationManager;->startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;)Z
    .locals 10
    .param p1, "coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p2, "label"    # Ljava/lang/String;
    .param p4, "streetAddress"    # Ljava/lang/String;
    .param p5, "destinationId"    # Ljava/lang/String;
    .param p6, "destinationType"    # Lcom/navdy/service/library/events/destination/Destination$FavoriteType;
    .param p7, "display"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p8, "requestId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/destination/Destination$FavoriteType;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 46
    .local p3, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/location/Coordinate;>;"
    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v9}, Lcom/navdy/client/debug/navigation/HUDNavigationManager;->startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Z)Z
    .locals 12
    .param p1, "coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p2, "label"    # Ljava/lang/String;
    .param p4, "streetAddress"    # Ljava/lang/String;
    .param p5, "destinationId"    # Ljava/lang/String;
    .param p6, "destinationType"    # Lcom/navdy/service/library/events/destination/Destination$FavoriteType;
    .param p7, "display"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p8, "requestId"    # Ljava/lang/String;
    .param p9, "useStreetAddress"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/destination/Destination$FavoriteType;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Z)Z"
        }
    .end annotation

    .prologue
    .line 59
    .local p3, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/location/Coordinate;>;"
    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    invoke-virtual/range {v0 .. v11}, Lcom/navdy/client/debug/navigation/HUDNavigationManager;->startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;ZZLcom/navdy/service/library/events/destination/Destination;)Z

    move-result v0

    return v0
.end method

.method public startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;ZZ)Z
    .locals 12
    .param p1, "coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p2, "label"    # Ljava/lang/String;
    .param p4, "streetAddress"    # Ljava/lang/String;
    .param p5, "destinationId"    # Ljava/lang/String;
    .param p6, "destinationType"    # Lcom/navdy/service/library/events/destination/Destination$FavoriteType;
    .param p7, "display"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p8, "requestId"    # Ljava/lang/String;
    .param p9, "useStreetAddress"    # Z
    .param p10, "initiatedOnHud"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/destination/Destination$FavoriteType;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "ZZ)Z"
        }
    .end annotation

    .prologue
    .line 73
    .local p3, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/location/Coordinate;>;"
    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    invoke-virtual/range {v0 .. v11}, Lcom/navdy/client/debug/navigation/HUDNavigationManager;->startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;ZZLcom/navdy/service/library/events/destination/Destination;)Z

    move-result v0

    return v0
.end method

.method public startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;ZZLcom/navdy/service/library/events/destination/Destination;)Z
    .locals 3
    .param p1, "coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p2, "label"    # Ljava/lang/String;
    .param p4, "streetAddress"    # Ljava/lang/String;
    .param p5, "destinationId"    # Ljava/lang/String;
    .param p6, "destinationType"    # Lcom/navdy/service/library/events/destination/Destination$FavoriteType;
    .param p7, "display"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p8, "requestId"    # Ljava/lang/String;
    .param p9, "useStreetAddress"    # Z
    .param p10, "initiatedOnHud"    # Z
    .param p11, "destination"    # Lcom/navdy/service/library/events/destination/Destination;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/destination/Destination$FavoriteType;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "ZZ",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 88
    .local p3, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/location/Coordinate;>;"
    new-instance v1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;-><init>()V

    .line 89
    invoke-virtual {v1, p1}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destination(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v1

    .line 90
    invoke-virtual {v1, p5}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destination_identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v1

    .line 91
    invoke-virtual {v1, p6}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destinationType(Lcom/navdy/service/library/events/destination/Destination$FavoriteType;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v1

    .line 92
    invoke-virtual {v1, p2}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->label(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v1

    .line 93
    invoke-virtual {v1, p3}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->waypoints(Ljava/util/List;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v1

    .line 94
    invoke-virtual {v1, p4}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->streetAddress(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v1

    .line 95
    invoke-static {p9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->geoCodeStreetAddress(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 96
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->cancelCurrent(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v1

    .line 97
    invoke-virtual {v1, p7}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destinationDisplay(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v1

    .line 98
    invoke-virtual {v1, p8}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->requestId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v1

    .line 99
    invoke-virtual {v1, p11}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->requestDestination(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v1

    .line 100
    invoke-static {p10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->originDisplay(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v1

    .line 101
    invoke-virtual {v1}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v0

    .line 105
    .local v0, "routeRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v1

    return v1
.end method
