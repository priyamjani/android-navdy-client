.class public final Lcom/navdy/client/debug/SubmitTicketFragment$$InjectAdapter;
.super Ldagger/internal/Binding;
.source "SubmitTicketFragment$$InjectAdapter.java"

# interfaces
.implements Ljavax/inject/Provider;
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/Binding",
        "<",
        "Lcom/navdy/client/debug/SubmitTicketFragment;",
        ">;",
        "Ljavax/inject/Provider",
        "<",
        "Lcom/navdy/client/debug/SubmitTicketFragment;",
        ">;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/navdy/client/debug/SubmitTicketFragment;",
        ">;"
    }
.end annotation


# instance fields
.field private mHttpManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/service/library/network/http/IHttpManager;",
            ">;"
        }
    .end annotation
.end field

.field private supertype:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/client/app/ui/base/BaseFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 29
    const-string v0, "com.navdy.client.debug.SubmitTicketFragment"

    const-string v1, "members/com.navdy.client.debug.SubmitTicketFragment"

    const/4 v2, 0x0

    const-class v3, Lcom/navdy/client/debug/SubmitTicketFragment;

    invoke-direct {p0, v0, v1, v2, v3}, Ldagger/internal/Binding;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Object;)V

    .line 30
    return-void
.end method


# virtual methods
.method public attach(Ldagger/internal/Linker;)V
    .locals 6
    .param p1, "linker"    # Ldagger/internal/Linker;

    .prologue
    .line 39
    const-string v0, "com.navdy.service.library.network.http.IHttpManager"

    const-class v1, Lcom/navdy/client/debug/SubmitTicketFragment;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment$$InjectAdapter;->mHttpManager:Ldagger/internal/Binding;

    .line 40
    const-string v1, "members/com.navdy.client.app.ui.base.BaseFragment"

    const-class v2, Lcom/navdy/client/debug/SubmitTicketFragment;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;ZZ)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment$$InjectAdapter;->supertype:Ldagger/internal/Binding;

    .line 41
    return-void
.end method

.method public get()Lcom/navdy/client/debug/SubmitTicketFragment;
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/navdy/client/debug/SubmitTicketFragment;

    invoke-direct {v0}, Lcom/navdy/client/debug/SubmitTicketFragment;-><init>()V

    .line 60
    .local v0, "result":Lcom/navdy/client/debug/SubmitTicketFragment;
    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/SubmitTicketFragment$$InjectAdapter;->injectMembers(Lcom/navdy/client/debug/SubmitTicketFragment;)V

    .line 61
    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/navdy/client/debug/SubmitTicketFragment$$InjectAdapter;->get()Lcom/navdy/client/debug/SubmitTicketFragment;

    move-result-object v0

    return-object v0
.end method

.method public getDependencies(Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p1, "getBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    .local p2, "injectMembersBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment$$InjectAdapter;->mHttpManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 50
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment$$InjectAdapter;->supertype:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 51
    return-void
.end method

.method public injectMembers(Lcom/navdy/client/debug/SubmitTicketFragment;)V
    .locals 1
    .param p1, "object"    # Lcom/navdy/client/debug/SubmitTicketFragment;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment$$InjectAdapter;->mHttpManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/network/http/IHttpManager;

    iput-object v0, p1, Lcom/navdy/client/debug/SubmitTicketFragment;->mHttpManager:Lcom/navdy/service/library/network/http/IHttpManager;

    .line 71
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment$$InjectAdapter;->supertype:Ldagger/internal/Binding;

    invoke-virtual {v0, p1}, Ldagger/internal/Binding;->injectMembers(Ljava/lang/Object;)V

    .line 72
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/navdy/client/debug/SubmitTicketFragment;

    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/SubmitTicketFragment$$InjectAdapter;->injectMembers(Lcom/navdy/client/debug/SubmitTicketFragment;)V

    return-void
.end method
