.class public Lcom/navdy/client/debug/common/BaseDebugActivity;
.super Landroid/app/Activity;
.source "BaseDebugActivity.java"

# interfaces
.implements Lcom/navdy/client/app/ui/SimpleDialogFragment$DialogProvider;


# instance fields
.field protected destroyed:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public createDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1, "id"    # I
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/navdy/client/debug/common/BaseDebugActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/client/debug/util/FragmentHelper;->getTopFragment(Landroid/app/FragmentManager;)Landroid/app/Fragment;

    move-result-object v0

    .line 34
    .local v0, "topFragment":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/navdy/client/app/ui/base/BaseFragment;

    if-eqz v1, :cond_0

    .line 35
    check-cast v0, Lcom/navdy/client/app/ui/base/BaseFragment;

    .end local v0    # "topFragment":Landroid/app/Fragment;
    invoke-virtual {v0, p1, p2}, Lcom/navdy/client/app/ui/base/BaseFragment;->createDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v1

    .line 37
    :goto_0
    return-object v1

    .restart local v0    # "topFragment":Landroid/app/Fragment;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/navdy/client/debug/common/BaseDebugActivity;->destroyed:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/client/debug/common/BaseDebugActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/debug/common/BaseDebugActivity;->destroyed:Z

    .line 23
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 24
    return-void
.end method
