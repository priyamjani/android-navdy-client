.class Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;
.super Ljava/lang/Object;
.source "OTAUpdateManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->onStateChanged(ILcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

.field final synthetic val$id:I

.field final synthetic val$state:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;


# direct methods
.method constructor <init>(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;ILcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

    .prologue
    .line 257
    iput-object p1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;->this$1:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

    iput p2, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;->val$id:I

    iput-object p3, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;->val$state:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 260
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;->this$1:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

    iget-boolean v1, v1, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->mStarted:Z

    if-nez v1, :cond_0

    .line 261
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;->this$1:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

    iget-object v1, v1, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    invoke-static {v1}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$000(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;)Lcom/navdy/client/ota/OTAUpdateListener;

    move-result-object v1

    sget-object v2, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->STARTED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    iget v3, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;->val$id:I

    move-wide v6, v4

    invoke-interface/range {v1 .. v7}, Lcom/navdy/client/ota/OTAUpdateListener;->onDownloadProgress(Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;IJJ)V

    .line 262
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;->this$1:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->mStarted:Z

    .line 265
    :cond_0
    :try_start_0
    sget-object v1, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$3;->$SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState:[I

    iget-object v2, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;->val$state:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;

    invoke-virtual {v2}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 282
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;->val$state:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 288
    :goto_0
    return-void

    .line 267
    :pswitch_0
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Download completed successfully"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 268
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;->this$1:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

    iget-object v1, v1, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    invoke-static {v1}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$000(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;)Lcom/navdy/client/ota/OTAUpdateListener;

    move-result-object v1

    sget-object v2, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->COMPLETED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    iget v3, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;->val$id:I

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/navdy/client/ota/OTAUpdateListener;->onDownloadProgress(Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;IJJ)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 285
    :catch_0
    move-exception v0

    .line 286
    .local v0, "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Bad listener "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 271
    .end local v0    # "t":Ljava/lang/Throwable;
    :pswitch_1
    :try_start_1
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Download failed"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 272
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;->this$1:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

    iget-object v1, v1, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    invoke-static {v1}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$000(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;)Lcom/navdy/client/ota/OTAUpdateListener;

    move-result-object v1

    sget-object v2, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->DOWNLOAD_FAILED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    iget v3, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;->val$id:I

    const-wide/16 v4, 0x0

    iget-object v6, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;->this$1:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

    iget-object v6, v6, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->val$info:Lcom/navdy/client/ota/model/UpdateInfo;

    iget-wide v6, v6, Lcom/navdy/client/ota/model/UpdateInfo;->size:J

    invoke-interface/range {v1 .. v7}, Lcom/navdy/client/ota/OTAUpdateListener;->onDownloadProgress(Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;IJJ)V

    goto :goto_0

    .line 275
    :pswitch_2
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Download failed"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 276
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;->this$1:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

    iget-object v1, v1, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    invoke-static {v1}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$000(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;)Lcom/navdy/client/ota/OTAUpdateListener;

    move-result-object v1

    sget-object v2, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->PAUSED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    iget v3, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;->val$id:I

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/navdy/client/ota/OTAUpdateListener;->onDownloadProgress(Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;IJJ)V

    goto :goto_0

    .line 279
    :pswitch_3
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Download in progress"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 265
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
