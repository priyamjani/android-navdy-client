.class public interface abstract Lcom/navdy/client/ota/OTAUpdateManager;
.super Ljava/lang/Object;
.source "OTAUpdateManager.java"


# virtual methods
.method public abstract abortDownload()V
.end method

.method public abstract abortUpload()V
.end method

.method public abstract checkForUpdate(IZ)V
.end method

.method public abstract download(Lcom/navdy/client/ota/model/UpdateInfo;Ljava/io/File;I)V
.end method

.method public abstract uploadToHUD(Ljava/io/File;JLjava/lang/String;)V
.end method
