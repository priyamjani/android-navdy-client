.class public final Lcom/navdy/client/app/framework/util/BusProvider;
.super Lcom/squareup/otto/Bus;
.source "BusProvider.java"


# static fields
.field private static final singleton:Lcom/navdy/client/app/framework/util/BusProvider;


# instance fields
.field private final mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lcom/navdy/client/app/framework/util/BusProvider;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/util/BusProvider;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/BusProvider;->singleton:Lcom/navdy/client/app/framework/util/BusProvider;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/squareup/otto/Bus;-><init>()V

    .line 15
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/BusProvider;->mHandler:Landroid/os/Handler;

    .line 19
    return-void
.end method

.method static synthetic access$001(Lcom/navdy/client/app/framework/util/BusProvider;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/BusProvider;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 12
    invoke-super {p0, p1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$101(Lcom/navdy/client/app/framework/util/BusProvider;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/BusProvider;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 12
    invoke-super {p0, p1}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$201(Lcom/navdy/client/app/framework/util/BusProvider;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/BusProvider;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 12
    invoke-super {p0, p1}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/navdy/client/app/framework/util/BusProvider;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/navdy/client/app/framework/util/BusProvider;->singleton:Lcom/navdy/client/app/framework/util/BusProvider;

    return-object v0
.end method


# virtual methods
.method public post(Ljava/lang/Object;)V
    .locals 2
    .param p1, "event"    # Ljava/lang/Object;

    .prologue
    .line 27
    if-nez p1, :cond_0

    .line 36
    :goto_0
    return-void

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/BusProvider;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/framework/util/BusProvider$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/util/BusProvider$1;-><init>(Lcom/navdy/client/app/framework/util/BusProvider;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public register(Ljava/lang/Object;)V
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 40
    if-nez p1, :cond_0

    .line 53
    :goto_0
    return-void

    .line 43
    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 44
    invoke-super {p0, p1}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    goto :goto_0

    .line 46
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/BusProvider;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/framework/util/BusProvider$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/util/BusProvider$2;-><init>(Lcom/navdy/client/app/framework/util/BusProvider;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public unregister(Ljava/lang/Object;)V
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 57
    if-nez p1, :cond_0

    .line 70
    :goto_0
    return-void

    .line 60
    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 61
    invoke-super {p0, p1}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    goto :goto_0

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/BusProvider;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/framework/util/BusProvider$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/util/BusProvider$3;-><init>(Lcom/navdy/client/app/framework/util/BusProvider;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
