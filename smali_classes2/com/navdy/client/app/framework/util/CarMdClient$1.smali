.class Lcom/navdy/client/app/framework/util/CarMdClient$1;
.super Ljava/lang/Object;
.source "CarMdClient.java"

# interfaces
.implements Lokhttp3/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/CarMdClient;->sendCarMdRequest(Lcom/navdy/client/app/framework/util/CarMdCallBack;Lokhttp3/Request;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/util/CarMdClient;

.field final synthetic val$callback:Lcom/navdy/client/app/framework/util/CarMdCallBack;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/util/CarMdClient;Lcom/navdy/client/app/framework/util/CarMdCallBack;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/util/CarMdClient;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/CarMdClient$1;->this$0:Lcom/navdy/client/app/framework/util/CarMdClient;

    iput-object p2, p0, Lcom/navdy/client/app/framework/util/CarMdClient$1;->val$callback:Lcom/navdy/client/app/framework/util/CarMdCallBack;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lokhttp3/Call;Ljava/io/IOException;)V
    .locals 1
    .param p1, "call"    # Lokhttp3/Call;
    .param p2, "e"    # Ljava/io/IOException;

    .prologue
    .line 201
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/CarMdClient$1;->val$callback:Lcom/navdy/client/app/framework/util/CarMdCallBack;

    invoke-virtual {v0, p2}, Lcom/navdy/client/app/framework/util/CarMdCallBack;->processFailure(Ljava/lang/Throwable;)V

    .line 202
    return-void
.end method

.method public onResponse(Lokhttp3/Call;Lokhttp3/Response;)V
    .locals 8
    .param p1, "call"    # Lokhttp3/Call;
    .param p2, "response"    # Lokhttp3/Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 185
    :try_start_0
    invoke-virtual {p2}, Lokhttp3/Response;->code()I

    move-result v1

    .line 186
    .local v1, "responseCode":I
    const/16 v3, 0xc8

    if-eq v1, v3, :cond_0

    const/16 v3, 0xc9

    if-ne v1, v3, :cond_2

    .line 187
    :cond_0
    invoke-virtual {p2}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v0

    .line 188
    .local v0, "responseBody":Lokhttp3/ResponseBody;
    invoke-virtual {v0}, Lokhttp3/ResponseBody;->contentLength()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_1

    .line 189
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/CarMdClient$1;->val$callback:Lcom/navdy/client/app/framework/util/CarMdCallBack;

    invoke-virtual {v3, v0}, Lcom/navdy/client/app/framework/util/CarMdCallBack;->processResponse(Lokhttp3/ResponseBody;)V

    .line 197
    .end local v0    # "responseBody":Lokhttp3/ResponseBody;
    .end local v1    # "responseCode":I
    :cond_1
    :goto_0
    return-void

    .line 192
    .restart local v1    # "responseCode":I
    :cond_2
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/CarMdClient$1;->val$callback:Lcom/navdy/client/app/framework/util/CarMdCallBack;

    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Car MD request failed with "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/framework/util/CarMdCallBack;->processFailure(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 194
    .end local v1    # "responseCode":I
    :catch_0
    move-exception v2

    .line 195
    .local v2, "t":Ljava/lang/Throwable;
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/CarMdClient$1;->val$callback:Lcom/navdy/client/app/framework/util/CarMdCallBack;

    invoke-virtual {v3, v2}, Lcom/navdy/client/app/framework/util/CarMdCallBack;->processFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
