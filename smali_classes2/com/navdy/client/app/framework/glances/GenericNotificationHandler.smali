.class public Lcom/navdy/client/app/framework/glances/GenericNotificationHandler;
.super Ljava/lang/Object;
.source "GenericNotificationHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getBestAvailableText(Landroid/app/Notification;)Ljava/lang/String;
    .locals 7
    .param p0, "notification"    # Landroid/app/Notification;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 54
    iget-object v4, p0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 56
    .local v4, "ticker":Ljava/lang/CharSequence;
    invoke-static {p0}, Landroid/support/v7/app/NotificationCompat;->getExtras(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v1

    .line 58
    .local v1, "extras":Landroid/os/Bundle;
    if-eqz v1, :cond_3

    .line 59
    const-string v6, "android.bigText"

    invoke-static {v1, v6}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getStringSafely(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "bigText":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 79
    .end local v0    # "bigText":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 63
    .restart local v0    # "bigText":Ljava/lang/String;
    :cond_0
    const-string v6, "android.text"

    invoke-static {v1, v6}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getStringSafely(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 64
    .local v3, "text":Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    move-object v0, v3

    .line 65
    goto :goto_0

    .line 67
    :cond_1
    const-string v6, "android.subText"

    invoke-static {v1, v6}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getStringSafely(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 68
    .local v2, "subText":Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    move-object v0, v2

    .line 69
    goto :goto_0

    .line 71
    :cond_2
    const-string v6, "android.title"

    invoke-static {v1, v6}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getStringSafely(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 72
    .local v5, "title":Ljava/lang/String;
    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    move-object v0, v5

    .line 73
    goto :goto_0

    .line 76
    .end local v0    # "bigText":Ljava/lang/String;
    .end local v2    # "subText":Ljava/lang/String;
    .end local v3    # "text":Ljava/lang/String;
    .end local v5    # "title":Ljava/lang/String;
    :cond_3
    if-eqz v4, :cond_4

    .line 77
    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 79
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static handleGenericNotification(Landroid/service/notification/StatusBarNotification;)V
    .locals 10
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 25
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 26
    .local v5, "packageName":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v4

    .line 28
    .local v4, "notification":Landroid/app/Notification;
    invoke-static {v5}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->isThisGlanceEnabledAsWellAsGlobal(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 50
    :cond_0
    :goto_0
    return-void

    .line 32
    :cond_1
    invoke-static {v4}, Lcom/navdy/client/app/framework/glances/GenericNotificationHandler;->getBestAvailableText(Landroid/app/Notification;)Ljava/lang/String;

    move-result-object v3

    .line 33
    .local v3, "message":Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 37
    invoke-static {}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->getId()Ljava/lang/String;

    move-result-object v2

    .line 39
    .local v2, "id":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 40
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v6, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v7, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_MESSAGE:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v7}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v3}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    new-instance v6, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v7, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_GENERIC:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 43
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v6

    .line 44
    invoke-virtual {v6, v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v6

    .line 45
    invoke-virtual {v6, v2}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v6

    iget-wide v8, v4, Landroid/app/Notification;->when:J

    .line 46
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v6

    .line 47
    invoke-virtual {v6, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v1

    .line 49
    .local v1, "genericEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    invoke-static {v1}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->sendEvent(Lcom/navdy/service/library/events/glances/GlanceEvent;)V

    goto :goto_0
.end method
