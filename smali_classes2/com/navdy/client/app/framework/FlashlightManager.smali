.class public Lcom/navdy/client/app/framework/FlashlightManager;
.super Ljava/lang/Object;
.source "FlashlightManager.java"


# static fields
.field private static instance:Lcom/navdy/client/app/framework/FlashlightManager;


# instance fields
.field private cam:Landroid/hardware/Camera;

.field private flashlightIsOn:Z

.field private isCapableOfFlashlighting:Z

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private previousScreenBrightness:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    sput-object v0, Lcom/navdy/client/app/framework/FlashlightManager;->instance:Lcom/navdy/client/app/framework/FlashlightManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v2, Lcom/navdy/service/library/log/Logger;

    const-class v3, Lcom/navdy/client/app/framework/FlashlightManager;

    invoke-direct {v2, v3}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v2, p0, Lcom/navdy/client/app/framework/FlashlightManager;->logger:Lcom/navdy/service/library/log/Logger;

    .line 26
    iput-boolean v4, p0, Lcom/navdy/client/app/framework/FlashlightManager;->flashlightIsOn:Z

    .line 27
    iput-boolean v4, p0, Lcom/navdy/client/app/framework/FlashlightManager;->isCapableOfFlashlighting:Z

    .line 28
    const/high16 v2, 0x3f000000    # 0.5f

    iput v2, p0, Lcom/navdy/client/app/framework/FlashlightManager;->previousScreenBrightness:F

    .line 33
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 34
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 35
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    if-eqz v1, :cond_0

    .line 36
    const-string v2, "android.hardware.camera.flash"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/navdy/client/app/framework/FlashlightManager;->isCapableOfFlashlighting:Z

    .line 38
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/navdy/client/app/framework/FlashlightManager;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/navdy/client/app/framework/FlashlightManager;->instance:Lcom/navdy/client/app/framework/FlashlightManager;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/navdy/client/app/framework/FlashlightManager;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/FlashlightManager;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/FlashlightManager;->instance:Lcom/navdy/client/app/framework/FlashlightManager;

    .line 44
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/FlashlightManager;->instance:Lcom/navdy/client/app/framework/FlashlightManager;

    return-object v0
.end method


# virtual methods
.method public setScreenBackToNormalBrightness(Landroid/view/Window;)V
    .locals 2
    .param p1, "window"    # Landroid/view/Window;

    .prologue
    .line 129
    invoke-virtual {p1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 130
    .local v0, "layout":Landroid/view/WindowManager$LayoutParams;
    iget v1, p0, Lcom/navdy/client/app/framework/FlashlightManager;->previousScreenBrightness:F

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 131
    invoke-virtual {p1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 132
    return-void
.end method

.method public setScreenToFullBrightness(Landroid/view/Window;)V
    .locals 2
    .param p1, "window"    # Landroid/view/Window;

    .prologue
    .line 122
    invoke-virtual {p1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 123
    .local v0, "layout":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    iput v1, p0, Lcom/navdy/client/app/framework/FlashlightManager;->previousScreenBrightness:F

    .line 124
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 125
    invoke-virtual {p1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 126
    return-void
.end method

.method public setTorchModeOn(Z)V
    .locals 4
    .param p1, "isOn"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/hardware/camera2/CameraAccessException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 106
    .local v1, "context":Landroid/content/Context;
    const-string v3, "camera"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/camera2/CameraManager;

    .line 107
    .local v2, "manager":Landroid/hardware/camera2/CameraManager;
    if-eqz v2, :cond_0

    .line 108
    invoke-virtual {v2}, Landroid/hardware/camera2/CameraManager;->getCameraIdList()[Ljava/lang/String;

    move-result-object v0

    .line 109
    .local v0, "cameraList":[Ljava/lang/String;
    array-length v3, v0

    if-lez v3, :cond_0

    .line 110
    const/4 v3, 0x0

    aget-object v3, v0, v3

    invoke-virtual {v2, v3, p1}, Landroid/hardware/camera2/CameraManager;->setTorchMode(Ljava/lang/String;Z)V

    .line 113
    .end local v0    # "cameraList":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public startScreenLightActivity(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 116
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 118
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 119
    return-void
.end method

.method public switchLight(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/FlashlightManager;->flashlightIsOn:Z

    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/FlashlightManager;->turnFlashLightOff()V

    .line 53
    :goto_0
    return-void

    .line 51
    :cond_0
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/FlashlightManager;->turnFlashLightOn(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public turnFlashLightOff()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 83
    :try_start_0
    iget-boolean v1, p0, Lcom/navdy/client/app/framework/FlashlightManager;->flashlightIsOn:Z

    if-nez v1, :cond_0

    .line 101
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-boolean v1, p0, Lcom/navdy/client/app/framework/FlashlightManager;->isCapableOfFlashlighting:Z

    if-nez v1, :cond_1

    .line 87
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Phone not capable of flashlighting"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/navdy/client/app/framework/FlashlightManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to turn the flashlight off the normal way. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 100
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    iput-boolean v4, p0, Lcom/navdy/client/app/framework/FlashlightManager;->flashlightIsOn:Z

    goto :goto_0

    .line 90
    :cond_1
    :try_start_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_2

    .line 91
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/framework/FlashlightManager;->setTorchModeOn(Z)V

    goto :goto_1

    .line 93
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/framework/FlashlightManager;->cam:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->stopPreview()V

    .line 94
    iget-object v1, p0, Lcom/navdy/client/app/framework/FlashlightManager;->cam:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->release()V

    .line 95
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/client/app/framework/FlashlightManager;->cam:Landroid/hardware/Camera;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public turnFlashLightOn(Landroid/app/Activity;)V
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v5, 0x1

    .line 57
    :try_start_0
    iget-boolean v2, p0, Lcom/navdy/client/app/framework/FlashlightManager;->flashlightIsOn:Z

    if-eqz v2, :cond_0

    .line 79
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-boolean v2, p0, Lcom/navdy/client/app/framework/FlashlightManager;->isCapableOfFlashlighting:Z

    if-nez v2, :cond_1

    .line 61
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "Phone not capable of flashlighting"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/navdy/client/app/framework/FlashlightManager;->isCapableOfFlashlighting:Z

    .line 75
    iget-object v2, p0, Lcom/navdy/client/app/framework/FlashlightManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to turn the flashlight on the normal way. Using the screen brightness instead. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/FlashlightManager;->startScreenLightActivity(Landroid/app/Activity;)V

    .line 78
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    iput-boolean v5, p0, Lcom/navdy/client/app/framework/FlashlightManager;->flashlightIsOn:Z

    goto :goto_0

    .line 64
    :cond_1
    :try_start_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v2, v3, :cond_2

    .line 65
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/framework/FlashlightManager;->setTorchModeOn(Z)V

    goto :goto_1

    .line 67
    :cond_2
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/client/app/framework/FlashlightManager;->cam:Landroid/hardware/Camera;

    .line 68
    iget-object v2, p0, Lcom/navdy/client/app/framework/FlashlightManager;->cam:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 69
    .local v1, "p":Landroid/hardware/Camera$Parameters;
    const-string v2, "torch"

    invoke-virtual {v1, v2}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 70
    iget-object v2, p0, Lcom/navdy/client/app/framework/FlashlightManager;->cam:Landroid/hardware/Camera;

    invoke-virtual {v2, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 71
    iget-object v2, p0, Lcom/navdy/client/app/framework/FlashlightManager;->cam:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->startPreview()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
