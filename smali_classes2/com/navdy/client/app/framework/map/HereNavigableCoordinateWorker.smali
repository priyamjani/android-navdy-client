.class public Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;
.super Ljava/lang/Object;
.source "HereNavigableCoordinateWorker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$NavigableCoordinateWorkerFinishCallback;
    }
.end annotation


# static fields
.field private static final MIN_WORDS_IN_ADDRESS:I = 0x4

.field private static final lockObj:Ljava/lang/Object;

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final calendarEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/CalendarEvent;",
            ">;"
        }
    .end annotation
.end field

.field private callback:Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$NavigableCoordinateWorkerFinishCallback;

.field private currentCalendarEventIndex:I

.field private final processedCalendarEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/CalendarEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final uiThreadHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->logger:Lcom/navdy/service/library/log/Logger;

    .line 29
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->lockObj:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$NavigableCoordinateWorkerFinishCallback;)V
    .locals 2
    .param p2, "callback"    # Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$NavigableCoordinateWorkerFinishCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/CalendarEvent;",
            ">;",
            "Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$NavigableCoordinateWorkerFinishCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 41
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/CalendarEvent;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->currentCalendarEventIndex:I

    .line 42
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->calendarEvents:Ljava/util/List;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->processedCalendarEvents:Ljava/util/List;

    .line 44
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->uiThreadHandler:Landroid/os/Handler;

    .line 46
    invoke-direct {p0, p2}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->start(Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$NavigableCoordinateWorkerFinishCallback;)V

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->processedCalendarEvents:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;)Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$NavigableCoordinateWorkerFinishCallback;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->callback:Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$NavigableCoordinateWorkerFinishCallback;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;Lcom/navdy/client/app/framework/models/CalendarEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/models/CalendarEvent;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->process(Lcom/navdy/client/app/framework/models/CalendarEvent;)V

    return-void
.end method

.method static synthetic access$300()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;Lcom/navdy/client/app/framework/models/CalendarEvent;DD)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/models/CalendarEvent;
    .param p2, "x2"    # D
    .param p4, "x3"    # D

    .prologue
    .line 26
    invoke-direct/range {p0 .. p5}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->addToProcessedEventsListAndCallBack(Lcom/navdy/client/app/framework/models/CalendarEvent;DD)V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->processNextCalendarEvent()V

    return-void
.end method

.method private addToProcessedEventsListAndCallBack(Lcom/navdy/client/app/framework/models/CalendarEvent;DD)V
    .locals 2
    .param p1, "calendarEvent"    # Lcom/navdy/client/app/framework/models/CalendarEvent;
    .param p2, "navigationLat"    # D
    .param p4, "navigationLng"    # D

    .prologue
    .line 159
    sget-object v1, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->lockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 160
    :try_start_0
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v0, p2, p3, p4, p5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iput-object v0, p1, Lcom/navdy/client/app/framework/models/CalendarEvent;->latLng:Lcom/google/android/gms/maps/model/LatLng;

    .line 161
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->processedCalendarEvents:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    invoke-direct {p0}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->processNextCalendarEvent()V

    .line 165
    return-void

    .line 162
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private process(Lcom/navdy/client/app/framework/models/CalendarEvent;)V
    .locals 10
    .param p1, "calendarEvent"    # Lcom/navdy/client/app/framework/models/CalendarEvent;

    .prologue
    .line 84
    iget-object v7, p1, Lcom/navdy/client/app/framework/models/CalendarEvent;->location:Ljava/lang/String;

    invoke-static {v7}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 85
    sget-object v7, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Now processing: The address is missing for: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/CalendarEvent;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 86
    invoke-direct {p0}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->processNextCalendarEvent()V

    .line 156
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v7, p1, Lcom/navdy/client/app/framework/models/CalendarEvent;->location:Ljava/lang/String;

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 91
    sget-object v7, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Now processing: The address does not contain a comma: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/CalendarEvent;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 92
    invoke-direct {p0}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->processNextCalendarEvent()V

    goto :goto_0

    .line 96
    :cond_1
    iget-object v7, p1, Lcom/navdy/client/app/framework/models/CalendarEvent;->location:Ljava/lang/String;

    const-string v8, "\\W"

    const-string v9, " "

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 97
    .local v6, "wordsString":Ljava/lang/String;
    const-string v7, "[0-9]"

    const-string v8, " "

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 99
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, " +"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 101
    .local v5, "words":[Ljava/lang/String;
    array-length v7, v5

    const/4 v8, 0x4

    if-ge v7, v8, :cond_4

    .line 102
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    .local v4, "sb":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v7, v5

    if-ge v2, v7, :cond_3

    .line 104
    aget-object v7, v5, v2

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    add-int/lit8 v7, v2, 0x1

    array-length v8, v5

    if-ge v7, v8, :cond_2

    .line 106
    const-string v7, ","

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 109
    :cond_3
    sget-object v7, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Now processing: Not enough words: words are: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", event: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/CalendarEvent;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 111
    invoke-direct {p0}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->processNextCalendarEvent()V

    goto/16 :goto_0

    .line 115
    .end local v2    # "i":I
    .end local v4    # "sb":Ljava/lang/StringBuilder;
    :cond_4
    new-instance v3, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$3;

    invoke-direct {v3, p0, p1}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$3;-><init>(Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;Lcom/navdy/client/app/framework/models/CalendarEvent;)V

    .line 139
    .local v3, "onCompleteCallback":Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;
    iget-object v7, p1, Lcom/navdy/client/app/framework/models/CalendarEvent;->location:Ljava/lang/String;

    invoke-static {v7}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getCacheEntryIfExists(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/DestinationCacheEntry;

    move-result-object v1

    .line 141
    .local v1, "entry":Lcom/navdy/client/app/framework/models/DestinationCacheEntry;
    if-eqz v1, :cond_6

    .line 142
    iget-object v7, v1, Lcom/navdy/client/app/framework/models/DestinationCacheEntry;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v7, :cond_5

    .line 144
    sget-object v7, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "destination with address "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p1, Lcom/navdy/client/app/framework/models/CalendarEvent;->location:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "found in cache, returning"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 146
    iget-object v7, v1, Lcom/navdy/client/app/framework/models/DestinationCacheEntry;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-interface {v3, v7}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;->onSuccess(Lcom/navdy/client/app/framework/models/Destination;)V

    goto/16 :goto_0

    .line 148
    :cond_5
    const/4 v7, 0x0

    sget-object v8, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->CACHE_NO_COORDS:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    invoke-interface {v3, v7, v8}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;->onFailure(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V

    goto/16 :goto_0

    .line 151
    :cond_6
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination;

    iget-object v7, p1, Lcom/navdy/client/app/framework/models/CalendarEvent;->displayName:Ljava/lang/String;

    iget-object v8, p1, Lcom/navdy/client/app/framework/models/CalendarEvent;->location:Ljava/lang/String;

    invoke-direct {v0, v7, v8}, Lcom/navdy/client/app/framework/models/Destination;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    .local v0, "calendarDestination":Lcom/navdy/client/app/framework/models/Destination;
    invoke-static {v0, v3}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->processDestination(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    goto/16 :goto_0
.end method

.method private processNextCalendarEvent()V
    .locals 5

    .prologue
    .line 55
    sget-object v2, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->lockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 56
    :try_start_0
    iget v1, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->currentCalendarEventIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->currentCalendarEventIndex:I

    .line 58
    iget-object v1, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->calendarEvents:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->calendarEvents:Ljava/util/List;

    .line 59
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->calendarEvents:Ljava/util/List;

    .line 60
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget v3, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->currentCalendarEventIndex:I

    if-gt v1, v3, :cond_2

    .line 61
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->callback:Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$NavigableCoordinateWorkerFinishCallback;

    if-eqz v1, :cond_1

    .line 62
    iget-object v1, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->uiThreadHandler:Landroid/os/Handler;

    new-instance v3, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$1;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$1;-><init>(Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;)V

    invoke-virtual {v1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 69
    :cond_1
    monitor-exit v2

    .line 80
    :goto_0
    return-void

    .line 72
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->calendarEvents:Ljava/util/List;

    iget v3, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->currentCalendarEventIndex:I

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/CalendarEvent;

    .line 73
    .local v0, "calendarEvent":Lcom/navdy/client/app/framework/models/CalendarEvent;
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v3, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$2;

    invoke-direct {v3, p0, v0}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$2;-><init>(Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;Lcom/navdy/client/app/framework/models/CalendarEvent;)V

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 79
    monitor-exit v2

    goto :goto_0

    .end local v0    # "calendarEvent":Lcom/navdy/client/app/framework/models/CalendarEvent;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private start(Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$NavigableCoordinateWorkerFinishCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$NavigableCoordinateWorkerFinishCallback;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->callback:Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$NavigableCoordinateWorkerFinishCallback;

    .line 51
    invoke-direct {p0}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->processNextCalendarEvent()V

    .line 52
    return-void
.end method
