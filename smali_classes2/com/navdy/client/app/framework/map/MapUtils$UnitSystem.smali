.class final enum Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;
.super Ljava/lang/Enum;
.source "MapUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/map/MapUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "UnitSystem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;

.field public static final enum IMPERIAL:Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;

.field public static final enum METRIC:Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 137
    new-instance v0, Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;

    const-string v1, "METRIC"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;->METRIC:Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;

    .line 138
    new-instance v0, Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;

    const-string v1, "IMPERIAL"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;->IMPERIAL:Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;

    .line 136
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;

    sget-object v1, Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;->METRIC:Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;->IMPERIAL:Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;->$VALUES:[Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 136
    const-class v0, Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;->$VALUES:[Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;

    return-object v0
.end method
