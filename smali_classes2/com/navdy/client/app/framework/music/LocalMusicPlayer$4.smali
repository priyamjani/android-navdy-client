.class Lcom/navdy/client/app/framework/music/LocalMusicPlayer$4;
.super Ljava/lang/Object;
.source "LocalMusicPlayer.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/music/LocalMusicPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$4;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 5
    .param p1, "focusChange"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 92
    invoke-static {}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAudioFocusChange "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 93
    if-lt p1, v3, :cond_2

    .line 94
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$4;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-static {v0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$600(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$4;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-static {v0, v4}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$602(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;Z)Z

    .line 96
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$4;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-static {v0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$700(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)V

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$4;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-static {v0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$800(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$4;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-static {v0, v4}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$802(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;Z)Z

    .line 100
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$4;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->play()V

    .line 111
    :cond_1
    :goto_0
    return-void

    .line 102
    :cond_2
    const/4 v0, -0x3

    if-ne p1, v0, :cond_3

    .line 103
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$4;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-static {v0, v3}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$602(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;Z)Z

    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$4;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-static {v0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$900(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)V

    goto :goto_0

    .line 105
    :cond_3
    const/4 v0, -0x2

    if-ne p1, v0, :cond_4

    .line 106
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$4;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-static {v0, v3}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$802(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;Z)Z

    .line 107
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$4;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-virtual {v0, v4}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->pause(Z)V

    goto :goto_0

    .line 109
    :cond_4
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$4;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-virtual {v0, v3}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->pause(Z)V

    goto :goto_0
.end method
