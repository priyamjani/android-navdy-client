.class Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;
.super Ljava/lang/Object;
.source "VoiceServiceHandler.java"

# interfaces
.implements Landroid/speech/RecognitionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static final MAX_LISTENING_TIME:J = 0x7530L

.field private static final MIN_LISTENING_TIME:J = 0x7d0L


# instance fields
.field private beginTimestamp:J

.field private handler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)V
    .locals 2
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .prologue
    .line 327
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->beginTimestamp:J

    .line 332
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->handler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public onBeginningOfSpeech()V
    .locals 4

    .prologue
    .line 359
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->beginTimestamp:J

    .line 362
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->handler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 364
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->startTimeoutTimer()V

    .line 366
    new-instance v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;-><init>()V

    sget-object v2, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_LISTENING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    .line 367
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->state(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v1

    const/16 v2, 0x32

    .line 368
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->volumeLevel(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .line 369
    invoke-static {v2}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$000(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->listeningOverBluetooth(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v1

    .line 370
    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->build()Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    move-result-object v0

    .line 371
    .local v0, "response":Lcom/navdy/service/library/events/audio/VoiceSearchResponse;
    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 372
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "onBeginningOfSpeech"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 373
    return-void
.end method

.method public onBufferReceived([B)V
    .locals 0
    .param p1, "buffer"    # [B

    .prologue
    .line 381
    return-void
.end method

.method public onEndOfSpeech()V
    .locals 3

    .prologue
    .line 385
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "onEndOfSpeech"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 388
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->handler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 390
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->startTimeoutTimer()V

    .line 392
    new-instance v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;-><init>()V

    sget-object v2, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_SEARCHING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    .line 394
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->state(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .line 395
    invoke-static {v2}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$000(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->listeningOverBluetooth(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v1

    .line 396
    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->build()Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    move-result-object v0

    .line 397
    .local v0, "response":Lcom/navdy/service/library/events/audio/VoiceSearchResponse;
    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 398
    return-void
.end method

.method public onError(I)V
    .locals 13
    .param p1, "error"    # I

    .prologue
    .line 404
    iget-object v10, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->handler:Landroid/os/Handler;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 406
    iget-object v10, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v10}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$800(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 407
    sget-object v10, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Voice search was canceled on the HUD so ignoring this error: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 527
    :goto_0
    return-void

    .line 411
    :cond_0
    iget-object v10, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v10}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$700(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Z

    move-result v10

    if-nez v10, :cond_1

    const/4 v10, 0x7

    if-ne p1, v10, :cond_1

    .line 412
    sget-object v10, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Ignoring an error that occurred before the engine was ready for speech. Error: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 416
    :cond_1
    new-instance v7, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    invoke-direct {v7}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;-><init>()V

    .line 417
    .local v7, "responseBuilder":Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    sget-object v10, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_ERROR:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v7, v10}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->state(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 420
    new-instance v10, Ljava/util/Date;

    invoke-direct {v10}, Ljava/util/Date;-><init>()V

    invoke-virtual {v10}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    .line 421
    .local v4, "now":J
    iget-wide v10, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->beginTimestamp:J

    sub-long v8, v4, v10

    .line 422
    .local v8, "timeListened":J
    const/4 v10, 0x7

    if-ne p1, v10, :cond_3

    .line 423
    const-wide/16 v10, 0x0

    cmp-long v10, v8, v10

    if-lez v10, :cond_2

    const-wide/16 v10, 0x7d0

    cmp-long v10, v8, v10

    if-gez v10, :cond_2

    .line 425
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 426
    .local v0, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v10, "Noise_Retry"

    iget-object v11, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v11}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$900(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Z

    move-result v11

    invoke-static {v11}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 427
    const-string v10, "Hud_Voice_Search_ambient_noise_failure"

    invoke-static {v10, v0}, Lcom/navdy/client/app/framework/LocalyticsManager;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 430
    iget-object v10, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v10}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$900(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Z

    move-result v10

    if-nez v10, :cond_3

    iget-object v10, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v10}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$300(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Landroid/speech/SpeechRecognizer;

    move-result-object v10

    if-eqz v10, :cond_3

    .line 431
    sget-object v10, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Only "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "ms. elapsed since we started listening and we haven\'t retried yet so starting the mic again."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 432
    iget-object v10, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    const/4 v11, 0x1

    invoke-static {v10, v11}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$902(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Z)Z

    .line 435
    new-instance v10, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    invoke-direct {v10}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;-><init>()V

    sget-object v11, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_LISTENING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    .line 436
    invoke-virtual {v10, v11}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->state(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v10

    iget-object v11, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .line 437
    invoke-static {v11}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$000(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Z

    move-result v11

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->listeningOverBluetooth(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v10

    .line 438
    invoke-virtual {v10}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->build()Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    move-result-object v6

    .line 439
    .local v6, "response":Lcom/navdy/service/library/events/audio/VoiceSearchResponse;
    invoke-static {v6}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 441
    iget-object v10, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v10}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$300(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Landroid/speech/SpeechRecognizer;

    move-result-object v10

    iget-object v11, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v11}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$500(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/speech/SpeechRecognizer;->startListening(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 445
    .end local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v6    # "response":Lcom/navdy/service/library/events/audio/VoiceSearchResponse;
    :cond_2
    sget-object v10, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "ms. elapsed since we started listening."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 450
    :cond_3
    iget-object v10, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v10}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$1000(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)V

    .line 451
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 453
    .local v1, "context":Landroid/content/Context;
    packed-switch p1, :pswitch_data_0

    .line 520
    sget-object v10, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onError "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 521
    sget-object v10, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->FAILED_TO_START:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-virtual {v7, v10}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->error(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 524
    :goto_1
    iget-object v10, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v10}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$000(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v7, v10}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->listeningOverBluetooth(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 525
    invoke-virtual {v7}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->build()Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    move-result-object v10

    invoke-static {v10}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 526
    iget-object v10, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v10}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$1100(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)V

    goto/16 :goto_0

    .line 459
    :pswitch_0
    packed-switch p1, :pswitch_data_1

    .line 465
    :pswitch_1
    const-string v2, "ERROR_NETWORK"

    .line 474
    .local v2, "errorType":Ljava/lang/String;
    :goto_2
    sget-object v10, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onError: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 475
    const v10, 0x7f0802f0

    invoke-virtual {v1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 476
    .local v3, "message":Ljava/lang/String;
    sget-object v10, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->OFFLINE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-virtual {v7, v10}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->error(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 477
    iget-object v10, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    iget-object v10, v10, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v10, v3, v11, v12}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    .line 461
    .end local v2    # "errorType":Ljava/lang/String;
    .end local v3    # "message":Ljava/lang/String;
    :pswitch_2
    const-string v2, "ERROR_NETWORK_TIMEOUT"

    .line 462
    .restart local v2    # "errorType":Ljava/lang/String;
    goto :goto_2

    .line 468
    .end local v2    # "errorType":Ljava/lang/String;
    :pswitch_3
    const-string v2, "ERROR_SERVER"

    .line 469
    .restart local v2    # "errorType":Ljava/lang/String;
    goto :goto_2

    .line 471
    .end local v2    # "errorType":Ljava/lang/String;
    :pswitch_4
    const-string v2, "ERROR_CLIENT"

    .restart local v2    # "errorType":Ljava/lang/String;
    goto :goto_2

    .line 481
    .end local v2    # "errorType":Ljava/lang/String;
    :pswitch_5
    sget-object v10, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "onError: ERROR_AUDIO"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 482
    const v10, 0x7f0800cc

    invoke-virtual {v1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 483
    .restart local v3    # "message":Ljava/lang/String;
    sget-object v10, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->FAILED_TO_START:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-virtual {v7, v10}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->error(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 484
    iget-object v10, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    iget-object v10, v10, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v10, v3, v11, v12}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    .line 489
    .end local v3    # "message":Ljava/lang/String;
    :pswitch_6
    sget-object v11, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onError: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v10, 0x6

    if-ne p1, v10, :cond_4

    const-string v10, "SpeechRecognizer.ERROR_SPEECH_TIMEOUT"

    :goto_3
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 492
    const/4 v10, 0x7

    if-ne p1, v10, :cond_5

    const-wide/16 v10, 0x0

    cmp-long v10, v8, v10

    if-lez v10, :cond_5

    const-wide/16 v10, 0x7d0

    cmp-long v10, v8, v10

    if-gez v10, :cond_5

    .line 494
    sget-object v10, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Only "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "ms. elapsed since we started listening and we\'ve already retried so giving up due to environment noise."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 495
    sget-object v10, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->AMBIENT_NOISE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-virtual {v7, v10}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->error(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 496
    const v10, 0x7f08015f

    invoke-virtual {v1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 497
    .restart local v3    # "message":Ljava/lang/String;
    iget-object v10, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    iget-object v10, v10, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v10, v3, v11, v12}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 489
    .end local v3    # "message":Ljava/lang/String;
    :cond_4
    const-string v10, "ERROR_NO_MATCH"

    goto :goto_3

    .line 499
    :cond_5
    sget-object v10, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NO_WORDS_RECOGNIZED:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-virtual {v7, v10}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->error(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 500
    const v10, 0x7f080309

    invoke-virtual {v1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 501
    .restart local v3    # "message":Ljava/lang/String;
    iget-object v10, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    iget-object v10, v10, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v10, v3, v11, v12}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 506
    .end local v3    # "message":Ljava/lang/String;
    :pswitch_7
    sget-object v10, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "onError: ERROR_RECOGNIZER_BUSY"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 507
    sget-object v10, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->FAILED_TO_START:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-virtual {v7, v10}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->error(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 508
    const v10, 0x7f0802c6

    invoke-virtual {v1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 509
    .restart local v3    # "message":Ljava/lang/String;
    iget-object v10, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    iget-object v10, v10, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v10, v3, v11, v12}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 513
    .end local v3    # "message":Ljava/lang/String;
    :pswitch_8
    sget-object v10, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "onError: ERROR_INSUFFICIENT_PERMISSIONS"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 514
    sget-object v10, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NEED_PERMISSION:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-virtual {v7, v10}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->error(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 515
    const v10, 0x7f0802ed

    invoke-virtual {v1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 516
    .restart local v3    # "message":Ljava/lang/String;
    iget-object v10, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    iget-object v10, v10, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v10, v3, v11, v12}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 453
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 459
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onEvent(ILandroid/os/Bundle;)V
    .locals 3
    .param p1, "eventType"    # I
    .param p2, "params"    # Landroid/os/Bundle;

    .prologue
    .line 585
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onEvent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 586
    return-void
.end method

.method public onPartialResults(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "partialResults"    # Landroid/os/Bundle;

    .prologue
    .line 532
    return-void
.end method

.method public onReadyForSpeech(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "params"    # Landroid/os/Bundle;

    .prologue
    .line 336
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$702(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Z)Z

    .line 338
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "onReadyForSpeech true"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 339
    new-instance v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;-><init>()V

    sget-object v2, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_LISTENING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    .line 340
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->state(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .line 341
    invoke-static {v2}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$000(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->listeningOverBluetooth(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v1

    .line 342
    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->build()Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    move-result-object v0

    .line 343
    .local v0, "response":Lcom/navdy/service/library/events/audio/VoiceSearchResponse;
    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 345
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->startTimeoutTimer()V

    .line 346
    return-void
.end method

.method public onResults(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "results"    # Landroid/os/Bundle;

    .prologue
    .line 538
    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->handler:Landroid/os/Handler;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 540
    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v6}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$800(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 541
    sget-object v6, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Voice search was canceled on the HUD so ignoring the results."

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 581
    :cond_0
    :goto_0
    return-void

    .line 545
    :cond_1
    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v6}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$1000(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)V

    .line 546
    const/4 v4, 0x0

    .line 547
    .local v4, "query":Ljava/lang/String;
    const/high16 v1, -0x40800000    # -1.0f

    .line 548
    .local v1, "confidenceLevel":F
    if-eqz p1, :cond_2

    .line 549
    const-string v6, "confidence_scores"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v0

    .line 550
    .local v0, "confidence":[F
    const-string v6, "results_recognition"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 551
    .local v2, "data":Ljava/util/ArrayList;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v3, v6, :cond_2

    .line 552
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 553
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "query":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 554
    .restart local v4    # "query":Ljava/lang/String;
    if-eqz v0, :cond_2

    array-length v6, v0

    if-ge v3, v6, :cond_2

    .line 555
    aget v1, v0, v3

    .line 562
    .end local v0    # "confidence":[F
    .end local v2    # "data":Ljava/util/ArrayList;
    .end local v3    # "i":I
    :cond_2
    sget-object v6, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onResult: \""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\" confidence level: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 565
    const/4 v6, 0x0

    cmpl-float v6, v1, v6

    if-lez v6, :cond_4

    const/high16 v6, 0x3f000000    # 0.5f

    cmpg-float v6, v1, v6

    if-gez v6, :cond_4

    .line 566
    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    const v7, 0x7f080311

    invoke-static {v6, v7}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$1200(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;I)V

    goto :goto_0

    .line 551
    .restart local v0    # "confidence":[F
    .restart local v2    # "data":Ljava/util/ArrayList;
    .restart local v3    # "i":I
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 569
    .end local v0    # "confidence":[F
    .end local v2    # "data":Ljava/util/ArrayList;
    .end local v3    # "i":I
    :cond_4
    new-instance v6, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    invoke-direct {v6}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;-><init>()V

    sget-object v7, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_SEARCHING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    .line 570
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->state(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v6

    .line 571
    invoke-virtual {v6, v4}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->recognizedWords(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .line 572
    invoke-static {v7}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$000(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->listeningOverBluetooth(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v6

    const/high16 v7, 0x42c80000    # 100.0f

    mul-float/2addr v7, v1

    float-to-int v7, v7

    .line 573
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->confidenceLevel(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v6

    .line 574
    invoke-virtual {v6}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->build()Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    move-result-object v5

    .line 575
    .local v5, "response":Lcom/navdy/service/library/events/audio/VoiceSearchResponse;
    invoke-static {v5}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 576
    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v6, v4}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$1300(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Ljava/lang/String;)V

    .line 577
    if-eqz v4, :cond_0

    .line 578
    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v6, v4}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$1400(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onRmsChanged(F)V
    .locals 0
    .param p1, "rmsdB"    # F

    .prologue
    .line 377
    return-void
.end method

.method startTimeoutTimer()V
    .locals 4

    .prologue
    .line 349
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2$1;-><init>(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;)V

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 355
    return-void
.end method
