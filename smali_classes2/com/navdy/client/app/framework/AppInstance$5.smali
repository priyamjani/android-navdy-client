.class Lcom/navdy/client/app/framework/AppInstance$5;
.super Ljava/lang/Object;
.source "AppInstance.java"

# interfaces
.implements Lcom/navdy/client/app/framework/AppInstance$EventDispatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/AppInstance;->onDeviceChanged(Lcom/navdy/service/library/device/RemoteDevice;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/AppInstance;

.field final synthetic val$newDevice:Lcom/navdy/service/library/device/RemoteDevice;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/AppInstance;Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/AppInstance;

    .prologue
    .line 492
    iput-object p1, p0, Lcom/navdy/client/app/framework/AppInstance$5;->this$0:Lcom/navdy/client/app/framework/AppInstance;

    iput-object p2, p0, Lcom/navdy/client/app/framework/AppInstance$5;->val$newDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchEvent(Lcom/navdy/client/app/framework/AppInstance;Lcom/navdy/client/app/framework/AppInstance$Listener;)V
    .locals 1
    .param p1, "source"    # Lcom/navdy/client/app/framework/AppInstance;
    .param p2, "listener"    # Lcom/navdy/client/app/framework/AppInstance$Listener;

    .prologue
    .line 495
    iget-object v0, p0, Lcom/navdy/client/app/framework/AppInstance$5;->val$newDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-interface {p2, v0}, Lcom/navdy/client/app/framework/AppInstance$Listener;->onDeviceChanged(Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 496
    return-void
.end method

.method public bridge synthetic dispatchEvent(Lcom/navdy/service/library/util/Listenable;Lcom/navdy/service/library/util/Listenable$Listener;)V
    .locals 0

    .prologue
    .line 492
    check-cast p1, Lcom/navdy/client/app/framework/AppInstance;

    check-cast p2, Lcom/navdy/client/app/framework/AppInstance$Listener;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/framework/AppInstance$5;->dispatchEvent(Lcom/navdy/client/app/framework/AppInstance;Lcom/navdy/client/app/framework/AppInstance$Listener;)V

    return-void
.end method
