.class final Lcom/navdy/client/app/providers/NavdyContentProvider$2;
.super Ljava/lang/Object;
.source "NavdyContentProvider.java"

# interfaces
.implements Lcom/navdy/client/app/providers/NavdyContentProvider$CacheQuerier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/providers/NavdyContentProvider;->getCacheKeysForDestinationId(Landroid/content/ContentResolver;I)Ljava/util/ArrayList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$cr:Landroid/content/ContentResolver;


# direct methods
.method constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 762
    iput-object p1, p0, Lcom/navdy/client/app/providers/NavdyContentProvider$2;->val$cr:Landroid/content/ContentResolver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public runQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 765
    iget-object v0, p0, Lcom/navdy/client/app/providers/NavdyContentProvider$2;->val$cr:Landroid/content/ContentResolver;

    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATION_CACHE_CONTENT_URI:Landroid/net/Uri;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
