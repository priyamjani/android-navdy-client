.class Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$9;
.super Ljava/lang/Object;
.source "BaseGoogleMapFragment.java"

# interfaces
.implements Lcom/google/android/gms/maps/OnMapReadyCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->moveMapInternal(Lcom/google/android/gms/maps/CameraUpdate;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

.field final synthetic val$animate:Z

.field final synthetic val$cameraUpdate:Lcom/google/android/gms/maps/CameraUpdate;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;ZLcom/google/android/gms/maps/CameraUpdate;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 530
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$9;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    iput-boolean p2, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$9;->val$animate:Z

    iput-object p3, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$9;->val$cameraUpdate:Lcom/google/android/gms/maps/CameraUpdate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapReady(Lcom/google/android/gms/maps/GoogleMap;)V
    .locals 3
    .param p1, "googleMap"    # Lcom/google/android/gms/maps/GoogleMap;

    .prologue
    .line 539
    :try_start_0
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$9;->val$animate:Z

    if-eqz v1, :cond_0

    .line 556
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$9;->val$cameraUpdate:Lcom/google/android/gms/maps/CameraUpdate;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 571
    :goto_0
    return-void

    .line 562
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$9;->val$cameraUpdate:Lcom/google/android/gms/maps/CameraUpdate;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 568
    :catch_0
    move-exception v0

    .line 569
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "moveMapInternal: Unable to move the map"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
