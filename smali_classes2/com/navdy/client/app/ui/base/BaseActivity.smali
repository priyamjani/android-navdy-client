.class public Lcom/navdy/client/app/ui/base/BaseActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "BaseActivity.java"

# interfaces
.implements Lcom/navdy/client/app/ui/SimpleDialogFragment$DialogProvider;


# static fields
.field private static final DIALOG:Ljava/lang/String; = "DIALOG"

.field private static final REQUEST_CALENDAR:I = 0x7

.field private static final REQUEST_CAMERA:I = 0x8

.field private static final REQUEST_CONTACTS:I = 0x3

.field private static final REQUEST_LOCATION:I = 0x1

.field private static final REQUEST_MICROPHONE:I = 0x2

.field private static final REQUEST_PHONE:I = 0x4

.field private static final REQUEST_SMS:I = 0x5

.field private static final REQUEST_STORAGE:I = 0x6

.field protected static final VERBOSE:Z

.field protected static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static toast:Landroid/widget/Toast;


# instance fields
.field private final calendarPermissionDenialHandlers:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final cameraPermissionDenialHandlers:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final contactPermissionDenialHandlers:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private destroying:Z

.field protected final handler:Landroid/os/Handler;

.field public imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

.field instanceStateIsSaved:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final locationPermissionDenialHandlers:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final lock:Ljava/lang/Object;

.field protected final logger:Lcom/navdy/service/library/log/Logger;

.field private mActionMode:Landroid/view/ActionMode;

.field private volatile mIsInForeground:Z

.field private final microphonePermissionDenialHandlers:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private obdDialog:Landroid/app/AlertDialog;

.field private final phonePermissionDenialHandlers:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private progressDialog:Landroid/app/ProgressDialog;

.field private questionDialog:Landroid/app/AlertDialog;

.field private requestRouteDialog:Landroid/app/AlertDialog;

.field private final smsPermissionDenialHandlers:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final storagePermissionDenialHandlers:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final thingsThatRequireCalendarPermission:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final thingsThatRequireCameraPermission:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final thingsThatRequireContactPermission:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final thingsThatRequireLocationPermission:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final thingsThatRequireMicrophonePermission:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final thingsThatRequirePhonePermission:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final thingsThatRequireSmsPermission:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final thingsThatRequireStoragePermission:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 93
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/base/BaseActivity;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/base/BaseActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 78
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    .line 95
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    .line 96
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->handler:Landroid/os/Handler;

    .line 101
    iput-boolean v3, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->mIsInForeground:Z

    .line 102
    iput-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->progressDialog:Landroid/app/ProgressDialog;

    .line 103
    iput-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->questionDialog:Landroid/app/AlertDialog;

    .line 104
    iput-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->requestRouteDialog:Landroid/app/AlertDialog;

    .line 105
    iput-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->obdDialog:Landroid/app/AlertDialog;

    .line 108
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->lock:Ljava/lang/Object;

    .line 109
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireLocationPermission:Ljava/util/Queue;

    .line 110
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->locationPermissionDenialHandlers:Ljava/util/Queue;

    .line 111
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireMicrophonePermission:Ljava/util/Queue;

    .line 112
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->microphonePermissionDenialHandlers:Ljava/util/Queue;

    .line 113
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireContactPermission:Ljava/util/Queue;

    .line 114
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->contactPermissionDenialHandlers:Ljava/util/Queue;

    .line 115
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireSmsPermission:Ljava/util/Queue;

    .line 116
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->smsPermissionDenialHandlers:Ljava/util/Queue;

    .line 117
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequirePhonePermission:Ljava/util/Queue;

    .line 118
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->phonePermissionDenialHandlers:Ljava/util/Queue;

    .line 119
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireStoragePermission:Ljava/util/Queue;

    .line 120
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->storagePermissionDenialHandlers:Ljava/util/Queue;

    .line 121
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireCalendarPermission:Ljava/util/Queue;

    .line 122
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->calendarPermissionDenialHandlers:Ljava/util/Queue;

    .line 123
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireCameraPermission:Ljava/util/Queue;

    .line 124
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->cameraPermissionDenialHandlers:Ljava/util/Queue;

    .line 126
    new-instance v0, Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/util/ImageCache;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    .line 211
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->instanceStateIsSaved:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/base/BaseActivity;Landroid/app/AlertDialog$Builder;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseActivity;
    .param p1, "x1"    # Landroid/app/AlertDialog$Builder;

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->createAndShowQuestionDialog(Landroid/app/AlertDialog$Builder;)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/base/BaseActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseActivity;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->questionDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$200()Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/navdy/client/app/ui/base/BaseActivity;->toast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$202(Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Landroid/widget/Toast;

    .prologue
    .line 78
    sput-object p0, Lcom/navdy/client/app/ui/base/BaseActivity;->toast:Landroid/widget/Toast;

    return-object p0
.end method

.method public static alreadyAskedForCalendarPermission()Z
    .locals 1

    .prologue
    .line 689
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getCalendarPermissions()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isThisTheFirstTimeWeRequestedThisPermission(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static alreadyAskedForCameraPermission()Z
    .locals 1

    .prologue
    .line 693
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getCameraPermissions()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isThisTheFirstTimeWeRequestedThisPermission(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static alreadyAskedForContactsPermission()Z
    .locals 1

    .prologue
    .line 673
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getContactsPermission()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isThisTheFirstTimeWeRequestedThisPermission(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static alreadyAskedForLocationPermission()Z
    .locals 1

    .prologue
    .line 665
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getLocationPermissions()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isThisTheFirstTimeWeRequestedThisPermission([Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static alreadyAskedForMicrophonePermission()Z
    .locals 1

    .prologue
    .line 669
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getMicrophonePermissions()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isThisTheFirstTimeWeRequestedThisPermission(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static alreadyAskedForPhonePermission()Z
    .locals 1

    .prologue
    .line 681
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getPhonePermissions()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isThisTheFirstTimeWeRequestedThisPermission([Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static alreadyAskedForSmsPermission()Z
    .locals 1

    .prologue
    .line 677
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getSmsPermissions()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isThisTheFirstTimeWeRequestedThisPermission([Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static alreadyAskedForStoragePermission()Z
    .locals 1

    .prologue
    .line 685
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getStoragePermissions()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isThisTheFirstTimeWeRequestedThisPermission([Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private createAndShowQuestionDialog(Landroid/app/AlertDialog$Builder;)V
    .locals 4
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isActivityDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 302
    :goto_0
    return-void

    .line 297
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->questionDialog:Landroid/app/AlertDialog;

    .line 298
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->questionDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 299
    :catch_0
    move-exception v0

    .line 300
    .local v0, "t":Ljava/lang/Throwable;
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error happened while trying to create question dialog: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static dismissProgressDialog(Landroid/app/Activity;Landroid/app/ProgressDialog;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "progressDialog"    # Landroid/app/ProgressDialog;

    .prologue
    .line 1090
    if-nez p1, :cond_0

    .line 1104
    :goto_0
    return-void

    .line 1094
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 1095
    invoke-virtual {p1}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0

    .line 1097
    :cond_1
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseActivity$9;

    invoke-direct {v0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity$9;-><init>(Landroid/app/ProgressDialog;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private endActionMode()V
    .locals 1

    .prologue
    .line 1178
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 1179
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 1181
    :cond_0
    return-void
.end method

.method private static getCalendarPermissions()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 447
    const-string v0, "android.permission.READ_CALENDAR"

    return-object v0
.end method

.method private static getCameraPermissions()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 452
    const-string v0, "android.permission.CAMERA"

    return-object v0
.end method

.method private static getContactsPermission()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 409
    const-string v0, "android.permission.READ_CONTACTS"

    return-object v0
.end method

.method private static getLocationPermissions()[Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 395
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v1, v0, v3

    .line 396
    .local v0, "permissions":[Ljava/lang/String;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x19

    if-le v1, v2, :cond_0

    .line 397
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    .end local v0    # "permissions":[Ljava/lang/String;
    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v1, v0, v3

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v1, v0, v4

    .line 400
    .restart local v0    # "permissions":[Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private static getMicrophonePermissions()Ljava/lang/String;
    .locals 1

    .prologue
    .line 404
    const-string v0, "android.permission.RECORD_AUDIO"

    return-object v0
.end method

.method private static getPhonePermissions()[Ljava/lang/String;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 424
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.permission.CALL_PHONE"

    aput-object v1, v0, v3

    .line 425
    .local v0, "permissions":[Ljava/lang/String;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x19

    if-le v1, v2, :cond_0

    .line 426
    const/4 v1, 0x5

    new-array v0, v1, [Ljava/lang/String;

    .end local v0    # "permissions":[Ljava/lang/String;
    const-string v1, "android.permission.CALL_PHONE"

    aput-object v1, v0, v3

    const-string v1, "android.permission.PROCESS_OUTGOING_CALLS"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "android.permission.READ_PHONE_STATE"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "android.permission.ANSWER_PHONE_CALLS"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "android.permission.READ_PHONE_NUMBERS"

    aput-object v2, v0, v1

    .line 432
    .restart local v0    # "permissions":[Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public static getScreenSize(Landroid/support/v4/app/FragmentActivity;)Landroid/graphics/Point;
    .locals 5
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1134
    if-eqz p0, :cond_0

    .line 1135
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 1141
    .local v1, "display":Landroid/view/Display;
    :goto_0
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 1142
    .local v2, "size":Landroid/graphics/Point;
    invoke-virtual {v1, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1143
    return-object v2

    .line 1137
    .end local v1    # "display":Landroid/view/Display;
    .end local v2    # "size":Landroid/graphics/Point;
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 1138
    .local v0, "context":Landroid/content/Context;
    const-string v4, "window"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    .line 1139
    .local v3, "wm":Landroid/view/WindowManager;
    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .restart local v1    # "display":Landroid/view/Display;
    goto :goto_0
.end method

.method private static getSmsPermissions()[Ljava/lang/String;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 414
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.permission.RECEIVE_SMS"

    aput-object v1, v0, v3

    .line 415
    .local v0, "permissions":[Ljava/lang/String;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x19

    if-le v1, v2, :cond_0

    .line 416
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    .end local v0    # "permissions":[Ljava/lang/String;
    const-string v1, "android.permission.RECEIVE_SMS"

    aput-object v1, v0, v3

    const-string v1, "android.permission.SEND_SMS"

    aput-object v1, v0, v4

    .line 419
    .restart local v0    # "permissions":[Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private static getStoragePermissions()[Ljava/lang/String;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 437
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v1, v0, v3

    .line 438
    .local v0, "permissions":[Ljava/lang/String;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x19

    if-le v1, v2, :cond_0

    .line 439
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    .end local v0    # "permissions":[Ljava/lang/String;
    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v1, v0, v3

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v1, v0, v4

    .line 442
    .restart local v0    # "permissions":[Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public static hideProgressDialog(Landroid/app/Activity;Landroid/app/ProgressDialog;)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "progressDialog"    # Landroid/app/ProgressDialog;

    .prologue
    .line 1069
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1077
    :cond_0
    :goto_0
    return-void

    .line 1076
    :cond_1
    invoke-static {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->dismissProgressDialog(Landroid/app/Activity;Landroid/app/ProgressDialog;)V

    goto :goto_0
.end method

.method public static isEnding(Landroid/app/Activity;)Z
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1147
    if-eqz p0, :cond_0

    .line 1148
    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1149
    invoke-virtual {p0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/navdy/client/app/ui/base/BaseActivity;

    .end local p0    # "activity":Landroid/app/Activity;
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->instanceStateIsSaved:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1151
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isThisTheFirstTimeWeRequestedThisPermission(Ljava/lang/String;)Z
    .locals 2
    .param p0, "permission"    # Ljava/lang/String;

    .prologue
    .line 651
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getPermissionsSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 652
    .local v0, "sp":Landroid/content/SharedPreferences;
    const/4 v1, 0x1

    invoke-interface {v0, p0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static isThisTheFirstTimeWeRequestedThisPermission([Ljava/lang/String;)Z
    .locals 8
    .param p0, "permissions"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 656
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getPermissionsSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 657
    .local v2, "sp":Landroid/content/SharedPreferences;
    const/4 v0, 0x1

    .line 658
    .local v0, "isFirstTime":Z
    array-length v6, p0

    move v5, v4

    :goto_0
    if-ge v5, v6, :cond_1

    aget-object v1, p0, v5

    .line 659
    .local v1, "permission":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-interface {v2, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_0

    move v0, v3

    .line 658
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    move v0, v4

    .line 659
    goto :goto_1

    .line 661
    .end local v1    # "permission":Ljava/lang/String;
    :cond_1
    return v0
.end method

.method public static requestLocationPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;Landroid/app/Activity;)V
    .locals 1
    .param p0, "doThingsThatRequireLocationPermission"    # Ljava/lang/Runnable;
    .param p1, "handleLocationPermissionDenial"    # Ljava/lang/Runnable;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 700
    if-eqz p2, :cond_0

    instance-of v0, p2, Lcom/navdy/client/app/ui/base/BaseActivity;

    if-eqz v0, :cond_0

    .line 701
    check-cast p2, Lcom/navdy/client/app/ui/base/BaseActivity;

    .end local p2    # "activity":Landroid/app/Activity;
    invoke-virtual {p2, p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->requestLocationPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 707
    :goto_0
    return-void

    .line 702
    .restart local p2    # "activity":Landroid/app/Activity;
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveLocationPermission()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 703
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 705
    :cond_1
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method private requestThisPermission(Ljava/lang/String;Ljava/util/Queue;Ljava/util/Queue;Ljava/lang/Runnable;Ljava/lang/Runnable;I)V
    .locals 7
    .param p1, "permission"    # Ljava/lang/String;
    .param p4, "doThingsThatRequireThisPermission"    # Ljava/lang/Runnable;
    .param p5, "handleThisPermissionDenial"    # Ljava/lang/Runnable;
    .param p6, "requestCode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;",
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;",
            "Ljava/lang/Runnable;",
            "Ljava/lang/Runnable;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 584
    .local p2, "thingsThatRequireThisPermission":Ljava/util/Queue;, "Ljava/util/Queue<Ljava/lang/Runnable;>;"
    .local p3, "thisPermissionDenialHandlers":Ljava/util/Queue;, "Ljava/util/Queue<Ljava/lang/Runnable;>;"
    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v1, v0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/navdy/client/app/ui/base/BaseActivity;->requestThisPermission([Ljava/lang/String;Ljava/util/Queue;Ljava/util/Queue;Ljava/lang/Runnable;Ljava/lang/Runnable;I)V

    .line 590
    return-void
.end method

.method private requestThisPermission([Ljava/lang/String;Ljava/util/Queue;Ljava/util/Queue;Ljava/lang/Runnable;Ljava/lang/Runnable;I)V
    .locals 12
    .param p1, "permissions"    # [Ljava/lang/String;
    .param p4, "doThingsThatRequireThisPermission"    # Ljava/lang/Runnable;
    .param p5, "handleThisPermissionDenial"    # Ljava/lang/Runnable;
    .param p6, "requestCode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;",
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;",
            "Ljava/lang/Runnable;",
            "Ljava/lang/Runnable;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 599
    .local p2, "thingsThatRequireThisPermission":Ljava/util/Queue;, "Ljava/util/Queue<Ljava/lang/Runnable;>;"
    .local p3, "thisPermissionDenialHandlers":Ljava/util/Queue;, "Ljava/util/Queue<Ljava/lang/Runnable;>;"
    const/4 v7, 0x0

    .line 600
    .local v7, "weDontHavePermission":Z
    array-length v9, p1

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v9, :cond_2

    aget-object v2, p1, v8

    .line 601
    .local v2, "permission":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10, v2}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    .line 602
    .local v1, "hasPermission":I
    if-nez v7, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    const/4 v7, 0x1

    .line 600
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 602
    :cond_1
    const/4 v7, 0x0

    goto :goto_1

    .line 604
    .end local v1    # "hasPermission":I
    .end local v2    # "permission":Ljava/lang/String;
    :cond_2
    if-eqz v7, :cond_b

    .line 607
    const/4 v4, 0x0

    .line 608
    .local v4, "showTheToast":Z
    array-length v9, p1

    const/4 v8, 0x0

    :goto_2
    if-ge v8, v9, :cond_6

    aget-object v2, p1, v8

    .line 609
    .restart local v2    # "permission":Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/client/app/ui/base/BaseActivity;->isThisTheFirstTimeWeRequestedThisPermission(Ljava/lang/String;)Z

    move-result v6

    .line 610
    .local v6, "thisIsTheFirstTimeWeAsk":Z
    invoke-static {p0, v2}, Landroid/support/v4/app/ActivityCompat;->shouldShowRequestPermissionRationale(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v3

    .line 611
    .local v3, "shouldShowRequestPermissionRationale":Z
    if-nez v4, :cond_3

    if-nez v3, :cond_5

    if-nez v6, :cond_5

    :cond_3
    const/4 v4, 0x1

    .line 613
    :goto_3
    if-eqz v6, :cond_4

    .line 614
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getPermissionsSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v5

    .line 615
    .local v5, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    const/4 v11, 0x0

    invoke-interface {v10, v2, v11}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    invoke-interface {v10}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 608
    .end local v5    # "sp":Landroid/content/SharedPreferences;
    :cond_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 611
    :cond_5
    const/4 v4, 0x0

    goto :goto_3

    .line 625
    .end local v2    # "permission":Ljava/lang/String;
    .end local v3    # "shouldShowRequestPermissionRationale":Z
    .end local v6    # "thisIsTheFirstTimeWeAsk":Z
    :cond_6
    if-eqz v4, :cond_7

    .line 627
    const v8, 0x7f0802ee

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v8, v9}, Lcom/navdy/client/app/ui/base/BaseActivity;->showLongToast(I[Ljava/lang/Object;)V

    .line 631
    :cond_7
    iget-object v9, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->lock:Ljava/lang/Object;

    monitor-enter v9

    .line 632
    if-eqz p4, :cond_8

    if-eqz p2, :cond_8

    .line 633
    :try_start_0
    move-object/from16 v0, p4

    invoke-interface {p2, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 635
    :cond_8
    if-eqz p5, :cond_9

    if-eqz p3, :cond_9

    .line 636
    move-object/from16 v0, p5

    invoke-interface {p3, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 638
    :cond_9
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 641
    move/from16 v0, p6

    invoke-static {p0, p1, v0}, Landroid/support/v4/app/ActivityCompat;->requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V

    .line 648
    .end local v4    # "showTheToast":Z
    :cond_a
    :goto_4
    return-void

    .line 638
    .restart local v4    # "showTheToast":Z
    :catchall_0
    move-exception v8

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v8

    .line 644
    .end local v4    # "showTheToast":Z
    :cond_b
    if-eqz p4, :cond_a

    .line 645
    invoke-interface/range {p4 .. p4}, Ljava/lang/Runnable;->run()V

    goto :goto_4
.end method

.method public static varargs showLongToast(I[Ljava/lang/Object;)V
    .locals 1
    .param p0, "messageResId"    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p1, "args"    # [Ljava/lang/Object;

    .prologue
    .line 1211
    const/4 v0, 0x1

    invoke-static {v0, p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->showToast(II[Ljava/lang/Object;)V

    .line 1212
    return-void
.end method

.method public static showProgressDialog(Landroid/app/Activity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 6
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "progressDialog"    # Landroid/app/ProgressDialog;

    .prologue
    .line 1029
    invoke-static {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isEnding(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v2, p1

    .line 1055
    .end local p1    # "progressDialog":Landroid/app/ProgressDialog;
    .local v2, "progressDialog":Landroid/app/ProgressDialog;
    :goto_0
    return-object v2

    .line 1034
    .end local v2    # "progressDialog":Landroid/app/ProgressDialog;
    .restart local p1    # "progressDialog":Landroid/app/ProgressDialog;
    :cond_0
    if-nez p1, :cond_1

    .line 1035
    :try_start_0
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .end local p1    # "progressDialog":Landroid/app/ProgressDialog;
    .restart local v2    # "progressDialog":Landroid/app/ProgressDialog;
    move-object p1, v2

    .line 1037
    .end local v2    # "progressDialog":Landroid/app/ProgressDialog;
    .restart local p1    # "progressDialog":Landroid/app/ProgressDialog;
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 1038
    const v3, 0x7f08028b

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1039
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 1040
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 1041
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    if-ne v3, v4, :cond_2

    .line 1042
    invoke-virtual {p1}, Landroid/app/ProgressDialog;->show()V

    :goto_1
    move-object v2, p1

    .line 1055
    .end local p1    # "progressDialog":Landroid/app/ProgressDialog;
    .restart local v2    # "progressDialog":Landroid/app/ProgressDialog;
    goto :goto_0

    .line 1044
    .end local v2    # "progressDialog":Landroid/app/ProgressDialog;
    .restart local p1    # "progressDialog":Landroid/app/ProgressDialog;
    :cond_2
    move-object v1, p1

    .line 1045
    .local v1, "finalProgressDialog":Landroid/app/ProgressDialog;
    new-instance v3, Lcom/navdy/client/app/ui/base/BaseActivity$8;

    invoke-direct {v3, v1}, Lcom/navdy/client/app/ui/base/BaseActivity$8;-><init>(Landroid/app/ProgressDialog;)V

    invoke-virtual {p0, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1052
    .end local v1    # "finalProgressDialog":Landroid/app/ProgressDialog;
    :catch_0
    move-exception v0

    .line 1053
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/navdy/client/app/ui/base/BaseActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to show progress dialog. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static varargs showShortToast(I[Ljava/lang/Object;)V
    .locals 1
    .param p0, "messageResId"    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p1, "args"    # [Ljava/lang/Object;

    .prologue
    .line 1219
    const/4 v0, 0x0

    invoke-static {v0, p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->showToast(II[Ljava/lang/Object;)V

    .line 1220
    return-void
.end method

.method private static varargs showToast(II[Ljava/lang/Object;)V
    .locals 1
    .param p0, "length"    # I
    .param p1, "messageResId"    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 1228
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseActivity$11;

    invoke-direct {v0, p1, p2, p0}, Lcom/navdy/client/app/ui/base/BaseActivity$11;-><init>(I[Ljava/lang/Object;I)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SystemUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1240
    return-void
.end method

.method public static weHaveAllPermissions()Z
    .locals 1

    .prologue
    .line 461
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveNotificationPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveContactsPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 465
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveLocationPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveMicrophonePermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveSmsPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 471
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHavePhonePermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 473
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveStoragePermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 475
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveCalendarPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static weHaveCalendarPermission()Z
    .locals 2

    .prologue
    .line 519
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getCalendarPermissions()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveThisPermission(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static weHaveCameraPermission()Z
    .locals 2

    .prologue
    .line 523
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getCameraPermissions()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveThisPermission(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static weHaveContactsPermission()Z
    .locals 2

    .prologue
    .line 503
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getContactsPermission()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveThisPermission(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static weHaveLocationPermission()Z
    .locals 2

    .prologue
    .line 495
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getLocationPermissions()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveThisPermission(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static weHaveMicrophonePermission()Z
    .locals 2

    .prologue
    .line 499
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getMicrophonePermissions()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveThisPermission(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static weHaveNotificationPermission()Z
    .locals 5

    .prologue
    .line 485
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 486
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 487
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const-string v4, "enabled_notification_listeners"

    invoke-static {v0, v4}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 488
    .local v2, "enabledNotificationListeners":Ljava/lang/String;
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 491
    .local v3, "packageName":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static weHavePhonePermission()Z
    .locals 2

    .prologue
    .line 511
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getPhonePermissions()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveThisPermission(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static weHaveSmsPermission()Z
    .locals 2

    .prologue
    .line 507
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getSmsPermissions()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveThisPermission(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static weHaveStoragePermission()Z
    .locals 2

    .prologue
    .line 515
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getStoragePermissions()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveThisPermission(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static weHaveThisPermission(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "permission"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 556
    if-nez p0, :cond_0

    .line 557
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object p0

    .line 559
    :cond_0
    if-nez p0, :cond_2

    .line 563
    :cond_1
    :goto_0
    return v1

    .line 562
    :cond_2
    invoke-static {p0, p1}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 563
    .local v0, "hasPermission":I
    if-nez v0, :cond_1

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static weHaveThisPermission(Landroid/content/Context;[Ljava/lang/String;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "permissions"    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 542
    const/4 v0, 0x1

    .line 543
    .local v0, "hasPermissions":Z
    array-length v4, p1

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, p1, v3

    .line 544
    .local v1, "permission":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-static {p0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveThisPermission(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v0, 0x1

    .line 543
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    move v0, v2

    .line 544
    goto :goto_1

    .line 546
    .end local v1    # "permission":Ljava/lang/String;
    :cond_1
    return v0
.end method


# virtual methods
.method public createDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "id"    # I
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 225
    if-eqz p2, :cond_3

    const-string v5, "title"

    .line 226
    invoke-virtual {p2, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_0

    const-string v5, "message"

    .line 227
    invoke-virtual {p2, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 228
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 229
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 230
    const-string v5, "title"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 231
    .local v3, "title":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 232
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 233
    :cond_1
    const-string v5, "message"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 234
    .local v1, "message":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 235
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 237
    :cond_2
    const-string v5, "positive_button_title"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 238
    .local v2, "positiveButtonTitle":Ljava/lang/String;
    if-eqz v2, :cond_4

    .line 239
    invoke-virtual {v0, v2, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 243
    :goto_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    .line 245
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v1    # "message":Ljava/lang/String;
    .end local v2    # "positiveButtonTitle":Ljava/lang/String;
    .end local v3    # "title":Ljava/lang/String;
    :cond_3
    return-object v4

    .line 241
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    .restart local v1    # "message":Ljava/lang/String;
    .restart local v2    # "positiveButtonTitle":Ljava/lang/String;
    .restart local v3    # "title":Ljava/lang/String;
    :cond_4
    const v5, 0x7f08031e

    invoke-virtual {p0, v5}, Lcom/navdy/client/app/ui/base/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public dismissProgressDialog()V
    .locals 1

    .prologue
    .line 1083
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-static {p0, v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->dismissProgressDialog(Landroid/app/Activity;Landroid/app/ProgressDialog;)V

    .line 1084
    return-void
.end method

.method public dismissQuestionDialog()V
    .locals 2

    .prologue
    .line 1110
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->questionDialog:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 1124
    :goto_0
    return-void

    .line 1114
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 1115
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->questionDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    goto :goto_0

    .line 1117
    :cond_1
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseActivity$10;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/base/BaseActivity$10;-><init>(Lcom/navdy/client/app/ui/base/BaseActivity;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public getScreenSize()Landroid/graphics/Point;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1128
    invoke-static {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->getScreenSize(Landroid/support/v4/app/FragmentActivity;)Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method public hideProgressDialog()V
    .locals 1

    .prologue
    .line 1062
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-static {p0, v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->hideProgressDialog(Landroid/app/Activity;Landroid/app/ProgressDialog;)V

    .line 1063
    return-void
.end method

.method public hideSystemUI()V
    .locals 3

    .prologue
    .line 1248
    invoke-static {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isEnding(Landroid/app/Activity;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1269
    :cond_0
    :goto_0
    return-void

    .line 1256
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1257
    .local v1, "window":Landroid/view/Window;
    if-eqz v1, :cond_0

    .line 1258
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 1259
    .local v0, "decorView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1260
    const/16 v2, 0x1504

    invoke-virtual {v0, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_0
.end method

.method public hideSystemUiDependingOnOrientation()V
    .locals 4

    .prologue
    .line 1293
    invoke-static {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isEnding(Landroid/app/Activity;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1308
    :cond_0
    :goto_0
    return-void

    .line 1297
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1298
    .local v1, "resources":Landroid/content/res/Resources;
    if-eqz v1, :cond_0

    .line 1299
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1300
    .local v0, "configuration":Landroid/content/res/Configuration;
    if-eqz v0, :cond_0

    .line 1301
    iget v2, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 1302
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->hideSystemUI()V

    goto :goto_0

    .line 1304
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->showSystemUI()V

    goto :goto_0
.end method

.method public isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->destroying:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInForeground()Z
    .locals 1

    .prologue
    .line 249
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->mIsInForeground:Z

    return v0
.end method

.method public loadImage(II)V
    .locals 2
    .param p1, "imgId"    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param
    .param p2, "drawableRes"    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 1321
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1322
    .local v0, "imgView":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 1323
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-static {v0, p2, v1}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    .line 1325
    :cond_0
    return-void
.end method

.method public onActionModeFinished(Landroid/view/ActionMode;)V
    .locals 1
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    .line 1188
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onActionModeFinished(Landroid/view/ActionMode;)V

    .line 1189
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->mActionMode:Landroid/view/ActionMode;

    .line 1190
    return-void
.end method

.method public onActionModeStarted(Landroid/view/ActionMode;)V
    .locals 0
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    .line 1166
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onActionModeStarted(Landroid/view/ActionMode;)V

    .line 1167
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->mActionMode:Landroid/view/ActionMode;

    .line 1168
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 203
    :try_start_0
    invoke-static {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isEnding(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 204
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onBackPressed()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 206
    :catch_0
    move-exception v0

    .line 207
    .local v0, "e":Ljava/lang/IllegalStateException;
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Unable to perform back pressed"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onCreate"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->instanceStateIsSaved:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 132
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 133
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onDestroy"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 179
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->destroying:Z

    .line 182
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireLocationPermission:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 183
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->locationPermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 184
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireContactPermission:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 185
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->contactPermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 186
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireSmsPermission:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 187
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->smsPermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 188
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequirePhonePermission:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 189
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->phonePermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 190
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireStoragePermission:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 191
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->storagePermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 193
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->dismissProgressDialog()V

    .line 194
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->dismissQuestionDialog()V

    .line 196
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/util/ImageCache;->clearCache()V

    .line 197
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onDestroy()V

    .line 198
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onPause"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->mIsInForeground:Z

    .line 156
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->hideProgressDialog()V

    .line 161
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->endActionMode()V

    .line 164
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->requiresBus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->unregister(Ljava/lang/Object;)V

    .line 167
    :cond_0
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onPause()V

    .line 168
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 5
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "grantResults"    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 832
    packed-switch p1, :pswitch_data_0

    .line 978
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/AppCompatActivity;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 980
    :goto_0
    return-void

    .line 834
    :pswitch_0
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 835
    :try_start_0
    array-length v2, p3

    if-lez v2, :cond_0

    const/4 v2, 0x0

    aget v2, p3, v2

    if-nez v2, :cond_0

    .line 836
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireLocationPermission:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 837
    .local v1, "r":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    .line 850
    .end local v1    # "r":Ljava/lang/Runnable;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 840
    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 841
    .local v0, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "Type"

    const-string v4, "Location"

    invoke-virtual {v0, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 842
    const-string v2, "Permission_Rejected"

    invoke-static {v2, v0}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 844
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->locationPermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 845
    .restart local v1    # "r":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_2

    .line 848
    .end local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "r":Ljava/lang/Runnable;
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireLocationPermission:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 849
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->locationPermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 850
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 853
    :pswitch_1
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 854
    :try_start_2
    array-length v2, p3

    if-lez v2, :cond_2

    const/4 v2, 0x0

    aget v2, p3, v2

    if-nez v2, :cond_2

    .line 855
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireMicrophonePermission:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 856
    .restart local v1    # "r":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_3

    .line 869
    .end local v1    # "r":Ljava/lang/Runnable;
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v2

    .line 859
    :cond_2
    :try_start_3
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 860
    .restart local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "Type"

    const-string v4, "Microphone"

    invoke-virtual {v0, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 861
    const-string v2, "Permission_Rejected"

    invoke-static {v2, v0}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 863
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->microphonePermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 864
    .restart local v1    # "r":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_4

    .line 867
    .end local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "r":Ljava/lang/Runnable;
    :cond_3
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireMicrophonePermission:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 868
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->microphonePermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 869
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/16 :goto_0

    .line 872
    :pswitch_2
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 873
    :try_start_4
    array-length v2, p3

    if-lez v2, :cond_4

    const/4 v2, 0x0

    aget v2, p3, v2

    if-nez v2, :cond_4

    .line 874
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireContactPermission:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 875
    .restart local v1    # "r":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_5

    .line 888
    .end local v1    # "r":Ljava/lang/Runnable;
    :catchall_2
    move-exception v2

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v2

    .line 878
    :cond_4
    :try_start_5
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 879
    .restart local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "Type"

    const-string v4, "Contacts"

    invoke-virtual {v0, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 880
    const-string v2, "Permission_Rejected"

    invoke-static {v2, v0}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 882
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->contactPermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 883
    .restart local v1    # "r":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_6

    .line 886
    .end local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "r":Ljava/lang/Runnable;
    :cond_5
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireContactPermission:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 887
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->contactPermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 888
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto/16 :goto_0

    .line 891
    :pswitch_3
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 892
    :try_start_6
    array-length v2, p3

    if-lez v2, :cond_6

    const/4 v2, 0x0

    aget v2, p3, v2

    if-nez v2, :cond_6

    .line 893
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireSmsPermission:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 894
    .restart local v1    # "r":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_7

    .line 907
    .end local v1    # "r":Ljava/lang/Runnable;
    :catchall_3
    move-exception v2

    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    throw v2

    .line 897
    :cond_6
    :try_start_7
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 898
    .restart local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "Type"

    const-string v4, "Sms"

    invoke-virtual {v0, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 899
    const-string v2, "Permission_Rejected"

    invoke-static {v2, v0}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 901
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->smsPermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 902
    .restart local v1    # "r":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_8

    .line 905
    .end local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "r":Ljava/lang/Runnable;
    :cond_7
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireSmsPermission:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 906
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->smsPermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 907
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto/16 :goto_0

    .line 910
    :pswitch_4
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 911
    :try_start_8
    array-length v2, p3

    if-lez v2, :cond_8

    const/4 v2, 0x0

    aget v2, p3, v2

    if-nez v2, :cond_8

    .line 912
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequirePhonePermission:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 913
    .restart local v1    # "r":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_9

    .line 926
    .end local v1    # "r":Ljava/lang/Runnable;
    :catchall_4
    move-exception v2

    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    throw v2

    .line 916
    :cond_8
    :try_start_9
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 917
    .restart local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "Type"

    const-string v4, "Phone"

    invoke-virtual {v0, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 918
    const-string v2, "Permission_Rejected"

    invoke-static {v2, v0}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 920
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->phonePermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 921
    .restart local v1    # "r":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_a

    .line 924
    .end local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "r":Ljava/lang/Runnable;
    :cond_9
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequirePhonePermission:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 925
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->phonePermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 926
    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    goto/16 :goto_0

    .line 929
    :pswitch_5
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 930
    :try_start_a
    array-length v2, p3

    if-lez v2, :cond_a

    const/4 v2, 0x0

    aget v2, p3, v2

    if-nez v2, :cond_a

    .line 931
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireStoragePermission:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 932
    .restart local v1    # "r":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_b

    .line 945
    .end local v1    # "r":Ljava/lang/Runnable;
    :catchall_5
    move-exception v2

    monitor-exit v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    throw v2

    .line 935
    :cond_a
    :try_start_b
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 936
    .restart local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "Type"

    const-string v4, "Storage"

    invoke-virtual {v0, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 937
    const-string v2, "Permission_Rejected"

    invoke-static {v2, v0}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 939
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->storagePermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 940
    .restart local v1    # "r":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_c

    .line 943
    .end local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "r":Ljava/lang/Runnable;
    :cond_b
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireStoragePermission:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 944
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->storagePermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 945
    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    goto/16 :goto_0

    .line 948
    :pswitch_6
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 949
    :try_start_c
    array-length v2, p3

    if-lez v2, :cond_c

    const/4 v2, 0x0

    aget v2, p3, v2

    if-nez v2, :cond_c

    .line 950
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireCalendarPermission:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 951
    .restart local v1    # "r":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_d

    .line 960
    .end local v1    # "r":Ljava/lang/Runnable;
    :catchall_6
    move-exception v2

    monitor-exit v3
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    throw v2

    .line 954
    :cond_c
    :try_start_d
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->calendarPermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 955
    .restart local v1    # "r":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_e

    .line 958
    .end local v1    # "r":Ljava/lang/Runnable;
    :cond_d
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireCalendarPermission:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 959
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->calendarPermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 960
    monitor-exit v3
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    goto/16 :goto_0

    .line 963
    :pswitch_7
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 964
    :try_start_e
    array-length v2, p3

    if-lez v2, :cond_e

    const/4 v2, 0x0

    aget v2, p3, v2

    if-nez v2, :cond_e

    .line 965
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireCameraPermission:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 966
    .restart local v1    # "r":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_f

    .line 975
    .end local v1    # "r":Ljava/lang/Runnable;
    :catchall_7
    move-exception v2

    monitor-exit v3
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_7

    throw v2

    .line 969
    :cond_e
    :try_start_f
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->cameraPermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 970
    .restart local v1    # "r":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_10

    .line 973
    .end local v1    # "r":Ljava/lang/Runnable;
    :cond_f
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireCameraPermission:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 974
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->cameraPermissionDenialHandlers:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 975
    monitor-exit v3
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_7

    goto/16 :goto_0

    .line 832
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 143
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onResume()V

    .line 144
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onResume"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 145
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->mIsInForeground:Z

    .line 147
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->requiresBus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 150
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;Landroid/os/PersistableBundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;
    .param p2, "outPersistentState"    # Landroid/os/PersistableBundle;

    .prologue
    .line 215
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->instanceStateIsSaved:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 216
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/AppCompatActivity;->onSaveInstanceState(Landroid/os/Bundle;Landroid/os/PersistableBundle;)V

    .line 217
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onStart"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 138
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onStart()V

    .line 139
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onStop"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 173
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onStop()V

    .line 174
    return-void
.end method

.method public openBrowserFor(Landroid/net/Uri;)V
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1312
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1313
    .local v0, "browserIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1318
    .end local v0    # "browserIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 1314
    :catch_0
    move-exception v2

    .line 1315
    .local v2, "e":Landroid/content/ActivityNotFoundException;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 1316
    .local v1, "context":Landroid/content/Context;
    const v3, 0x7f0802fc

    const/4 v4, 0x1

    invoke-static {v1, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public openBtConnectionDialog()V
    .locals 4

    .prologue
    .line 989
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Opening BT connection dialog"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 991
    const/4 v1, 0x0

    .line 992
    .local v1, "isFirstLaunch":Z
    instance-of v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    if-eqz v2, :cond_0

    .line 993
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "instance of FirstLaunchActivity"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 994
    const/4 v1, 0x1

    .line 997
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/settings/BluetoothPairActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 998
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "1"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 999
    const-string v2, "USER_WAS_IN_FIRST_LAUNCH"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1000
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->startActivity(Landroid/content/Intent;)V

    .line 1001
    return-void
.end method

.method protected openMarketAppFor(Ljava/lang/String;)V
    .locals 5
    .param p1, "appPackageName"    # Ljava/lang/String;

    .prologue
    .line 1198
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "market://details?id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1202
    :goto_0
    return-void

    .line 1199
    :catch_0
    move-exception v0

    .line 1200
    .local v0, "anfe":Landroid/content/ActivityNotFoundException;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "https://play.google.com/store/apps/details?id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public requestCalendarPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 7
    .param p1, "doThingsThatRequireCalendarPermission"    # Ljava/lang/Runnable;
    .param p2, "handleCalendarPermissionDenial"    # Ljava/lang/Runnable;

    .prologue
    .line 807
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getCalendarPermissions()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireCalendarPermission:Ljava/util/Queue;

    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->calendarPermissionDenialHandlers:Ljava/util/Queue;

    const/4 v6, 0x7

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/navdy/client/app/ui/base/BaseActivity;->requestThisPermission(Ljava/lang/String;Ljava/util/Queue;Ljava/util/Queue;Ljava/lang/Runnable;Ljava/lang/Runnable;I)V

    .line 813
    return-void
.end method

.method public requestCameraPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 7
    .param p1, "doThingsThatRequireCameraPermission"    # Ljava/lang/Runnable;
    .param p2, "handleCameraPermissionDenial"    # Ljava/lang/Runnable;

    .prologue
    .line 821
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getCameraPermissions()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireCameraPermission:Ljava/util/Queue;

    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->cameraPermissionDenialHandlers:Ljava/util/Queue;

    const/16 v6, 0x8

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/navdy/client/app/ui/base/BaseActivity;->requestThisPermission(Ljava/lang/String;Ljava/util/Queue;Ljava/util/Queue;Ljava/lang/Runnable;Ljava/lang/Runnable;I)V

    .line 827
    return-void
.end method

.method public requestContactsPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 7
    .param p1, "doThingsThatRequireContactPermission"    # Ljava/lang/Runnable;
    .param p2, "handleContactPermissionDenial"    # Ljava/lang/Runnable;

    .prologue
    .line 751
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getContactsPermission()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireContactPermission:Ljava/util/Queue;

    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->contactPermissionDenialHandlers:Ljava/util/Queue;

    const/4 v6, 0x3

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/navdy/client/app/ui/base/BaseActivity;->requestThisPermission(Ljava/lang/String;Ljava/util/Queue;Ljava/util/Queue;Ljava/lang/Runnable;Ljava/lang/Runnable;I)V

    .line 757
    return-void
.end method

.method public requestLocationPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 7
    .param p1, "doThingsThatRequireLocationPermission"    # Ljava/lang/Runnable;
    .param p2, "handleLocationPermissionDenial"    # Ljava/lang/Runnable;

    .prologue
    .line 715
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getLocationPermissions()[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireLocationPermission:Ljava/util/Queue;

    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->locationPermissionDenialHandlers:Ljava/util/Queue;

    new-instance v4, Lcom/navdy/client/app/ui/base/BaseActivity$7;

    invoke-direct {v4, p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity$7;-><init>(Lcom/navdy/client/app/ui/base/BaseActivity;Ljava/lang/Runnable;)V

    const/4 v6, 0x1

    move-object v0, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/navdy/client/app/ui/base/BaseActivity;->requestThisPermission([Ljava/lang/String;Ljava/util/Queue;Ljava/util/Queue;Ljava/lang/Runnable;Ljava/lang/Runnable;I)V

    .line 729
    return-void
.end method

.method public requestMicrophonePermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 7
    .param p1, "doThingsThatRequireMicrophonePermission"    # Ljava/lang/Runnable;
    .param p2, "handleMicrophonePermissionDenial"    # Ljava/lang/Runnable;

    .prologue
    .line 737
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getMicrophonePermissions()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireMicrophonePermission:Ljava/util/Queue;

    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->microphonePermissionDenialHandlers:Ljava/util/Queue;

    const/4 v6, 0x2

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/navdy/client/app/ui/base/BaseActivity;->requestThisPermission(Ljava/lang/String;Ljava/util/Queue;Ljava/util/Queue;Ljava/lang/Runnable;Ljava/lang/Runnable;I)V

    .line 743
    return-void
.end method

.method public requestNotificationPermission()V
    .locals 2

    .prologue
    .line 983
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveNotificationPermission()Z

    move-result v0

    if-nez v0, :cond_0

    .line 984
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->startActivity(Landroid/content/Intent;)V

    .line 986
    :cond_0
    return-void
.end method

.method public requestPhonePermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 7
    .param p1, "doThingsThatRequirePhonePermission"    # Ljava/lang/Runnable;
    .param p2, "handlePhonePermissionDenial"    # Ljava/lang/Runnable;

    .prologue
    .line 779
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getPhonePermissions()[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequirePhonePermission:Ljava/util/Queue;

    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->phonePermissionDenialHandlers:Ljava/util/Queue;

    const/4 v6, 0x4

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/navdy/client/app/ui/base/BaseActivity;->requestThisPermission([Ljava/lang/String;Ljava/util/Queue;Ljava/util/Queue;Ljava/lang/Runnable;Ljava/lang/Runnable;I)V

    .line 785
    return-void
.end method

.method public requestSmsPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 7
    .param p1, "doThingsThatRequireSmsPermission"    # Ljava/lang/Runnable;
    .param p2, "handleSmsPermissionDenial"    # Ljava/lang/Runnable;

    .prologue
    .line 765
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getSmsPermissions()[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireSmsPermission:Ljava/util/Queue;

    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->smsPermissionDenialHandlers:Ljava/util/Queue;

    const/4 v6, 0x5

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/navdy/client/app/ui/base/BaseActivity;->requestThisPermission([Ljava/lang/String;Ljava/util/Queue;Ljava/util/Queue;Ljava/lang/Runnable;Ljava/lang/Runnable;I)V

    .line 771
    return-void
.end method

.method public requestStoragePermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 7
    .param p1, "doThingsThatRequireStoragePermission"    # Ljava/lang/Runnable;
    .param p2, "handleStoragePermissionDenial"    # Ljava/lang/Runnable;

    .prologue
    .line 793
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->getStoragePermissions()[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->thingsThatRequireStoragePermission:Ljava/util/Queue;

    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->storagePermissionDenialHandlers:Ljava/util/Queue;

    const/4 v6, 0x6

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/navdy/client/app/ui/base/BaseActivity;->requestThisPermission([Ljava/lang/String;Ljava/util/Queue;Ljava/util/Queue;Ljava/lang/Runnable;Ljava/lang/Runnable;I)V

    .line 799
    return-void
.end method

.method public requiresBus()Z
    .locals 1

    .prologue
    .line 1158
    const/4 v0, 0x1

    return v0
.end method

.method public setStatusBarColor(I)V
    .locals 3
    .param p1, "color"    # I

    .prologue
    .line 1009
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 1010
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 1011
    .local v0, "window":Landroid/view/Window;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 1012
    const/high16 v1, -0x80000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1013
    invoke-virtual {v0, p1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 1015
    .end local v0    # "window":Landroid/view/Window;
    :cond_0
    return-void
.end method

.method public showExplicitObdSettingForBlacklistDialog()V
    .locals 5

    .prologue
    .line 334
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 336
    .local v1, "sharedPrefs":Landroid/content/SharedPreferences;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 337
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f080457

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f080456

    .line 338
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f080152

    new-instance v4, Lcom/navdy/client/app/ui/base/BaseActivity$5;

    invoke-direct {v4, p0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity$5;-><init>(Lcom/navdy/client/app/ui/base/BaseActivity;Landroid/content/SharedPreferences;)V

    .line 339
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0800e5

    new-instance v4, Lcom/navdy/client/app/ui/base/BaseActivity$4;

    invoke-direct {v4, p0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity$4;-><init>(Lcom/navdy/client/app/ui/base/BaseActivity;Landroid/content/SharedPreferences;)V

    .line 353
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 361
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isActivityDestroyed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 369
    :goto_0
    return-void

    .line 364
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->obdDialog:Landroid/app/AlertDialog;

    if-eqz v2, :cond_1

    .line 365
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->obdDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->hide()V

    .line 367
    :cond_1
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->obdDialog:Landroid/app/AlertDialog;

    .line 368
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->obdDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method public showObdDialogIfCarHasBeenAddedToBlacklist()V
    .locals 3

    .prologue
    .line 372
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/base/BaseActivity$6;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/base/BaseActivity$6;-><init>(Lcom/navdy/client/app/ui/base/BaseActivity;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 390
    return-void
.end method

.method public showProgressDialog()V
    .locals 1

    .prologue
    .line 1021
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-static {p0, v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->showProgressDialog(Landroid/app/Activity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->progressDialog:Landroid/app/ProgressDialog;

    .line 1022
    return-void
.end method

.method public showQuestionDialog(IIIILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 3
    .param p1, "titleResId"    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p2, "messageResId"    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p3, "positiveResId"    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p4, "negativeResId"    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p5, "dialogClickListener"    # Landroid/content/DialogInterface$OnClickListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p6, "onCancelListener"    # Landroid/content/DialogInterface$OnCancelListener;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 271
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 272
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 273
    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 274
    invoke-virtual {v1, p3, p5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 275
    invoke-virtual {v1, p4, p5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 276
    if-eqz p6, :cond_0

    .line 277
    invoke-virtual {v0, p6}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 280
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v1, v2, :cond_1

    .line 281
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->createAndShowQuestionDialog(Landroid/app/AlertDialog$Builder;)V

    .line 290
    :goto_0
    return-void

    .line 283
    :cond_1
    new-instance v1, Lcom/navdy/client/app/ui/base/BaseActivity$1;

    invoke-direct {v1, p0, v0}, Lcom/navdy/client/app/ui/base/BaseActivity$1;-><init>(Lcom/navdy/client/app/ui/base/BaseActivity;Landroid/app/AlertDialog$Builder;)V

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public showRequestNewRouteDialog(Ljava/lang/Runnable;)V
    .locals 5
    .param p1, "requestRouteRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 305
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isActivityDestroyed()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getInstance()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->isInOneOfTheActiveTripStates()Z

    move-result v2

    if-nez v2, :cond_1

    .line 306
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 331
    :goto_0
    return-void

    .line 310
    :cond_1
    new-instance v1, Lcom/navdy/client/app/ui/base/BaseActivity$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity$2;-><init>(Lcom/navdy/client/app/ui/base/BaseActivity;Ljava/lang/Runnable;)V

    .line 316
    .local v1, "dialogClickListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 317
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0803d7

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0803d6

    .line 318
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f080505

    .line 319
    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0800e6

    new-instance v4, Lcom/navdy/client/app/ui/base/BaseActivity$3;

    invoke-direct {v4, p0}, Lcom/navdy/client/app/ui/base/BaseActivity$3;-><init>(Lcom/navdy/client/app/ui/base/BaseActivity;)V

    .line 320
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 326
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->requestRouteDialog:Landroid/app/AlertDialog;

    if-eqz v2, :cond_2

    .line 327
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->requestRouteDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->hide()V

    .line 329
    :cond_2
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->requestRouteDialog:Landroid/app/AlertDialog;

    .line 330
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity;->requestRouteDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method public showSimpleDialog(III)V
    .locals 2
    .param p1, "dialogId"    # I
    .param p2, "titleId"    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p3, "messageId"    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 253
    invoke-virtual {p0, p2}, Lcom/navdy/client/app/ui/base/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p3}, Lcom/navdy/client/app/ui/base/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    .line 254
    return-void
.end method

.method public showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "dialogId"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 259
    .local v2, "manager":Landroid/app/FragmentManager;
    if-eqz v2, :cond_0

    .line 260
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 261
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "title"

    invoke-virtual {v0, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const-string v3, "message"

    invoke-virtual {v0, v3, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    invoke-static {p1, v0}, Lcom/navdy/client/app/ui/SimpleDialogFragment;->newInstance(ILandroid/os/Bundle;)Lcom/navdy/client/app/ui/SimpleDialogFragment;

    move-result-object v1

    .line 264
    .local v1, "dialogFragment":Lcom/navdy/client/app/ui/SimpleDialogFragment;
    if-eqz v1, :cond_0

    .line 265
    const-string v3, "DIALOG"

    invoke-virtual {v1, v2, v3}, Lcom/navdy/client/app/ui/SimpleDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 268
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "dialogFragment":Lcom/navdy/client/app/ui/SimpleDialogFragment;
    :cond_0
    return-void
.end method

.method public showSystemUI()V
    .locals 3

    .prologue
    .line 1275
    invoke-static {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isEnding(Landroid/app/Activity;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1286
    :cond_0
    :goto_0
    return-void

    .line 1279
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1280
    .local v1, "window":Landroid/view/Window;
    if-eqz v1, :cond_0

    .line 1281
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 1282
    .local v0, "decorView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1283
    const/16 v2, 0x100

    invoke-virtual {v0, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_0
.end method

.method public weHaveThisPermission(Ljava/lang/String;)Z
    .locals 1
    .param p1, "permission"    # Ljava/lang/String;

    .prologue
    .line 532
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveThisPermission(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
