.class final Lcom/navdy/client/app/ui/homescreen/CalendarUtils$2;
.super Ljava/lang/Object;
.source "CalendarUtils.java"

# interfaces
.implements Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$NavigableCoordinateWorkerFinishCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->readCalendarEvent(Lcom/navdy/client/app/ui/homescreen/CalendarUtils$CalendarEventReader;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$callback:Lcom/navdy/client/app/ui/homescreen/CalendarUtils$CalendarEventReader;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/CalendarUtils$CalendarEventReader;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/CalendarUtils$2;->val$callback:Lcom/navdy/client/app/ui/homescreen/CalendarUtils$CalendarEventReader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinish(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/CalendarEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 235
    .local p1, "results":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/CalendarEvent;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 236
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/client/app/framework/models/CalendarEvent;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 237
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/CalendarEvent;

    .line 238
    .local v0, "event":Lcom/navdy/client/app/framework/models/CalendarEvent;
    iget-object v2, v0, Lcom/navdy/client/app/framework/models/CalendarEvent;->latLng:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v2}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(Lcom/google/android/gms/maps/model/LatLng;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 239
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 240
    invoke-interface {p1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 243
    .end local v0    # "event":Lcom/navdy/client/app/framework/models/CalendarEvent;
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Here found coordinates for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Calendar Event(s)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 244
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/CalendarUtils$2;->val$callback:Lcom/navdy/client/app/ui/homescreen/CalendarUtils$CalendarEventReader;

    invoke-interface {v2, p1}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils$CalendarEventReader;->onCalendarEventsRead(Ljava/util/List;)V

    .line 246
    invoke-static {p1}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->access$100(Ljava/util/List;)V

    .line 247
    return-void
.end method
