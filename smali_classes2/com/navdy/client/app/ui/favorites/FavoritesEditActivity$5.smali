.class Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$5;
.super Landroid/os/AsyncTask;
.source "FavoritesEditActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->saveChanges()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

.field final synthetic val$labelString:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    .prologue
    .line 270
    iput-object p1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$5;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$5;->val$labelString:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Boolean;
    .locals 5
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    .line 273
    const/4 v1, 0x0

    .line 275
    .local v1, "success":Z
    iget-object v3, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$5;->val$labelString:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 276
    iget-object v3, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$5;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$000(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/Destination;->getFavoriteLabel()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$5;->val$labelString:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 278
    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$5;->cancel(Z)Z

    .line 285
    :cond_0
    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    return-object v2

    .line 280
    :cond_1
    iget-object v3, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$5;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$000(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$5;->val$labelString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/framework/models/Destination;->setFavoriteLabel(Ljava/lang/String;)V

    .line 281
    iget-object v3, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$5;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$000(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/Destination;->updateOnlyFavoriteFieldsInDb()I

    move-result v0

    .line 282
    .local v0, "nbRows":I
    if-lez v0, :cond_2

    move v1, v2

    :goto_1
    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 270
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$5;->doInBackground([Ljava/lang/Object;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "success"    # Ljava/lang/Boolean;

    .prologue
    const/4 v2, 0x0

    .line 291
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$5;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$000(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    iget v0, v0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 292
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 293
    const v0, 0x7f0804bd

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->showLongToast(I[Ljava/lang/Object;)V

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 295
    :cond_1
    const v0, 0x7f0804bc

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->showLongToast(I[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 270
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$5;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
