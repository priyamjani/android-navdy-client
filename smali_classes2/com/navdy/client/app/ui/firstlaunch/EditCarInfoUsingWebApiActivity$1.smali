.class Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;
.super Lcom/navdy/client/app/framework/util/CarMdCallBack;
.source "EditCarInfoUsingWebApiActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->initCarSelectorScreen()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

.field final synthetic val$carMdClient:Lcom/navdy/client/app/framework/util/CarMdClient;

.field final synthetic val$make:Landroid/widget/Spinner;

.field final synthetic val$makeString:Ljava/lang/String;

.field final synthetic val$model:Landroid/widget/Spinner;

.field final synthetic val$modelAdapter:Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

.field final synthetic val$year:Landroid/widget/Spinner;

.field final synthetic val$yearAdapter:Lorg/droidparts/adapter/widget/StringSpinnerAdapter;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;Lcom/navdy/client/app/framework/util/CarMdClient;Landroid/widget/Spinner;Landroid/widget/Spinner;Lorg/droidparts/adapter/widget/StringSpinnerAdapter;Landroid/widget/Spinner;Lorg/droidparts/adapter/widget/StringSpinnerAdapter;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;->val$carMdClient:Lcom/navdy/client/app/framework/util/CarMdClient;

    iput-object p3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;->val$make:Landroid/widget/Spinner;

    iput-object p4, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;->val$year:Landroid/widget/Spinner;

    iput-object p5, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;->val$yearAdapter:Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

    iput-object p6, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;->val$model:Landroid/widget/Spinner;

    iput-object p7, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;->val$modelAdapter:Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

    iput-object p8, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;->val$makeString:Ljava/lang/String;

    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/CarMdCallBack;-><init>()V

    return-void
.end method


# virtual methods
.method public processFailure(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/Throwable;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    new-instance v1, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1$2;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 170
    return-void
.end method

.method public processResponse(Lokhttp3/ResponseBody;)V
    .locals 6
    .param p1, "responseBody"    # Lokhttp3/ResponseBody;

    .prologue
    .line 124
    :try_start_0
    invoke-virtual {p1}, Lokhttp3/ResponseBody;->string()Ljava/lang/String;

    move-result-object v1

    .line 125
    .local v1, "jsonString":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->access$000(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Car MD: response: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 127
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;->val$carMdClient:Lcom/navdy/client/app/framework/util/CarMdClient;

    invoke-virtual {v3, v1}, Lcom/navdy/client/app/framework/util/CarMdClient;->getListFromJsonResponse(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 128
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 129
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    new-instance v4, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1$1;

    invoke-direct {v4, p0, v2}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;Ljava/util/ArrayList;)V

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 157
    .end local v1    # "jsonString":Ljava/lang/String;
    .end local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    return-void

    .line 150
    .restart local v1    # "jsonString":Ljava/lang/String;
    .restart local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "Empty make list!"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    .end local v1    # "jsonString":Ljava/lang/String;
    .end local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 153
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->access$200(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Car MD: unable to get the list of makes out of the response body: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 155
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;->processFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
