.class Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$2;
.super Ljava/lang/Object;
.source "AppSetupActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setUpViewPager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 200
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 164
    return-void
.end method

.method public onPageSelected(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    const/4 v4, 0x0

    .line 168
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-static {v2, v4}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->access$502(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;Z)Z

    .line 169
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/SystemUtils;->dismissKeyboard(Landroid/app/Activity;)V

    .line 170
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->getCurrentScreen()Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    move-result-object v0

    .line 172
    .local v0, "currentScreen":Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;
    if-eqz v0, :cond_2

    iget-object v2, v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v3, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->PROFILE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-ne v2, v3, :cond_2

    .line 174
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->access$600(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)V

    .line 189
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    .line 190
    invoke-static {v2}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->access$900(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v3, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->BLUETOOTH:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    .line 192
    invoke-virtual {v3}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 194
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-static {v2, v4}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->access$902(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;Z)Z

    .line 195
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->access$1000(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)V

    .line 197
    :cond_1
    return-void

    .line 177
    :cond_2
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->access$700(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)V

    .line 178
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-virtual {v2, p1}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->updateIllustration(I)V

    .line 179
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->access$800(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 180
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    .line 182
    invoke-static {v2}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->access$800(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->getItem(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;

    .line 183
    .local v1, "frag":Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;
    if-eqz v1, :cond_0

    .line 184
    invoke-virtual {v1}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->showNormal()V

    goto :goto_0
.end method
