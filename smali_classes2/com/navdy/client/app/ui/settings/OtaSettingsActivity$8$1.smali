.class Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8$1;
.super Ljava/lang/Object;
.source "OtaSettingsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;

.field final synthetic val$state:Lcom/navdy/client/ota/OTAUpdateService$State;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;Lcom/navdy/client/ota/OTAUpdateService$State;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;

    .prologue
    .line 733
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8$1;->this$1:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;

    iput-object p2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8$1;->val$state:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;
    .param p2, "isPositive"    # I

    .prologue
    .line 736
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 737
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8$1;->this$1:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->access$1200(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;Z)V

    .line 738
    sget-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->FEATURE_MODE_BETA:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->setFeatureMode(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;)V

    .line 739
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8$1;->this$1:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8$1;->val$state:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->access$1300(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;Lcom/navdy/client/ota/OTAUpdateService$State;)V

    .line 743
    :goto_0
    return-void

    .line 741
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8$1;->this$1:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->access$1200(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;Z)V

    goto :goto_0
.end method
