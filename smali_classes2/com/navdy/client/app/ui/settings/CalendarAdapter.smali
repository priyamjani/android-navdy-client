.class Lcom/navdy/client/app/ui/settings/CalendarAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "CalendarAdapter.java"


# instance fields
.field private appContext:Landroid/content/Context;

.field private final calendars:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Calendar;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 25
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarAdapter;->appContext:Landroid/content/Context;

    .line 28
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarAdapter;->appContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->listCalendars(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarAdapter;->calendars:Ljava/util/ArrayList;

    .line 29
    return-void
.end method


# virtual methods
.method public getItem(I)Lcom/navdy/client/app/framework/models/Calendar;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarAdapter;->calendars:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarAdapter;->calendars:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Calendar;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarAdapter;->calendars:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarAdapter;->calendars:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/settings/CalendarAdapter;->getItem(I)Lcom/navdy/client/app/framework/models/Calendar;

    move-result-object v0

    .line 38
    .local v0, "calendar":Lcom/navdy/client/app/framework/models/Calendar;
    if-eqz v0, :cond_0

    iget-wide v2, v0, Lcom/navdy/client/app/framework/models/Calendar;->id:J

    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/settings/CalendarAdapter;->getItem(I)Lcom/navdy/client/app/framework/models/Calendar;

    move-result-object v0

    .line 44
    .local v0, "calendar":Lcom/navdy/client/app/framework/models/Calendar;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/navdy/client/app/framework/models/Calendar;->type:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->getValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 3
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 78
    invoke-virtual {p0, p2}, Lcom/navdy/client/app/ui/settings/CalendarAdapter;->getItem(I)Lcom/navdy/client/app/framework/models/Calendar;

    move-result-object v0

    .line 79
    .local v0, "calendar":Lcom/navdy/client/app/framework/models/Calendar;
    if-eqz v0, :cond_0

    .line 80
    sget-object v1, Lcom/navdy/client/app/ui/settings/CalendarAdapter$2;->$SwitchMap$com$navdy$client$app$framework$models$Calendar$CalendarListItemType:[I

    iget-object v2, v0, Lcom/navdy/client/app/framework/models/Calendar;->type:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 96
    .end local p1    # "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 85
    .restart local p1    # "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :pswitch_1
    instance-of v1, p1, Lcom/navdy/client/app/ui/settings/CalendarTitleViewHolder;

    if-eqz v1, :cond_0

    .line 86
    check-cast p1, Lcom/navdy/client/app/ui/settings/CalendarTitleViewHolder;

    .end local p1    # "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    iget-object v1, v0, Lcom/navdy/client/app/framework/models/Calendar;->displayName:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/navdy/client/app/ui/settings/CalendarTitleViewHolder;->setName(Ljava/lang/String;)V

    goto :goto_0

    .line 90
    .restart local p1    # "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :pswitch_2
    instance-of v1, p1, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;

    if-eqz v1, :cond_0

    .line 91
    check-cast p1, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;

    .end local p1    # "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p1, v0}, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->setCalendar(Lcom/navdy/client/app/framework/models/Calendar;)V

    goto :goto_0

    .line 80
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    const/4 v4, 0x0

    .line 56
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/CalendarAdapter;->appContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 57
    .local v1, "vi":Landroid/view/LayoutInflater;
    sget-object v2, Lcom/navdy/client/app/ui/settings/CalendarAdapter$2;->$SwitchMap$com$navdy$client$app$framework$models$Calendar$CalendarListItemType:[I

    invoke-static {p2}, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->fromValue(I)Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 73
    const/4 v2, 0x0

    :goto_0
    return-object v2

    .line 59
    :pswitch_0
    const v2, 0x7f0300fd

    invoke-virtual {v1, v2, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 60
    .local v0, "v":Landroid/view/View;
    new-instance v2, Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder;

    new-instance v3, Lcom/navdy/client/app/ui/settings/CalendarAdapter$1;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/settings/CalendarAdapter$1;-><init>(Lcom/navdy/client/app/ui/settings/CalendarAdapter;)V

    invoke-direct {v2, v0, v3}, Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder;-><init>(Landroid/view/View;Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    .line 67
    .end local v0    # "v":Landroid/view/View;
    :pswitch_1
    const v2, 0x7f030031

    invoke-virtual {v1, v2, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 68
    .restart local v0    # "v":Landroid/view/View;
    new-instance v2, Lcom/navdy/client/app/ui/settings/CalendarTitleViewHolder;

    invoke-direct {v2, v0}, Lcom/navdy/client/app/ui/settings/CalendarTitleViewHolder;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 70
    .end local v0    # "v":Landroid/view/View;
    :pswitch_2
    const v2, 0x7f030030

    invoke-virtual {v1, v2, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 71
    .restart local v0    # "v":Landroid/view/View;
    new-instance v2, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;

    invoke-direct {v2, v0}, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 57
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
