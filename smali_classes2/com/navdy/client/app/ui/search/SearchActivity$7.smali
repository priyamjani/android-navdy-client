.class Lcom/navdy/client/app/ui/search/SearchActivity$7;
.super Ljava/lang/Object;
.source "SearchActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/SearchActivity;->runAutoCompleteSearch(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

.field final synthetic val$constraint:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/SearchActivity;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 545
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7;->val$constraint:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 548
    invoke-static {}, Lcom/navdy/client/app/framework/util/ContactsManager;->getInstance()Lcom/navdy/client/app/framework/util/ContactsManager;

    move-result-object v1

    .line 549
    .local v1, "contactsManager":Lcom/navdy/client/app/framework/util/ContactsManager;
    iget-object v4, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7;->val$constraint:Ljava/lang/String;

    sget-object v5, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->ADDRESS:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    .line 550
    invoke-static {v5}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v5

    .line 549
    invoke-virtual {v1, v4, v5}, Lcom/navdy/client/app/framework/util/ContactsManager;->getContactsAndLoadPhotos(Ljava/lang/String;Ljava/util/EnumSet;)Ljava/util/List;

    move-result-object v0

    .line 552
    .local v0, "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    new-instance v3, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    invoke-direct {v3, p0, v0}, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity$7;Ljava/util/List;)V

    .line 604
    .local v3, "googleSearchListener":Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;
    new-instance v2, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    iget-object v4, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v4}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1600(Lcom/navdy/client/app/ui/search/SearchActivity;)Landroid/os/Handler;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;-><init>(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;Landroid/os/Handler;)V

    .line 605
    .local v2, "googlePlacesSearch":Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    iget-object v4, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7;->val$constraint:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runQueryAutoCompleteWebApi(Ljava/lang/String;)V

    .line 606
    return-void
.end method
