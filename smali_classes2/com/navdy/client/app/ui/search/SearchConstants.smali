.class public Lcom/navdy/client/app/ui/search/SearchConstants;
.super Ljava/lang/Object;
.source "SearchConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;,
        Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;
    }
.end annotation


# static fields
.field public static final ACTION_SEARCH:Ljava/lang/String; = "com.navdy.client.actions.SEARCH_ACTION"

.field public static final ACTION_SEND:Ljava/lang/String; = "ACTION_SEND"

.field public static final CONTACT_CODE:I = 0x4

.field public static final DETAILS_CODE:I = 0x5

.field public static final EXTRA_IS_VOICE_SEARCH:Ljava/lang/String; = "extra_is_voice_search"

.field public static final FAVORITE_CODE:I = 0x1

.field public static final GOOGLE_ASSISTANT_SHORT_URL:Ljava/lang/String; = "https://g.co"

.field public static final GOOGLE_MAPS_SHORT_URL:Ljava/lang/String; = "https://goo.gl/maps/"

.field public static final GOOGLE_MAPS_TO_SYNTAX:Ljava/lang/String; = "to"

.field public static final GOOGLE_MAPS_VIA_SYNTAX:Ljava/lang/String; = "via"

.field public static final GOOGLE_SEARCH_SHORT_URL:Ljava/lang/String; = "https://www.google.com/search"

.field public static final HOME_CODE:I = 0x2

.field public static final SEARCH_CODE:I = 0x0

.field public static final SEARCH_RESULT:Ljava/lang/String; = "search_result"

.field public static final SEARCH_SOURCE_AUTOCOMPLETE:I = 0x0

.field public static final SEARCH_SOURCE_CONTACT:I = 0x1

.field public static final SEARCH_SOURCE_MAP:I = 0x3

.field public static final SEARCH_SOURCE_RESULT:I = 0x2

.field public static final SEARCH_TYPE_EXTRA:Ljava/lang/String; = "search_type"

.field public static final WORK_CODE:I = 0x3

.field public static final YELP_MAPS_URL:Ljava/lang/String; = "https://www.yelp.com/biz/"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
