.class public final Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest;
.super Lcom/squareup/wire/Message;
.source "PhoneStatusRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest$Builder;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 11
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest$Builder;)V
    .locals 0
    .param p1, "builder"    # Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest$Builder;

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 14
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 15
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest$Builder;Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest$1;

    .prologue
    .line 7
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest;-><init>(Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 19
    instance-of v0, p1, Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return v0
.end method
