.class public final enum Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;
.super Ljava/lang/Enum;
.source "VoiceAssistResponse.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/VoiceAssistResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VoiceAssistState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

.field public static final enum VOICE_ASSIST_AVAILABLE:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

.field public static final enum VOICE_ASSIST_LISTENING:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

.field public static final enum VOICE_ASSIST_NOT_AVAILABLE:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

.field public static final enum VOICE_ASSIST_STOPPED:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 76
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    const-string v1, "VOICE_ASSIST_AVAILABLE"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->VOICE_ASSIST_AVAILABLE:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    .line 80
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    const-string v1, "VOICE_ASSIST_NOT_AVAILABLE"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->VOICE_ASSIST_NOT_AVAILABLE:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    .line 84
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    const-string v1, "VOICE_ASSIST_LISTENING"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->VOICE_ASSIST_LISTENING:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    .line 90
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    const-string v1, "VOICE_ASSIST_STOPPED"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->VOICE_ASSIST_STOPPED:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    .line 70
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->VOICE_ASSIST_AVAILABLE:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->VOICE_ASSIST_NOT_AVAILABLE:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->VOICE_ASSIST_LISTENING:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->VOICE_ASSIST_STOPPED:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->$VALUES:[Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 95
    iput p3, p0, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->value:I

    .line 96
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 70
    const-class v0, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->$VALUES:[Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->value:I

    return v0
.end method
