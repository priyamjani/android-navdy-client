.class public final enum Lcom/navdy/service/library/events/audio/MusicDataSource;
.super Ljava/lang/Enum;
.source "MusicDataSource.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicDataSource;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/audio/MusicDataSource;

.field public static final enum MUSIC_SOURCE_ANDROID_INTENT:Lcom/navdy/service/library/events/audio/MusicDataSource;

.field public static final enum MUSIC_SOURCE_MEDIA_CONTROLLER:Lcom/navdy/service/library/events/audio/MusicDataSource;

.field public static final enum MUSIC_SOURCE_NONE:Lcom/navdy/service/library/events/audio/MusicDataSource;

.field public static final enum MUSIC_SOURCE_OS_NOTIFICATION:Lcom/navdy/service/library/events/audio/MusicDataSource;

.field public static final enum MUSIC_SOURCE_PANDORA_API:Lcom/navdy/service/library/events/audio/MusicDataSource;

.field public static final enum MUSIC_SOURCE_SPOTIFY_INTENT:Lcom/navdy/service/library/events/audio/MusicDataSource;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicDataSource;

    const-string v1, "MUSIC_SOURCE_NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/MusicDataSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_NONE:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicDataSource;

    const-string v1, "MUSIC_SOURCE_ANDROID_INTENT"

    invoke-direct {v0, v1, v3, v4}, Lcom/navdy/service/library/events/audio/MusicDataSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_ANDROID_INTENT:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 11
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicDataSource;

    const-string v1, "MUSIC_SOURCE_MEDIA_CONTROLLER"

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/service/library/events/audio/MusicDataSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_MEDIA_CONTROLLER:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicDataSource;

    const-string v1, "MUSIC_SOURCE_OS_NOTIFICATION"

    invoke-direct {v0, v1, v5, v6}, Lcom/navdy/service/library/events/audio/MusicDataSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_OS_NOTIFICATION:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 13
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicDataSource;

    const-string v1, "MUSIC_SOURCE_SPOTIFY_INTENT"

    invoke-direct {v0, v1, v6, v7}, Lcom/navdy/service/library/events/audio/MusicDataSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_SPOTIFY_INTENT:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 14
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicDataSource;

    const-string v1, "MUSIC_SOURCE_PANDORA_API"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v7, v2}, Lcom/navdy/service/library/events/audio/MusicDataSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_PANDORA_API:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 7
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/service/library/events/audio/MusicDataSource;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_NONE:Lcom/navdy/service/library/events/audio/MusicDataSource;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_ANDROID_INTENT:Lcom/navdy/service/library/events/audio/MusicDataSource;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_MEDIA_CONTROLLER:Lcom/navdy/service/library/events/audio/MusicDataSource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_OS_NOTIFICATION:Lcom/navdy/service/library/events/audio/MusicDataSource;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_SPOTIFY_INTENT:Lcom/navdy/service/library/events/audio/MusicDataSource;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_PANDORA_API:Lcom/navdy/service/library/events/audio/MusicDataSource;

    aput-object v1, v0, v7

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicDataSource;->$VALUES:[Lcom/navdy/service/library/events/audio/MusicDataSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19
    iput p3, p0, Lcom/navdy/service/library/events/audio/MusicDataSource;->value:I

    .line 20
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicDataSource;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/audio/MusicDataSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/MusicDataSource;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/audio/MusicDataSource;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicDataSource;->$VALUES:[Lcom/navdy/service/library/events/audio/MusicDataSource;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/audio/MusicDataSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/audio/MusicDataSource;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/navdy/service/library/events/audio/MusicDataSource;->value:I

    return v0
.end method
