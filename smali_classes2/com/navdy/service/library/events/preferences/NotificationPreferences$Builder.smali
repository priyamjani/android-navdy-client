.class public final Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NotificationPreferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/preferences/NotificationPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/preferences/NotificationPreferences;",
        ">;"
    }
.end annotation


# instance fields
.field public enabled:Ljava/lang/Boolean;

.field public readAloud:Ljava/lang/Boolean;

.field public serial_number:Ljava/lang/Long;

.field public settings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/notification/NotificationSetting;",
            ">;"
        }
    .end annotation
.end field

.field public showContent:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 108
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 112
    if-nez p1, :cond_0

    .line 118
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->serial_number:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->serial_number:Ljava/lang/Long;

    .line 114
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->settings:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->settings:Ljava/util/List;

    .line 115
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->enabled:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->enabled:Ljava/lang/Boolean;

    .line 116
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->readAloud:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->readAloud:Ljava/lang/Boolean;

    .line 117
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->showContent:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->showContent:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    .locals 2

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->checkRequiredFields()V

    .line 164
    new-instance v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/preferences/NotificationPreferences;-><init>(Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;Lcom/navdy/service/library/events/preferences/NotificationPreferences$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    move-result-object v0

    return-object v0
.end method

.method public enabled(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;
    .locals 0
    .param p1, "enabled"    # Ljava/lang/Boolean;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->enabled:Ljava/lang/Boolean;

    .line 142
    return-object p0
.end method

.method public readAloud(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;
    .locals 0
    .param p1, "readAloud"    # Ljava/lang/Boolean;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->readAloud:Ljava/lang/Boolean;

    .line 150
    return-object p0
.end method

.method public serial_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;
    .locals 0
    .param p1, "serial_number"    # Ljava/lang/Long;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->serial_number:Ljava/lang/Long;

    .line 126
    return-object p0
.end method

.method public settings(Ljava/util/List;)Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/notification/NotificationSetting;",
            ">;)",
            "Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;"
        }
    .end annotation

    .prologue
    .line 133
    .local p1, "settings":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/notification/NotificationSetting;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->settings:Ljava/util/List;

    .line 134
    return-object p0
.end method

.method public showContent(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;
    .locals 0
    .param p1, "showContent"    # Ljava/lang/Boolean;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->showContent:Ljava/lang/Boolean;

    .line 158
    return-object p0
.end method
