.class public final enum Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;
.super Ljava/lang/Enum;
.source "InputPreferences.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/preferences/InputPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DialSensitivity"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

.field public static final enum DIAL_SENSITIVITY_LOW:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

.field public static final enum DIAL_SENSITIVITY_STANDARD:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 137
    new-instance v0, Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    const-string v1, "DIAL_SENSITIVITY_STANDARD"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;->DIAL_SENSITIVITY_STANDARD:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    .line 138
    new-instance v0, Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    const-string v1, "DIAL_SENSITIVITY_LOW"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;->DIAL_SENSITIVITY_LOW:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    .line 135
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    sget-object v1, Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;->DIAL_SENSITIVITY_STANDARD:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;->DIAL_SENSITIVITY_LOW:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;->$VALUES:[Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 142
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 143
    iput p3, p0, Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;->value:I

    .line 144
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 135
    const-class v0, Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;->$VALUES:[Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;->value:I

    return v0
.end method
