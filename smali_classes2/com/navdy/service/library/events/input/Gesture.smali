.class public final enum Lcom/navdy/service/library/events/input/Gesture;
.super Ljava/lang/Enum;
.source "Gesture.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/input/Gesture;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/input/Gesture;

.field public static final enum GESTURE_FINGER:Lcom/navdy/service/library/events/input/Gesture;

.field public static final enum GESTURE_FINGER_DOWN:Lcom/navdy/service/library/events/input/Gesture;

.field public static final enum GESTURE_FINGER_TO_PINCH:Lcom/navdy/service/library/events/input/Gesture;

.field public static final enum GESTURE_FINGER_UP:Lcom/navdy/service/library/events/input/Gesture;

.field public static final enum GESTURE_FIST:Lcom/navdy/service/library/events/input/Gesture;

.field public static final enum GESTURE_FIST_TO_HAND:Lcom/navdy/service/library/events/input/Gesture;

.field public static final enum GESTURE_HAND_DOWN:Lcom/navdy/service/library/events/input/Gesture;

.field public static final enum GESTURE_HAND_TO_FIST:Lcom/navdy/service/library/events/input/Gesture;

.field public static final enum GESTURE_HAND_UP:Lcom/navdy/service/library/events/input/Gesture;

.field public static final enum GESTURE_PALM:Lcom/navdy/service/library/events/input/Gesture;

.field public static final enum GESTURE_PINCH:Lcom/navdy/service/library/events/input/Gesture;

.field public static final enum GESTURE_PINCH_TO_FINGER:Lcom/navdy/service/library/events/input/Gesture;

.field public static final enum GESTURE_SWIPE_DOWN:Lcom/navdy/service/library/events/input/Gesture;

.field public static final enum GESTURE_SWIPE_LEFT:Lcom/navdy/service/library/events/input/Gesture;

.field public static final enum GESTURE_SWIPE_RIGHT:Lcom/navdy/service/library/events/input/Gesture;

.field public static final enum GESTURE_SWIPE_UP:Lcom/navdy/service/library/events/input/Gesture;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/input/Gesture;

    const-string v1, "GESTURE_SWIPE_LEFT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/service/library/events/input/Gesture;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_LEFT:Lcom/navdy/service/library/events/input/Gesture;

    .line 13
    new-instance v0, Lcom/navdy/service/library/events/input/Gesture;

    const-string v1, "GESTURE_SWIPE_RIGHT"

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/service/library/events/input/Gesture;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_RIGHT:Lcom/navdy/service/library/events/input/Gesture;

    .line 14
    new-instance v0, Lcom/navdy/service/library/events/input/Gesture;

    const-string v1, "GESTURE_SWIPE_UP"

    invoke-direct {v0, v1, v5, v6}, Lcom/navdy/service/library/events/input/Gesture;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_UP:Lcom/navdy/service/library/events/input/Gesture;

    .line 15
    new-instance v0, Lcom/navdy/service/library/events/input/Gesture;

    const-string v1, "GESTURE_SWIPE_DOWN"

    invoke-direct {v0, v1, v6, v7}, Lcom/navdy/service/library/events/input/Gesture;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_DOWN:Lcom/navdy/service/library/events/input/Gesture;

    .line 20
    new-instance v0, Lcom/navdy/service/library/events/input/Gesture;

    const-string v1, "GESTURE_FINGER_UP"

    invoke-direct {v0, v1, v7, v8}, Lcom/navdy/service/library/events/input/Gesture;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER_UP:Lcom/navdy/service/library/events/input/Gesture;

    .line 24
    new-instance v0, Lcom/navdy/service/library/events/input/Gesture;

    const-string v1, "GESTURE_FINGER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/service/library/events/input/Gesture;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER:Lcom/navdy/service/library/events/input/Gesture;

    .line 28
    new-instance v0, Lcom/navdy/service/library/events/input/Gesture;

    const-string v1, "GESTURE_FINGER_DOWN"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/input/Gesture;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER_DOWN:Lcom/navdy/service/library/events/input/Gesture;

    .line 32
    new-instance v0, Lcom/navdy/service/library/events/input/Gesture;

    const-string v1, "GESTURE_FINGER_TO_PINCH"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/input/Gesture;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER_TO_PINCH:Lcom/navdy/service/library/events/input/Gesture;

    .line 36
    new-instance v0, Lcom/navdy/service/library/events/input/Gesture;

    const-string v1, "GESTURE_PINCH_TO_FINGER"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/input/Gesture;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_PINCH_TO_FINGER:Lcom/navdy/service/library/events/input/Gesture;

    .line 40
    new-instance v0, Lcom/navdy/service/library/events/input/Gesture;

    const-string v1, "GESTURE_PINCH"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/input/Gesture;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_PINCH:Lcom/navdy/service/library/events/input/Gesture;

    .line 45
    new-instance v0, Lcom/navdy/service/library/events/input/Gesture;

    const-string v1, "GESTURE_HAND_UP"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/input/Gesture;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_HAND_UP:Lcom/navdy/service/library/events/input/Gesture;

    .line 49
    new-instance v0, Lcom/navdy/service/library/events/input/Gesture;

    const-string v1, "GESTURE_PALM"

    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/input/Gesture;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_PALM:Lcom/navdy/service/library/events/input/Gesture;

    .line 53
    new-instance v0, Lcom/navdy/service/library/events/input/Gesture;

    const-string v1, "GESTURE_HAND_TO_FIST"

    const/16 v2, 0xc

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/input/Gesture;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_HAND_TO_FIST:Lcom/navdy/service/library/events/input/Gesture;

    .line 57
    new-instance v0, Lcom/navdy/service/library/events/input/Gesture;

    const-string v1, "GESTURE_FIST"

    const/16 v2, 0xd

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/input/Gesture;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FIST:Lcom/navdy/service/library/events/input/Gesture;

    .line 61
    new-instance v0, Lcom/navdy/service/library/events/input/Gesture;

    const-string v1, "GESTURE_FIST_TO_HAND"

    const/16 v2, 0xe

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/input/Gesture;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FIST_TO_HAND:Lcom/navdy/service/library/events/input/Gesture;

    .line 65
    new-instance v0, Lcom/navdy/service/library/events/input/Gesture;

    const-string v1, "GESTURE_HAND_DOWN"

    const/16 v2, 0xf

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/input/Gesture;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_HAND_DOWN:Lcom/navdy/service/library/events/input/Gesture;

    .line 7
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/navdy/service/library/events/input/Gesture;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_LEFT:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_RIGHT:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_UP:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_DOWN:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER_UP:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER_DOWN:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER_TO_PINCH:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_PINCH_TO_FINGER:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_PINCH:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_HAND_UP:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_PALM:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_HAND_TO_FIST:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FIST:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FIST_TO_HAND:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_HAND_DOWN:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/events/input/Gesture;->$VALUES:[Lcom/navdy/service/library/events/input/Gesture;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 70
    iput p3, p0, Lcom/navdy/service/library/events/input/Gesture;->value:I

    .line 71
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/input/Gesture;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/input/Gesture;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/input/Gesture;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/input/Gesture;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/input/Gesture;->$VALUES:[Lcom/navdy/service/library/events/input/Gesture;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/input/Gesture;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/input/Gesture;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/navdy/service/library/events/input/Gesture;->value:I

    return v0
.end method
