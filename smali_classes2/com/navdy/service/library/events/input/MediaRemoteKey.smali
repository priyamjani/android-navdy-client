.class public final enum Lcom/navdy/service/library/events/input/MediaRemoteKey;
.super Ljava/lang/Enum;
.source "MediaRemoteKey.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/input/MediaRemoteKey;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/input/MediaRemoteKey;

.field public static final enum MEDIA_REMOTE_KEY_NEXT:Lcom/navdy/service/library/events/input/MediaRemoteKey;

.field public static final enum MEDIA_REMOTE_KEY_PLAY:Lcom/navdy/service/library/events/input/MediaRemoteKey;

.field public static final enum MEDIA_REMOTE_KEY_PREV:Lcom/navdy/service/library/events/input/MediaRemoteKey;

.field public static final enum MEDIA_REMOTE_KEY_SIRI:Lcom/navdy/service/library/events/input/MediaRemoteKey;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/input/MediaRemoteKey;

    const-string v1, "MEDIA_REMOTE_KEY_SIRI"

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/service/library/events/input/MediaRemoteKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/MediaRemoteKey;->MEDIA_REMOTE_KEY_SIRI:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/input/MediaRemoteKey;

    const-string v1, "MEDIA_REMOTE_KEY_PLAY"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/input/MediaRemoteKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/MediaRemoteKey;->MEDIA_REMOTE_KEY_PLAY:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    .line 11
    new-instance v0, Lcom/navdy/service/library/events/input/MediaRemoteKey;

    const-string v1, "MEDIA_REMOTE_KEY_NEXT"

    invoke-direct {v0, v1, v3, v4}, Lcom/navdy/service/library/events/input/MediaRemoteKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/MediaRemoteKey;->MEDIA_REMOTE_KEY_NEXT:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/input/MediaRemoteKey;

    const-string v1, "MEDIA_REMOTE_KEY_PREV"

    invoke-direct {v0, v1, v4, v6}, Lcom/navdy/service/library/events/input/MediaRemoteKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/MediaRemoteKey;->MEDIA_REMOTE_KEY_PREV:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    .line 7
    new-array v0, v6, [Lcom/navdy/service/library/events/input/MediaRemoteKey;

    sget-object v1, Lcom/navdy/service/library/events/input/MediaRemoteKey;->MEDIA_REMOTE_KEY_SIRI:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/input/MediaRemoteKey;->MEDIA_REMOTE_KEY_PLAY:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/input/MediaRemoteKey;->MEDIA_REMOTE_KEY_NEXT:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/input/MediaRemoteKey;->MEDIA_REMOTE_KEY_PREV:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/service/library/events/input/MediaRemoteKey;->$VALUES:[Lcom/navdy/service/library/events/input/MediaRemoteKey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 17
    iput p3, p0, Lcom/navdy/service/library/events/input/MediaRemoteKey;->value:I

    .line 18
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/input/MediaRemoteKey;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/input/MediaRemoteKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/input/MediaRemoteKey;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/input/MediaRemoteKey;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/input/MediaRemoteKey;->$VALUES:[Lcom/navdy/service/library/events/input/MediaRemoteKey;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/input/MediaRemoteKey;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/input/MediaRemoteKey;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/navdy/service/library/events/input/MediaRemoteKey;->value:I

    return v0
.end method
