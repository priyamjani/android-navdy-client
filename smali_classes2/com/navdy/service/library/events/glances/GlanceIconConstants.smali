.class public final enum Lcom/navdy/service/library/events/glances/GlanceIconConstants;
.super Ljava/lang/Enum;
.source "GlanceIconConstants.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/glances/GlanceIconConstants;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/glances/GlanceIconConstants;

.field public static final enum GLANCE_ICON_MESSAGE_SIDE_BLUE:Lcom/navdy/service/library/events/glances/GlanceIconConstants;

.field public static final enum GLANCE_ICON_NAVDY_MAIN:Lcom/navdy/service/library/events/glances/GlanceIconConstants;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/glances/GlanceIconConstants;

    const-string v1, "GLANCE_ICON_NAVDY_MAIN"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/service/library/events/glances/GlanceIconConstants;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceIconConstants;->GLANCE_ICON_NAVDY_MAIN:Lcom/navdy/service/library/events/glances/GlanceIconConstants;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/glances/GlanceIconConstants;

    const-string v1, "GLANCE_ICON_MESSAGE_SIDE_BLUE"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/service/library/events/glances/GlanceIconConstants;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceIconConstants;->GLANCE_ICON_MESSAGE_SIDE_BLUE:Lcom/navdy/service/library/events/glances/GlanceIconConstants;

    .line 7
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/service/library/events/glances/GlanceIconConstants;

    sget-object v1, Lcom/navdy/service/library/events/glances/GlanceIconConstants;->GLANCE_ICON_NAVDY_MAIN:Lcom/navdy/service/library/events/glances/GlanceIconConstants;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/glances/GlanceIconConstants;->GLANCE_ICON_MESSAGE_SIDE_BLUE:Lcom/navdy/service/library/events/glances/GlanceIconConstants;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceIconConstants;->$VALUES:[Lcom/navdy/service/library/events/glances/GlanceIconConstants;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 15
    iput p3, p0, Lcom/navdy/service/library/events/glances/GlanceIconConstants;->value:I

    .line 16
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceIconConstants;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/glances/GlanceIconConstants;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/glances/GlanceIconConstants;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/glances/GlanceIconConstants;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/glances/GlanceIconConstants;->$VALUES:[Lcom/navdy/service/library/events/glances/GlanceIconConstants;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/glances/GlanceIconConstants;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/glances/GlanceIconConstants;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/navdy/service/library/events/glances/GlanceIconConstants;->value:I

    return v0
.end method
