.class public final Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NotificationSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/notification/NotificationSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/notification/NotificationSetting;",
        ">;"
    }
.end annotation


# instance fields
.field public app:Ljava/lang/String;

.field public enabled:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 71
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/notification/NotificationSetting;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/notification/NotificationSetting;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 75
    if-nez p1, :cond_0

    .line 78
    :goto_0
    return-void

    .line 76
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationSetting;->app:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->app:Ljava/lang/String;

    .line 77
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationSetting;->enabled:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->enabled:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public app(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;
    .locals 0
    .param p1, "app"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->app:Ljava/lang/String;

    .line 85
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/notification/NotificationSetting;
    .locals 2

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->checkRequiredFields()V

    .line 100
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationSetting;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/notification/NotificationSetting;-><init>(Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;Lcom/navdy/service/library/events/notification/NotificationSetting$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->build()Lcom/navdy/service/library/events/notification/NotificationSetting;

    move-result-object v0

    return-object v0
.end method

.method public enabled(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;
    .locals 0
    .param p1, "enabled"    # Ljava/lang/Boolean;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->enabled:Ljava/lang/Boolean;

    .line 94
    return-object p0
.end method
