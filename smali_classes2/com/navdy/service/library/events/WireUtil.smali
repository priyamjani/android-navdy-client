.class public Lcom/navdy/service/library/events/WireUtil;
.super Ljava/lang/Object;
.source "WireUtil.java"


# static fields
.field private static final sMessageTypes:[Lcom/navdy/service/library/events/NavdyEvent$MessageType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->values()[Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/WireUtil;->sMessageTypes:[Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getEventType([B)Lcom/navdy/service/library/events/NavdyEvent$MessageType;
    .locals 3
    .param p0, "eventData"    # [B

    .prologue
    .line 16
    invoke-static {p0}, Lcom/navdy/service/library/events/WireUtil;->parseEventType([B)I

    move-result v0

    .line 17
    .local v0, "eventType":I
    if-lez v0, :cond_0

    sget-object v1, Lcom/navdy/service/library/events/WireUtil;->sMessageTypes:[Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    array-length v1, v1

    if-gt v0, v1, :cond_0

    .line 18
    sget-object v1, Lcom/navdy/service/library/events/WireUtil;->sMessageTypes:[Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    .line 20
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getEventTypeIndex([B)I
    .locals 1
    .param p0, "eventData"    # [B

    .prologue
    .line 12
    invoke-static {p0}, Lcom/navdy/service/library/events/WireUtil;->parseEventType([B)I

    move-result v0

    return v0
.end method

.method public static parseEventType([B)I
    .locals 6
    .param p0, "eventData"    # [B

    .prologue
    .line 24
    const/4 v3, 0x0

    aget-byte v0, p0, v3

    .line 25
    .local v0, "firstByte":I
    and-int/lit8 v2, v0, 0x7

    .line 26
    .local v2, "varType":I
    sget-object v3, Lcom/squareup/wire/WireType;->VARINT:Lcom/squareup/wire/WireType;

    invoke-virtual {v3}, Lcom/squareup/wire/WireType;->value()I

    move-result v3

    if-eq v3, v2, :cond_0

    .line 27
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "expecting varint:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 29
    :cond_0
    shr-int/lit8 v1, v0, 0x3

    .line 30
    .local v1, "tag":I
    const/4 v3, 0x2

    if-eq v1, v3, :cond_1

    .line 31
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unexpected tag:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 34
    :cond_1
    invoke-static {p0}, Lcom/navdy/service/library/events/WireUtil;->readVarint32([B)I

    move-result v3

    return v3
.end method

.method public static readVarint32([B)I
    .locals 6
    .param p0, "data"    # [B

    .prologue
    .line 38
    const/4 v1, 0x1

    .line 39
    .local v1, "pos":I
    aget-byte v3, p0, v1

    .line 40
    .local v3, "tmp":B
    if-ltz v3, :cond_1

    move v2, v3

    .line 74
    :cond_0
    :goto_0
    return v2

    .line 43
    :cond_1
    and-int/lit8 v2, v3, 0x7f

    .line 44
    .local v2, "result":I
    add-int/lit8 v1, v1, 0x1

    .line 45
    aget-byte v3, p0, v1

    if-ltz v3, :cond_2

    .line 46
    shl-int/lit8 v4, v3, 0x7

    or-int/2addr v2, v4

    goto :goto_0

    .line 48
    :cond_2
    and-int/lit8 v4, v3, 0x7f

    shl-int/lit8 v4, v4, 0x7

    or-int/2addr v2, v4

    .line 49
    add-int/lit8 v1, v1, 0x1

    .line 50
    aget-byte v3, p0, v1

    if-ltz v3, :cond_3

    .line 51
    shl-int/lit8 v4, v3, 0xe

    or-int/2addr v2, v4

    goto :goto_0

    .line 53
    :cond_3
    and-int/lit8 v4, v3, 0x7f

    shl-int/lit8 v4, v4, 0xe

    or-int/2addr v2, v4

    .line 54
    add-int/lit8 v1, v1, 0x1

    .line 55
    aget-byte v3, p0, v1

    if-ltz v3, :cond_4

    .line 56
    shl-int/lit8 v4, v3, 0x15

    or-int/2addr v2, v4

    goto :goto_0

    .line 58
    :cond_4
    and-int/lit8 v4, v3, 0x7f

    shl-int/lit8 v4, v4, 0x15

    or-int/2addr v2, v4

    .line 59
    add-int/lit8 v1, v1, 0x1

    .line 60
    aget-byte v3, p0, v1

    shl-int/lit8 v4, v3, 0x1c

    or-int/2addr v2, v4

    .line 61
    if-gez v3, :cond_0

    .line 63
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v4, 0x5

    if-ge v0, v4, :cond_5

    .line 64
    add-int/lit8 v1, v1, 0x1

    .line 65
    aget-byte v4, p0, v1

    if-gez v4, :cond_0

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 69
    :cond_5
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "marlformed varint"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
.end method
