.class public final Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NavigationRouteCancelRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public handle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 52
    if-nez p1, :cond_0

    .line 54
    :goto_0
    return-void

    .line 53
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest$Builder;->handle:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest$Builder;Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;

    move-result-object v0

    return-object v0
.end method

.method public handle(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest$Builder;
    .locals 0
    .param p1, "handle"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest$Builder;->handle:Ljava/lang/String;

    .line 61
    return-object p0
.end method
