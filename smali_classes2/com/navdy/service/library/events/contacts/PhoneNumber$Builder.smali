.class public final Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PhoneNumber.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/contacts/PhoneNumber;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/contacts/PhoneNumber;",
        ">;"
    }
.end annotation


# instance fields
.field public customType:Ljava/lang/String;

.field public number:Ljava/lang/String;

.field public numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 79
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/contacts/PhoneNumber;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/contacts/PhoneNumber;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 83
    if-nez p1, :cond_0

    .line 87
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/PhoneNumber;->number:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;->number:Ljava/lang/String;

    .line 85
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/PhoneNumber;->numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    iput-object v0, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;->numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    .line 86
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/PhoneNumber;->customType:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;->customType:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/contacts/PhoneNumber;
    .locals 2

    .prologue
    .line 115
    new-instance v0, Lcom/navdy/service/library/events/contacts/PhoneNumber;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/contacts/PhoneNumber;-><init>(Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;Lcom/navdy/service/library/events/contacts/PhoneNumber$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;->build()Lcom/navdy/service/library/events/contacts/PhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public customType(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;
    .locals 0
    .param p1, "customType"    # Ljava/lang/String;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;->customType:Ljava/lang/String;

    .line 110
    return-object p0
.end method

.method public number(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;
    .locals 0
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;->number:Ljava/lang/String;

    .line 94
    return-object p0
.end method

.method public numberType(Lcom/navdy/service/library/events/contacts/PhoneNumberType;)Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;
    .locals 0
    .param p1, "numberType"    # Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;->numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    .line 102
    return-object p0
.end method
