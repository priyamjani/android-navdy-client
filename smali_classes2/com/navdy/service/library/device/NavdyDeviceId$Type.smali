.class public final enum Lcom/navdy/service/library/device/NavdyDeviceId$Type;
.super Ljava/lang/Enum;
.source "NavdyDeviceId.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/NavdyDeviceId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/device/NavdyDeviceId$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/device/NavdyDeviceId$Type;

.field public static final enum BT:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

.field public static final enum EA:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

.field public static final enum EMU:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

.field public static final enum UNK:Lcom/navdy/service/library/device/NavdyDeviceId$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 44
    new-instance v0, Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    const-string v1, "UNK"

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/device/NavdyDeviceId$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->UNK:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    .line 45
    new-instance v0, Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    const-string v1, "BT"

    invoke-direct {v0, v1, v3}, Lcom/navdy/service/library/device/NavdyDeviceId$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->BT:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    .line 46
    new-instance v0, Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    const-string v1, "EMU"

    invoke-direct {v0, v1, v4}, Lcom/navdy/service/library/device/NavdyDeviceId$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->EMU:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    .line 47
    new-instance v0, Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    const-string v1, "EA"

    invoke-direct {v0, v1, v5}, Lcom/navdy/service/library/device/NavdyDeviceId$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->EA:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    .line 43
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    sget-object v1, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->UNK:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->BT:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->EMU:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->EA:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->$VALUES:[Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/device/NavdyDeviceId$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 43
    const-class v0, Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/device/NavdyDeviceId$Type;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->$VALUES:[Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    invoke-virtual {v0}, [Lcom/navdy/service/library/device/NavdyDeviceId$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    return-object v0
.end method
