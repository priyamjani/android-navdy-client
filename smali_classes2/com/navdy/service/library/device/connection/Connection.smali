.class public abstract Lcom/navdy/service/library/device/connection/Connection;
.super Lcom/navdy/service/library/util/Listenable;
.source "Connection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;,
        Lcom/navdy/service/library/device/connection/Connection$Listener;,
        Lcom/navdy/service/library/device/connection/Connection$Status;,
        Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;,
        Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;,
        Lcom/navdy/service/library/device/connection/Connection$EventDispatcher;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/service/library/util/Listenable",
        "<",
        "Lcom/navdy/service/library/device/connection/Connection$Listener;",
        ">;"
    }
.end annotation


# static fields
.field private static factoryMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/service/library/device/connection/ConnectionType;",
            "Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;",
            ">;"
        }
    .end annotation
.end field

.field private static sDefaultFactory:Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;


# instance fields
.field protected logger:Lcom/navdy/service/library/log/Logger;

.field protected mConnectionInfo:Lcom/navdy/service/library/device/connection/ConnectionInfo;

.field protected mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/service/library/device/connection/Connection;->factoryMap:Ljava/util/Map;

    .line 78
    new-instance v0, Lcom/navdy/service/library/device/connection/Connection$1;

    invoke-direct {v0}, Lcom/navdy/service/library/device/connection/Connection$1;-><init>()V

    sput-object v0, Lcom/navdy/service/library/device/connection/Connection;->sDefaultFactory:Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;

    .line 103
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    sget-object v1, Lcom/navdy/service/library/device/connection/Connection;->sDefaultFactory:Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;

    invoke-static {v0, v1}, Lcom/navdy/service/library/device/connection/Connection;->registerConnectionType(Lcom/navdy/service/library/device/connection/ConnectionType;Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;)V

    .line 104
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_TUNNEL:Lcom/navdy/service/library/device/connection/ConnectionType;

    sget-object v1, Lcom/navdy/service/library/device/connection/Connection;->sDefaultFactory:Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;

    invoke-static {v0, v1}, Lcom/navdy/service/library/device/connection/Connection;->registerConnectionType(Lcom/navdy/service/library/device/connection/ConnectionType;Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;)V

    .line 105
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionType;->TCP_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    sget-object v1, Lcom/navdy/service/library/device/connection/Connection;->sDefaultFactory:Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;

    invoke-static {v0, v1}, Lcom/navdy/service/library/device/connection/Connection;->registerConnectionType(Lcom/navdy/service/library/device/connection/ConnectionType;Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;)V

    .line 106
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_IAP2_LINK:Lcom/navdy/service/library/device/connection/ConnectionType;

    sget-object v1, Lcom/navdy/service/library/device/connection/Connection;->sDefaultFactory:Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;

    invoke-static {v0, v1}, Lcom/navdy/service/library/device/connection/Connection;->registerConnectionType(Lcom/navdy/service/library/device/connection/ConnectionType;Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;)V

    .line 107
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionType;->EA_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    sget-object v1, Lcom/navdy/service/library/device/connection/Connection;->sDefaultFactory:Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;

    invoke-static {v0, v1}, Lcom/navdy/service/library/device/connection/Connection;->registerConnectionType(Lcom/navdy/service/library/device/connection/ConnectionType;Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;)V

    .line 108
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V
    .locals 2
    .param p1, "connectionInfo"    # Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/navdy/service/library/util/Listenable;-><init>()V

    .line 18
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/Connection;->logger:Lcom/navdy/service/library/log/Logger;

    .line 111
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/Connection;->mConnectionInfo:Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .line 112
    sget-object v0, Lcom/navdy/service/library/device/connection/Connection$Status;->DISCONNECTED:Lcom/navdy/service/library/device/connection/Connection$Status;

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/Connection;->mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;

    .line 113
    return-void
.end method

.method public static instantiateFromConnectionInfo(Landroid/content/Context;Lcom/navdy/service/library/device/connection/ConnectionInfo;)Lcom/navdy/service/library/device/connection/Connection;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "connectionInfo"    # Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .prologue
    .line 64
    sget-object v1, Lcom/navdy/service/library/device/connection/Connection;->factoryMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getType()Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;

    .line 65
    .local v0, "factory":Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;
    if-eqz v0, :cond_0

    .line 66
    invoke-interface {v0, p0, p1}, Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;->build(Landroid/content/Context;Lcom/navdy/service/library/device/connection/ConnectionInfo;)Lcom/navdy/service/library/device/connection/Connection;

    move-result-object v1

    return-object v1

    .line 68
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown connection class for type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getType()Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static registerConnectionType(Lcom/navdy/service/library/device/connection/ConnectionType;Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;)V
    .locals 1
    .param p0, "type"    # Lcom/navdy/service/library/device/connection/ConnectionType;
    .param p1, "factory"    # Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;

    .prologue
    .line 75
    sget-object v0, Lcom/navdy/service/library/device/connection/Connection;->factoryMap:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    return-void
.end method


# virtual methods
.method public abstract connect()Z
.end method

.method public abstract disconnect()Z
.end method

.method protected dispatchConnectEvent()V
    .locals 1

    .prologue
    .line 152
    new-instance v0, Lcom/navdy/service/library/device/connection/Connection$2;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/connection/Connection$2;-><init>(Lcom/navdy/service/library/device/connection/Connection;)V

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/Connection;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 158
    return-void
.end method

.method protected dispatchConnectionFailedEvent(Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V
    .locals 1
    .param p1, "cause"    # Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;

    .prologue
    .line 161
    new-instance v0, Lcom/navdy/service/library/device/connection/Connection$3;

    invoke-direct {v0, p0, p1}, Lcom/navdy/service/library/device/connection/Connection$3;-><init>(Lcom/navdy/service/library/device/connection/Connection;Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/Connection;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 167
    return-void
.end method

.method protected dispatchDisconnectEvent(Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    .locals 1
    .param p1, "cause"    # Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .prologue
    .line 170
    new-instance v0, Lcom/navdy/service/library/device/connection/Connection$4;

    invoke-direct {v0, p0, p1}, Lcom/navdy/service/library/device/connection/Connection$4;-><init>(Lcom/navdy/service/library/device/connection/Connection;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/Connection;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 176
    return-void
.end method

.method public getConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/Connection;->mConnectionInfo:Lcom/navdy/service/library/device/connection/ConnectionInfo;

    return-object v0
.end method

.method public abstract getSocket()Lcom/navdy/service/library/network/SocketAdapter;
.end method

.method public getStatus()Lcom/navdy/service/library/device/connection/Connection$Status;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/Connection;->mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;

    return-object v0
.end method

.method public getType()Lcom/navdy/service/library/device/connection/ConnectionType;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/Connection;->mConnectionInfo:Lcom/navdy/service/library/device/connection/ConnectionInfo;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getType()Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v0

    return-object v0
.end method
