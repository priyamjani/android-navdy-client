.class Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;
.super Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;
.source "AcceptorListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/connection/AcceptorListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AcceptThread"
.end annotation


# instance fields
.field private volatile closing:Z

.field final synthetic this$0:Lcom/navdy/service/library/device/connection/AcceptorListener;


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/device/connection/AcceptorListener;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->this$0:Lcom/navdy/service/library/device/connection/AcceptorListener;

    invoke-direct {p0, p1}, Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;-><init>(Lcom/navdy/service/library/device/connection/ConnectionListener;)V

    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AcceptThread-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/navdy/service/library/device/connection/AcceptorListener;->access$000(Lcom/navdy/service/library/device/connection/AcceptorListener;)Lcom/navdy/service/library/network/SocketAcceptor;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->setName(Ljava/lang/String;)V

    .line 39
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 3

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->this$0:Lcom/navdy/service/library/device/connection/AcceptorListener;

    iget-object v0, v0, Lcom/navdy/service/library/device/connection/AcceptorListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Socket cancel "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->closing:Z

    .line 68
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->this$0:Lcom/navdy/service/library/device/connection/AcceptorListener;

    invoke-static {v0}, Lcom/navdy/service/library/device/connection/AcceptorListener;->access$000(Lcom/navdy/service/library/device/connection/AcceptorListener;)Lcom/navdy/service/library/network/SocketAcceptor;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 69
    return-void
.end method

.method public run()V
    .locals 7

    .prologue
    .line 42
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->this$0:Lcom/navdy/service/library/device/connection/AcceptorListener;

    iget-object v4, v4, Lcom/navdy/service/library/device/connection/AcceptorListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " started, closing:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->closing:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 43
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->this$0:Lcom/navdy/service/library/device/connection/AcceptorListener;

    invoke-virtual {v4}, Lcom/navdy/service/library/device/connection/AcceptorListener;->dispatchStarted()V

    .line 45
    const/4 v3, 0x0

    .line 49
    .local v3, "socket":Lcom/navdy/service/library/network/SocketAdapter;
    :try_start_0
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->this$0:Lcom/navdy/service/library/device/connection/AcceptorListener;

    invoke-static {v4}, Lcom/navdy/service/library/device/connection/AcceptorListener;->access$000(Lcom/navdy/service/library/device/connection/AcceptorListener;)Lcom/navdy/service/library/network/SocketAcceptor;

    move-result-object v4

    invoke-interface {v4}, Lcom/navdy/service/library/network/SocketAcceptor;->accept()Lcom/navdy/service/library/network/SocketAdapter;

    move-result-object v3

    .line 50
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->this$0:Lcom/navdy/service/library/device/connection/AcceptorListener;

    invoke-static {v4}, Lcom/navdy/service/library/device/connection/AcceptorListener;->access$000(Lcom/navdy/service/library/device/connection/AcceptorListener;)Lcom/navdy/service/library/network/SocketAcceptor;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->this$0:Lcom/navdy/service/library/device/connection/AcceptorListener;

    invoke-static {v5}, Lcom/navdy/service/library/device/connection/AcceptorListener;->access$100(Lcom/navdy/service/library/device/connection/AcceptorListener;)Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Lcom/navdy/service/library/network/SocketAcceptor;->getRemoteConnectionInfo(Lcom/navdy/service/library/network/SocketAdapter;Lcom/navdy/service/library/device/connection/ConnectionType;)Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v1

    .line 51
    .local v1, "connectionInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    new-instance v0, Lcom/navdy/service/library/device/connection/SocketConnection;

    invoke-direct {v0, v1, v3}, Lcom/navdy/service/library/device/connection/SocketConnection;-><init>(Lcom/navdy/service/library/device/connection/ConnectionInfo;Lcom/navdy/service/library/network/SocketAdapter;)V

    .line 52
    .local v0, "connection":Lcom/navdy/service/library/device/connection/SocketConnection;
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->this$0:Lcom/navdy/service/library/device/connection/AcceptorListener;

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/device/connection/AcceptorListener;->dispatchConnected(Lcom/navdy/service/library/device/connection/Connection;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->cancel()V

    .line 61
    .end local v0    # "connection":Lcom/navdy/service/library/device/connection/SocketConnection;
    .end local v1    # "connectionInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    :goto_0
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->this$0:Lcom/navdy/service/library/device/connection/AcceptorListener;

    invoke-virtual {v4}, Lcom/navdy/service/library/device/connection/AcceptorListener;->dispatchStopped()V

    .line 62
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->this$0:Lcom/navdy/service/library/device/connection/AcceptorListener;

    iget-object v4, v4, Lcom/navdy/service/library/device/connection/AcceptorListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "END "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 63
    return-void

    .line 53
    :catch_0
    move-exception v2

    .line 54
    .local v2, "e":Ljava/lang/Throwable;
    :try_start_1
    iget-boolean v4, p0, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->closing:Z

    if-nez v4, :cond_0

    .line 55
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->this$0:Lcom/navdy/service/library/device/connection/AcceptorListener;

    iget-object v4, v4, Lcom/navdy/service/library/device/connection/AcceptorListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Socket accept() failed"

    invoke-virtual {v4, v5, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 57
    :cond_0
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 59
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->cancel()V

    goto :goto_0

    .end local v2    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v4

    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;->cancel()V

    throw v4
.end method
