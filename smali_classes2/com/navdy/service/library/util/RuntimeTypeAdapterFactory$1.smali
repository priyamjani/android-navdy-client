.class Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;
.super Lcom/google/gson/TypeAdapter;
.source "RuntimeTypeAdapterFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->create(Lcom/google/gson/Gson;Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<TR;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

.field final synthetic val$labelToDelegate:Ljava/util/Map;

.field final synthetic val$subtypeToDelegate:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    .prologue
    .line 228
    .local p0, "this":Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;, "Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;"
    iput-object p1, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;->this$0:Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    iput-object p2, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;->val$labelToDelegate:Ljava/util/Map;

    iput-object p3, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;->val$subtypeToDelegate:Ljava/util/Map;

    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 7
    .param p1, "in"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/stream/JsonReader;",
            ")TR;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 230
    .local p0, "this":Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;, "Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;"
    invoke-static {p1}, Lcom/google/gson/internal/Streams;->parse(Lcom/google/gson/stream/JsonReader;)Lcom/google/gson/JsonElement;

    move-result-object v1

    .line 231
    .local v1, "jsonElement":Lcom/google/gson/JsonElement;
    invoke-virtual {v1}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;->this$0:Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    invoke-static {v5}, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->access$000(Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/gson/JsonObject;->remove(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v3

    .line 232
    .local v3, "labelJsonElement":Lcom/google/gson/JsonElement;
    if-nez v3, :cond_0

    .line 233
    new-instance v4, Lcom/google/gson/JsonParseException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cannot deserialize "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;->this$0:Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    invoke-static {v6}, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->access$100(Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;)Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " because it does not define a field named "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;->this$0:Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    .line 234
    invoke-static {v6}, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->access$000(Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/gson/JsonParseException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 236
    :cond_0
    invoke-virtual {v3}, Lcom/google/gson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v2

    .line 238
    .local v2, "label":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;->val$labelToDelegate:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/TypeAdapter;

    .line 239
    .local v0, "delegate":Lcom/google/gson/TypeAdapter;, "Lcom/google/gson/TypeAdapter<TR;>;"
    if-nez v0, :cond_1

    .line 240
    new-instance v4, Lcom/google/gson/JsonParseException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cannot deserialize "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;->this$0:Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    invoke-static {v6}, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->access$100(Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;)Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " subtype named "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; did you forget to register a subtype?"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/gson/JsonParseException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 243
    :cond_1
    invoke-virtual {v0, v1}, Lcom/google/gson/TypeAdapter;->fromJsonTree(Lcom/google/gson/JsonElement;)Ljava/lang/Object;

    move-result-object v4

    return-object v4
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 9
    .param p1, "out"    # Lcom/google/gson/stream/JsonWriter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/stream/JsonWriter;",
            "TR;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 247
    .local p0, "this":Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;, "Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;"
    .local p2, "value":Ljava/lang/Object;, "TR;"
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    .line 248
    .local v5, "srcType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iget-object v6, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;->this$0:Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    invoke-static {v6}, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->access$200(Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 250
    .local v4, "label":Ljava/lang/String;
    iget-object v6, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;->val$subtypeToDelegate:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/gson/TypeAdapter;

    .line 251
    .local v1, "delegate":Lcom/google/gson/TypeAdapter;, "Lcom/google/gson/TypeAdapter<TR;>;"
    if-nez v1, :cond_0

    .line 252
    new-instance v6, Lcom/google/gson/JsonParseException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "cannot serialize "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "; did you forget to register a subtype?"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/google/gson/JsonParseException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 255
    :cond_0
    invoke-virtual {v1, p2}, Lcom/google/gson/TypeAdapter;->toJsonTree(Ljava/lang/Object;)Lcom/google/gson/JsonElement;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v3

    .line 256
    .local v3, "jsonObject":Lcom/google/gson/JsonObject;
    iget-object v6, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;->this$0:Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    invoke-static {v6}, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->access$000(Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 257
    new-instance v6, Lcom/google/gson/JsonParseException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "cannot serialize "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " because it already defines a field named "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;->this$0:Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    .line 258
    invoke-static {v8}, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->access$000(Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/google/gson/JsonParseException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 260
    :cond_1
    new-instance v0, Lcom/google/gson/JsonObject;

    invoke-direct {v0}, Lcom/google/gson/JsonObject;-><init>()V

    .line 261
    .local v0, "clone":Lcom/google/gson/JsonObject;
    iget-object v6, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;->this$0:Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    invoke-static {v6}, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->access$000(Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/google/gson/JsonPrimitive;

    invoke-direct {v7, v4}, Lcom/google/gson/JsonPrimitive;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6, v7}, Lcom/google/gson/JsonObject;->add(Ljava/lang/String;Lcom/google/gson/JsonElement;)V

    .line 262
    invoke-virtual {v3}, Lcom/google/gson/JsonObject;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 263
    .local v2, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/gson/JsonElement;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/gson/JsonElement;

    invoke-virtual {v0, v6, v7}, Lcom/google/gson/JsonObject;->add(Ljava/lang/String;Lcom/google/gson/JsonElement;)V

    goto :goto_0

    .line 265
    .end local v2    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/gson/JsonElement;>;"
    :cond_2
    invoke-static {v0, p1}, Lcom/google/gson/internal/Streams;->write(Lcom/google/gson/JsonElement;Lcom/google/gson/stream/JsonWriter;)V

    .line 266
    return-void
.end method
