.class public Lcom/nimbusds/jose/util/X509CertUtils;
.super Ljava/lang/Object;
.source "X509CertUtils.java"


# static fields
.field private static final PEM_BEGIN_MARKER:Ljava/lang/String; = "-----BEGIN CERTIFICATE-----"

.field private static final PEM_END_MARKER:Ljava/lang/String; = "-----END CERTIFICATE-----"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Ljava/security/cert/X509Certificate;
    .locals 5
    .param p0, "pemEncodedCert"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 74
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 96
    :cond_0
    :goto_0
    return-object v3

    .line 78
    :cond_1
    const-string v4, "-----BEGIN CERTIFICATE-----"

    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 80
    .local v2, "markerStart":I
    if-ltz v2, :cond_0

    .line 84
    const-string v4, "-----BEGIN CERTIFICATE-----"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {p0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, "buf":Ljava/lang/String;
    const-string v4, "-----END CERTIFICATE-----"

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 88
    .local v1, "markerEnd":I
    if-ltz v1, :cond_0

    .line 92
    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 94
    const-string v3, "\\s"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 96
    new-instance v3, Lcom/nimbusds/jose/util/Base64;

    invoke-direct {v3, v0}, Lcom/nimbusds/jose/util/Base64;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/nimbusds/jose/util/Base64;->decode()[B

    move-result-object v3

    invoke-static {v3}, Lcom/nimbusds/jose/util/X509CertUtils;->parse([B)Ljava/security/cert/X509Certificate;

    move-result-object v3

    goto :goto_0
.end method

.method public static parse([B)Ljava/security/cert/X509Certificate;
    .locals 4
    .param p0, "derEncodedCert"    # [B

    .prologue
    const/4 v2, 0x0

    .line 44
    if-eqz p0, :cond_0

    array-length v3, p0

    if-nez v3, :cond_1

    :cond_0
    move-object v0, v2

    .line 60
    :goto_0
    return-object v0

    .line 50
    :cond_1
    :try_start_0
    const-string v3, "X.509"

    invoke-static {v3}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v1

    .line 51
    .local v1, "cf":Ljava/security/cert/CertificateFactory;
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v1, v3}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 56
    .local v0, "cert":Ljava/security/cert/Certificate;
    instance-of v3, v0, Ljava/security/cert/X509Certificate;

    if-nez v3, :cond_2

    move-object v0, v2

    .line 57
    goto :goto_0

    .line 53
    .end local v0    # "cert":Ljava/security/cert/Certificate;
    .end local v1    # "cf":Ljava/security/cert/CertificateFactory;
    :catch_0
    move-exception v3

    move-object v0, v2

    goto :goto_0

    .line 60
    .restart local v0    # "cert":Ljava/security/cert/Certificate;
    .restart local v1    # "cf":Ljava/security/cert/CertificateFactory;
    :cond_2
    check-cast v0, Ljava/security/cert/X509Certificate;

    goto :goto_0
.end method
