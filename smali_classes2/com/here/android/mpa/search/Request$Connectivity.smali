.class public final enum Lcom/here/android/mpa/search/Request$Connectivity;
.super Ljava/lang/Enum;
.source "Request.java"


# annotations
.annotation build Lcom/nokia/maps/annotation/Online;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/here/android/mpa/search/Request;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Connectivity"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/here/android/mpa/search/Request$Connectivity;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DEFAULT:Lcom/here/android/mpa/search/Request$Connectivity;

.field public static final enum OFFLINE:Lcom/here/android/mpa/search/Request$Connectivity;

.field public static final enum ONLINE:Lcom/here/android/mpa/search/Request$Connectivity;

.field private static final synthetic a:[Lcom/here/android/mpa/search/Request$Connectivity;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 227
    new-instance v0, Lcom/here/android/mpa/search/Request$Connectivity;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, Lcom/here/android/mpa/search/Request$Connectivity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/here/android/mpa/search/Request$Connectivity;->DEFAULT:Lcom/here/android/mpa/search/Request$Connectivity;

    .line 233
    new-instance v0, Lcom/here/android/mpa/search/Request$Connectivity;

    const-string v1, "OFFLINE"

    invoke-direct {v0, v1, v3}, Lcom/here/android/mpa/search/Request$Connectivity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/here/android/mpa/search/Request$Connectivity;->OFFLINE:Lcom/here/android/mpa/search/Request$Connectivity;

    .line 238
    new-instance v0, Lcom/here/android/mpa/search/Request$Connectivity;

    const-string v1, "ONLINE"

    invoke-direct {v0, v1, v4}, Lcom/here/android/mpa/search/Request$Connectivity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/here/android/mpa/search/Request$Connectivity;->ONLINE:Lcom/here/android/mpa/search/Request$Connectivity;

    .line 220
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/here/android/mpa/search/Request$Connectivity;

    sget-object v1, Lcom/here/android/mpa/search/Request$Connectivity;->DEFAULT:Lcom/here/android/mpa/search/Request$Connectivity;

    aput-object v1, v0, v2

    sget-object v1, Lcom/here/android/mpa/search/Request$Connectivity;->OFFLINE:Lcom/here/android/mpa/search/Request$Connectivity;

    aput-object v1, v0, v3

    sget-object v1, Lcom/here/android/mpa/search/Request$Connectivity;->ONLINE:Lcom/here/android/mpa/search/Request$Connectivity;

    aput-object v1, v0, v4

    sput-object v0, Lcom/here/android/mpa/search/Request$Connectivity;->a:[Lcom/here/android/mpa/search/Request$Connectivity;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 221
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/here/android/mpa/search/Request$Connectivity;
    .locals 1

    .prologue
    .line 220
    const-class v0, Lcom/here/android/mpa/search/Request$Connectivity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/search/Request$Connectivity;

    return-object v0
.end method

.method public static values()[Lcom/here/android/mpa/search/Request$Connectivity;
    .locals 1

    .prologue
    .line 220
    sget-object v0, Lcom/here/android/mpa/search/Request$Connectivity;->a:[Lcom/here/android/mpa/search/Request$Connectivity;

    invoke-virtual {v0}, [Lcom/here/android/mpa/search/Request$Connectivity;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/here/android/mpa/search/Request$Connectivity;

    return-object v0
.end method
