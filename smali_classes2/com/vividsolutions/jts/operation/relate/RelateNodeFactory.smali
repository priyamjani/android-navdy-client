.class public Lcom/vividsolutions/jts/operation/relate/RelateNodeFactory;
.super Lcom/vividsolutions/jts/geomgraph/NodeFactory;
.source "RelateNodeFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/vividsolutions/jts/geomgraph/NodeFactory;-><init>()V

    return-void
.end method


# virtual methods
.method public createNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Node;
    .locals 2
    .param p1, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 50
    new-instance v0, Lcom/vividsolutions/jts/operation/relate/RelateNode;

    new-instance v1, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundleStar;

    invoke-direct {v1}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundleStar;-><init>()V

    invoke-direct {v0, p1, v1}, Lcom/vividsolutions/jts/operation/relate/RelateNode;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;)V

    return-object v0
.end method
