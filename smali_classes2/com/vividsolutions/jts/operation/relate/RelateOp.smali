.class public Lcom/vividsolutions/jts/operation/relate/RelateOp;
.super Lcom/vividsolutions/jts/operation/GeometryGraphOperation;
.source "RelateOp.java"


# instance fields
.field private relate:Lcom/vividsolutions/jts/operation/relate/RelateComputer;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/operation/GeometryGraphOperation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 114
    new-instance v0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/relate/RelateOp;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;-><init>([Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/relate/RelateOp;->relate:Lcom/vividsolutions/jts/operation/relate/RelateComputer;

    .line 115
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;)V
    .locals 2
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p3, "boundaryNodeRule"    # Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .prologue
    .line 126
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/operation/GeometryGraphOperation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;)V

    .line 127
    new-instance v0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/relate/RelateOp;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;-><init>([Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/relate/RelateOp;->relate:Lcom/vividsolutions/jts/operation/relate/RelateComputer;

    .line 128
    return-void
.end method

.method public static relate(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/IntersectionMatrix;
    .locals 2
    .param p0, "a"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "b"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 82
    new-instance v1, Lcom/vividsolutions/jts/operation/relate/RelateOp;

    invoke-direct {v1, p0, p1}, Lcom/vividsolutions/jts/operation/relate/RelateOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 83
    .local v1, "relOp":Lcom/vividsolutions/jts/operation/relate/RelateOp;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/relate/RelateOp;->getIntersectionMatrix()Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    move-result-object v0

    .line 84
    .local v0, "im":Lcom/vividsolutions/jts/geom/IntersectionMatrix;
    return-object v0
.end method

.method public static relate(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;)Lcom/vividsolutions/jts/geom/IntersectionMatrix;
    .locals 2
    .param p0, "a"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "b"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "boundaryNodeRule"    # Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .prologue
    .line 98
    new-instance v1, Lcom/vividsolutions/jts/operation/relate/RelateOp;

    invoke-direct {v1, p0, p1, p2}, Lcom/vividsolutions/jts/operation/relate/RelateOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;)V

    .line 99
    .local v1, "relOp":Lcom/vividsolutions/jts/operation/relate/RelateOp;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/relate/RelateOp;->getIntersectionMatrix()Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    move-result-object v0

    .line 100
    .local v0, "im":Lcom/vividsolutions/jts/geom/IntersectionMatrix;
    return-object v0
.end method


# virtual methods
.method public getIntersectionMatrix()Lcom/vividsolutions/jts/geom/IntersectionMatrix;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/relate/RelateOp;->relate:Lcom/vividsolutions/jts/operation/relate/RelateComputer;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->computeIM()Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    move-result-object v0

    return-object v0
.end method
