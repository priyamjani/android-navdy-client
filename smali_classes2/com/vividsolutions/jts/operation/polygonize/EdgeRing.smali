.class Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
.super Ljava/lang/Object;
.source "EdgeRing.java"


# instance fields
.field private deList:Ljava/util/List;

.field private factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field private holes:Ljava/util/List;

.field private ring:Lcom/vividsolutions/jts/geom/LinearRing;

.field private ringPts:[Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 2
    .param p1, "factory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    const/4 v1, 0x0

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->deList:Ljava/util/List;

    .line 143
    iput-object v1, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->ring:Lcom/vividsolutions/jts/geom/LinearRing;

    .line 145
    iput-object v1, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->ringPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 150
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 151
    return-void
.end method

.method private static addEdge([Lcom/vividsolutions/jts/geom/Coordinate;ZLcom/vividsolutions/jts/geom/CoordinateList;)V
    .locals 3
    .param p0, "coords"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "isForward"    # Z
    .param p2, "coordList"    # Lcom/vividsolutions/jts/geom/CoordinateList;

    .prologue
    const/4 v2, 0x0

    .line 269
    if-eqz p1, :cond_0

    .line 270
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 271
    aget-object v1, p0, v0

    invoke-virtual {p2, v1, v2}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 270
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 275
    .end local v0    # "i":I
    :cond_0
    array-length v1, p0

    add-int/lit8 v0, v1, -0x1

    .restart local v0    # "i":I
    :goto_1
    if-ltz v0, :cond_1

    .line 276
    aget-object v1, p0, v0

    invoke-virtual {p2, v1, v2}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 275
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 279
    :cond_1
    return-void
.end method

.method public static findEdgeRingContaining(Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;Ljava/util/List;)Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
    .locals 12
    .param p0, "testEr"    # Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
    .param p1, "shellList"    # Ljava/util/List;

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->getRing()Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v6

    .line 68
    .local v6, "testRing":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-virtual {v6}, Lcom/vividsolutions/jts/geom/LinearRing;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v4

    .line 69
    .local v4, "testEnv":Lcom/vividsolutions/jts/geom/Envelope;
    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    .line 71
    .local v5, "testPt":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v2, 0x0

    .line 72
    .local v2, "minShell":Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
    const/4 v3, 0x0

    .line 73
    .local v3, "minShellEnv":Lcom/vividsolutions/jts/geom/Envelope;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 74
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;

    .line 75
    .local v7, "tryShell":Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
    invoke-virtual {v7}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->getRing()Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v9

    .line 76
    .local v9, "tryShellRing":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-virtual {v9}, Lcom/vividsolutions/jts/geom/LinearRing;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v8

    .line 79
    .local v8, "tryShellEnv":Lcom/vividsolutions/jts/geom/Envelope;
    invoke-virtual {v8, v4}, Lcom/vividsolutions/jts/geom/Envelope;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 81
    invoke-virtual {v8, v4}, Lcom/vividsolutions/jts/geom/Envelope;->contains(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 83
    invoke-virtual {v6}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v10

    invoke-virtual {v9}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->ptNotInList([Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    .line 84
    const/4 v0, 0x0

    .line 85
    .local v0, "isContained":Z
    invoke-virtual {v9}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v10

    invoke-static {v5, v10}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isPointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 86
    const/4 v0, 0x1

    .line 89
    :cond_1
    if-eqz v0, :cond_0

    .line 90
    if-eqz v2, :cond_2

    invoke-virtual {v3, v8}, Lcom/vividsolutions/jts/geom/Envelope;->contains(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 92
    :cond_2
    move-object v2, v7

    .line 93
    invoke-virtual {v2}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->getRing()Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vividsolutions/jts/geom/LinearRing;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v3

    goto :goto_0

    .line 97
    .end local v0    # "isContained":Z
    .end local v7    # "tryShell":Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
    .end local v8    # "tryShellEnv":Lcom/vividsolutions/jts/geom/Envelope;
    .end local v9    # "tryShellRing":Lcom/vividsolutions/jts/geom/LinearRing;
    :cond_3
    return-object v2
.end method

.method private getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 6

    .prologue
    .line 223
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->ringPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    if-nez v4, :cond_1

    .line 224
    new-instance v0, Lcom/vividsolutions/jts/geom/CoordinateList;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/CoordinateList;-><init>()V

    .line 225
    .local v0, "coordList":Lcom/vividsolutions/jts/geom/CoordinateList;
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->deList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 226
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .line 227
    .local v1, "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getEdge()Lcom/vividsolutions/jts/planargraph/Edge;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeEdge;

    .line 228
    .local v2, "edge":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeEdge;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeEdge;->getLine()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    invoke-virtual {v1}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getEdgeDirection()Z

    move-result v5

    invoke-static {v4, v5, v0}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->addEdge([Lcom/vividsolutions/jts/geom/Coordinate;ZLcom/vividsolutions/jts/geom/CoordinateList;)V

    goto :goto_0

    .line 230
    .end local v1    # "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    .end local v2    # "edge":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeEdge;
    :cond_0
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/CoordinateList;->toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    iput-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->ringPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 232
    .end local v0    # "coordList":Lcom/vividsolutions/jts/geom/CoordinateList;
    .end local v3    # "i":Ljava/util/Iterator;
    :cond_1
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->ringPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v4
.end method

.method public static isInList(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 2
    .param p0, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 131
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 132
    aget-object v1, p1, v0

    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    const/4 v1, 0x1

    .line 135
    :goto_1
    return v1

    .line 131
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 135
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static ptNotInList([Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 3
    .param p0, "testPts"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 111
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 112
    aget-object v1, p0, v0

    .line 113
    .local v1, "testPt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-static {v1, p1}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->isInList(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 116
    .end local v1    # "testPt":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_1
    return-object v1

    .line 111
    .restart local v1    # "testPt":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 116
    .end local v1    # "testPt":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V
    .locals 1
    .param p1, "de"    # Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->deList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    return-void
.end method

.method public addHole(Lcom/vividsolutions/jts/geom/LinearRing;)V
    .locals 1
    .param p1, "hole"    # Lcom/vividsolutions/jts/geom/LinearRing;

    .prologue
    .line 179
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->holes:Ljava/util/List;

    if-nez v0, :cond_0

    .line 180
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->holes:Ljava/util/List;

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->holes:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    return-void
.end method

.method public getLineString()Lcom/vividsolutions/jts/geom/LineString;
    .locals 2

    .prologue
    .line 244
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 245
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->ringPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    return-object v0
.end method

.method public getPolygon()Lcom/vividsolutions/jts/geom/Polygon;
    .locals 5

    .prologue
    .line 191
    const/4 v0, 0x0

    .line 192
    .local v0, "holeLR":[Lcom/vividsolutions/jts/geom/LinearRing;
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->holes:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 193
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->holes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v0, v3, [Lcom/vividsolutions/jts/geom/LinearRing;

    .line 194
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->holes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 195
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->holes:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/LinearRing;

    aput-object v3, v0, v1

    .line 194
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 198
    .end local v1    # "i":I
    :cond_0
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    iget-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->ring:Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v3, v4, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v2

    .line 199
    .local v2, "poly":Lcom/vividsolutions/jts/geom/Polygon;
    return-object v2
.end method

.method public getRing()Lcom/vividsolutions/jts/geom/LinearRing;
    .locals 3

    .prologue
    .line 255
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->ring:Lcom/vividsolutions/jts/geom/LinearRing;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->ring:Lcom/vividsolutions/jts/geom/LinearRing;

    .line 264
    :goto_0
    return-object v1

    .line 256
    :cond_0
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 257
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->ringPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v1, v1

    const/4 v2, 0x3

    if-ge v1, v2, :cond_1

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v2, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->ringPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 259
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    iget-object v2, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->ringPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v1

    iput-object v1, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->ring:Lcom/vividsolutions/jts/geom/LinearRing;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 264
    :goto_1
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->ring:Lcom/vividsolutions/jts/geom/LinearRing;

    goto :goto_0

    .line 261
    :catch_0
    move-exception v0

    .line 262
    .local v0, "ex":Ljava/lang/Exception;
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v2, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->ringPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public isHole()Z
    .locals 2

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->getRing()Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v0

    .line 171
    .local v0, "ring":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-static {v1}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isCCW([Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v1

    return v1
.end method

.method public isValid()Z
    .locals 2

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 210
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->ringPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v0, v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    const/4 v0, 0x0

    .line 212
    :goto_0
    return v0

    .line 211
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->getRing()Lcom/vividsolutions/jts/geom/LinearRing;

    .line 212
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->ring:Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LinearRing;->isValid()Z

    move-result v0

    goto :goto_0
.end method
