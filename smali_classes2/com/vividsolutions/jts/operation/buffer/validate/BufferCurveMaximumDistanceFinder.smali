.class public Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder;
.super Ljava/lang/Object;
.source "BufferCurveMaximumDistanceFinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxMidpointDistanceFilter;,
        Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxPointDistanceFilter;
    }
.end annotation


# instance fields
.field private inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

.field private maxPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "inputGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    invoke-direct {v0}, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder;->maxPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    .line 59
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 60
    return-void
.end method

.method private computeMaxMidpointDistance(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 3
    .param p1, "curve"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 82
    new-instance v0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxMidpointDistanceFilter;

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxMidpointDistanceFilter;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 83
    .local v0, "distFilter":Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxMidpointDistanceFilter;
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;)V

    .line 84
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder;->maxPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxMidpointDistanceFilter;->getMaxPointDistance()Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->setMaximum(Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;)V

    .line 85
    return-void
.end method

.method private computeMaxVertexDistance(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 3
    .param p1, "curve"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 75
    new-instance v0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxPointDistanceFilter;

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxPointDistanceFilter;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 76
    .local v0, "distFilter":Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxPointDistanceFilter;
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/CoordinateFilter;)V

    .line 77
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder;->maxPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxPointDistanceFilter;->getMaxPointDistance()Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->setMaximum(Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;)V

    .line 78
    return-void
.end method


# virtual methods
.method public findDistance(Lcom/vividsolutions/jts/geom/Geometry;)D
    .locals 2
    .param p1, "bufferCurve"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder;->computeMaxVertexDistance(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 65
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder;->computeMaxMidpointDistance(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 66
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder;->maxPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->getDistance()D

    move-result-wide v0

    return-wide v0
.end method

.method public getDistancePoints()Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder;->maxPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    return-object v0
.end method
