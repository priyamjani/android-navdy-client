.class public Lcom/vividsolutions/jts/operation/overlay/validate/FuzzyPointLocator;
.super Ljava/lang/Object;
.source "FuzzyPointLocator.java"


# instance fields
.field private boundaryDistanceTolerance:D

.field private g:Lcom/vividsolutions/jts/geom/Geometry;

.field private linework:Lcom/vividsolutions/jts/geom/MultiLineString;

.field private ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

.field private seg:Lcom/vividsolutions/jts/geom/LineSegment;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;D)V
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "boundaryDistanceTolerance"    # D

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Lcom/vividsolutions/jts/algorithm/PointLocator;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/PointLocator;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/validate/FuzzyPointLocator;->ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

    .line 58
    new-instance v0, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/validate/FuzzyPointLocator;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    .line 62
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/overlay/validate/FuzzyPointLocator;->g:Lcom/vividsolutions/jts/geom/Geometry;

    .line 63
    iput-wide p2, p0, Lcom/vividsolutions/jts/operation/overlay/validate/FuzzyPointLocator;->boundaryDistanceTolerance:D

    .line 64
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/overlay/validate/FuzzyPointLocator;->extractLinework(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/MultiLineString;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/validate/FuzzyPointLocator;->linework:Lcom/vividsolutions/jts/geom/MultiLineString;

    .line 65
    return-void
.end method

.method private extractLinework(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/MultiLineString;
    .locals 4
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 91
    new-instance v0, Lcom/vividsolutions/jts/operation/overlay/validate/PolygonalLineworkExtracter;

    invoke-direct {v0}, Lcom/vividsolutions/jts/operation/overlay/validate/PolygonalLineworkExtracter;-><init>()V

    .line 92
    .local v0, "extracter":Lcom/vividsolutions/jts/operation/overlay/validate/PolygonalLineworkExtracter;
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryFilter;)V

    .line 93
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/overlay/validate/PolygonalLineworkExtracter;->getLinework()Ljava/util/List;

    move-result-object v2

    .line 94
    .local v2, "linework":Ljava/util/List;
    invoke-static {v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->toLineStringArray(Ljava/util/Collection;)[Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    .line 95
    .local v1, "lines":[Lcom/vividsolutions/jts/geom/LineString;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiLineString([Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geom/MultiLineString;

    move-result-object v3

    return-object v3
.end method

.method private isWithinToleranceOfBoundary(Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 8
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 100
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/overlay/validate/FuzzyPointLocator;->linework:Lcom/vividsolutions/jts/geom/MultiLineString;

    invoke-virtual {v6}, Lcom/vividsolutions/jts/geom/MultiLineString;->getNumGeometries()I

    move-result v6

    if-ge v2, v6, :cond_2

    .line 101
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/overlay/validate/FuzzyPointLocator;->linework:Lcom/vividsolutions/jts/geom/MultiLineString;

    invoke-virtual {v6, v2}, Lcom/vividsolutions/jts/geom/MultiLineString;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    check-cast v4, Lcom/vividsolutions/jts/geom/LineString;

    .line 102
    .local v4, "line":Lcom/vividsolutions/jts/geom/LineString;
    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v5

    .line 103
    .local v5, "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    invoke-interface {v5}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ge v3, v6, :cond_1

    .line 104
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/overlay/validate/FuzzyPointLocator;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v6, v6, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v5, v3, v6}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 105
    add-int/lit8 v6, v3, 0x1

    iget-object v7, p0, Lcom/vividsolutions/jts/operation/overlay/validate/FuzzyPointLocator;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v7, v7, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v5, v6, v7}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 106
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/overlay/validate/FuzzyPointLocator;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-virtual {v6, p1}, Lcom/vividsolutions/jts/geom/LineSegment;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 107
    .local v0, "dist":D
    iget-wide v6, p0, Lcom/vividsolutions/jts/operation/overlay/validate/FuzzyPointLocator;->boundaryDistanceTolerance:D

    cmpg-double v6, v0, v6

    if-gtz v6, :cond_0

    .line 108
    const/4 v6, 0x1

    .line 111
    .end local v0    # "dist":D
    .end local v3    # "j":I
    .end local v4    # "line":Lcom/vividsolutions/jts/geom/LineString;
    .end local v5    # "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    :goto_2
    return v6

    .line 103
    .restart local v0    # "dist":D
    .restart local v3    # "j":I
    .restart local v4    # "line":Lcom/vividsolutions/jts/geom/LineString;
    .restart local v5    # "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 100
    .end local v0    # "dist":D
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 111
    .end local v3    # "j":I
    .end local v4    # "line":Lcom/vividsolutions/jts/geom/LineString;
    .end local v5    # "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    :cond_2
    const/4 v6, 0x0

    goto :goto_2
.end method


# virtual methods
.method public getLocation(Lcom/vividsolutions/jts/geom/Coordinate;)I
    .locals 2
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/overlay/validate/FuzzyPointLocator;->isWithinToleranceOfBoundary(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    const/4 v0, 0x1

    .line 80
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/validate/FuzzyPointLocator;->ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/overlay/validate/FuzzyPointLocator;->g:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0, p1, v1}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)I

    move-result v0

    goto :goto_0
.end method
