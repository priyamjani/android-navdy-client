.class public Lcom/vividsolutions/jts/geom/CoordinateArrays;
.super Ljava/lang/Object;
.source "CoordinateArrays.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/geom/CoordinateArrays$BidirectionalComparator;,
        Lcom/vividsolutions/jts/geom/CoordinateArrays$ForwardComparator;
    }
.end annotation


# static fields
.field private static final coordArrayType:[Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/vividsolutions/jts/geom/Coordinate;

    sput-object v0, Lcom/vividsolutions/jts/geom/CoordinateArrays;->coordArrayType:[Lcom/vividsolutions/jts/geom/Coordinate;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    return-void
.end method

.method static synthetic access$000([Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 1
    .param p0, "x0"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "x1"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 46
    invoke-static {p0, p1}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->isEqualReversed([Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    return v0
.end method

.method public static atLeastNCoordinatesOrNothing(I[Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1
    .param p0, "n"    # I
    .param p1, "c"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 282
    array-length v0, p1

    if-lt v0, p0, :cond_0

    .end local p1    # "c":[Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_0
    return-object p1

    .restart local p1    # "c":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    const/4 v0, 0x0

    new-array p1, v0, [Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_0
.end method

.method public static compare([Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)I
    .locals 4
    .param p0, "pts1"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "pts2"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 92
    const/4 v1, 0x0

    .line 93
    .local v1, "i":I
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_1

    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 94
    aget-object v2, p0, v1

    aget-object v3, p1, v1

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;->compareTo(Ljava/lang/Object;)I

    move-result v0

    .line 95
    .local v0, "compare":I
    if-eqz v0, :cond_0

    .line 103
    .end local v0    # "compare":I
    :goto_1
    return v0

    .line 97
    .restart local v0    # "compare":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 98
    goto :goto_0

    .line 100
    .end local v0    # "compare":I
    :cond_1
    array-length v2, p1

    if-ge v1, v2, :cond_2

    const/4 v0, -0x1

    goto :goto_1

    .line 101
    :cond_2
    array-length v2, p0

    if-ge v1, v2, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    .line 103
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static copyDeep([Lcom/vividsolutions/jts/geom/Coordinate;I[Lcom/vividsolutions/jts/geom/Coordinate;II)V
    .locals 4
    .param p0, "src"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "srcStart"    # I
    .param p2, "dest"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "destStart"    # I
    .param p4, "length"    # I

    .prologue
    .line 250
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p4, :cond_0

    .line 251
    add-int v1, p3, v0

    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    add-int v3, p1, v0

    aget-object v3, p0, v3

    invoke-direct {v2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v2, p2, v1

    .line 250
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 253
    :cond_0
    return-void
.end method

.method public static copyDeep([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 4
    .param p0, "coordinates"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 230
    array-length v2, p0

    new-array v0, v2, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 231
    .local v0, "copy":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_0

    .line 232
    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v3, p0, v1

    invoke-direct {v2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v2, v0, v1

    .line 231
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 234
    :cond_0
    return-object v0
.end method

.method public static equals([Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 5
    .param p0, "coord1"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "coord2"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 344
    if-ne p0, p1, :cond_1

    .line 350
    :cond_0
    :goto_0
    return v1

    .line 345
    :cond_1
    if-eqz p0, :cond_2

    if-nez p1, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    .line 346
    :cond_3
    array-length v3, p0

    array-length v4, p1

    if-eq v3, v4, :cond_4

    move v1, v2

    goto :goto_0

    .line 347
    :cond_4
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, p0

    if-ge v0, v3, :cond_0

    .line 348
    aget-object v3, p0, v0

    aget-object v4, p1, v0

    invoke-virtual {v3, v4}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    goto :goto_0

    .line 347
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static equals([Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;Ljava/util/Comparator;)Z
    .locals 5
    .param p0, "coord1"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "coord2"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "coordinateComparator"    # Ljava/util/Comparator;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 366
    if-ne p0, p1, :cond_1

    .line 373
    :cond_0
    :goto_0
    return v1

    .line 367
    :cond_1
    if-eqz p0, :cond_2

    if-nez p1, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    .line 368
    :cond_3
    array-length v3, p0

    array-length v4, p1

    if-eq v3, v4, :cond_4

    move v1, v2

    goto :goto_0

    .line 369
    :cond_4
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, p0

    if-ge v0, v3, :cond_0

    .line 370
    aget-object v3, p0, v0

    aget-object v4, p1, v0

    invoke-interface {p2, v3, v4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-eqz v3, :cond_5

    move v1, v2

    .line 371
    goto :goto_0

    .line 369
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static extract([Lcom/vividsolutions/jts/geom/Coordinate;II)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 7
    .param p0, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 442
    const/4 v5, 0x0

    array-length v6, p0

    invoke-static {p1, v5, v6}, Lcom/vividsolutions/jts/math/MathUtil;->clamp(III)I

    move-result p1

    .line 443
    const/4 v5, -0x1

    array-length v6, p0

    invoke-static {p2, v5, v6}, Lcom/vividsolutions/jts/math/MathUtil;->clamp(III)I

    move-result p2

    .line 445
    sub-int v5, p2, p1

    add-int/lit8 v4, v5, 0x1

    .line 446
    .local v4, "npts":I
    if-gez p2, :cond_0

    const/4 v4, 0x0

    .line 447
    :cond_0
    array-length v5, p0

    if-lt p1, v5, :cond_1

    const/4 v4, 0x0

    .line 448
    :cond_1
    if-ge p2, p1, :cond_2

    const/4 v4, 0x0

    .line 450
    :cond_2
    new-array v0, v4, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 451
    .local v0, "extractPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    if-nez v4, :cond_4

    .line 457
    :cond_3
    return-object v0

    .line 453
    :cond_4
    const/4 v2, 0x0

    .line 454
    .local v2, "iPts":I
    move v1, p1

    .local v1, "i":I
    move v3, v2

    .end local v2    # "iPts":I
    .local v3, "iPts":I
    :goto_0
    if-gt v1, p2, :cond_3

    .line 455
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "iPts":I
    .restart local v2    # "iPts":I
    aget-object v5, p0, v1

    aput-object v5, v0, v3

    .line 454
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    .end local v2    # "iPts":I
    .restart local v3    # "iPts":I
    goto :goto_0
.end method

.method public static hasRepeatedPoints([Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 3
    .param p0, "coord"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 269
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 270
    add-int/lit8 v1, v0, -0x1

    aget-object v1, p0, v1

    aget-object v2, p0, v0

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 271
    const/4 v1, 0x1

    .line 274
    :goto_1
    return v1

    .line 269
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 274
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static increasingDirection([Lcom/vividsolutions/jts/geom/Coordinate;)I
    .locals 5
    .param p0, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 138
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p0

    div-int/lit8 v3, v3, 0x2

    if-ge v1, v3, :cond_1

    .line 139
    array-length v3, p0

    add-int/lit8 v3, v3, -0x1

    sub-int v2, v3, v1

    .line 141
    .local v2, "j":I
    aget-object v3, p0, v1

    aget-object v4, p0, v2

    invoke-virtual {v3, v4}, Lcom/vividsolutions/jts/geom/Coordinate;->compareTo(Ljava/lang/Object;)I

    move-result v0

    .line 142
    .local v0, "comp":I
    if-eqz v0, :cond_0

    .line 146
    .end local v0    # "comp":I
    .end local v2    # "j":I
    :goto_1
    return v0

    .line 138
    .restart local v0    # "comp":I
    .restart local v2    # "j":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 146
    .end local v0    # "comp":I
    .end local v2    # "j":I
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public static indexOf(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)I
    .locals 2
    .param p0, "coordinate"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "coordinates"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 419
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 420
    aget-object v1, p1, v0

    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 424
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 419
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 424
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private static isEqualReversed([Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 4
    .param p0, "pts1"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "pts2"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 159
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p0

    if-ge v0, v3, :cond_1

    .line 160
    aget-object v1, p0, v0

    .line 161
    .local v1, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    array-length v3, p0

    sub-int/2addr v3, v0

    add-int/lit8 v3, v3, -0x1

    aget-object v2, p1, v3

    .line 162
    .local v2, "p2":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/Coordinate;->compareTo(Ljava/lang/Object;)I

    move-result v3

    if-eqz v3, :cond_0

    .line 163
    const/4 v3, 0x0

    .line 165
    .end local v1    # "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v2    # "p2":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_1
    return v3

    .line 159
    .restart local v1    # "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    .restart local v2    # "p2":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 165
    .end local v1    # "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v2    # "p2":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_1
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public static isRing([Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 3
    .param p0, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v0, 0x0

    .line 60
    array-length v1, p0

    const/4 v2, 0x4

    if-ge v1, v2, :cond_1

    .line 62
    :cond_0
    :goto_0
    return v0

    .line 61
    :cond_1
    aget-object v1, p0, v0

    array-length v2, p0

    add-int/lit8 v2, v2, -0x1

    aget-object v2, p0, v2

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static minCoordinate([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 3
    .param p0, "coordinates"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 385
    const/4 v1, 0x0

    .line 386
    .local v1, "minCoord":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 387
    if-eqz v1, :cond_0

    aget-object v2, p0, v0

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/Coordinate;->compareTo(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_1

    .line 388
    :cond_0
    aget-object v1, p0, v0

    .line 386
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 391
    :cond_2
    return-object v1
.end method

.method public static ptNotInList([Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 3
    .param p0, "testPts"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 74
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 75
    aget-object v1, p0, v0

    .line 76
    .local v1, "testPt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-static {v1, p1}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->indexOf(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v2

    if-gez v2, :cond_0

    .line 79
    .end local v1    # "testPt":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_1
    return-object v1

    .line 74
    .restart local v1    # "testPt":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 79
    .end local v1    # "testPt":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static removeNull([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 6
    .param p0, "coord"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 306
    const/4 v4, 0x0

    .line 307
    .local v4, "nonNull":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v5, p0

    if-ge v0, v5, :cond_1

    .line 308
    aget-object v5, p0, v0

    if-eqz v5, :cond_0

    add-int/lit8 v4, v4, 0x1

    .line 307
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 310
    :cond_1
    new-array v3, v4, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 312
    .local v3, "newCoord":[Lcom/vividsolutions/jts/geom/Coordinate;
    if-nez v4, :cond_3

    .line 318
    :cond_2
    return-object v3

    .line 314
    :cond_3
    const/4 v1, 0x0

    .line 315
    .local v1, "j":I
    const/4 v0, 0x0

    :goto_1
    array-length v5, p0

    if-ge v0, v5, :cond_2

    .line 316
    aget-object v5, p0, v0

    if-eqz v5, :cond_4

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "j":I
    .local v2, "j":I
    aget-object v5, p0, v0

    aput-object v5, v3, v1

    move v1, v2

    .line 315
    .end local v2    # "j":I
    .restart local v1    # "j":I
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static removeRepeatedPoints([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 2
    .param p0, "coord"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 293
    invoke-static {p0}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->hasRepeatedPoints([Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 295
    .end local p0    # "coord":[Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_0
    return-object p0

    .line 294
    .restart local p0    # "coord":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    new-instance v0, Lcom/vividsolutions/jts/geom/CoordinateList;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vividsolutions/jts/geom/CoordinateList;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 295
    .local v0, "coordList":Lcom/vividsolutions/jts/geom/CoordinateList;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/CoordinateList;->toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object p0

    goto :goto_0
.end method

.method public static reverse([Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 5
    .param p0, "coord"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 326
    array-length v4, p0

    add-int/lit8 v1, v4, -0x1

    .line 327
    .local v1, "last":I
    div-int/lit8 v2, v1, 0x2

    .line 328
    .local v2, "mid":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-gt v0, v2, :cond_0

    .line 329
    aget-object v3, p0, v0

    .line 330
    .local v3, "tmp":Lcom/vividsolutions/jts/geom/Coordinate;
    sub-int v4, v1, v0

    aget-object v4, p0, v4

    aput-object v4, p0, v0

    .line 331
    sub-int v4, v1, v0

    aput-object v3, p0, v4

    .line 328
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 333
    .end local v3    # "tmp":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    return-void
.end method

.method public static scroll([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 4
    .param p0, "coordinates"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "firstCoordinate"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v3, 0x0

    .line 401
    invoke-static {p1, p0}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->indexOf(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v0

    .line 402
    .local v0, "i":I
    if-gez v0, :cond_0

    .line 407
    :goto_0
    return-void

    .line 403
    :cond_0
    array-length v2, p0

    new-array v1, v2, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 404
    .local v1, "newCoordinates":[Lcom/vividsolutions/jts/geom/Coordinate;
    array-length v2, p0

    sub-int/2addr v2, v0

    invoke-static {p0, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 405
    array-length v2, p0

    sub-int/2addr v2, v0

    invoke-static {p0, v3, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 406
    array-length v2, p0

    invoke-static {v1, v3, p0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public static toCoordinateArray(Ljava/util/Collection;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1
    .param p0, "coordList"    # Ljava/util/Collection;

    .prologue
    .line 260
    sget-object v0, Lcom/vividsolutions/jts/geom/CoordinateArrays;->coordArrayType:[Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {p0, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vividsolutions/jts/geom/Coordinate;

    check-cast v0, [Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method
