.class Lcom/vividsolutions/jts/geom/OctagonalEnvelope$BoundingOctagonComponentFilter;
.super Ljava/lang/Object;
.source "OctagonalEnvelope.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/GeometryComponentFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/geom/OctagonalEnvelope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BoundingOctagonComponentFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vividsolutions/jts/geom/OctagonalEnvelope;


# direct methods
.method private constructor <init>(Lcom/vividsolutions/jts/geom/OctagonalEnvelope;)V
    .locals 0

    .prologue
    .line 349
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope$BoundingOctagonComponentFilter;->this$0:Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vividsolutions/jts/geom/OctagonalEnvelope;Lcom/vividsolutions/jts/geom/OctagonalEnvelope$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vividsolutions/jts/geom/OctagonalEnvelope;
    .param p2, "x1"    # Lcom/vividsolutions/jts/geom/OctagonalEnvelope$1;

    .prologue
    .line 349
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope$BoundingOctagonComponentFilter;-><init>(Lcom/vividsolutions/jts/geom/OctagonalEnvelope;)V

    return-void
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 354
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v0, :cond_1

    .line 355
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope$BoundingOctagonComponentFilter;->this$0:Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    check-cast p1, Lcom/vividsolutions/jts/geom/LineString;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->expandToInclude(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 357
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope$BoundingOctagonComponentFilter;->this$0:Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    check-cast p1, Lcom/vividsolutions/jts/geom/Point;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Point;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->expandToInclude(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    goto :goto_0
.end method
