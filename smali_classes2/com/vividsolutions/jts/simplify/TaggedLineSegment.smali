.class Lcom/vividsolutions/jts/simplify/TaggedLineSegment;
.super Lcom/vividsolutions/jts/geom/LineSegment;
.source "TaggedLineSegment.java"


# instance fields
.field private index:I

.field private parent:Lcom/vividsolutions/jts/geom/Geometry;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 2
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 56
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/vividsolutions/jts/simplify/TaggedLineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;I)V

    .line 57
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;I)V
    .locals 0
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "parent"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p4, "index"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 51
    iput-object p3, p0, Lcom/vividsolutions/jts/simplify/TaggedLineSegment;->parent:Lcom/vividsolutions/jts/geom/Geometry;

    .line 52
    iput p4, p0, Lcom/vividsolutions/jts/simplify/TaggedLineSegment;->index:I

    .line 53
    return-void
.end method


# virtual methods
.method public getIndex()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineSegment;->index:I

    return v0
.end method

.method public getParent()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineSegment;->parent:Lcom/vividsolutions/jts/geom/Geometry;

    return-object v0
.end method
