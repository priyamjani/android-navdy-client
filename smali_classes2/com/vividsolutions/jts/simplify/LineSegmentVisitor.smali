.class Lcom/vividsolutions/jts/simplify/LineSegmentVisitor;
.super Ljava/lang/Object;
.source "LineSegmentIndex.java"

# interfaces
.implements Lcom/vividsolutions/jts/index/ItemVisitor;


# instance fields
.field private items:Ljava/util/ArrayList;

.field private querySeg:Lcom/vividsolutions/jts/geom/LineSegment;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/LineSegment;)V
    .locals 1
    .param p1, "querySeg"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/simplify/LineSegmentVisitor;->items:Ljava/util/ArrayList;

    .line 102
    iput-object p1, p0, Lcom/vividsolutions/jts/simplify/LineSegmentVisitor;->querySeg:Lcom/vividsolutions/jts/geom/LineSegment;

    .line 103
    return-void
.end method


# virtual methods
.method public getItems()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/vividsolutions/jts/simplify/LineSegmentVisitor;->items:Ljava/util/ArrayList;

    return-object v0
.end method

.method public visitItem(Ljava/lang/Object;)V
    .locals 5
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 107
    move-object v0, p1

    check-cast v0, Lcom/vividsolutions/jts/geom/LineSegment;

    .line 108
    .local v0, "seg":Lcom/vividsolutions/jts/geom/LineSegment;
    iget-object v1, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v2, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v3, p0, Lcom/vividsolutions/jts/simplify/LineSegmentVisitor;->querySeg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v3, v3, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v4, p0, Lcom/vividsolutions/jts/simplify/LineSegmentVisitor;->querySeg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v4, v4, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-static {v1, v2, v3, v4}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    iget-object v1, p0, Lcom/vividsolutions/jts/simplify/LineSegmentVisitor;->items:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    :cond_0
    return-void
.end method
