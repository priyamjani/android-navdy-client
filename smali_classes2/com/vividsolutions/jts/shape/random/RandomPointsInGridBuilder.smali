.class public Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;
.super Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;
.source "RandomPointsInGridBuilder.java"


# instance fields
.field private gutterFraction:D

.field private isConstrainedToCircle:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 64
    new-instance v0, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>()V

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->isConstrainedToCircle:Z

    .line 56
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->gutterFraction:D

    .line 65
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 2
    .param p1, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->isConstrainedToCircle:Z

    .line 56
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->gutterFraction:D

    .line 76
    return-void
.end method

.method private randomPointInCell(DDDD)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1
    .param p1, "orgX"    # D
    .param p3, "orgY"    # D
    .param p5, "xLen"    # D
    .param p7, "yLen"    # D

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->isConstrainedToCircle:Z

    if-eqz v0, :cond_0

    .line 141
    invoke-static/range {p1 .. p8}, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->randomPointInCircle(DDDD)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 146
    :goto_0
    return-object v0

    :cond_0
    invoke-direct/range {p0 .. p8}, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->randomPointInGridCell(DDDD)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    goto :goto_0
.end method

.method private static randomPointInCircle(DDDD)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 28
    .param p0, "orgX"    # D
    .param p2, "orgY"    # D
    .param p4, "width"    # D
    .param p6, "height"    # D

    .prologue
    .line 158
    const-wide/high16 v24, 0x4000000000000000L    # 2.0

    div-double v24, p4, v24

    add-double v6, p0, v24

    .line 159
    .local v6, "centreX":D
    const-wide/high16 v24, 0x4000000000000000L    # 2.0

    div-double v24, p6, v24

    add-double v8, p2, v24

    .line 161
    .local v8, "centreY":D
    const-wide v24, 0x401921fb54442d18L    # 6.283185307179586

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v26

    mul-double v10, v24, v26

    .line 162
    .local v10, "rndAng":D
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v12

    .line 164
    .local v12, "rndRadius":D
    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v14

    .line 165
    .local v14, "rndRadius2":D
    const-wide/high16 v24, 0x4000000000000000L    # 2.0

    div-double v24, p4, v24

    mul-double v24, v24, v14

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v26

    mul-double v16, v24, v26

    .line 166
    .local v16, "rndX":D
    const-wide/high16 v24, 0x4000000000000000L    # 2.0

    div-double v24, p6, v24

    mul-double v24, v24, v14

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v26

    mul-double v18, v24, v26

    .line 168
    .local v18, "rndY":D
    add-double v20, v6, v16

    .line 169
    .local v20, "x0":D
    add-double v22, v8, v18

    .line 170
    .local v22, "y0":D
    new-instance v24, Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v0, v24

    move-wide/from16 v1, v20

    move-wide/from16 v3, v22

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    return-object v24
.end method

.method private randomPointInGridCell(DDDD)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 7
    .param p1, "orgX"    # D
    .param p3, "orgY"    # D
    .param p5, "xLen"    # D
    .param p7, "yLen"    # D

    .prologue
    .line 151
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    mul-double/2addr v4, p5

    add-double v0, p1, v4

    .line 152
    .local v0, "x":D
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    mul-double/2addr v4, p7

    add-double v2, p3, v4

    .line 153
    .local v2, "y":D
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->createCoord(DD)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    return-object v4
.end method


# virtual methods
.method public getGeometry()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 32

    .prologue
    .line 111
    move-object/from16 v0, p0

    iget v2, v0, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->numPts:I

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-int v0, v2

    move/from16 v28, v0

    .line 113
    .local v28, "nCells":I
    mul-int v2, v28, v28

    move-object/from16 v0, p0

    iget v3, v0, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->numPts:I

    if-ge v2, v3, :cond_0

    .line 114
    add-int/lit8 v28, v28, 0x1

    .line 116
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->getExtent()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Envelope;->getWidth()D

    move-result-wide v2

    move/from16 v0, v28

    int-to-double v0, v0

    move-wide/from16 v30, v0

    div-double v14, v2, v30

    .line 117
    .local v14, "gridDX":D
    invoke-virtual/range {p0 .. p0}, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->getExtent()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Envelope;->getHeight()D

    move-result-wide v2

    move/from16 v0, v28

    int-to-double v0, v0

    move-wide/from16 v30, v0

    div-double v16, v2, v30

    .line 119
    .local v16, "gridDY":D
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->gutterFraction:D

    const-wide/16 v4, 0x0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-static/range {v2 .. v7}, Lcom/vividsolutions/jts/math/MathUtil;->clamp(DDD)D

    move-result-wide v18

    .line 120
    .local v18, "gutterFrac":D
    mul-double v2, v14, v18

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    div-double v20, v2, v30

    .line 121
    .local v20, "gutterOffsetX":D
    mul-double v2, v16, v18

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    div-double v22, v2, v30

    .line 122
    .local v22, "gutterOffsetY":D
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double v12, v2, v18

    .line 123
    .local v12, "cellFrac":D
    mul-double v8, v12, v14

    .line 124
    .local v8, "cellDX":D
    mul-double v10, v12, v16

    .line 126
    .local v10, "cellDY":D
    mul-int v2, v28, v28

    new-array v0, v2, [Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v29, v0

    .line 127
    .local v29, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/16 v25, 0x0

    .line 128
    .local v25, "index":I
    const/16 v24, 0x0

    .local v24, "i":I
    :goto_0
    move/from16 v0, v24

    move/from16 v1, v28

    if-ge v0, v1, :cond_2

    .line 129
    const/16 v27, 0x0

    .local v27, "j":I
    move/from16 v26, v25

    .end local v25    # "index":I
    .local v26, "index":I
    :goto_1
    move/from16 v0, v27

    move/from16 v1, v28

    if-ge v0, v1, :cond_1

    .line 130
    invoke-virtual/range {p0 .. p0}, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->getExtent()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v2

    move/from16 v0, v24

    int-to-double v0, v0

    move-wide/from16 v30, v0

    mul-double v30, v30, v14

    add-double v2, v2, v30

    add-double v4, v2, v20

    .line 131
    .local v4, "orgX":D
    invoke-virtual/range {p0 .. p0}, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->getExtent()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v2

    move/from16 v0, v27

    int-to-double v0, v0

    move-wide/from16 v30, v0

    mul-double v30, v30, v16

    add-double v2, v2, v30

    add-double v6, v2, v22

    .line 132
    .local v6, "orgY":D
    add-int/lit8 v25, v26, 0x1

    .end local v26    # "index":I
    .restart local v25    # "index":I
    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v11}, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->randomPointInCell(DDDD)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    aput-object v2, v29, v26

    .line 129
    add-int/lit8 v27, v27, 0x1

    move/from16 v26, v25

    .end local v25    # "index":I
    .restart local v26    # "index":I
    goto :goto_1

    .line 128
    .end local v4    # "orgX":D
    .end local v6    # "orgY":D
    :cond_1
    add-int/lit8 v24, v24, 0x1

    move/from16 v25, v26

    .end local v26    # "index":I
    .restart local v25    # "index":I
    goto :goto_0

    .line 135
    .end local v27    # "j":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiPoint([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/MultiPoint;

    move-result-object v2

    return-object v2
.end method

.method public setConstrainedToCircle(Z)V
    .locals 0
    .param p1, "isConstrainedToCircle"    # Z

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->isConstrainedToCircle:Z

    .line 90
    return-void
.end method

.method public setGutterFraction(D)V
    .locals 1
    .param p1, "gutterFraction"    # D

    .prologue
    .line 101
    iput-wide p1, p0, Lcom/vividsolutions/jts/shape/random/RandomPointsInGridBuilder;->gutterFraction:D

    .line 102
    return-void
.end method
