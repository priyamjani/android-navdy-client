.class public Lcom/vividsolutions/jts/algorithm/InteriorPointArea;
.super Ljava/lang/Object;
.source "InteriorPointArea.java"


# instance fields
.field private factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field private interiorPoint:Lcom/vividsolutions/jts/geom/Coordinate;

.field private maxWidth:D


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->interiorPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 67
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->maxWidth:D

    .line 71
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 72
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 73
    return-void
.end method

.method private add(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 3
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 88
    instance-of v2, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v2, :cond_1

    .line 89
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->addPolygon(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 97
    :cond_0
    return-void

    .line 91
    :cond_1
    instance-of v2, p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 92
    check-cast v0, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .line 93
    .local v0, "gc":Lcom/vividsolutions/jts/geom/GeometryCollection;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 94
    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 93
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static avg(DD)D
    .locals 4
    .param p0, "a"    # D
    .param p2, "b"    # D

    .prologue
    .line 62
    add-double v0, p0, p2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private widestGeometry(Lcom/vividsolutions/jts/geom/GeometryCollection;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 6
    .param p1, "gc"    # Lcom/vividsolutions/jts/geom/GeometryCollection;

    .prologue
    .line 126
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 137
    .end local p1    # "gc":Lcom/vividsolutions/jts/geom/GeometryCollection;
    :goto_0
    return-object p1

    .line 130
    .restart local p1    # "gc":Lcom/vividsolutions/jts/geom/GeometryCollection;
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    .line 131
    .local v1, "widestGeometry":Lcom/vividsolutions/jts/geom/Geometry;
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 132
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Envelope;->getWidth()D

    move-result-wide v2

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Envelope;->getWidth()D

    move-result-wide v4

    cmpl-double v2, v2, v4

    if-lez v2, :cond_1

    .line 134
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    .line 131
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object p1, v1

    .line 137
    goto :goto_0
.end method


# virtual methods
.method public addPolygon(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 8
    .param p1, "geometry"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 104
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->horizontalBisector(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    .line 106
    .local v0, "bisector":Lcom/vividsolutions/jts/geom/LineString;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/LineString;->intersection(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    .line 107
    .local v1, "intersections":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->widestGeometry(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    .line 109
    .local v2, "widestIntersection":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/Envelope;->getWidth()D

    move-result-wide v4

    .line 110
    .local v4, "width":D
    iget-object v3, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->interiorPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    if-eqz v3, :cond_0

    iget-wide v6, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->maxWidth:D

    cmpl-double v3, v4, v6

    if-lez v3, :cond_1

    .line 111
    :cond_0
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->centre(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    iput-object v3, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->interiorPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 112
    iput-wide v4, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->maxWidth:D

    .line 114
    :cond_1
    return-void
.end method

.method public centre(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 8
    .param p1, "envelope"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 157
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->avg(DD)D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->avg(DD)D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    return-object v0
.end method

.method public getInteriorPoint()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->interiorPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method protected horizontalBisector(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/LineString;
    .locals 10
    .param p1, "geometry"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 141
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v2

    .line 144
    .local v2, "envelope":Lcom/vividsolutions/jts/geom/Envelope;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v4

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->avg(DD)D

    move-result-wide v0

    .line 145
    .local v0, "avgY":D
    iget-object v3, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v5, 0x0

    new-instance v6, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v8

    invoke-direct {v6, v8, v9, v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    aput-object v6, v4, v5

    const/4 v5, 0x1

    new-instance v6, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v8

    invoke-direct {v6, v8, v9, v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v3

    return-object v3
.end method

.method protected widestGeometry(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "geometry"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 119
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-nez v0, :cond_0

    .line 122
    .end local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :goto_0
    return-object p1

    .restart local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    check-cast p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .end local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->widestGeometry(Lcom/vividsolutions/jts/geom/GeometryCollection;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object p1

    goto :goto_0
.end method
