.class public Lcom/vividsolutions/jts/algorithm/SimplePointInRing;
.super Ljava/lang/Object;
.source "SimplePointInRing.java"

# interfaces
.implements Lcom/vividsolutions/jts/algorithm/PointInRing;


# instance fields
.field private pts:[Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/LinearRing;)V
    .locals 1
    .param p1, "ring"    # Lcom/vividsolutions/jts/geom/LinearRing;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/SimplePointInRing;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 53
    return-void
.end method


# virtual methods
.method public isInside(Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 1
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/SimplePointInRing;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-static {p1, v0}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isPointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    return v0
.end method
