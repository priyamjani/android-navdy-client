.class public Lcom/vividsolutions/jts/index/quadtree/Quadtree;
.super Ljava/lang/Object;
.source "Quadtree.java"

# interfaces
.implements Lcom/vividsolutions/jts/index/SpatialIndex;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x678b60c967a255b4L


# instance fields
.field private minExtent:D

.field private root:Lcom/vividsolutions/jts/index/quadtree/Root;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->minExtent:D

    .line 121
    new-instance v0, Lcom/vividsolutions/jts/index/quadtree/Root;

    invoke-direct {v0}, Lcom/vividsolutions/jts/index/quadtree/Root;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->root:Lcom/vividsolutions/jts/index/quadtree/Root;

    .line 122
    return-void
.end method

.method private collectStats(Lcom/vividsolutions/jts/geom/Envelope;)V
    .locals 8
    .param p1, "itemEnv"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    const-wide/16 v6, 0x0

    .line 250
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getWidth()D

    move-result-wide v0

    .line 251
    .local v0, "delX":D
    iget-wide v4, p0, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->minExtent:D

    cmpg-double v4, v0, v4

    if-gez v4, :cond_0

    cmpl-double v4, v0, v6

    if-lez v4, :cond_0

    .line 252
    iput-wide v0, p0, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->minExtent:D

    .line 254
    :cond_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getHeight()D

    move-result-wide v2

    .line 255
    .local v2, "delY":D
    iget-wide v4, p0, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->minExtent:D

    cmpg-double v4, v2, v4

    if-gez v4, :cond_1

    cmpl-double v4, v2, v6

    if-lez v4, :cond_1

    .line 256
    iput-wide v2, p0, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->minExtent:D

    .line 257
    :cond_1
    return-void
.end method

.method public static ensureExtent(Lcom/vividsolutions/jts/geom/Envelope;D)Lcom/vividsolutions/jts/geom/Envelope;
    .locals 13
    .param p0, "itemEnv"    # Lcom/vividsolutions/jts/geom/Envelope;
    .param p1, "minExtent"    # D

    .prologue
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    .line 85
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v2

    .line 86
    .local v2, "minx":D
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v4

    .line 87
    .local v4, "maxx":D
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v6

    .line 88
    .local v6, "miny":D
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v8

    .line 90
    .local v8, "maxy":D
    cmpl-double v0, v2, v4

    if-eqz v0, :cond_0

    cmpl-double v0, v6, v8

    if-eqz v0, :cond_0

    .line 101
    .end local p0    # "itemEnv":Lcom/vividsolutions/jts/geom/Envelope;
    :goto_0
    return-object p0

    .line 93
    .restart local p0    # "itemEnv":Lcom/vividsolutions/jts/geom/Envelope;
    :cond_0
    cmpl-double v0, v2, v4

    if-nez v0, :cond_1

    .line 94
    div-double v0, p1, v10

    sub-double/2addr v2, v0

    .line 95
    div-double v0, p1, v10

    add-double v4, v2, v0

    .line 97
    :cond_1
    cmpl-double v0, v6, v8

    if-nez v0, :cond_2

    .line 98
    div-double v0, p1, v10

    sub-double/2addr v6, v0

    .line 99
    div-double v0, p1, v10

    add-double v8, v6, v0

    .line 101
    :cond_2
    new-instance v1, Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct/range {v1 .. v9}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(DDDD)V

    move-object p0, v1

    goto :goto_0
.end method


# virtual methods
.method public depth()I
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->root:Lcom/vividsolutions/jts/index/quadtree/Root;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->root:Lcom/vividsolutions/jts/index/quadtree/Root;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/quadtree/Root;->depth()I

    move-result v0

    .line 133
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public insert(Lcom/vividsolutions/jts/geom/Envelope;Ljava/lang/Object;)V
    .locals 4
    .param p1, "itemEnv"    # Lcom/vividsolutions/jts/geom/Envelope;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->collectStats(Lcom/vividsolutions/jts/geom/Envelope;)V

    .line 161
    iget-wide v2, p0, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->minExtent:D

    invoke-static {p1, v2, v3}, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->ensureExtent(Lcom/vividsolutions/jts/geom/Envelope;D)Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    .line 162
    .local v0, "insertEnv":Lcom/vividsolutions/jts/geom/Envelope;
    iget-object v1, p0, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->root:Lcom/vividsolutions/jts/index/quadtree/Root;

    invoke-virtual {v1, v0, p2}, Lcom/vividsolutions/jts/index/quadtree/Root;->insert(Lcom/vividsolutions/jts/geom/Envelope;Ljava/lang/Object;)V

    .line 163
    return-void
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->root:Lcom/vividsolutions/jts/index/quadtree/Root;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 144
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public query(Lcom/vividsolutions/jts/geom/Envelope;)Ljava/util/List;
    .locals 2
    .param p1, "searchEnv"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 211
    new-instance v0, Lcom/vividsolutions/jts/index/ArrayListVisitor;

    invoke-direct {v0}, Lcom/vividsolutions/jts/index/ArrayListVisitor;-><init>()V

    .line 212
    .local v0, "visitor":Lcom/vividsolutions/jts/index/ArrayListVisitor;
    invoke-virtual {p0, p1, v0}, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->query(Lcom/vividsolutions/jts/geom/Envelope;Lcom/vividsolutions/jts/index/ItemVisitor;)V

    .line 213
    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/ArrayListVisitor;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    return-object v1
.end method

.method public query(Lcom/vividsolutions/jts/geom/Envelope;Lcom/vividsolutions/jts/index/ItemVisitor;)V
    .locals 1
    .param p1, "searchEnv"    # Lcom/vividsolutions/jts/geom/Envelope;
    .param p2, "visitor"    # Lcom/vividsolutions/jts/index/ItemVisitor;

    .prologue
    .line 235
    iget-object v0, p0, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->root:Lcom/vividsolutions/jts/index/quadtree/Root;

    invoke-virtual {v0, p1, p2}, Lcom/vividsolutions/jts/index/quadtree/Root;->visit(Lcom/vividsolutions/jts/geom/Envelope;Lcom/vividsolutions/jts/index/ItemVisitor;)V

    .line 236
    return-void
.end method

.method public queryAll()Ljava/util/List;
    .locals 2

    .prologue
    .line 243
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 244
    .local v0, "foundItems":Ljava/util/List;
    iget-object v1, p0, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->root:Lcom/vividsolutions/jts/index/quadtree/Root;

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/index/quadtree/Root;->addAllItems(Ljava/util/List;)Ljava/util/List;

    .line 245
    return-object v0
.end method

.method public remove(Lcom/vividsolutions/jts/geom/Envelope;Ljava/lang/Object;)Z
    .locals 4
    .param p1, "itemEnv"    # Lcom/vividsolutions/jts/geom/Envelope;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 174
    iget-wide v2, p0, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->minExtent:D

    invoke-static {p1, v2, v3}, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->ensureExtent(Lcom/vividsolutions/jts/geom/Envelope;D)Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    .line 175
    .local v0, "posEnv":Lcom/vividsolutions/jts/geom/Envelope;
    iget-object v1, p0, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->root:Lcom/vividsolutions/jts/index/quadtree/Root;

    invoke-virtual {v1, v0, p2}, Lcom/vividsolutions/jts/index/quadtree/Root;->remove(Lcom/vividsolutions/jts/geom/Envelope;Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public size()I
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->root:Lcom/vividsolutions/jts/index/quadtree/Root;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->root:Lcom/vividsolutions/jts/index/quadtree/Root;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/quadtree/Root;->size()I

    move-result v0

    .line 155
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
