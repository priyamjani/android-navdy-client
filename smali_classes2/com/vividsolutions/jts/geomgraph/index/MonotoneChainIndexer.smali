.class public Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainIndexer;
.super Ljava/lang/Object;
.source "MonotoneChainIndexer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    return-void
.end method

.method private findChainEnd([Lcom/vividsolutions/jts/geom/Coordinate;I)I
    .locals 5
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "start"    # I

    .prologue
    .line 100
    aget-object v3, p1, p2

    add-int/lit8 v4, p2, 0x1

    aget-object v4, p1, v4

    invoke-static {v3, v4}, Lcom/vividsolutions/jts/geomgraph/Quadrant;->quadrant(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v0

    .line 101
    .local v0, "chainQuad":I
    add-int/lit8 v1, p2, 0x1

    .line 102
    .local v1, "last":I
    :goto_0
    array-length v3, p1

    if-ge v1, v3, :cond_0

    .line 104
    add-int/lit8 v3, v1, -0x1

    aget-object v3, p1, v3

    aget-object v4, p1, v1

    invoke-static {v3, v4}, Lcom/vividsolutions/jts/geomgraph/Quadrant;->quadrant(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v2

    .line 105
    .local v2, "quad":I
    if-eq v2, v0, :cond_1

    .line 108
    .end local v2    # "quad":I
    :cond_0
    add-int/lit8 v3, v1, -0x1

    return v3

    .line 106
    .restart local v2    # "quad":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 107
    goto :goto_0
.end method

.method public static toIntArray(Ljava/util/List;)[I
    .locals 3
    .param p0, "list"    # Ljava/util/List;

    .prologue
    .line 68
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    new-array v0, v2, [I

    .line 69
    .local v0, "array":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 70
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v0, v1

    .line 69
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 72
    :cond_0
    return-object v0
.end method


# virtual methods
.method public getChainStartIndices([Lcom/vividsolutions/jts/geom/Coordinate;)[I
    .locals 5
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 81
    const/4 v1, 0x0

    .line 82
    .local v1, "start":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 83
    .local v3, "startIndexList":Ljava/util/List;
    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    :cond_0
    invoke-direct {p0, p1, v1}, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainIndexer;->findChainEnd([Lcom/vividsolutions/jts/geom/Coordinate;I)I

    move-result v0

    .line 86
    .local v0, "last":I
    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    move v1, v0

    .line 88
    array-length v4, p1

    add-int/lit8 v4, v4, -0x1

    if-lt v1, v4, :cond_0

    .line 90
    invoke-static {v3}, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainIndexer;->toIntArray(Ljava/util/List;)[I

    move-result-object v2

    .line 91
    .local v2, "startIndex":[I
    return-object v2
.end method
