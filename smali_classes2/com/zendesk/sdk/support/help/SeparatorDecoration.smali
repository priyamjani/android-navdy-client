.class Lcom/zendesk/sdk/support/help/SeparatorDecoration;
.super Landroid/support/v7/widget/RecyclerView$ItemDecoration;
.source "SeparatorDecoration.java"


# instance fields
.field private mDivider:Landroid/graphics/drawable/Drawable;


# direct methods
.method constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "divider"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$ItemDecoration;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/SeparatorDecoration;->mDivider:Landroid/graphics/drawable/Drawable;

    .line 23
    return-void
.end method

.method private isItemACategory(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z
    .locals 1
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 84
    instance-of v0, p1, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;

    return v0
.end method

.method private isItemAnExpandedCategory(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z
    .locals 1
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 94
    instance-of v0, p1, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;

    .line 95
    .end local p1    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isItemAnUnexpandedCategory(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z
    .locals 1
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 105
    instance-of v0, p1, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;

    .line 106
    .end local p1    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->isExpanded()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onDrawOver(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 10
    .param p1, "c"    # Landroid/graphics/Canvas;
    .param p2, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p3, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    .line 27
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView$ItemDecoration;->onDraw(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;)V

    .line 33
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getItemAnimator()Landroid/support/v7/widget/RecyclerView$ItemAnimator;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getItemAnimator()Landroid/support/v7/widget/RecyclerView$ItemAnimator;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v7/widget/RecyclerView$ItemAnimator;->isRunning()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 55
    :cond_0
    return-void

    .line 37
    :cond_1
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v1

    .line 38
    .local v1, "childCount":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v1, :cond_0

    .line 40
    invoke-virtual {p2, v6}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 42
    .local v0, "child":Landroid/view/View;
    invoke-virtual {p0, p2, v6}, Lcom/zendesk/sdk/support/help/SeparatorDecoration;->shouldShowTopSeparator(Landroid/support/v7/widget/RecyclerView;I)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 43
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    .line 44
    .local v3, "dividerLeft":I
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v8

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v9

    sub-int v4, v8, v9

    .line 46
    .local v4, "dividerRight":I
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .line 48
    .local v7, "params":Landroid/support/v7/widget/RecyclerView$LayoutParams;
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v8

    iget v9, v7, Landroid/support/v7/widget/RecyclerView$LayoutParams;->topMargin:I

    add-int v5, v8, v9

    .line 49
    .local v5, "dividerTop":I
    iget-object v8, p0, Lcom/zendesk/sdk/support/help/SeparatorDecoration;->mDivider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    add-int v2, v5, v8

    .line 51
    .local v2, "dividerBottom":I
    iget-object v8, p0, Lcom/zendesk/sdk/support/help/SeparatorDecoration;->mDivider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8, v3, v5, v4, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 52
    iget-object v8, p0, Lcom/zendesk/sdk/support/help/SeparatorDecoration;->mDivider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 38
    .end local v2    # "dividerBottom":I
    .end local v3    # "dividerLeft":I
    .end local v4    # "dividerRight":I
    .end local v5    # "dividerTop":I
    .end local v7    # "params":Landroid/support/v7/widget/RecyclerView$LayoutParams;
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method

.method shouldShowTopSeparator(Landroid/support/v7/widget/RecyclerView;I)Z
    .locals 6
    .param p1, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "position"    # I
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 69
    invoke-virtual {p1, p2}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/support/v7/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/zendesk/sdk/support/help/SeparatorDecoration;->isItemACategory(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z

    move-result v0

    .line 70
    .local v0, "itemIsACategory":Z
    invoke-virtual {p1, p2}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/support/v7/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/zendesk/sdk/support/help/SeparatorDecoration;->isItemAnExpandedCategory(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z

    move-result v1

    .line 71
    .local v1, "itemIsAnExpandedCategory":Z
    if-lez p2, :cond_1

    add-int/lit8 v5, p2, -0x1

    .line 72
    invoke-virtual {p1, v5}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/support/v7/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/zendesk/sdk/support/help/SeparatorDecoration;->isItemAnUnexpandedCategory(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v2, v3

    .line 74
    .local v2, "previousItemIsAnUnexpandedCategory":Z
    :goto_0
    if-eqz v0, :cond_2

    if-nez v1, :cond_0

    if-nez v2, :cond_2

    :cond_0
    :goto_1
    return v3

    .end local v2    # "previousItemIsAnUnexpandedCategory":Z
    :cond_1
    move v2, v4

    .line 72
    goto :goto_0

    .restart local v2    # "previousItemIsAnUnexpandedCategory":Z
    :cond_2
    move v3, v4

    .line 74
    goto :goto_1
.end method
