.class Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;
.super Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$HelpViewHolder;
.source "HelpRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SeeAllViewHolder"
.end annotation


# instance fields
.field private progressBar:Landroid/widget/ProgressBar;

.field final synthetic this$0:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;Landroid/view/View;)V
    .locals 1
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 301
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;->this$0:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    .line 302
    invoke-direct {p0, p2}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$HelpViewHolder;-><init>(Landroid/view/View;)V

    .line 303
    sget v0, Lcom/zendesk/sdk/R$id;->help_section_action_button:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;->textView:Landroid/widget/TextView;

    .line 304
    sget v0, Lcom/zendesk/sdk/R$id;->help_section_loading_progress:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;->progressBar:Landroid/widget/ProgressBar;

    .line 305
    return-void
.end method

.method static synthetic access$700(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;

    .prologue
    .line 297
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;->progressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method


# virtual methods
.method public bindTo(Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;I)V
    .locals 8
    .param p1, "item"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;
    .param p2, "position"    # I

    .prologue
    const/16 v4, 0x8

    const/4 v7, 0x0

    .line 310
    instance-of v3, p1, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;

    if-nez v3, :cond_0

    .line 311
    const-string v3, "HelpRecyclerViewAdapter"

    const-string v4, "SeeAll item was null, cannot bind"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 350
    :goto_0
    return-void

    :cond_0
    move-object v2, p1

    .line 315
    check-cast v2, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;

    .line 321
    .local v2, "seeAllArticlesItem":Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;
    invoke-virtual {v2}, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;->isLoading()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 322
    iget-object v3, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;->textView:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 323
    iget-object v3, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 329
    :goto_1
    invoke-virtual {v2}, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;->getSection()Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    move-result-object v0

    .line 332
    .local v0, "section":Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;
    if-eqz v0, :cond_2

    .line 333
    iget-object v3, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;->this$0:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    invoke-static {v3}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->access$000(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;)Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/zendesk/sdk/R$string;->help_see_all_n_articles_label:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    .line 334
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->getTotalArticlesCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    .line 333
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 339
    .local v1, "sectionLabel":Ljava/lang/String;
    :goto_2
    iget-object v3, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;->textView:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    iget-object v3, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;->textView:Landroid/widget/TextView;

    new-instance v4, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder$1;

    invoke-direct {v4, p0, p1, v2}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder$1;-><init>(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 325
    .end local v0    # "section":Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;
    .end local v1    # "sectionLabel":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;->textView:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 326
    iget-object v3, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1

    .line 336
    .restart local v0    # "section":Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;
    :cond_2
    iget-object v3, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;->this$0:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    invoke-static {v3}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->access$000(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;)Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/zendesk/sdk/R$string;->help_see_all_articles_label:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "sectionLabel":Ljava/lang/String;
    goto :goto_2
.end method
