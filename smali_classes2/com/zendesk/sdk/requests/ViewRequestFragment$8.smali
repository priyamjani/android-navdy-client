.class Lcom/zendesk/sdk/requests/ViewRequestFragment$8;
.super Lcom/zendesk/service/ZendeskCallback;
.source "ViewRequestFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/requests/ViewRequestFragment;->setupCallbacks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/request/Comment;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;

    .prologue
    .line 344
    iput-object p1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 2
    .param p1, "error"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 388
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$400(Lcom/zendesk/sdk/requests/ViewRequestFragment;Z)V

    .line 390
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$1100(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Lcom/zendesk/sdk/network/SubmissionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$1100(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Lcom/zendesk/sdk/network/SubmissionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/zendesk/sdk/network/SubmissionListener;->onSubmissionError(Lcom/zendesk/service/ErrorResponse;)V

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    sget-object v1, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;->SEND_COMMENT:Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    invoke-static {v0, p1, v1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$700(Lcom/zendesk/sdk/requests/ViewRequestFragment;Lcom/zendesk/service/ErrorResponse;Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;)V

    .line 395
    return-void
.end method

.method public onSuccess(Lcom/zendesk/sdk/model/request/Comment;)V
    .locals 8
    .param p1, "result"    # Lcom/zendesk/sdk/model/request/Comment;

    .prologue
    const/4 v7, 0x0

    .line 354
    sget-object v5, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v5}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v5

    invoke-interface {v5}, Lcom/zendesk/sdk/storage/StorageStore;->requestStorage()Lcom/zendesk/sdk/storage/RequestStorage;

    move-result-object v5

    iget-object v6, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v6}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$500(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/zendesk/sdk/storage/RequestStorage;->getCommentCount(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 355
    .local v0, "commentCount":Ljava/lang/Integer;
    iget-object v5, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v5}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$800(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v5}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$800(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 357
    .local v3, "listItemCount":Ljava/lang/Integer;
    :goto_0
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-ge v5, v6, :cond_2

    .line 358
    iget-object v5, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v5}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$900(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V

    .line 376
    :goto_1
    iget-object v5, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v5}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$1100(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Lcom/zendesk/sdk/network/SubmissionListener;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 377
    iget-object v5, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v5}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$1100(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Lcom/zendesk/sdk/network/SubmissionListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/zendesk/sdk/network/SubmissionListener;->onSubmissionCompleted()V

    .line 380
    :cond_0
    iget-object v5, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v5}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$1000(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Landroid/widget/EditText;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 382
    iget-object v5, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v5}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$300(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->reset()V

    .line 383
    iget-object v5, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v5}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$200(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    move-result-object v5

    invoke-virtual {v5}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->reset()V

    .line 384
    return-void

    .line 355
    .end local v3    # "listItemCount":Ljava/lang/Integer;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 362
    .restart local v3    # "listItemCount":Ljava/lang/Integer;
    :cond_2
    iget-object v5, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v5, v7}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$400(Lcom/zendesk/sdk/requests/ViewRequestFragment;Z)V

    .line 364
    new-instance v4, Lcom/zendesk/sdk/model/request/CommentResponse;

    invoke-direct {v4}, Lcom/zendesk/sdk/model/request/CommentResponse;-><init>()V

    .line 365
    .local v4, "submittedComment":Lcom/zendesk/sdk/model/request/CommentResponse;
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v5}, Lcom/zendesk/sdk/model/request/CommentResponse;->setCreatedAt(Ljava/util/Date;)V

    .line 366
    iget-object v5, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v5}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$1000(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/zendesk/sdk/model/request/CommentResponse;->setBody(Ljava/lang/String;)V

    .line 367
    iget-object v5, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v5}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$300(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->getUploadedAttachments()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/zendesk/sdk/model/request/CommentResponse;->setAttachments(Ljava/util/List;)V

    .line 369
    new-instance v2, Lcom/zendesk/sdk/model/request/User;

    invoke-direct {v2}, Lcom/zendesk/sdk/model/request/User;-><init>()V

    .line 370
    .local v2, "dummyUser":Lcom/zendesk/sdk/model/request/User;
    invoke-static {v4, v2}, Lcom/zendesk/sdk/requests/CommentWithUser;->build(Lcom/zendesk/sdk/model/request/CommentResponse;Lcom/zendesk/sdk/model/request/User;)Lcom/zendesk/sdk/requests/CommentWithUser;

    move-result-object v1

    .line 371
    .local v1, "commentWithUser":Lcom/zendesk/sdk/requests/CommentWithUser;
    iget-object v5, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v5}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$800(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;

    move-result-object v5

    invoke-virtual {v5, v1, v7}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;->insert(Ljava/lang/Object;I)V

    .line 373
    iget-object v5, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v5}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$000(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V

    goto :goto_1
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 344
    check-cast p1, Lcom/zendesk/sdk/model/request/Comment;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;->onSuccess(Lcom/zendesk/sdk/model/request/Comment;)V

    return-void
.end method
