.class Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow$1;
.super Ljava/lang/Object;
.source "RequestCommentsListAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->bind(Lcom/zendesk/sdk/requests/CommentWithUser;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;

.field final synthetic val$comment:Lcom/zendesk/sdk/requests/CommentWithUser;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;Lcom/zendesk/sdk/requests/CommentWithUser;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;

    .prologue
    .line 266
    iput-object p1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow$1;->this$0:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;

    iput-object p2, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow$1;->val$comment:Lcom/zendesk/sdk/requests/CommentWithUser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 270
    new-instance v0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;-><init>(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$1;)V

    iget-object v1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow$1;->this$0:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;

    .line 272
    invoke-static {v1}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->access$200(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;)Landroid/view/ViewGroup;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow$1;->val$comment:Lcom/zendesk/sdk/requests/CommentWithUser;

    invoke-virtual {v2}, Lcom/zendesk/sdk/requests/CommentWithUser;->getComment()Lcom/zendesk/sdk/model/request/CommentResponse;

    move-result-object v2

    iget-object v3, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow$1;->this$0:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;

    invoke-static {v3}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->access$300(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow$1;->this$0:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;

    invoke-static {v4}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->access$400(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;)Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;

    move-result-object v4

    .line 271
    invoke-static {v0, v1, v2, v3, v4}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;->access$600(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;Landroid/view/ViewGroup;Lcom/zendesk/sdk/model/request/CommentResponse;Landroid/content/Context;Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;)V

    .line 273
    return-void
.end method
