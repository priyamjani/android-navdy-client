.class Lcom/zendesk/sdk/requests/RequestsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "RequestsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/zendesk/sdk/model/request/Request;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/Request;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p2, "requests":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/request/Request;>;"
    sget v0, Lcom/zendesk/sdk/R$layout;->row_request:I

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 31
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 38
    instance-of v1, p2, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;

    if-eqz v1, :cond_0

    move-object v0, p2

    .line 39
    check-cast v0, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;

    .line 44
    .local v0, "requestRowView":Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/requests/RequestsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/zendesk/sdk/model/request/Request;

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->bind(Lcom/zendesk/sdk/model/request/Request;)V

    .line 46
    invoke-virtual {v0}, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->getView()Landroid/view/View;

    move-result-object v1

    return-object v1

    .line 41
    .end local v0    # "requestRowView":Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;
    :cond_0
    new-instance v0, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;

    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestsAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;-><init>(Landroid/content/Context;)V

    .restart local v0    # "requestRowView":Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;
    goto :goto_0
.end method
