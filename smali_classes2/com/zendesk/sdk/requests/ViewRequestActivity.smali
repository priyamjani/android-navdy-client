.class public Lcom/zendesk/sdk/requests/ViewRequestActivity;
.super Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;
.source "ViewRequestActivity.java"


# static fields
.field public static final EXTRA_REQUEST_ID:Ljava/lang/String; = "requestId"

.field public static final EXTRA_SUBJECT:Ljava/lang/String; = "subject"

.field private static final FRAGMENT_TAG:Ljava/lang/String;

.field private static final LOG_TAG:Ljava/lang/String; = "ViewRequestActivity"


# instance fields
.field private mViewRequestFragment:Lcom/zendesk/sdk/requests/ViewRequestFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/zendesk/sdk/requests/ViewRequestActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/requests/ViewRequestActivity;->FRAGMENT_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;-><init>()V

    return-void
.end method

.method private abort()V
    .locals 3

    .prologue
    .line 104
    const-string v0, "ViewRequestActivity"

    const-string v1, "Insufficient data to start activity"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestActivity;->finish()V

    .line 106
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 35
    sget v7, Lcom/zendesk/sdk/R$layout;->activity_view_request:I

    invoke-virtual {p0, v7}, Lcom/zendesk/sdk/requests/ViewRequestActivity;->setContentView(I)V

    .line 37
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 39
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_0

    .line 40
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 43
    :cond_0
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 44
    .local v1, "extras":Landroid/os/Bundle;
    if-eqz v1, :cond_3

    const-string v7, "requestId"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 45
    const-string v7, "requestId"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 46
    .local v5, "requestId":Ljava/lang/String;
    const-string v7, "subject"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 48
    .local v6, "subject":Ljava/lang/String;
    invoke-static {v5}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 50
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    .line 51
    .local v3, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    sget-object v7, Lcom/zendesk/sdk/requests/ViewRequestActivity;->FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v3, v7}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 53
    .local v2, "fragment":Landroid/support/v4/app/Fragment;
    if-eqz v2, :cond_1

    instance-of v7, v2, Lcom/zendesk/sdk/requests/ViewRequestFragment;

    if-eqz v7, :cond_1

    .line 54
    check-cast v2, Lcom/zendesk/sdk/requests/ViewRequestFragment;

    .end local v2    # "fragment":Landroid/support/v4/app/Fragment;
    iput-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestActivity;->mViewRequestFragment:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    .line 72
    .end local v3    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    .end local v5    # "requestId":Ljava/lang/String;
    .end local v6    # "subject":Ljava/lang/String;
    :goto_0
    return-void

    .line 57
    .restart local v2    # "fragment":Landroid/support/v4/app/Fragment;
    .restart local v3    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    .restart local v5    # "requestId":Ljava/lang/String;
    .restart local v6    # "subject":Ljava/lang/String;
    :cond_1
    invoke-static {v5, v6}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/zendesk/sdk/requests/ViewRequestFragment;

    move-result-object v7

    iput-object v7, p0, Lcom/zendesk/sdk/requests/ViewRequestActivity;->mViewRequestFragment:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    .line 58
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    .line 59
    .local v4, "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    sget v7, Lcom/zendesk/sdk/R$id;->activity_request_view_root:I

    iget-object v8, p0, Lcom/zendesk/sdk/requests/ViewRequestActivity;->mViewRequestFragment:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    sget-object v9, Lcom/zendesk/sdk/requests/ViewRequestActivity;->FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v4, v7, v8, v9}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 60
    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0

    .line 65
    .end local v2    # "fragment":Landroid/support/v4/app/Fragment;
    .end local v3    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    .end local v4    # "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_2
    invoke-direct {p0}, Lcom/zendesk/sdk/requests/ViewRequestActivity;->abort()V

    goto :goto_0

    .line 69
    .end local v5    # "requestId":Ljava/lang/String;
    .end local v6    # "subject":Ljava/lang/String;
    :cond_3
    invoke-direct {p0}, Lcom/zendesk/sdk/requests/ViewRequestActivity;->abort()V

    goto :goto_0
.end method

.method public onNetworkAvailable()V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->onNetworkAvailable()V

    .line 89
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestActivity;->mViewRequestFragment:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestActivity;->mViewRequestFragment:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-virtual {v0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->onNetworkAvailable()V

    .line 92
    :cond_0
    return-void
.end method

.method public onNetworkUnavailable()V
    .locals 1

    .prologue
    .line 96
    invoke-super {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->onNetworkUnavailable()V

    .line 98
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestActivity;->mViewRequestFragment:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestActivity;->mViewRequestFragment:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-virtual {v0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->onNetworkUnavailable()V

    .line 101
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 77
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestActivity;->onBackPressed()V

    .line 79
    const/4 v0, 0x1

    .line 82
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
