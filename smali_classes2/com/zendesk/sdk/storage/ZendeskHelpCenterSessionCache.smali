.class Lcom/zendesk/sdk/storage/ZendeskHelpCenterSessionCache;
.super Ljava/lang/Object;
.source "ZendeskHelpCenterSessionCache.java"

# interfaces
.implements Lcom/zendesk/sdk/storage/HelpCenterSessionCache;


# static fields
.field public static final DEFAULT_SEARCH:Lcom/zendesk/sdk/model/helpcenter/LastSearch;


# instance fields
.field private lastSearch:Lcom/zendesk/sdk/model/helpcenter/LastSearch;

.field private uniqueSearchResultClick:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 10
    new-instance v0, Lcom/zendesk/sdk/model/helpcenter/LastSearch;

    const-string v1, ""

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/model/helpcenter/LastSearch;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/storage/ZendeskHelpCenterSessionCache;->DEFAULT_SEARCH:Lcom/zendesk/sdk/model/helpcenter/LastSearch;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zendesk/sdk/storage/ZendeskHelpCenterSessionCache;->uniqueSearchResultClick:Z

    return-void
.end method


# virtual methods
.method public getLastSearch()Lcom/zendesk/sdk/model/helpcenter/LastSearch;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 19
    iget-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskHelpCenterSessionCache;->lastSearch:Lcom/zendesk/sdk/model/helpcenter/LastSearch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskHelpCenterSessionCache;->lastSearch:Lcom/zendesk/sdk/model/helpcenter/LastSearch;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/zendesk/sdk/storage/ZendeskHelpCenterSessionCache;->DEFAULT_SEARCH:Lcom/zendesk/sdk/model/helpcenter/LastSearch;

    goto :goto_0
.end method

.method public isUniqueSearchResultClick()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/zendesk/sdk/storage/ZendeskHelpCenterSessionCache;->uniqueSearchResultClick:Z

    return v0
.end method

.method reset()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskHelpCenterSessionCache;->lastSearch:Lcom/zendesk/sdk/model/helpcenter/LastSearch;

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zendesk/sdk/storage/ZendeskHelpCenterSessionCache;->uniqueSearchResultClick:Z

    .line 60
    return-void
.end method

.method public setLastSearch(Ljava/lang/String;I)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "resultCount"    # I

    .prologue
    .line 33
    new-instance v0, Lcom/zendesk/sdk/model/helpcenter/LastSearch;

    invoke-direct {v0, p1, p2}, Lcom/zendesk/sdk/model/helpcenter/LastSearch;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskHelpCenterSessionCache;->lastSearch:Lcom/zendesk/sdk/model/helpcenter/LastSearch;

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/zendesk/sdk/storage/ZendeskHelpCenterSessionCache;->uniqueSearchResultClick:Z

    .line 35
    return-void
.end method

.method public unsetUniqueSearchResultClick()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zendesk/sdk/storage/ZendeskHelpCenterSessionCache;->uniqueSearchResultClick:Z

    .line 40
    return-void
.end method
