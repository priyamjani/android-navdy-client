.class public interface abstract Lcom/zendesk/sdk/storage/SdkStorage;
.super Ljava/lang/Object;
.source "SdkStorage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;
    }
.end annotation


# virtual methods
.method public abstract clearUserData()V
.end method

.method public abstract getUserStorage()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;",
            ">;"
        }
    .end annotation
.end method

.method public abstract registerUserStorage(Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;)V
.end method
