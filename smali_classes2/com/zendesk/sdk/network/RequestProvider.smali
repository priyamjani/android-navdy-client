.class public interface abstract Lcom/zendesk/sdk/network/RequestProvider;
.super Ljava/lang/Object;
.source "RequestProvider.java"


# virtual methods
.method public abstract addComment(Ljava/lang/String;Lcom/zendesk/sdk/model/request/EndUserComment;Lcom/zendesk/service/ZendeskCallback;)V
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/sdk/model/request/EndUserComment;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/sdk/model/request/EndUserComment;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/Comment;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract createRequest(Lcom/zendesk/sdk/model/request/CreateRequest;Lcom/zendesk/service/ZendeskCallback;)V
    .param p1    # Lcom/zendesk/sdk/model/request/CreateRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/request/CreateRequest;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/CreateRequest;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getAllRequests(Lcom/zendesk/service/ZendeskCallback;)V
    .param p1    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/Request;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public abstract getComments(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/CommentsResponse;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getRequest(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/Request;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getRequests(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/Request;",
            ">;>;)V"
        }
    .end annotation
.end method
