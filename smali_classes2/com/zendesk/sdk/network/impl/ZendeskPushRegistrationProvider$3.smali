.class Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$3;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskPushRegistrationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;->unregisterDevice(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/SdkConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;

.field final synthetic val$identifier:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$3;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$3;->val$identifier:Ljava/lang/String;

    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$3;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
    .locals 4
    .param p1, "config"    # Lcom/zendesk/sdk/model/SdkConfiguration;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$3;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;->access$000(Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;)Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;

    move-result-object v0

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getBearerAuthorizationHeader()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$3;->val$identifier:Ljava/lang/String;

    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$3;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v0, v1, v2, v3}, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;->unregisterDevice(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    .line 88
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 84
    check-cast p1, Lcom/zendesk/sdk/model/SdkConfiguration;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$3;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V

    return-void
.end method
