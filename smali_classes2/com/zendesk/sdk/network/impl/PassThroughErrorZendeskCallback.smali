.class abstract Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.super Lcom/zendesk/service/ZendeskCallback;
.source "PassThroughErrorZendeskCallback.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/zendesk/service/ZendeskCallback",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final callback:Lcom/zendesk/service/ZendeskCallback;


# direct methods
.method constructor <init>(Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 10
    .local p0, "this":Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;, "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback<TE;>;"
    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;->callback:Lcom/zendesk/service/ZendeskCallback;

    .line 12
    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 1
    .param p1, "errorResponse"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 19
    .local p0, "this":Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;, "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback<TE;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;->callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;->callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v0, p1}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 22
    :cond_0
    return-void
.end method

.method public abstract onSuccess(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation
.end method
