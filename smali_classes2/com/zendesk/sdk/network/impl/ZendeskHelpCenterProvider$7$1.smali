.class Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7$1;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskHelpCenterProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/ArticlesSearchResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$1"    # Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 265
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/helpcenter/ArticlesSearchResponse;)V
    .locals 4
    .param p1, "articlesSearchResponse"    # Lcom/zendesk/sdk/model/helpcenter/ArticlesSearchResponse;

    .prologue
    .line 268
    const/4 v1, 0x0

    .line 270
    .local v1, "size":I
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/ArticlesSearchResponse;->getArticles()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/zendesk/util/CollectionUtils;->isNotEmpty(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 271
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/ArticlesSearchResponse;->getArticles()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 274
    :cond_0
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;

    iget-object v2, v2, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    invoke-static {v2}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->access$100(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;)Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    move-result-object v2

    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;

    iget-object v3, v3, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->val$search:Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;

    invoke-virtual {v3}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->getQuery()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Lcom/zendesk/sdk/storage/HelpCenterSessionCache;->setLastSearch(Ljava/lang/String;I)V

    .line 276
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;

    iget-object v2, v2, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    invoke-virtual {v2, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->asSearchArticleList(Lcom/zendesk/sdk/model/helpcenter/ArticlesResponse;)Ljava/util/List;

    move-result-object v0

    .line 278
    .local v0, "searchArticles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/SearchArticle;>;"
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;

    iget-object v2, v2, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v2, :cond_1

    .line 283
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;

    iget-object v2, v2, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v2, v0}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    .line 285
    :cond_1
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 265
    check-cast p1, Lcom/zendesk/sdk/model/helpcenter/ArticlesSearchResponse;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7$1;->onSuccess(Lcom/zendesk/sdk/model/helpcenter/ArticlesSearchResponse;)V

    return-void
.end method
