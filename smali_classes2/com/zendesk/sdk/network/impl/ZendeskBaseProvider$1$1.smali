.class Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1$1;
.super Lcom/zendesk/service/ZendeskCallback;
.source "ZendeskBaseProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;->onSuccess(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/access/AccessToken;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;

.field final synthetic val$mobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V
    .locals 0
    .param p1, "this$1"    # Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;

    iput-object p2, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1$1;->val$mobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 1
    .param p1, "errorResponse"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;

    iget-object v0, v0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;

    iget-object v0, v0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v0, p1}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 70
    :cond_0
    return-void
.end method

.method public onSuccess(Lcom/zendesk/sdk/model/access/AccessToken;)V
    .locals 4
    .param p1, "accessToken"    # Lcom/zendesk/sdk/model/access/AccessToken;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;

    iget-object v0, v0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;

    iget-object v1, v0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    new-instance v2, Lcom/zendesk/sdk/model/SdkConfiguration;

    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1$1;->val$mobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    if-nez v0, :cond_1

    new-instance v0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    const/4 v3, 0x0

    invoke-direct {v0, v3}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;-><init>(Lcom/zendesk/sdk/model/settings/MobileSettings;)V

    :goto_0
    invoke-direct {v2, p1, v0}, Lcom/zendesk/sdk/model/SdkConfiguration;-><init>(Lcom/zendesk/sdk/model/access/AccessToken;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V

    invoke-virtual {v1, v2}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    .line 63
    :cond_0
    return-void

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1$1;->val$mobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 56
    check-cast p1, Lcom/zendesk/sdk/model/access/AccessToken;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1$1;->onSuccess(Lcom/zendesk/sdk/model/access/AccessToken;)V

    return-void
.end method
