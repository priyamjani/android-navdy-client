.class public interface abstract Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;
.super Ljava/lang/Object;
.source "ImageUploadHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/attachment/ImageUploadHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ImageUploadProgressListener"
.end annotation


# virtual methods
.method public abstract allImagesUploaded(Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/io/File;",
            "Lcom/zendesk/sdk/model/request/UploadResponse;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract imageUploadError(Lcom/zendesk/service/ErrorResponse;Lcom/zendesk/belvedere/BelvedereResult;)V
.end method

.method public abstract imageUploaded(Lcom/zendesk/sdk/model/request/UploadResponse;Lcom/zendesk/belvedere/BelvedereResult;)V
.end method
