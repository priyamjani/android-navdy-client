.class public Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;
.super Landroid/widget/LinearLayout;
.source "AttachmentContainerHost.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;,
        Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;,
        Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mAttachmentContainerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;",
            ">;"
        }
    .end annotation
.end field

.field private mAttachmentContainerListener:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;

.field private mParent:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 68
    iput-object p0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mParent:Landroid/view/View;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mAttachmentContainerList:Ljava/util/List;

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 74
    iput-object p0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mParent:Landroid/view/View;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mAttachmentContainerList:Ljava/util/List;

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 80
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 81
    iput-object p0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mParent:Landroid/view/View;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mAttachmentContainerList:Ljava/util/List;

    .line 83
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 88
    iput-object p0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mParent:Landroid/view/View;

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mAttachmentContainerList:Ljava/util/List;

    .line 90
    return-void
.end method


# virtual methods
.method public addAttachment(Ljava/io/File;)V
    .locals 6
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 141
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mParent:Landroid/view/View;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 142
    new-instance v0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;

    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->getOrientation()I

    move-result v5

    move-object v1, p0

    move-object v3, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;-><init>(Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;Landroid/content/Context;Ljava/io/File;Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;I)V

    .line 143
    .local v0, "attachmentContainer":Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;
    invoke-virtual {p1}, Ljava/io/File;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->setId(I)V

    .line 144
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mAttachmentContainerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->addView(Landroid/view/View;)V

    .line 146
    return-void
.end method

.method public removeAttachment(Ljava/io/File;)V
    .locals 8
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 177
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 179
    .local v4, "newAttachmentContainer":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;>;"
    iget-object v5, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mAttachmentContainerList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;

    .line 180
    .local v0, "attachmentContainer":Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;
    invoke-virtual {v0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->getFile()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 181
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 185
    .end local v0    # "attachmentContainer":Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;
    :cond_1
    iput-object v4, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mAttachmentContainerList:Ljava/util/List;

    .line 187
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->getChildCount()I

    move-result v2

    .line 188
    .local v2, "childCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v2, :cond_2

    .line 190
    invoke-virtual {p0, v3}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 191
    .local v1, "childAt":Landroid/view/View;
    instance-of v5, v1, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;

    if-eqz v5, :cond_4

    .line 192
    check-cast v1, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;

    .end local v1    # "childAt":Landroid/view/View;
    invoke-virtual {v1}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->getFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 193
    invoke-virtual {p0, v3}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->removeViewAt(I)V

    .line 199
    :cond_2
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->getChildCount()I

    move-result v5

    const/4 v6, 0x1

    if-ge v5, v6, :cond_3

    .line 200
    iget-object v5, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mParent:Landroid/view/View;

    const/16 v6, 0x8

    invoke-static {v5, v6}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 203
    :cond_3
    return-void

    .line 188
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public declared-synchronized removeAttachmentAndNotify(Ljava/io/File;)V
    .locals 1
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 211
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->removeAttachment(Ljava/io/File;)V

    .line 212
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mAttachmentContainerListener:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mAttachmentContainerListener:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;

    invoke-interface {v0, p1}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;->attachmentRemoved(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 215
    :cond_0
    monitor-exit p0

    return-void

    .line 211
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mParent:Landroid/view/View;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 258
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->removeAllViews()V

    .line 259
    return-void
.end method

.method public setAttachmentContainerListener(Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;)V
    .locals 0
    .param p1, "attachmentContainerListener"    # Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mAttachmentContainerListener:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;

    .line 94
    return-void
.end method

.method public setAttachmentState(Ljava/io/File;Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;)V
    .locals 4
    .param p1, "file"    # Ljava/io/File;
    .param p2, "attachmentState"    # Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    .prologue
    .line 164
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mAttachmentContainerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;

    .line 165
    .local v0, "attachmentContainer":Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;
    invoke-virtual {v0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->getFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 166
    invoke-virtual {v0, p2}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->setState(Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;)V

    goto :goto_0

    .line 169
    .end local v0    # "attachmentContainer":Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;
    :cond_1
    return-void
.end method

.method public setAttachmentUploaded(Ljava/io/File;)V
    .locals 1
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 154
    sget-object v0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->UPLOADED:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    invoke-virtual {p0, p1, v0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->setAttachmentState(Ljava/io/File;Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;)V

    .line 155
    return-void
.end method

.method public setAttachmentsDeletable(Z)V
    .locals 6
    .param p1, "deletable"    # Z

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->getChildCount()I

    move-result v3

    .line 232
    .local v3, "childCount":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v3, :cond_2

    .line 234
    invoke-virtual {p0, v4}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 235
    .local v2, "childAt":Landroid/view/View;
    instance-of v5, v2, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;

    if-eqz v5, :cond_0

    move-object v0, v2

    .line 236
    check-cast v0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;

    .line 237
    .local v0, "attachment":Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;
    invoke-virtual {v0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->getAttachmentState()Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    move-result-object v1

    .line 239
    .local v1, "attachmentState":Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;
    if-eqz p1, :cond_1

    sget-object v5, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->DISABLE:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    if-ne v1, v5, :cond_1

    .line 240
    sget-object v5, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->UPLOADED:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    invoke-virtual {v0, v5}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->setState(Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;)V

    .line 232
    .end local v0    # "attachment":Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;
    .end local v1    # "attachmentState":Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 242
    .restart local v0    # "attachment":Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;
    .restart local v1    # "attachmentState":Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;
    :cond_1
    if-nez p1, :cond_0

    sget-object v5, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->UPLOADED:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    if-ne v1, v5, :cond_0

    .line 243
    sget-object v5, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->DISABLE:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    invoke-virtual {v0, v5}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->setState(Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;)V

    goto :goto_1

    .line 247
    .end local v0    # "attachment":Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;
    .end local v1    # "attachmentState":Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;
    .end local v2    # "childAt":Landroid/view/View;
    :cond_2
    return-void
.end method

.method public setParent(Landroid/view/View;)V
    .locals 0
    .param p1, "parent"    # Landroid/view/View;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->mParent:Landroid/view/View;

    .line 98
    return-void
.end method

.method public setState(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)V
    .locals 7
    .param p1, "imageUploadHelper"    # Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    .prologue
    .line 102
    if-nez p1, :cond_1

    .line 103
    sget-object v4, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Please provide a non null ImageUploadHelper"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    :cond_0
    return-void

    .line 107
    :cond_1
    invoke-virtual {p1}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->getRecentState()Ljava/util/HashMap;

    move-result-object v1

    .line 109
    .local v1, "stateMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;Ljava/util/List<Ljava/io/File;>;>;"
    sget-object v4, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->UPLOADING:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 110
    sget-object v4, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->UPLOADING:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 112
    .local v3, "uploading":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 113
    .local v0, "file":Ljava/io/File;
    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->addAttachment(Ljava/io/File;)V

    goto :goto_0

    .line 117
    .end local v0    # "file":Ljava/io/File;
    .end local v3    # "uploading":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    :cond_2
    sget-object v4, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->UPLOADED:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 118
    sget-object v4, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->UPLOADED:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 120
    .local v2, "uploaded":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 121
    .restart local v0    # "file":Ljava/io/File;
    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->addAttachment(Ljava/io/File;)V

    .line 122
    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->setAttachmentUploaded(Ljava/io/File;)V

    goto :goto_1
.end method
