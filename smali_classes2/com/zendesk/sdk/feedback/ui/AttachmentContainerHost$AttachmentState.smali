.class public final enum Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;
.super Ljava/lang/Enum;
.source "AttachmentContainerHost.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AttachmentState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

.field public static final enum DISABLE:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

.field public static final enum UPLOADED:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

.field public static final enum UPLOADING:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 45
    new-instance v0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    const-string v1, "UPLOADING"

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->UPLOADING:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    .line 50
    new-instance v0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    const-string v1, "UPLOADED"

    invoke-direct {v0, v1, v3}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->UPLOADED:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    .line 55
    new-instance v0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    const-string v1, "DISABLE"

    invoke-direct {v0, v1, v4}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->DISABLE:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    .line 40
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    sget-object v1, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->UPLOADING:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->UPLOADED:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->DISABLE:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->$VALUES:[Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 40
    const-class v0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->$VALUES:[Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    invoke-virtual {v0}, [Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    return-object v0
.end method
