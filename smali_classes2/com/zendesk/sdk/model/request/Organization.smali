.class public Lcom/zendesk/sdk/model/request/Organization;
.super Ljava/lang/Object;
.source "Organization.java"


# instance fields
.field private id:Ljava/lang/Long;

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/Organization;->id:Ljava/lang/Long;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/Organization;->name:Ljava/lang/String;

    return-object v0
.end method
