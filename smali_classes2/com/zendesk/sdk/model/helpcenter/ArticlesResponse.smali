.class public interface abstract Lcom/zendesk/sdk/model/helpcenter/ArticlesResponse;
.super Ljava/lang/Object;
.source "ArticlesResponse.java"


# virtual methods
.method public abstract getArticles()Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Article;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCategories()Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Category;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSections()Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Section;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUsers()Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/User;",
            ">;"
        }
    .end annotation
.end method
