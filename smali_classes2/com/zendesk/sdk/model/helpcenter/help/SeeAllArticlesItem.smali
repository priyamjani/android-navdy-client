.class public Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;
.super Ljava/lang/Object;
.source "SeeAllArticlesItem.java"

# interfaces
.implements Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;


# instance fields
.field private isLoading:Z

.field private section:Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;)V
    .locals 0
    .param p1, "sectionItem"    # Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;->section:Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    .line 22
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 72
    if-ne p0, p1, :cond_1

    .line 77
    :cond_0
    :goto_0
    return v1

    .line 73
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 75
    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;

    .line 77
    .local v0, "that":Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;
    iget-object v3, p0, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;->section:Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    if-eqz v3, :cond_4

    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;->section:Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    iget-object v2, v0, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;->section:Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    :cond_4
    iget-object v3, v0, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;->section:Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getChildren()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;->section:Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->getId()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 32
    const-string v0, ""

    return-object v0
.end method

.method public getParentId()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;->section:Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->getParentId()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getSection()Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;->section:Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    return-object v0
.end method

.method public getViewType()I
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x4

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;->section:Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;->section:Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;->isLoading:Z

    return v0
.end method

.method public setLoading(Z)V
    .locals 0
    .param p1, "loading"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;->isLoading:Z

    .line 68
    return-void
.end method
