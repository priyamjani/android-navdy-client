.class public Lcom/zendesk/sdk/model/settings/RateMyAppSettings;
.super Ljava/lang/Object;
.source "RateMyAppSettings.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private androidStoreUrl:Ljava/lang/String;

.field private delay:J

.field private duration:J

.field private enabled:Z

.field private tags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private visits:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAndroidStoreUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lcom/zendesk/sdk/model/settings/RateMyAppSettings;->androidStoreUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getDelay()J
    .locals 4

    .prologue
    .line 86
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p0, Lcom/zendesk/sdk/model/settings/RateMyAppSettings;->delay:J

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getDuration()J
    .locals 4

    .prologue
    .line 68
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p0, Lcom/zendesk/sdk/model/settings/RateMyAppSettings;->duration:J

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTags()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lcom/zendesk/sdk/model/settings/RateMyAppSettings;->tags:Ljava/util/List;

    invoke-static {v0}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getVisits()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/zendesk/sdk/model/settings/RateMyAppSettings;->visits:I

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/zendesk/sdk/model/settings/RateMyAppSettings;->enabled:Z

    return v0
.end method
