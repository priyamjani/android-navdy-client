.class public Lcom/zendesk/sdk/model/settings/SdkSettings;
.super Ljava/lang/Object;
.source "SdkSettings.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private authentication:Ljava/lang/String;

.field private contactUs:Lcom/zendesk/sdk/model/settings/ContactUsSettings;

.field private conversations:Lcom/zendesk/sdk/model/settings/ConversationsSettings;

.field private helpCenter:Lcom/zendesk/sdk/model/settings/HelpCenterSettings;

.field private rma:Lcom/zendesk/sdk/model/settings/RateMyAppSettings;

.field private updatedAt:Ljava/util/Date;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAuthentication()Lcom/zendesk/sdk/model/access/AuthenticationType;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/zendesk/sdk/model/settings/SdkSettings;->authentication:Ljava/lang/String;

    invoke-static {v0}, Lcom/zendesk/sdk/model/access/AuthenticationType;->getAuthType(Ljava/lang/String;)Lcom/zendesk/sdk/model/access/AuthenticationType;

    move-result-object v0

    return-object v0
.end method

.method public getContactUsSettings()Lcom/zendesk/sdk/model/settings/ContactUsSettings;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/zendesk/sdk/model/settings/SdkSettings;->contactUs:Lcom/zendesk/sdk/model/settings/ContactUsSettings;

    return-object v0
.end method

.method public getConversationsSettings()Lcom/zendesk/sdk/model/settings/ConversationsSettings;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/zendesk/sdk/model/settings/SdkSettings;->conversations:Lcom/zendesk/sdk/model/settings/ConversationsSettings;

    return-object v0
.end method

.method public getHelpCenterSettings()Lcom/zendesk/sdk/model/settings/HelpCenterSettings;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/zendesk/sdk/model/settings/SdkSettings;->helpCenter:Lcom/zendesk/sdk/model/settings/HelpCenterSettings;

    return-object v0
.end method

.method public getRateMyAppSettings()Lcom/zendesk/sdk/model/settings/RateMyAppSettings;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/zendesk/sdk/model/settings/SdkSettings;->rma:Lcom/zendesk/sdk/model/settings/RateMyAppSettings;

    return-object v0
.end method

.method public getUpdatedAt()Ljava/util/Date;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/zendesk/sdk/model/settings/SdkSettings;->updatedAt:Ljava/util/Date;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SdkSettings;->updatedAt:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    goto :goto_0
.end method
