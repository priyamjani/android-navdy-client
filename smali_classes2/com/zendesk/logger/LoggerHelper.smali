.class Lcom/zendesk/logger/LoggerHelper;
.super Ljava/lang/Object;
.source "LoggerHelper.java"


# static fields
.field private static final DEFAULT_LOG_TAG:Ljava/lang/String; = "Zendesk"

.field private static final MAXIMUM_ANDROID_LOG_TAG_LENGTH:I = 0x17


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method static getAndroidTag(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;

    .prologue
    const/16 v1, 0x17

    .line 85
    invoke-static {p0}, Lcom/zendesk/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    const-string p0, "Zendesk"

    .line 89
    .end local p0    # "tag":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .restart local p0    # "tag":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method static getLevelFromPriority(I)C
    .locals 1
    .param p0, "priority"    # I

    .prologue
    const/16 v0, 0x49

    .line 58
    packed-switch p0, :pswitch_data_0

    .line 72
    :goto_0
    :pswitch_0
    return v0

    .line 60
    :pswitch_1
    const/16 v0, 0x41

    goto :goto_0

    .line 62
    :pswitch_2
    const/16 v0, 0x44

    goto :goto_0

    .line 64
    :pswitch_3
    const/16 v0, 0x45

    goto :goto_0

    .line 68
    :pswitch_4
    const/16 v0, 0x56

    goto :goto_0

    .line 70
    :pswitch_5
    const/16 v0, 0x57

    goto :goto_0

    .line 58
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_5
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method static splitLogMessage(Ljava/lang/String;I)Ljava/util/List;
    .locals 7
    .param p0, "message"    # Ljava/lang/String;
    .param p1, "maxLength"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 21
    .local v4, "messages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v6, 0x1

    if-ge p1, v6, :cond_2

    .line 23
    invoke-static {p0}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 24
    const-string v6, ""

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    :cond_0
    :goto_0
    return-object v4

    .line 28
    :cond_1
    invoke-interface {v4, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 32
    :cond_2
    invoke-static {p0}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 33
    const-string v6, ""

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 37
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v6, p1, :cond_4

    .line 38
    invoke-interface {v4, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 42
    :cond_4
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .local v3, "len":I
    :goto_1
    if-ge v1, v3, :cond_0

    .line 44
    sget-object v6, Lcom/zendesk/util/StringUtils;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {p0, v6, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    .line 45
    .local v2, "indexOfSeparator":I
    const/4 v6, -0x1

    if-eq v2, v6, :cond_6

    move v5, v2

    .line 48
    .local v5, "newLine":I
    :cond_5
    :goto_2
    add-int v6, v1, p1

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 49
    .local v0, "end":I
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    move v1, v0

    .line 51
    if-lt v1, v5, :cond_5

    .line 42
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v0    # "end":I
    .end local v5    # "newLine":I
    :cond_6
    move v5, v3

    .line 45
    goto :goto_2
.end method
