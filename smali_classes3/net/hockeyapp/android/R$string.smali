.class public final Lnet/hockeyapp/android/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/hockeyapp/android/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final hockeyapp_crash_dialog_app_name_fallback:I = 0x7f0801f9

.field public static final hockeyapp_crash_dialog_message:I = 0x7f0801fa

.field public static final hockeyapp_crash_dialog_negative_button:I = 0x7f0801fb

.field public static final hockeyapp_crash_dialog_neutral_button:I = 0x7f0801fc

.field public static final hockeyapp_crash_dialog_positive_button:I = 0x7f0801fd

.field public static final hockeyapp_crash_dialog_title:I = 0x7f0801fe

.field public static final hockeyapp_dialog_error_message:I = 0x7f0801ff

.field public static final hockeyapp_dialog_error_title:I = 0x7f080200

.field public static final hockeyapp_dialog_negative_button:I = 0x7f080201

.field public static final hockeyapp_dialog_positive_button:I = 0x7f080202

.field public static final hockeyapp_download_failed_dialog_message:I = 0x7f080203

.field public static final hockeyapp_download_failed_dialog_negative_button:I = 0x7f080204

.field public static final hockeyapp_download_failed_dialog_positive_button:I = 0x7f080205

.field public static final hockeyapp_download_failed_dialog_title:I = 0x7f080206

.field public static final hockeyapp_error_no_network_message:I = 0x7f080207

.field public static final hockeyapp_expiry_info_text:I = 0x7f080208

.field public static final hockeyapp_expiry_info_title:I = 0x7f080209

.field public static final hockeyapp_feedback_attach_file:I = 0x7f08020a

.field public static final hockeyapp_feedback_attach_picture:I = 0x7f08020b

.field public static final hockeyapp_feedback_attachment_button_text:I = 0x7f08020c

.field public static final hockeyapp_feedback_attachment_error:I = 0x7f08020d

.field public static final hockeyapp_feedback_attachment_loading:I = 0x7f08020e

.field public static final hockeyapp_feedback_email_hint:I = 0x7f08020f

.field public static final hockeyapp_feedback_failed_text:I = 0x7f080210

.field public static final hockeyapp_feedback_failed_title:I = 0x7f080211

.field public static final hockeyapp_feedback_fetching_feedback_text:I = 0x7f080212

.field public static final hockeyapp_feedback_generic_error:I = 0x7f080213

.field public static final hockeyapp_feedback_last_updated_text:I = 0x7f080214

.field public static final hockeyapp_feedback_max_attachments_allowed:I = 0x7f080215

.field public static final hockeyapp_feedback_message_hint:I = 0x7f080216

.field public static final hockeyapp_feedback_name_hint:I = 0x7f080217

.field public static final hockeyapp_feedback_refresh_button_text:I = 0x7f080218

.field public static final hockeyapp_feedback_response_button_text:I = 0x7f080219

.field public static final hockeyapp_feedback_select_file:I = 0x7f08021a

.field public static final hockeyapp_feedback_select_picture:I = 0x7f08021b

.field public static final hockeyapp_feedback_send_button_text:I = 0x7f08021c

.field public static final hockeyapp_feedback_send_generic_error:I = 0x7f08021d

.field public static final hockeyapp_feedback_send_network_error:I = 0x7f08021e

.field public static final hockeyapp_feedback_sending_feedback_text:I = 0x7f08021f

.field public static final hockeyapp_feedback_subject_hint:I = 0x7f080220

.field public static final hockeyapp_feedback_title:I = 0x7f080221

.field public static final hockeyapp_feedback_validate_email_empty:I = 0x7f080222

.field public static final hockeyapp_feedback_validate_email_error:I = 0x7f080223

.field public static final hockeyapp_feedback_validate_name_error:I = 0x7f080224

.field public static final hockeyapp_feedback_validate_subject_error:I = 0x7f080225

.field public static final hockeyapp_feedback_validate_text_error:I = 0x7f080226

.field public static final hockeyapp_login_email_hint:I = 0x7f080227

.field public static final hockeyapp_login_headline_text:I = 0x7f080228

.field public static final hockeyapp_login_headline_text_email_only:I = 0x7f080229

.field public static final hockeyapp_login_login_button_text:I = 0x7f08022a

.field public static final hockeyapp_login_missing_credentials_toast:I = 0x7f08022b

.field public static final hockeyapp_login_password_hint:I = 0x7f08022c

.field public static final hockeyapp_paint_dialog_message:I = 0x7f08022d

.field public static final hockeyapp_paint_dialog_negative_button:I = 0x7f08022e

.field public static final hockeyapp_paint_dialog_neutral_button:I = 0x7f08022f

.field public static final hockeyapp_paint_dialog_positive_button:I = 0x7f080230

.field public static final hockeyapp_paint_indicator_toast:I = 0x7f080231

.field public static final hockeyapp_paint_menu_clear:I = 0x7f080232

.field public static final hockeyapp_paint_menu_save:I = 0x7f080233

.field public static final hockeyapp_paint_menu_undo:I = 0x7f080234

.field public static final hockeyapp_permission_dialog_negative_button:I = 0x7f080235

.field public static final hockeyapp_permission_dialog_positive_button:I = 0x7f080236

.field public static final hockeyapp_permission_update_message:I = 0x7f080237

.field public static final hockeyapp_permission_update_title:I = 0x7f080238

.field public static final hockeyapp_update_button:I = 0x7f080239

.field public static final hockeyapp_update_dialog_message:I = 0x7f08023a

.field public static final hockeyapp_update_dialog_negative_button:I = 0x7f08023b

.field public static final hockeyapp_update_dialog_positive_button:I = 0x7f08023c

.field public static final hockeyapp_update_dialog_title:I = 0x7f08023d

.field public static final hockeyapp_update_mandatory_toast:I = 0x7f08023e

.field public static final hockeyapp_update_version_details_label:I = 0x7f08054a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
