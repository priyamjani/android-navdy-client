.class public Lorg/droidparts/net/http/RESTClient2;
.super Lorg/droidparts/net/http/RESTClient;
.source "RESTClient2.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lorg/droidparts/net/http/RESTClient;-><init>(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "userAgent"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lorg/droidparts/net/http/RESTClient;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/droidparts/net/http/worker/HTTPWorker;)V
    .locals 0
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "worker"    # Lorg/droidparts/net/http/worker/HTTPWorker;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lorg/droidparts/net/http/RESTClient;-><init>(Landroid/content/Context;Lorg/droidparts/net/http/worker/HTTPWorker;)V

    .line 41
    return-void
.end method


# virtual methods
.method public getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lorg/droidparts/net/http/RESTClient2;->get(Ljava/lang/String;)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v2

    iget-object v1, v2, Lorg/droidparts/net/http/HTTPResponse;->body:Ljava/lang/String;

    .line 55
    .local v1, "resp":Ljava/lang/String;
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 56
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Lorg/json/JSONException;
    new-instance v2, Lorg/droidparts/net/http/HTTPException;

    invoke-direct {v2, v0}, Lorg/droidparts/net/http/HTTPException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lorg/droidparts/net/http/RESTClient2;->get(Ljava/lang/String;)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v2

    iget-object v1, v2, Lorg/droidparts/net/http/HTTPResponse;->body:Ljava/lang/String;

    .line 46
    .local v1, "resp":Ljava/lang/String;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 47
    :catch_0
    move-exception v0

    .line 48
    .local v0, "e":Lorg/json/JSONException;
    new-instance v2, Lorg/droidparts/net/http/HTTPException;

    invoke-direct {v2, v0}, Lorg/droidparts/net/http/HTTPException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public post(Ljava/lang/String;Ljava/lang/String;)Lorg/droidparts/net/http/HTTPResponse;
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 74
    const-string v0, "text/plain"

    invoke-virtual {p0, p1, v0, p2}, Lorg/droidparts/net/http/RESTClient2;->post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v0

    return-object v0
.end method

.method public post(Ljava/lang/String;Ljava/util/Map;)Lorg/droidparts/net/http/HTTPResponse;
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/droidparts/net/http/HTTPResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 87
    .local p2, "formData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 88
    .local v0, "builder":Landroid/net/Uri$Builder;
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 89
    .local v2, "key":Ljava/lang/String;
    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 90
    .local v4, "val":Ljava/lang/String;
    if-eqz v4, :cond_0

    .end local v4    # "val":Ljava/lang/String;
    :goto_1
    invoke-virtual {v0, v2, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .restart local v4    # "val":Ljava/lang/String;
    :cond_0
    const-string v4, ""

    goto :goto_1

    .line 92
    .end local v2    # "key":Ljava/lang/String;
    .end local v4    # "val":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v3

    .line 93
    .local v3, "query":Ljava/lang/String;
    const-string v5, "application/x-www-form-urlencoded"

    invoke-virtual {p0, p1, v5, v3}, Lorg/droidparts/net/http/RESTClient2;->post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v5

    return-object v5
.end method

.method public post(Ljava/lang/String;Lorg/json/JSONArray;)Lorg/droidparts/net/http/HTTPResponse;
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "data"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 82
    const-string v0, "application/json"

    invoke-virtual {p2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lorg/droidparts/net/http/RESTClient2;->post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v0

    return-object v0
.end method

.method public post(Ljava/lang/String;Lorg/json/JSONObject;)Lorg/droidparts/net/http/HTTPResponse;
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "data"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 78
    const-string v0, "application/json"

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lorg/droidparts/net/http/RESTClient2;->post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/String;Ljava/lang/String;)Lorg/droidparts/net/http/HTTPResponse;
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 62
    const-string v0, "text/plain"

    invoke-virtual {p0, p1, v0, p2}, Lorg/droidparts/net/http/RESTClient2;->put(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/String;Lorg/json/JSONArray;)Lorg/droidparts/net/http/HTTPResponse;
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "data"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 70
    const-string v0, "application/json"

    invoke-virtual {p2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lorg/droidparts/net/http/RESTClient2;->put(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/String;Lorg/json/JSONObject;)Lorg/droidparts/net/http/HTTPResponse;
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "data"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 66
    const-string v0, "application/json"

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lorg/droidparts/net/http/RESTClient2;->put(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v0

    return-object v0
.end method
