.class public interface abstract Lorg/droidparts/contract/HTTP$Header;
.super Ljava/lang/Object;
.source "HTTP.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/droidparts/contract/HTTP;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Header"
.end annotation


# static fields
.field public static final ACCEPT_CHARSET:Ljava/lang/String; = "Accept-Charset"

.field public static final ACCEPT_ENCODING:Ljava/lang/String; = "Accept-Encoding"

.field public static final CACHE_CONTROL:Ljava/lang/String; = "Cache-Control"

.field public static final CONNECTION:Ljava/lang/String; = "Connection"

.field public static final CONTENT_ENCODING:Ljava/lang/String; = "Content-Encoding"

.field public static final CONTENT_LENGTH:Ljava/lang/String; = "Content-Length"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "Content-Type"

.field public static final DATE:Ljava/lang/String; = "Date"

.field public static final ESPIRES:Ljava/lang/String; = "Expires"

.field public static final ETAG:Ljava/lang/String; = "ETag"

.field public static final IF_MODIFIED_SINCE:Ljava/lang/String; = "If-Modified-Since"

.field public static final IF_NONE_MATCH:Ljava/lang/String; = "If-None-Match"

.field public static final KEEP_ALIVE:Ljava/lang/String; = "keep-alive"

.field public static final LAST_MODIFIED:Ljava/lang/String; = "Last-Modified"

.field public static final NO_CACHE:Ljava/lang/String; = "no-cache"

.field public static final USER_AGENT:Ljava/lang/String; = "User-Agent"
